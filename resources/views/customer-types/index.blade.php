@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการประเภทลูกค้า') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('customer-types.create') }}">สร้างประเภทลูกค้า</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th></th>
                            </tr>
                            @foreach ($customertypes as $customertype)
                                <tr>
                                    <td>{{ $customertype->name }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('customer-types.show', $customertype->id) }}">แสดง</a>
                                        <a class="btn btn-primary" href="{{ route('customer-types.edit', $customertype->id) }}">แก้ไข</a>
                                        <form action="{{ route('customer-types.destroy', $customertype->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
