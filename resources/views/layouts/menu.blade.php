<ul class="menu">

    @if (Auth::guest())
        <li class="sidebar-item active ">
            <a href="{{ route('login') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                <span>Login</span>
            </a>
        </li>
    @else
        <li class="sidebar-item">
            {{ Auth::user()->name }}
        </li>
        @if (Auth::user()->isAdmin())
            <li class="sidebar-item has-sub
                @if (isset($mainnav) && $mainnav == 'BaseData')
                    active
                @endif
            ">
                <a href="#" class='sidebar-link'>
                    <i class="bi bi-stack"></i>
                    <span>ข้อมูลหลัก</span>
                </a>
                <ul class="submenu
                    @if (isset($mainnav) && $mainnav == 'BaseData')
                    active
                    @endif
                ">
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'userdatas')
                            active
                        @endif
                    ">
                        <a href="{{ url('/userdatas') }}">User</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'crops')
                            active
                        @endif
                    ">
                        <a href="{{ url('/crops') }}">Crop</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'products')
                            active
                        @endif
                    ">
                        <a href="{{ url('/products') }}">สินค้า</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'teams')
                            active
                        @endif
                    ">
                        <a href="{{ url('/teams') }}">พนักงาน Load สินค้า</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'customer-types')
                            active
                        @endif
                    ">
                        <a href="{{ url('/customer-types') }}">ประเภทลูกค้า</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'broken-types')
                            active
                        @endif
                    ">
                        <a href="{{ url('/broken-types') }}">ประเภทกล่องแตก</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'container-types')
                            active
                        @endif
                    ">
                        <a href="{{ url('/container-types') }}">ประเภทตู้</a>
                    </li>
                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'ext_whs')
                            active
                        @endif
                    ">
                        <a href="{{ url('/ext_whs') }}">คลังนอก</a>
                    </li>

                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'check_points')
                            active
                        @endif
                    ">
                        <a href="{{ url('/admin/check_points') }}">จุดตรวจ</a>
                    </li>

                    <li class="submenu-item
                        @if (isset($namenav) && $namenav == 'check_sets')
                            active
                        @endif
                    ">
                        <a href="{{ url('/admin/check_sets') }}">ชุดการตรวจ</a>
                    </li>
                </ul>
            </li>
            @if (Auth::user()->isSuperAdmin())
            <li class="sidebar-item">
                <a href="{{ url('/master-plans') }}" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>Master Plan</span>
                </a>
            </li>
            @endif
        @endif
        @if (Auth::user()->group->name == trim('Superadmin') || Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('wh_sup'))
            <li class="sidebar-item ">
                <a href="{{ url('/planships') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                    <span>แผนจาก Planner</span></a>
            </li>
            <li class="sidebar-item">
                <a href="{{ url('/loadings') }}" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>แผน Load</span>
                </a>
            </li>
             <li class="sidebar-item">
                <a href="{{ url('/loading_carries') }}" class='sidebar-link'>
                    <i class="bi bi-grid-fill"></i>
                    <span>แผน Load ฝาก</span>
                </a>
            </li>
            <li class="sidebar-item ">
                <a href="{{ url('/list_load') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                    <span>ใบ LOT Load</span></a>
            </li>
            <li class="sidebar-item ">
                <a href="{{ url('/uploaddeliveryplans') }}" class='sidebar-link'>
                <i class="bi bi-grid-fill"></i>
                    <span>Delivery Plan</span></a>
            </li>

        <li class="sidebar-item  has-sub">
            <a href="#" class='sidebar-link'>
                <i class="bi bi-stack"></i>
                <span>รายงาน</span>
            </a>
            <ul class="submenu ">
                <li class="submenu-item ">
                    <a href="{{ url('/reports/exportdata') }}">Exports</a>
                </li>
                <li class="submenu-item ">
                    <a href="{{ url('/reports/loadplanvsact') }}">แผน vs act / กล่องแตก</a>
                </li>
                <li class="submenu-item ">
                    <a href="{{ url('/reports/exportcheckpoint') }}">สรุป Check Point</a>
                </li>
                <li class="submenu-item ">
                    <a href="{{ url('/loadings/search') }}">ข้อมูล Load สินค้า</a>
                </li>
            </ul>
        </li>
        @endif
        @guest

        @else
            <li class="sidebar-item ">

                <a  class='sidebar-link' href="{{ route('drivers.index') }}"><i class="bi bi-geo-fill"></i> Check Point</a>
            </li>
            <li class="sidebar-item ">
                {{-- <a  class='sidebar-link' href="{{ route('userdatas.changepass') }}">Change Pass</a> --}}
                <a  class='sidebar-link' href="{{ url('/userdatas/changepass') }}">Change Pass</a>
            </li>
            <li class="sidebar-item">
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a  class='sidebar-link' href="{{ route('logout') }}" onclick="event.preventDefault();
                            this.closest('form').submit();">
                        {{ __('Logout') }}
                    </a>
                </form>
            </li>
        @endguest
    @endif
</ul>
