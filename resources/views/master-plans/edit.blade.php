@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('master-plans.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Edit Master Plan Load') }} # {{ $masterplanm->id }}</div>
                    <div class="card-body"></div>
                        <form method="POST" action="{{ route('master-plans.update',$masterplanm->id) }}">
                            @csrf

                            @method('PUT')

                        <div class="row">
                            <div class="col-md-3"><strong>Name</strong> : 
                                {{ Form::text('name', $masterplanm->name, ['placeholder' => 'ชื่อ', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3"><strong>Customer Type</strong> :     
                                {{ Form::select('customer_type_id', $customertypelist , $masterplanm->customer_type_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : 
                                {{ Form::text('customer', $masterplanm->customer, ['placeholder' => 'ลูกค้า', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3"><strong>Note</strong> : 
                                {{ Form::text('note', $masterplanm->note, ['placeholder' => 'Note', 'class' => 'form-control']) }}
                                {{ Form::hidden('status', $masterplanm->status) }}
                            </div>
                        </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
