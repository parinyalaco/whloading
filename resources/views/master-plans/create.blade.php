@extends('layouts.maindp')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('master-plans.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้าง Master Plan Load') }}</div>
                    <div class="card-body"></div>
                        <form method="POST" action="{{ route('master-plans.store') }}">
                            @csrf

                        <div class="row">
                            <div class="col-md-3"><strong>Name</strong> : 
                                {{ Form::text('name', null, ['placeholder' => 'ชื่อ', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3"><strong>Customer Type</strong> :     
                                {{ Form::select('customer_type_id', $customertypelist , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : 
                                {{ Form::text('customer', null, ['placeholder' => 'ลูกค้า', 'class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3"><strong>Note</strong> : 
                                {{ Form::text('note', null, ['placeholder' => 'Note', 'class' => 'form-control']) }}
                                {{ Form::hidden('status', 'Active') }}
                            </div>
                        </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="5" class="w-35">แถวแนวขวาง</th>
                                        <th colspan="5" class="w-35">แถวแนวนอน</th>
                                        <th rowspan="2">Note</th>
                                    </tr>
                                    <tr>
                                        <th>Product</th>
                                        <th>จากแถว</th>
                                        <th>ถึงแถว</th>
                                        <th>ฐาน</th>
                                        <th>สูง</th>
                                        <th>Product</th>
                                        <th>จากแถว</th>
                                        <th>ถึงแถว</th>
                                        <th>ฐาน</th>
                                        <th>สูง</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @for ($i = 1; $i <= 10; $i++)
                                    <tr>
                                        <td>
                                            {{ Form::select('t_product_id_' . $i, $productlist, null, ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                               
                                        </td>
                                        <td>
                                            {{ Form::number('t_from_line' . $i, null, ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('t_to_line' . $i, null, ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('t_base' . $i, null, ['placeholder' => 'ฐาน', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('t_height' . $i, null, ['placeholder' => 'สูง', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::select('l_product_id_' . $i, $productlist, null, ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                               
                                        </td>
                                         <td>
                                            {{ Form::number('l_from_line' . $i, null, ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('l_to_line' . $i, null, ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('l_base' . $i, null, ['placeholder' => 'ฐาน', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('l_height' . $i, null, ['placeholder' => 'สูง', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::text('note_'.$i,null,['placeholder' => 'Note', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>                                        
                                    @endfor
                                </tbody>
                            </table>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Generate') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
