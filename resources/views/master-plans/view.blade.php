@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Master Plan Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('master-plans.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Master Plan Load') }} สถานะ: {{ $masterplanm->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Name</strong> : {{$masterplanm->name}}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{$masterplanm->customertype->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Customer</strong> : {{$masterplanm->customer}}</div>
                            <div class="col-md-3"><strong>Note</strong> : {{$masterplanm->note}}</div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="text-center"><a class="btn btn-sm btn-primary"
                                            href="{{ route('master-plans.createdetail',$masterplanm->id) }}">สร้าง</a></th>                                    
                                    <th colspan="6" class="w-25 text-center">แถวแนวขวาง</th>
                                    <th colspan="6" class="w-25 text-center">แถวแนวนอน</th>
                                    <th rowspan="2" class="text-center">รวม</th>
                                    <th rowspan="2" class="text-center">ยอดรวม</th>
                                    <th rowspan="2" class="text-center">Note</th>
                                </tr>
                                <tr>
                                    
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">แถว</th>                                    
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sumtotal = 0;
                                @endphp
                                @foreach ($masterplanm->masterplands()->orderBy('row', 'asc')->get()
        as $item)
                                    <tr>
                                        <td>
                                            <a class="btn btn-sm btn-primary"
                                                href="{{ route('master-plans.editdetail', $item->id) }}">Edit</a>
                                            <a class="btn btn-sm btn-danger"
                                                href="{{ route('master-plans.deletedetail', $item->id) }}">Delete</a>
                                        </td>
                                        
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productt->name ?? ''}}</td>
                                        <td>{{ $item->t_base }}</td>
                                        <td>{{ $item->t_height }}</td>
                                        <td>@php
                                            $texcess = 0;
                                            if($item->t_excess > 0){
                                                $texcess += $item->t_excess;
                                            }
                                            if($item->t_excess1 > 0){
                                                $texcess += $item->t_excess1;
                                            }
                                            if($item->t_excess2 > 0){
                                                $texcess += $item->t_excess2;
                                            }
                                            @endphp
                                            {{ $texcess }}
                                        </td>
                                        <td>{{ $item->t_total }}</td>
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productl->name ?? ''}}</td>
                                        <td>{{ $item->l_base }}</td>
                                        <td>{{ $item->l_height }}</td>
                                        <td>@php
                                            $lexcess = 0;
                                            if($item->l_excess > 0){
                                                $lexcess += $item->l_excess;
                                            }
                                            if($item->l_excess1 > 0){
                                                $lexcess += $item->l_excess1;
                                            }
                                            if($item->t_excess2 > 0){
                                                $lexcess += $item->l_excess2;
                                            }
                                            @endphp
                                            {{ $lexcess }}
                                        </td>
                                        <td>{{ $item->l_total }}</td>
                                        <td>{{ $item->all_total }}</td>
                                        <td>
                                            @php
                                                $sumtotal += $item->all_total;
                                            @endphp
                                            {{ $sumtotal }}</td>
                                        <td>{{ $item->note }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
