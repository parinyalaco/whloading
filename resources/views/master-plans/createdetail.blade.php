@extends('layouts.planload')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ Master Plan Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('master-plans.show',$masterplanm->id) }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้าง Master Plan Load Detail') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4"><strong>Name</strong> : {{$masterplanm->name}}</div>
                            <div class="col-md-4"><strong>Customer</strong> : {{$masterplanm->customer}}</div>
                            <div class="col-md-4"><strong>Customer Type</strong> : {{$masterplanm->customertype->name}}</div>
                        </div>
                        <form method="POST" action="{{ route('master-plans.createdetailAction',$masterplanm->id) }}">
                            @csrf

                            {{ Form::hidden('master_plan_m_id' , $masterplanm->id, ['placeholder' => 'แถว', 'class' => 'form-control']) }}    
                            {{ Form::hidden('main_type' , 'Normal', ['placeholder' => 'แถว', 'class' => 'form-control']) }}  
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th colspan="2"> แถวแนวขวาง</th>
                                        
                                        <th colspan="2"> แถวแนวนอน</th>
                                    </tr>
                                    <tr>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                    </tr>
                                </thead>
                                <tbody>    
                                    <tr>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('t_product_m_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('l_product_m_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>แถว</td>
                                        <td>
                                            {{ Form::number('row' , null, 
                                            ['placeholder' => 'แถว', 'class' => 'form-control',
                                            'id' => 'row']) }}                                                                    
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td>ฐาน</td>
                                        <td>
                                            {{ Form::number('t_base' , null, 
                                            ['placeholder' => 'ฐาน', 
                                            'class' => 'form-control calsum',
                                            'id' => 't_base']) }}                                                                                                          
                                        </td>
                                        <td>ฐาน</td>
                                        <td>
                                            {{ Form::number('l_base' , null, 
                                            ['placeholder' => 'ฐาน', 'class' => 'form-control calsum',
                                            'id' => 'l_base']) }}                                                                                                          
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('t_height' , null, 
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum',
                                            'id' => 't_height']) }}                                                                                                          
                                        </td>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('l_height' , null, 
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum',
                                            'id' => 'l_height']) }}                                                                                                          
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('t_product_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('l_product_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                    </tr>      
                                    <tr>
                                        <td>ฝาก1</td>
                                        <td>
                                            {{ Form::number('t_excess' ,null, 
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 't_excess']) }}                                                                                                          
                                        </td>
                                        <td>ฝาก1</td>
                                        <td>
                                            {{ Form::number('l_excess' , null, 
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 'l_excess']) }}                                                                                                          
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('t_product1_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('l_product1_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                    </tr>      
                                    <tr>
                                        <td>ฝาก2</td>
                                        <td>
                                            {{ Form::number('t_excess1' ,null, 
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 't_excess1']) }}                                                                                                          
                                        </td>
                                        <td>ฝาก2</td>
                                        <td>
                                            {{ Form::number('l_excess1' , null, 
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 'l_excess1']) }}                                                                                                          
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td>ฝากสินค้า3</td>
                                        <td>
                                            {{ Form::select('t_product2_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                        <td>ฝากสินค้า3</td>
                                        <td>
                                            {{ Form::select('l_product2_id', $productlist, null, 
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker','data-live-search'=>'true']) }}                                                                      
                                        </td>
                                    </tr>      
                                    <tr>
                                        <td>ฝาก3</td>
                                        <td>
                                            {{ Form::number('t_excess2' ,null, 
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 't_excess2']) }}                                                                                                          
                                        </td>
                                        <td>ฝาก3</td>
                                        <td>
                                            {{ Form::number('l_excess2' , null, 
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 'l_excess2']) }}                                                                                                          
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td>รวม</td>
                                        <td>
                                            {{ Form::number('t_total' , null, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'t_total']) }}                                                                                                          
                                        </td>
                                        <td>รวม</td>    
                                        <td>
                                            {{ Form::number('l_total' , null, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'l_total']) }}                                                                                                          
                                        </td>
                                    </tr>     
                                    <tr>
                                        <td>รวมทั้งหมด</td>
                                        <td colspan="3">
                                            {{ Form::number('all_total' , null, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'all_total']) }}                                                                                                          
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>Note</td>
                                        <td colspan="3">
                                            {{ Form::text('note' , null, ['placeholder' => 'note', 'class' => 'form-control']) }}                                                                                                          
                                        </td>
                                    </tr>    
                                </tbody>
                            </table>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('สร้าง') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
