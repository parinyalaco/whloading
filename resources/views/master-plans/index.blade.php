@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Master Plans') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('master-plans.create') }}"> สร้าง Master Plans</a>
                    </div>
                    <form method="GET" action="{{ route('master-plans.index') }}" accept-charset="UTF-8"
                        class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"
                                placeholder="ค้นหาจาก ลูกค้า / สินค้า" value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <th>Customer</th>
                                <th>Product</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            @foreach ($masterplans as $masterplanm)
                                <tr>
                                    <td>{{ $masterplanm->name }}</td>
                                    <td>{{ $masterplanm->customertype->name ?? '-' }} \ {{ $masterplanm->customer }}</td>
                                    <td>
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($masterplanm->masterplands as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif

                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                    </td>
                                    <td>{{ $masterplanm->status }}</td>
                                    <td><a class="btn btn-info"
                                            href="{{ route('master-plans.show', $masterplanm->id) }}">Show</a>
                                        <a class="btn btn-primary"
                                            href="{{ route('master-plans.edit', $masterplanm->id) }}">Edit</a>
                                
                                        <form action="{{ route('master-plans.destroy', $masterplanm->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                        <div class="pagination-wrapper"> {!! $masterplans->appends([ 'search' => Request::get('search')])->render() !!} </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
