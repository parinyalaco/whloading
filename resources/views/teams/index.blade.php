@extends('layouts.main')

@section('content')
@php
    $typeloadingteam = array(
    'Front' => 'พนักงานหน้าตู้',
                                        'Back' => 'พนักงานหลังตู้',
                                        'FL' => 'พนักงาน FL',
                                        'Ctrl' => 'พนักงานคุมโหลด',
                                        'Bfload' => 'พนักงานตรวจเช็คสินค้าก่อนโหลด',
                                        'Bkbox' => 'พนักงานตรวจเช็คกล่องแตก',
                                        'WRITE' => 'พนักงานเขียนกระสอบ');
@endphp
    <div class="page-heading">
        <h3>{{ __('จัดการพนักงาน Load สินค้า') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header row">
                        <a class="btn btn-success col-auto" href="{{ route('teams.create') }}">เพิ่มพนักงาน Load สินค้า</a>&nbsp;&nbsp;
                        <a class="btn btn-success col-auto" href="{{ route('team_importView') }}">Upload</a>
                        <form method="GET" action="{{ url('/teams') }}" accept-charset="UTF-8"
                            class="form-inline my-2 my-lg-0 float-right col" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search"
                                    placeholder="ค้นหาจาก ชื่อพนักงาน" value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-header">
                            <a class="btn @if (Request::get('status') == '') btn-warning @else btn-primary @endif" href="{{ route('teams.index') }}" > All </a>
                            <a class="btn @if (Request::get('status') == 'Active') btn-warning @else btn-primary @endif" href="{{ route('teams.index', ['status' => 'Active']) }}" > Active
                            </a>
                            <a class="btn @if (Request::get('status') == 'Inactive') btn-warning @else btn-primary @endif" href="{{ route('teams.index', ['status' => 'Inactive']) }}"> Inactive
                            </a>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th>ประเภท</th>
                                <th>สถานะ</th>
                                <th></th>
                            </tr>
                            @foreach ($teams as $team)
                                <tr>
                                    <td>{{ $team->name }}</td>
                                    <td>{{ $typeloadingteam[$team->type] ?? '-' }}</td>
                                    <td>{{ $team->status ?? '-' }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('teams.show', $team->id) }}">แสดง</a>
                                        <a class="btn btn-primary" href="{{ route('teams.edit', $team->id) }}">แก้ไข</a>
                                        <form action="{{ route('teams.destroy', $team->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="pagination-wrapper"> {!! $teams->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
