@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการพนักงาน Load สินค้า') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('teams.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไขพนักงาน Load สินค้า') }}</div>

                    <div class="card-body">
                        <form action="{{ route('teams.update', $team->id) }}" method="POST">
                            @csrf

                            @method('PUT')

                            <div class="form-team row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $team->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-team row">
                                <label for="type" class="col-md-3 col-form-label text-md-right">{{ __('ประเภท') }}</label>

                                <div class="col-md-6">
                                    {{ Form::select('type', [
                                        'Front' => 'พนักงานหน้าตู้',
                                        'Back' => 'พนักงานหลังตู้',
                                        'FL' => 'พนักงาน FL',
                                        'Ctrl' => 'พนักงานคุมโหลด',
                                        'Bfload' => 'พนักงานตรวจเช็คสินค้าก่อนโหลด',
                                        'Bkbox' => 'พนักงานตรวจเช็คกล่องแตก',
                                        'WRITE' => 'พนักงานเขียนกระสอบ'
                                        ], $team->type, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-team row">
                                <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('สถานะ') }}</label>

                                <div class="col-md-3">
                                    {{ Form::select('status', [
                                        'Active' => 'Active',
                                        'Inactive' => 'Inactive'
                                        ], $team->status, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-team row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
