@extends('layouts.main')

@section('content')
    {{-- @if(Session::has('error'))
        <div><h3> {{ Session::get('error') }}</h3></div>
    @endif --}}
    <div class="page-heading">
        <h3>{{ __('สินค้าที่ upload ไม่ได้') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('teams.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered">
                            <tr>
                                <th>บรรทัดที่</th>
                                <th>user</th>
                                <th>รายละเอียด</th>
                            </tr>
                            @foreach ($sort_team as $user)
                                <tr>
                                    <td>{{ $user['sort'] }}</td>
                                    <td>{{ $user['name'] }}</td>
                                    <td>{{ $user['note'] }}</td>                                    
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
