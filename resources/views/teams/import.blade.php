@extends('layouts.main')

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('พนักงาน Load สินค้า - Import') }}</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    {{-- <div class="card-header">Questions - Import</div> --}}
                    <div class="card-body">
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('teams.index') }}">Back</a>
                        </div>
                        <br/>
                        <div class="row g-4 settings-section">

                            <div class="col-12 col-md-12">
                                <div class="app-card app-card-settings shadow-sm p-4">

                                    <div class="app-card-body">
                                        <form action="{{ route('team_import') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_upload" class="custom-file-input" id="file_upload">
                                                </div>
                                                <br>
                     'Front' => 'พนักงานหน้าตู้'<br>
                     'Back' => 'พนักงานหลังตู้'<br>
                     'FL' => 'พนักงาน FL'<br>
                     'Ctrl' => 'พนักงานคุมโหลด'<br>
                    'Bfload' => 'พนักงานตรวจเช็คสินค้าก่อนโหลด'<br>
                     'Bkbox' => 'พนักงานตรวจเช็คกล่องแตก'<br>
                                        'WRITE' => 'พนักงานเขียนกระสอบ'<br>

                                                <br>
                                                <button class="btn btn-success">Import</button>
                                                {{-- <a class="btn btn-warning" href="{{ route('quiz_export') }}">Export</a> --}}
                                                <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('assets/team.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>
                                    </div><!--//app-card-body-->

                                </div><!--//app-card-->
                            </div>
                        </div><!--//row-->

                        {{-- <hr class="my-4">
                        <div class="row g-4 settings-section">
                            <h1 class="app-page-title">ตัวอย่างสำหรับการ Import</h1>
                            <img src="{{asset('/pic/quiz.jpg')}}"  class="photo" width="100" height="250" data-toggle="modal" data-target="#exampleModal">
                        </div><!--//row-->  --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
