@extends('layouts.main')

@section('content')
@php
    $typeloadingteam = array(
    'Front' => 'พนักงานหน้าตู้', 'Back' => 'พนักงานหน้าตู้', 'FL' => 'พนักงาน FL', 'Ctrl' => 'พนักงานคุมโหลด',
                                        'WRITE' => 'พนักงานเขียนกระสอบ');
@endphp
    <div class="page-heading">
        <h3>{{ __('จัดการพนักงาน Load สินค้า') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('teams.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงพนักงาน Load สินค้า') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <td>{{ $team->name }}</td>
                            </tr>
                            <tr>
                                <th>ประเภท</th>
                                <td>{{ $typeloadingteam[$team->type] ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>สถานะ</th>
                                <td>{{ $team->status }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('teams.edit', $team->id) }}">แก้ไข</a>
                                    <form action="{{ route('teams.destroy', $team->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
