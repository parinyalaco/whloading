@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการคลังนอก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('ext_whs.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงข้อมุลคลังนอก') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>name</th>
                                <td>{{ $ext_wh->name }}</td>
                            </tr>
                            <tr>
                                <th>desc</th>
                                <td>{{ $ext_wh->desc }}</td>
                            </tr>
                            <tr>
                                <th>status</th>
                                <td>{{ $ext_wh->status }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('ext_whs.edit', $ext_wh->id) }}">แก้ไข</a>
                                    <form action="{{ route('ext_whs.destroy', $ext_wh->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
