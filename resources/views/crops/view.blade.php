@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ CROP') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('crops.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงข้อมุล CROP') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>name</th>
                                <td>{{ $crop->name }}</td>
                            </tr>
                            <tr>
                                <th>desc</th>
                                <td>{{ $crop->desc }}</td>
                            </tr>
                            <tr>
                                <th>status</th>
                                <td>{{ $crop->status }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('crops.edit', $crop->id) }}">แก้ไข</a>
                                    <form action="{{ route('crops.destroy', $crop->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
