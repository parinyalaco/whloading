@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Change Password') }}</h3>
        {{-- <div class="pull-right">
            <a class="btn btn-success" href="{{ route('groups.index') }}">Back</a>
        </div> --}}
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Create New Group') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('userdatas.changepassaction') }}">
                            @csrf

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='password' name='password' type="password"
                                            class="form-control form-control-xl" placeholder="รหัสผ่าน" autocomplete="off"
                                            required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-shield-lock"></i>
                                        </div>
                                        @error('password')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='password_confirmation' name='password_confirmation' type="password"
                                            class="form-control form-control-xl" placeholder="รหัสผ่าน" autocomplete="off"
                                            required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-shield-lock"></i>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">Change Pass</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
