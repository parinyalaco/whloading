@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Group Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('userdatas.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Create New Group') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('userdatas.store') }}">
                            @csrf

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='username' name='username' type="text"
                                            class="form-control form-control-xl" placeholder="username (ใช้เข้าสู่ระบบ)" required
                                            autocomplete="off">
                                        <input id='group_id' name='group_id' type="hidden" value='2'>
                                        <div class="form-control-icon">
                                            <i class="bi bi-person"></i>
                                        </div>
                                        @error('username')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='email' name='email' type="text" class="form-control form-control-xl"
                                            placeholder="Email" autocomplete="off" required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-person"></i>
                                        </div>
                                        @error('email')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='name' name='name' type="text" class="form-control form-control-xl"
                                            placeholder="name" autocomplete="off" required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-person"></i>
                                        </div>
                                        @error('name')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="col-6">
                                    {{ Form::select('group_id',$groups,null,['class'=>"form-control form-control-xl",'required'=>true]) }}
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='password' name='password' type="password"
                                            class="form-control form-control-xl" placeholder="รหัสผ่าน" autocomplete="off"
                                            required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-shield-lock"></i>
                                        </div>
                                        @error('password')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='password_confirmation' name='password_confirmation' type="password"
                                            class="form-control form-control-xl" placeholder="รหัสผ่าน" autocomplete="off"
                                            required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-shield-lock"></i>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
