@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('User Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('userdatas.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Edit User') }}</div>

                    <div class="card-body">
                        <form action="{{ route('userdatas.update', $user->id) }}" method="POST">
                            @csrf

                            @method('PUT')
                            
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='username' name='username' type="text"
                                            class="form-control form-control-xl" placeholder="username" required
                                            autocomplete="off" value="{{ $user['username'] }}" />
                                        <div class="form-control-icon">
                                            <i class="bi bi-person"></i>
                                        </div>
                                        @error('username')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='email' name='email' type="text" class="form-control form-control-xl"
                                            placeholder="Email" autocomplete="off" required  value="{{ $user['email'] }}" >
                                        <div class="form-control-icon">
                                            <i class="bi bi-person"></i>
                                        </div>
                                        @error('email')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='name' name='name' type="text" class="form-control form-control-xl"
                                            placeholder="name" autocomplete="off" required  value="{{ $user['name'] }}"  >
                                        <div class="form-control-icon">
                                            <i class="bi bi-person"></i>
                                        </div>
                                        @error('name')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    {{ Form::select('group_id',$groups,$user->group_id,['class'=>"form-control form-control-xl"]) }}
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='password' name='password' type="password"
                                            class="form-control form-control-xl" placeholder="รหัสผ่าน" autocomplete="off"
                                            required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-shield-lock"></i>
                                        </div>
                                        @error('password')
                                            <span role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group position-relative has-icon-left mb-4">
                                        <input id='password_confirmation' name='password_confirmation' type="password"
                                            class="form-control form-control-xl" placeholder="รหัสผ่าน" autocomplete="off"
                                            required>
                                        <div class="form-control-icon">
                                            <i class="bi bi-shield-lock"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
