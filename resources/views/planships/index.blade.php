@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Plan Ship Management') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('planships.create') }}"> Create New
                            Plan Ship</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Date</th>
                                <th>Plan</th>
                                <th>Act</th>
                                <th></th>
                            </tr>
                            @foreach ($planships as $planship)
                                <tr>
                                    <td>{{ $planship->plan_date }}</td>
                                    <td>{{ $planship->plan_num_ship }}</td>
                                    <td>{{ $planship->act_num_ship }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('planships.show', $planship->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('planships.edit', $planship->id) }}">Edit</a>
                                        <form action="{{ route('planships.destroy', $planship->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
