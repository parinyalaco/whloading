@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Group Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('groups.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('View Group') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Plan Date</th>
                                <td>{{ $planship->plan_date }}</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>{{ $planship->plan_num_ship }}</td>
                            </tr>
                            <tr>
                                <th>Act</th>
                                <td>{{ $planship->act_num_ship }}</td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td>{{ $planship->note }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('planships.edit', $planship->id) }}">Edit</a>
                                    <form action="{{ route('planships.destroy', $planship->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
