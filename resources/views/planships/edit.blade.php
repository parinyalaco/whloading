@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('Plan Ship Management') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('planships.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Edit Plan Ship ') }}</div>

                    <div class="card-body">
                        <form action="{{ route('planships.update', $planship->id) }}" method="POST">
                            @csrf

                            @method('PUT')
                            
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Plan Date') }}</label>

                                <div class="col-md-6">
                                    <input id="plan_date" type="date" class="form-control @error('plan_date') is-invalid @enderror"
                                        name="plan_date" value="{{ $planship->plan_date }}" >

                                    @error('plan_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Plan Number') }}</label>

                                <div class="col-md-6">
                                    <input id="plan_num_ship" type="number" class="form-control @error('plan_num_ship') is-invalid @enderror"
                                        name="plan_num_ship" value="{{ $planship->plan_num_ship }}" >

                                    @error('plan_num_ship')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="desc" class="col-md-3 col-form-label text-md-right">{{ __('Note') }}</label>

                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror"
                                        name="note" value="{{  $planship->note }}">

                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
