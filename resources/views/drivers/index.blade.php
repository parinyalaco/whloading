@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการจุดตรวจ') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <form method="GET" action="{{ url('/drivers') }}" accept-charset="UTF-8"
                        class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"
                                placeholder="ค้นหาจาก Order / ลูกค้า / สินค้า" value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit"><i class="bi bi-search"></i>
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body table-responsive-lg d-none d-sm-block">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                @if (Auth::user()->group->name == trim('Admin') ||
                                        Auth::user()->group->name == trim('Superadmin') ||
                                        Auth::user()->username == trim('PIT') ||
                                        Auth::user()->username == trim('JJT'))
                                    <th>
                                        <a onclick="Myfunction()">
                                            <i class="bi bi-envelope"></i>
                                        </a>
                                    </th>
                                @endif
                                <th>Loading Date</th>
                                <th>Customer</th>
                                <th>Order</th>
                                <th>Product</th>
                                <th>Details</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            @foreach ($loadms as $loadm)
                                <tr>
                                    @if (Auth::user()->group->name == trim('Admin') ||
                                            Auth::user()->group->name == trim('Superadmin') ||
                                            Auth::user()->username == trim('PIT') ||
                                            Auth::user()->username == trim('JJT'))
                                        <td>
                                            @if ($loadm->status == 'Loaded')
                                                <input type="checkbox" value="{{ $loadm->id }}"
                                                    id="chk_print[{{ $loadm->id }}]"
                                                    name="chk_print[{{ $loadm->id }}]">
                                            @endif
                                        </td>
                                    @endif
                                    <td>{{ $loadm->load_date }}</td>
                                    <td>{{ $loadm->customertype->name ?? '-' }} \ {{ $loadm->customer }}</td>
                                    <td>{{ $loadm->order_no }}</td>
                                    <td>
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($loadm->loadds as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                    </td>
                                    <td>{{ $loadm->owner_type }}</td>
                                    <td>{{ $loadm->status }}
                                        @if (
                                            $loadm->status == 'Loading' &&
                                                (Auth::user()->group->name == trim('Admin') ||
                                                    Auth::user()->group->name == trim('Superadmin') ||
                                                    Auth::user()->group->name == trim('Wh') ||
                                                    Auth::user()->group->name == trim('wh_sup')))
                                            <a class="btn btn-primary"
                                                href="{{ route('loadings.entercountermanual', $loadm->id) }}">
                                                <i class="bi bi-file-post-fill"></i> Manual
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($loadm->status == 'Loaded')
                                            <a class="btn btn-secondary"
                                                href="{{ route('answers.index', $loadm->id) }}"><i
                                                    class="bi bi-camera-fill"></i> Check Point</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                        <div class="pagination-wrapper"> {!! $loadms->appends(['status' => Request::get('status'), 'search' => Request::get('search')])->render() !!} </div>

                    </div>

                    <!-- Mobile View -->


                    <div class="card-body table-responsive-lg d-block d-sm-none">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                @if (Auth::user()->group->name == trim('Admin') ||
                                        Auth::user()->group->name == trim('Superadmin') ||
                                        Auth::user()->username == trim('PIT') ||
                                        Auth::user()->username == trim('JJT'))
                                    <th>
                                        <a onclick="Myfunction()">
                                            <i class="bi bi-envelope"></i>
                                        </a>
                                    </th>
                                @endif
                                <th>Data</th>
                            </tr>
                            @foreach ($loadms as $loadm)
                                <tr>
                                    @if (Auth::user()->group->name == trim('Admin') ||
                                            Auth::user()->group->name == trim('Superadmin') ||
                                            Auth::user()->username == trim('PIT') ||
                                            Auth::user()->username == trim('JJT'))
                                        <td>
                                            @if ($loadm->status == 'Loaded')
                                                <input type="checkbox" value="{{ $loadm->id }}"
                                                    id="chk_print[{{ $loadm->id }}]"
                                                    name="chk_print[{{ $loadm->id }}]">
                                            @endif
                                        </td>
                                    @endif
                                    <td>{{ $loadm->load_date }}
                                        <br />
                                        {{ $loadm->customertype->name ?? '-' }} \ {{ $loadm->customer }}<br />
                                        {{ $loadm->order_no }}<br />
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($loadm->loadds as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                        <br />{{ $loadm->owner_type }}<br />{{ $loadm->status }}
                                        @if (
                                            $loadm->status == 'Loading' &&
                                                (Auth::user()->group->name == trim('Admin') ||
                                                    Auth::user()->group->name == trim('Superadmin') ||
                                                    Auth::user()->group->name == trim('Wh') ||
                                                    Auth::user()->group->name == trim('wh_sup')))
                                            <a class="btn btn-primary"
                                                href="{{ route('loadings.entercountermanual', $loadm->id) }}">
                                                <i class="bi bi-file-post-fill"></i> Manual
                                            </a>
                                        @endif
                                        <br />


                                        @if ($loadm->status == 'Loaded')
                                            <a class="btn btn-secondary"
                                                href="{{ route('answers.index', $loadm->id) }}">Check Point</a>
                                        @endif



                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('loadings.load_reason') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea id="reason" class="form-control col" name="reason" required></textarea>
                                            <input type="hidden" id="loadm_id" class="form-control col"
                                                name="loadm_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pagination-wrapper  table-responsive-lg "> {!! $loadms->appends(['status' => Request::get('status'), 'search' => Request::get('search')])->render() !!} </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <script type="text/javascript" src="{{ asset('assets/bootstrap/js/jquery-1.7.1.min.js') }}"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@endsection
