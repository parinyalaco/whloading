@extends('layouts.main')
<style>
    label.cameraButton {
        display: inline-block;
        /* margin: 1em 0; */

        /* Styles to make it look like a button */
        padding: 0.5em;
        border: 1px solid #000;
        border-color: #EEE #CCC #CCC #EEE;
        background-color: #DDD;
    }

    /* Look like a clicked/depressed button */
    label.cameraButton:active {
        border-color: #CCC #EEE #EEE #CCC;
    }

    /* This is the part that actually hides the 'Choose file' text box for camera inputs */
    label.cameraButton input[accept*="camera"] {
        display: none;
    }
</style>
@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load.index') }}">Back</a>
        </div>
        <h3 class="col-auto">{{ __('รายละเอียดการ Load') }} -> </h3>
        <div class="col">
            <h4 class="row">วันที่โหลด : {{ date('d/m/Y',strtotime($list_load->list_date)) }}</h4>
            <h4 class="row">shipment/order : {{ $list_load->shipment_order }}</h4>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('list_load_det.create') }}">สร้างรายการใหม่</a>
                        <a class="btn btn-success" href="{{ route('list_load_det_importView') }}">Upload</a>
                    </div>
                    
                    <form method="GET" action="{{ route('list_load_det.index') }}" accept-charset="UTF-8" class="form-inline mx-2 my-lg-2 float-right" role="search">
                        <div class="input-group">
                            <select class="form-control" name="search">
                                <option value="" @if(empty(request('search'))) selected @endif>...product...</option>
                                @foreach($pd_data as $key=>$value)
                                    <option value="{{ $key }}" @if(request('search')==$key) selected @endif>{{ $pd_data[$key]['name'] }}({{ $pd_data[$key]['desc_list'] }})</option>
                                @endforeach
                            </select>
                            <input type="text" class="form-control mx-2" name="search_pallet" placeholder="...Pallet..." value="{{ request('search_pallet') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body">
                        <table class="table table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 8%">No.</th>
                                    <th style="width: 20%">Product</th>
                                    <th style="width: 12%">Pallet</th>
                                    <th style="width: 12%">Batch</th>
                                    <th style="width: 10%">Box</th>
                                    <th style="width: 10%">Qty</th>
                                    <th style="width: 8%">รูป</th>
                                    <th style="width: 20%"></th>
                                </tr>
                            </thead>
                            @php
                                $i=0;
                            @endphp
                            <tbody>
                                @foreach ($ll_det as $key)
                                    <tr>
                                        <td style="text-align: center">{{ $key->sort }}</td>
                                        <td>{{ $pd_data[$key->product_id]['name'] }}</td>
                                        <td>{{ $key->pallet_no }}</td>
                                        <td>{{ $key->batch }}</td>
                                        <td>{{ $key->box_no }}</td>
                                        <td>{{ $key->ur_qty }}</td>
                                        <td style="text-align: center">
                                            <input type="checkbox" disabled @if(!empty($key->list_img)) checked @endif>
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('list_load_det.capture',$key->id) }}" enctype="multipart/form-data">
                                                @csrf
                                                <label class="cameraButton">รูป Pallet
                                                    <input type="file" accept="image/*; capture=camera" name="file_webcam{{ $key->id }}" id="file_webcam{{ $key->id }}" onchange="this.form.submit()"/>
                                                </label>
                                            </form>
                                            <a class="btn btn-primary" href="{{ route('list_load_det.edit', $key->id) }}">แก้ไข</a>
                                            <form action="{{ route('list_load_det.destroy', $key->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('ข้อมูลทั้งหมดจะถูกลบ คุณแน่ใจมั้ย ?')">ลบ</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- <div class="pagination-wrapper"> {!! $load_list->appends(['search_date' => Request::get('search_date'), 'search' => Request::get('search')])->render() !!} </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
