@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการสินค้า') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('products.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงข้อมุลสินค้า') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>รหัสสินค้า</th>
                                <td>{{ $product->name }}</td>
                            </tr>
                            <tr>
                                <th>รหัสกล่อง</th>
                                <td>{{ $product->boxcode }}</td>
                            </tr>
                            <tr>
                                <th>รหัสถุง</th>
                                <td>{{ $product->bagcode }}</td>
                            </tr>
                            <tr>
                                <th>Desc</th>
                                <td>{{ $product->desc }}</td>
                            </tr>
                            <tr>
                                <th>กว้าง (cm)</th>
                                <td>{{ $product->wide }}</td>
                            </tr>
                            <tr>
                                <th>ยาว (cm)</th>
                                <td>{{ $product->long }}</td>
                            </tr>
                            <tr>
                                <th>สูง (cm)</th>
                                <td>{{ $product->height }}</td>
                            </tr>
                            <tr>
                                <th>หนัก (kg)</th>
                                <td>{{ $product->weight }}</td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td>{{ $product->note }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">แก้ไข</a>
                                    <form action="{{ route('products.destroy', $product->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
