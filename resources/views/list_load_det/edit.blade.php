@extends('layouts.main')

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load_det.index') }}">Back</a>
        </div>
        <h3 class="col">{{ __('รายละเอียดการ Load') }} -> Edit</h3>
        <div class="row" style="margin-left: 10%; width: 90%">
            <h5 class="row">วันที่โหลด : {{ date('d/m/Y',strtotime($list_load->list_date)) }}, shipment/order : {{ $list_load->shipment_order }}</h5>
            <h5 class="row">ทะเบียนรถ : {{ $list_load->car_head }}/{{ $list_load->car_detail }}({{ $list_load->supplier_tran }})</h5>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <form action="{{ route('list_load_det.update', $list_loadD->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            
                            @foreach($col_name as $key => $value)
                                @if($key==0 || $key%2==0)
                                    <div class="row my-2">
                                @endif
                                    <label for="{{ $arr['all'][$key] }}" class="col-md-2 col-form-label text-md-right">{{ __($value) }}</label>

                                    <div class="col-md-4">
                                        @if(array_search($arr['all'][$key], $arr['link'],true)!==false)
                                            <select id="{{ $arr['all'][$key] }}" name="{{ $arr['all'][$key] }}" 
                                            class="form-control" @if($arr['all'][$key]=='product_id') onchange="show_pd(this.value)" @endif autofocus>
                                                <option value="" @if(empty($list_loadD[$arr['all'][$key]])) selected @endif>...{{ $value }}...</option>
                                                    @if($arr['all'][$key]=='product_id')
                                                        @foreach($base_data_id[$arr['all'][$key]]['name'] as $kselect => $vselect)
                                                            <option value="{{ $kselect }}" @if($list_loadD[$arr['all'][$key]] == $kselect) selected @endif>{{ $vselect }}</option>
                                                        @endforeach
                                                    @elseif($arr['all'][$key]=='usage_id')
                                                        @foreach($base_data_id['status_id'] as $kselect => $vselect)
                                                            <option value="{{ $kselect }}" @if($list_loadD[$arr['all'][$key]] == $kselect) selected @endif>{{ $vselect }}</option>
                                                        @endforeach
                                                    @else                                              
                                                        @foreach($base_data_id[$arr['all'][$key]] as $kselect => $vselect)
                                                            <option value="{{ $kselect }}" @if($list_loadD[$arr['all'][$key]] == $kselect) selected @endif>{{ $vselect }}</option>
                                                        @endforeach
                                                    @endif
                                            </select>
                                            {{-- --{{ $arr['all'][$key] }}-- --}}
                                        @else
                                            {{-- --{{ $arr['all'][$key] }}-- --}}
                                            <input @if(empty($arr['all'][$key])) id="pd_desc" name="pd_desc" disabled @else id="{{ $arr['all'][$key] }}" name="{{ $arr['all'][$key] }}" @endif 
                                                class="form-control" 
                                                @if(empty($arr['all'][$key]) && !empty($base_data_id['product_id']['desc_list'][$list_loadD['product_id']])) 
                                                    value="{{ $base_data_id['product_id']['desc_list'][$list_loadD['product_id']] }}"
                                                @else value="{{ $list_loadD[$arr['all'][$key]] }}" @endif
                                                @if(array_search($arr['all'][$key], $arr['date'],true)!==false)
                                                    type="date"                                                     
                                                @elseif(array_search($arr['all'][$key], $arr['str'],true)!==false || empty($arr['all'][$key]))
                                                    type="text" 
                                                @else
                                                    type="number" 
                                                @endif

                                                @if($key != 19 && $key != 20 && $key != 21 && $key != 22 && $key != 23)
                                                    required 
                                                @endif

                                                autocomplete="{{ $arr['all'][$key] }}" autofocus>
                                        @endif

                                        @error($arr['all'][$key])
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @if($key%2 != 0)                                
                                    </div>                                    
                                @endif
                            @endforeach                            

                            <div class="row my-2">                                
                                <label for="numperbox"
                                    class="col-md-2 col-form-label text-md-right">{{ __('รูปถ่าย') }}</label>

                                <div class="col-md-8">
                                    {{-- <input type="file" class="custom-file-input" accept="image/jpeg , image/jpg, image/gif, image/png" 
                                        id='list_img' name='list_img' onchange="show_img(event)"> --}}
                                    <input type="hidden" id="chk_img" name="chk_img" value="{{ $list_loadD->list_img }}"> 
                                    @php
                                        $fs = '';
                                        if($list_loadD->list_img_path){
                                            $size_img = getimagesize($list_loadD->list_img_path);
                                            $i_w = $size_img[0];
                                            $i_h = $size_img[1];    
                                            if($i_w<$i_h){
                                                $fs = 'height=300px'; 
                                            }else{
                                                $fs = 'width=300px'; 
                                            } 
                                            $path = $list_loadD->list_img_path;
                                            $type = pathinfo($path, PATHINFO_EXTENSION);
                                            $data = file_get_contents($path);
                                            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data); 
                                        }                                         
                                    @endphp
                                    <div id="content"> 
                                        @if(!empty($list_loadD->list_img_path))  
                                            <div class="alert alert-dismissible fade show" role="alert">
                                                {{-- onclick="zoom_img()" --}}
                                                <img id="output" name="output" {{ $fs }} src="{{ $base64 }}" />
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event)"></button>
                                            </div>
                                        @endif
                                    </div>    

                                    @error('list_img')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row my-2">
                                <div class="d-flex align-items-end flex-column">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div> 

                            {{-- <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div> --}}
                        </form>
                        <!-- The Modal -->
                        {{-- <div id="myModal" class="modal">
                            <span class="close">&times;</span>
                            <img class="modal-content" id="img01" >
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script type="text/javascript"> 
        
        function del_img(event) {
            var element2 = document.getElementById("chk_img");
            element2.value = null;
        }

        function show_pd(pd){
            // console.log(pd);
            let pd_desc = <?php echo json_encode($base_data_id['product_id']['desc_list']); ?>;
            let pd_input = document.getElementById("pd_desc");
            if(pd_desc[pd]){
                pd_input.value = pd_desc[pd];
            }else{
                pd_input.value = "";
            } 
        }

        // function zoom_img(){
        //     var modal = document.getElementById("myModal");

        //     // Get the image and insert it inside the modal - use its "alt" text as a caption
        //     var img = document.getElementById('output');
        //     console.log(img);
        //     var modalImg = document.getElementById("img01");
        //     // img.onclick = function(){
        //         modal.style.display = "block";
        //         modalImg.src = img.src;
        //     // }

        //     // Get the <span> element that closes the modal
        //     var span = document.getElementsByClassName("close")[0];

        //     // When the user clicks on <span> (x), close the modal
        //     span.onclick = function() {
        //         modal.style.display = "none";
        //     }
        // }
    </script>
@endsection
