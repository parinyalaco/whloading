@extends('layouts.main')

@section('content')
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load_det_importView') }}">Back</a>
        </div>
        <h3 class="col-auto">{{ __('รายการ Load ที่ซ้ำ') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th>บรรทัดที่</th>
                                <th>สินค้า</th>
                            </tr>
                            @foreach ($err_load as $key=>$value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $err_load[$key]['row'] }}</td>
                                    <td>{{ $err_load[$key]['product'] }}</td>                                  
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
