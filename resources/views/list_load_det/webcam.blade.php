@extends('layouts.main')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style type="text/css">
        #results { padding:20px; border:1px solid; background:#ccc; }
    </style>    
@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load_det.index') }}">Back</a>
        </div>
        <h3 class="col">ถ่ายรูป Pallet -> {{ $list_loadD->pallet_no }}( จำนวน {{ $list_loadD->ur_qty }})</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <form method="POST" action="{{ route('list_load_det.capture') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row bg-secondary text-white mx-2 py-2">
                            <label for="sort" class="col-auto my-2 mx-2">{{ __('ลำดับโหลดขึ้นตู้') }}</label>
                            <div class="col-auto">
                                <input class="form-control" type="text" id = "sort" name = "sort" value="@if(!empty($list_loadD->sort)){{ $list_loadD->sort }}@else{{ $max+1 }}@endif">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <div id="my_camera"></div>
                                <br/>
                                <input type="hidden" id = "id" name = "id" value="{{ $list_loadD->id }}">
                                <input type="button" value="ถ่ายรูป" onClick="take_snapshot()">
                                <input type="hidden" name="image" class="image-tag">
                            </div>
                            <div class="col-md-6 text-center">
                                <div id="results">...รูปตัวอย่าง...</div>
                            </div>
                            <div class="col-md-12 text-center">
                                <br/>
                                <button class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script language="JavaScript">
        Webcam.set({
            width: 300,
            height: 230,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        
        Webcam.attach( '#my_camera' );
        
        function take_snapshot() {
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img height="80%" src="'+data_uri+'"/>';
            } );
        }
    </script>
@endsection
