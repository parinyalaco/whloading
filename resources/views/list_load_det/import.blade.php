@extends('layouts.main')

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load_det.index') }}">Back</a>
        </div>
        <div class="col">
            <h3 class="row">{{ __('รายละเอียดการ Load') }} -> Import</h3>
            <div class="row" style="margin-left: 5%">
                <h5 class="row">วันที่โหลด : {{ date('d/m/Y',strtotime($list_load->list_date)) }}, shipment/order : {{ $list_load->shipment_order }}</h5>
                <h5 class="row">ทะเบียนรถ : {{ $list_load->car_head }}/{{ $list_load->car_detail }}({{ $list_load->supplier_tran }})</h5>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row g-4 settings-section">	                
                            <div class="col-12 col-md-12">
                                <div class="app-card app-card-settings shadow-sm p-4">
                                    
                                    <div class="app-card-body">
                                        <form action="{{ route('list_load_det_import') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" id="list_load_m_id" name="list_load_m_id" value="{{ $list_load->id }}">
                                            <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                                                <div class="custom-file text-left">
                                                    <input type="file" name="file_upload" class="custom-file-input" id="file_upload">
                                                </div>
                                            
                                                <br>
                                                <button class="btn btn-success">Import</button>
                                                {{-- <a class="btn btn-warning" href="{{ route('quiz_export') }}">Export</a>{{ asset('assets/product.xlsx') }} --}}
                                                <div class="col-auto" style="float: right; margin-right: 10%; width:50%"><a href="{{ asset('file/lot_load_detail.xlsx') }}">ตัวอย่างไฟล์..</a></div>
                                            </div>
                                        </form>  
                                    </div><!--//app-card-body-->
                                    
                                </div><!--//app-card-->
                            </div>
                        </div><!--//row-->

                        {{-- <hr class="my-4">
                        <div class="row g-4 settings-section">
                            <h1 class="app-page-title">ตัวอย่างสำหรับการ Import</h1>
                            <img src="{{asset('/pic/quiz.jpg')}}"  class="photo" width="100" height="250" data-toggle="modal" data-target="#exampleModal">
                        </div><!--//row-->  --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection