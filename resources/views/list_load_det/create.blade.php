@extends('layouts.main')

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load_det.index') }}">Back</a>
        </div>
        <h3 class="col">{{ __('รายละเอียดการ Load') }} -> Create</h3>
        <div class="row" style="margin-left: 10%; width: 90%">
            <h5 class="row">วันที่โหลด : {{ date('d/m/Y',strtotime($list_load->list_date)) }}, shipment/order : {{ $list_load->shipment_order }}</h5>
            <h5 class="row">ทะเบียนรถ : {{ $list_load->car_head }}/{{ $list_load->car_detail }}({{ $list_load->supplier_tran }})</h5>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('list_load_det.store') }}" enctype="multipart/form-data">
                            @csrf  
                            @foreach($col_name as $key => $value)
                                @if($key==0 || $key%2==0)
                                    <div class="row my-2">
                                @endif
                                    <label for="{{ $arr['all'][$key] }}" class="col-md-2 col-form-label text-md-right">{{ __($value) }}</label>

                                    <div class="col-md-4">
                                        @if(array_search($arr['all'][$key], $arr['link'],true)!==false)
                                            <select id="{{ $arr['all'][$key] }}" name="{{ $arr['all'][$key] }}" 
                                            class="form-control" @if($arr['all'][$key]=='product_id') onchange="show_pd(this.value)" @endif autofocus>
                                                <option value="" @if(empty(old($arr['all'][$key]))) selected @endif>...{{ $value }}...</option>
                                                    @if($arr['all'][$key]=='product_id')
                                                        @foreach($base_data_id[$arr['all'][$key]]['name'] as $kselect => $vselect)
                                                            <option value="{{ $kselect }}" @if(old($arr['all'][$key]) == $kselect) selected @endif>{{ $vselect }}</option>
                                                        @endforeach
                                                    @elseif($arr['all'][$key]=='usage_id')
                                                        @foreach($base_data_id['status_id'] as $kselect => $vselect)
                                                            <option value="{{ $kselect }}" @if(old($arr['all'][$key]) == $kselect) selected @endif>{{ $vselect }}</option>
                                                        @endforeach
                                                    @else                                              
                                                        @foreach($base_data_id[$arr['all'][$key]] as $kselect => $vselect)
                                                            <option value="{{ $kselect }}" @if(old($arr['all'][$key]) == $kselect) selected @endif>{{ $vselect }}</option>
                                                        @endforeach
                                                    @endif
                                            </select>
                                            {{-- --{{ $arr['all'][$key] }}-- --}}
                                        @else
                                            <input @if(empty($arr['all'][$key])) id="pd_desc" name="pd_desc" disabled @else id="{{ $arr['all'][$key] }}" name="{{ $arr['all'][$key] }}" @endif 
                                                value="{{ old($arr['all'][$key]) }}" 
                                                class="form-control"
                                                
                                                @if(array_search($arr['all'][$key], $arr['date'],true)!==false)
                                                    type="date" 
                                                @elseif(array_search($arr['all'][$key], $arr['str'],true)!==false || empty($arr['all'][$key]))
                                                    type="text" 
                                                @else
                                                    type="number"
                                                @endif

                                                @if($key != 19 && $key != 20 && $key != 21 && $key != 22 && $key != 23)
                                                    required 
                                                @endif

                                                autocomplete="{{ $arr['all'][$key] }}" autofocus>
                                        @endif

                                        @error($arr['all'][$key])
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @if($key%2 != 0)                                
                                    </div>                                    
                                @endif
                            @endforeach                            

                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script type="text/javascript"> 
        var show_img = function(event) {
            // console.log(event);
            if(event.target.files[0]){
                var element = document.getElementById("content");
                
                var add_att; 
                add_att = '<div class="alert alert-dismissible fade show" role="alert">';
                add_att += '<img id="output" name="output" src="'+URL.createObjectURL(event.target.files[0])+'" height="100px" />'
                add_att += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event)"></button>';
                add_att += '</div>';
                element.innerHTML = add_att;

            }            
        };

        function del_img(event) {
            var element = document.getElementById("list_img");
            element.value = null; 
        }

        function show_pd(pd){
            // console.log(pd);
            let pd_desc = <?php echo json_encode($base_data_id['product_id']['desc_list']); ?>;
            console.log(pd);
            let pd_input = document.getElementById("pd_desc");
            if(pd_desc[pd]){
                pd_input.value = pd_desc[pd];
            }else{
                pd_input.value = "";
            } 
        }
    </script>       
@endsection
