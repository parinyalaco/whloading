@extends('layouts.main')

@section('content')
    @if(Session::has('success'))
        <div><h3> {{ Session::get('success') }}</h3></div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('จัดการ CROP') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('answers.scan',$loadm->id) }}">เก็บข้อมูล</a>
                    </div>
                    <div class="p-2">
                    <form method="GET" action="{{ route('crops.index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>จุดตรวจ</th>
                                <th>จำนวน</th>
                                <th></th>
                            </tr>
                            @foreach ($checkansm->checkanswers as $checkanswer)
                                <tr>
                                    <td>{{ $checkanswer->checkquestion->name }}</td>
                                    @php
                                        $columnanswer1 = $checkanswer->ans1_column;
                                        $columnanswer2 = $checkanswer->ans2_column;
                                        $columnanswer3 = $checkanswer->ans3_column;
                                    @endphp
                                    @if ($checkanswer->type_question == 'IMAGE')
                                        <td><img src="{{ url($checkanswer->$columnanswer3) }}" width="200px" ></td>
                                    @else
                                    @if ($columnanswer1 == 'custom_f_1')
                                        <td>{{ number_format($checkanswer->$columnanswer1,2) }}</td>
                                    @else
                                         <td>{{ $checkanswer->$columnanswer1 }}</td>   
                                    @endif
                                        
                                    @endif


                                    <td>
                                        <a class="btn btn-primary" href="{{ route('answers.edit', [$loadm->id, $checkanswer->id]) }}">แก้ไข</a>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
