@extends('layouts.main')

@section('content')
    @if(Session::has('success'))
        <div><h3> {{ Session::get('success') }}</h3></div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('จัดการจุดตรวจ') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('answers.scan',$loadm->id) }}"><i class="bi bi-camera-fill"></i> แสกนคิวอาร์โค๊ด</a>
                    </div>
                    <div class="p-2">
                    <form method="GET" action="{{ route('answers.index',$loadm->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr><th>วันเวลา</th>
                                <th>จุดตรวจ</th>
                                <th>จำนวน</th>
                                <th>status</th>
                                <th></th>
                            </tr>
                            @foreach ($checkanswers as $checkanswer)
                                <tr>
                                    <td>{{ $checkanswer->created_at }}</td>
                                    <td>{{ $checkanswer->checkpoint->name }}</td>
                                    <td>{{ $checkanswer->checkanswers->count()}}</td>
                                    <td>{{ $checkanswer->status }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('answers.show',[$loadm->id, $checkanswer->id]) }}">แสดงข้อมูล</a>
                                        <a class="btn btn-primary" href="{{ route('answers.edit', [$loadm->id, $checkanswer->id]) }}">แก้ไข</a>
                                        <form action="{{ route('answers.destroy', [$loadm->id, $checkanswer->id]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="pagination-wrapper"> {!! $checkanswers->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
