@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการจุดตรวจ') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('answers.show',[$loadm->id,$checkanswer->id]) }}"><i class="bi bi-backspace-fill"></i> ย้อนกลับ</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('บันทึกข้อมูล') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('answers.update',[$loadm->id,$checkanswer->id]) }}" enctype="multipart/form-data" >
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="location" value="{{ $checkanswer->location}}"/>
                            <div class="form-product row">
                                <label for="question-{{$checkanswer->id}}" class="col-md-2 col-form-label text-md-right">{{ $checkanswer->checkquestion->name }}</label>
                                @php
                                    $columnanswer1 = $checkanswer->ans1_column;
                                    $columnanswer2 = $checkanswer->ans2_column;
                                    $columnanswer3 = $checkanswer->ans3_column;
                                @endphp
                                <div class="col-md-4">
                                    @if ($checkanswer->type_question == 'TEXT' )
                                        <input id="question-{{$checkanswer->id}}" type="text" class="form-control @error('question-'.$checkanswer->id) is-invalid @enderror"
                                        name="question-{{$checkanswer->id}}" value="{{ $checkanswer->$columnanswer1 }}"
                                        @if ($checkanswer->required_flag == 'Y')
                                            required
                                        @endif
                                        >
                                    @else
                                     @if ($checkanswer->type_question == 'IMAGE' )
                                        <input type="file" accept="image/*;capture=camera" name="question-{{$checkanswer->id}}"
                                        @if ($checkanswer->required_flag == 'Y')
                                            required
                                        @endif
                                        ><br/>
                                        <img src="{{ url($checkanswer->$columnanswer3) }}" width="200px" >
                                    @else
                                        <textarea id="question-{{$checkanswer->id}}" class="form-control @error('question-'.$checkanswer->id) is-invalid @enderror"
                                        name="question-{{$checkanswer->id}}"
                                        @if ($checkanswer->required_flag == 'Y')
                                            required
                                        @endif
                                        >{{ $checkanswer->$columnanswer1 }}</textarea>
                                    @endif
                                    @endif


                                    @error('question-'.$checkanswer->id)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
