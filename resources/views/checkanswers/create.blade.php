@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการจุดตรวจ') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('answers.index',$loadm->id) }}"><i class="bi bi-backspace-fill"></i> ย้อนกลับ</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('บันทึกข้อมูล') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('answers.store',$loadm->id) }}" enctype="multipart/form-data" >

                            @csrf
                            <input type="hidden" name="location" value="{{$location}}"/>
                            @foreach ($checkset->checkquestions()->orderBy('seq','ASC')->get() as $item)
                            <div class="form-product row">
                                <label for="question-{{$item->id}}" class="col-md-2 col-form-label text-md-right">{{ $item->name }}</label>

                                <div class="col-md-4">
                                    @if ($item->type_question == 'TEXT' )
                                        <input id="question-{{$item->id}}" type="text" class="form-control @error('question-'.$item->id) is-invalid @enderror"
                                        name="question-{{$item->id}}"

                                        @if($item->name == 'ชื่อ')
                                            value="{{ $uploaddeliveryplan->driver1 ?? old('question-'.$item->id) }}"
                                        @else
                                            value="{{ old('question-'.$item->id) }}"
                                        @endif

                                        @if ($item->required_flag == 'Y')
                                            required
                                        @endif


                                        >
                                    @else
                                     @if ($item->type_question == 'IMAGE' )
                                        <input type="file" accept="image/*;capture=camera" capture="camera" name="question-{{$item->id}}"
                                        @if ($item->required_flag == 'Y')
                                            required
                                        @endif
                                        >
                                    @else
                                        <textarea id="question-{{$item->id}}" class="form-control @error('question-'.$item->id) is-invalid @enderror"
                                        name="question-{{$item->id}}"
                                        @if ($item->required_flag == 'Y')
                                            required
                                        @endif
                                        >{{ old('question-'.$item->id) }}</textarea>
                                    @endif
                                    @endif


                                    @error('question-'.$item->id)
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            @endforeach

                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
