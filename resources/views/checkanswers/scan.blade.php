@extends('layouts.mainscan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                SCAN
            </div>
            <div class="col-12">
                <div id="reader" width="600px">
                </div>
            </div>
        </div>
    </div>
    @endsection
