@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ ชุดการตรวจ') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('check_sets.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้าง ชุดการตรวจ ใหม่') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('check_sets.store') }}">
                            @csrf


                            <div class="form-product row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('ชุดการตรวจ name') }}</label>

                                <div class="col-md-4">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" required >

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="status" class="col-md-2 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('status', $statusList , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('status')
                                        <span class="invalid-feedback" role="status">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-product row">
                                <label for="desc" class="col-md-2 col-form-label text-md-right">{{ __('Desc') }}</label>

                                <div class="col-md-10">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror"
                                        name="desc" value="{{ old('desc') }}">

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
