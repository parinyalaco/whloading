@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ ชุดคำถาม ใน ').$checkquestion->checkset->name }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('check_questions.index',[$checkquestion->checkset->id, $checkquestion->id]) }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไข ชุดคำถาม #').$checkquestion->id }}</div>

                    <div class="card-body">
                        <form action="{{ route('check_questions.update', [$checkquestion->checkset->id, $checkquestion->id]) }}" method="POST">
                            @csrf

                            @method('PUT')

                            <div class="form-product row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('ชุดคำถาม name') }}</label>

                                <div class="col-md-10">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $checkquestion->name }}" required >

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-product row">
                                <label for="status" class="col-md-2 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('status', $statusList , $checkquestion->status, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('status')
                                        <span class="invalid-feedback" role="status">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="type_question" class="col-md-2 col-form-label text-md-right">{{ __('Type') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('type_question', $typeQuestionList , $checkquestion->type_question, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('type_question')
                                        <span class="invalid-feedback" role="type_question">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-product row">

                                <label for="seq" class="col-md-2 col-form-label text-md-right">{{ __('ลำดับ') }}</label>

                                <div class="col-md-4">
                                    <input id="seq" type="number" class="form-control @error('seq') is-invalid @enderror"
                                        name="seq" value="{{ $checkquestion->seq }}" required >

                                    @error('seq')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="ans1_column" class="col-md-2 col-form-label text-md-right">{{ __('Col 1') }}</label>

                                <div class="col-md-4">
                                    <input id="ans1_column" type="text" class="form-control @error('ans1_column') is-invalid @enderror"
                                        name="ans1_column" value="{{ $checkquestion->ans1_column }}" required >

                                    @error('ans1_column')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-product row">
                                <label for="ans2_column" class="col-md-2 col-form-label text-md-right">{{ __('Col 2') }}</label>

                                <div class="col-md-4">
                                    <input id="ans2_column" type="text" class="form-control @error('ans2_column') is-invalid @enderror"
                                        name="ans2_column" value="{{ $checkquestion->ans2_column }}"  >

                                    @error('ans2_column')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="ans3_column" class="col-md-2 col-form-label text-md-right">{{ __('Col 3') }}</label>

                                <div class="col-md-4">
                                    <input id="ans3_column" type="text" class="form-control @error('ans3_column') is-invalid @enderror"
                                        name="ans3_column" value="{{ $checkquestion->ans3_column }}"  >

                                    @error('ans3_column')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-product row">
                                <label for="desc" class="col-md-2 col-form-label text-md-right">{{ __('Desc') }}</label>

                                <div class="col-md-10">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror"
                                        name="desc" value="{{ $checkquestion->desc }}">

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
