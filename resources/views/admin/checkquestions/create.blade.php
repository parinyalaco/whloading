@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการ ชุดคำถาม') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('check_questions.index', $checkset->id) }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้าง ชุดคำถาม ใหม่ ใน ').$checkset->name }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('check_questions.store', $checkset->id) }}">
                            @csrf


                            <div class="form-product row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('ชุดคำถาม name') }}</label>

                                <div class="col-md-10">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" required >

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-product row">
                                <label for="status" class="col-md-2 col-form-label text-md-right">{{ __('Status') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('status', $statusList , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('status')
                                        <span class="invalid-feedback" role="status">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="type_question" class="col-md-2 col-form-label text-md-right">{{ __('Type') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('type_question', $typeQuestionList , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('type_question')
                                        <span class="invalid-feedback" role="type_question">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-product row">

                                <label for="seq" class="col-md-2 col-form-label text-md-right">{{ __('ลำดับ') }}</label>

                                <div class="col-md-4">
                                    <input id="seq" type="number" class="form-control @error('seq') is-invalid @enderror"
                                        name="seq" value="{{ old('seq') }}" required >

                                    @error('seq')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="ans1_column" class="col-md-2 col-form-label text-md-right">{{ __('Col 1') }}</label>

                                <div class="col-md-4">
                                    <input id="ans1_column" type="text" class="form-control @error('ans1_column') is-invalid @enderror"
                                        name="ans1_column" value="{{ old('ans1_column') }}" required >

                                    @error('ans1_column')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-product row">
                                <label for="ans2_column" class="col-md-2 col-form-label text-md-right">{{ __('Col 2') }}</label>

                                <div class="col-md-4">
                                    <input id="ans2_column" type="text" class="form-control @error('ans2_column') is-invalid @enderror"
                                        name="ans2_column" value="{{ old('ans2_column') }}"  >

                                    @error('ans2_column')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="ans3_column" class="col-md-2 col-form-label text-md-right">{{ __('Col 3') }}</label>

                                <div class="col-md-4">
                                    <input id="ans3_column" type="text" class="form-control @error('ans3_column') is-invalid @enderror"
                                        name="ans3_column" value="{{ old('ans3_column') }}"  >

                                    @error('ans3_column')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-product row">
                                <label for="desc" class="col-md-2 col-form-label text-md-right">{{ __('Desc') }}</label>

                                <div class="col-md-10">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror"
                                        name="desc" value="{{ old('desc') }}">

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
