@extends('layouts.app')

@section('content')
    @if (Session::has('success'))
        <div>
            <h3> {{ Session::get('success') }}</h3>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row">
        @foreach ($checkpoints as $checkpoint)
            <div class="col-4 text-center p-3 border-3 border">
                {!! QrCode::size(180)->generate($checkpoint->id) !!}<br /><br /><b>{{ $checkpoint->name }}</b><br /><br />
            </div>
        @endforeach
    </div>
@endsection
