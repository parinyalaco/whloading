@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการสินค้า') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('products.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไขสินค้า') }}</div>

                    <div class="card-body">
                        <form action="{{ route('products.update', $product->id) }}" method="POST">
                            @csrf

                            @method('PUT')
                            
                            <div class="form-product row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('รหัสสินค้า') }}</label>

                                <div class="col-md-4">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $product->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="boxcode" class="col-md-2 col-form-label text-md-right">{{ __('รหัสกล่อง') }}</label>

                                <div class="col-md-4">
                                    <input id="boxcode" type="text" class="form-control @error('boxcode') is-invalid @enderror"
                                        name="boxcode" value="{{ $product->boxcode }}" autocomplete="boxcode" autofocus>

                                    @error('boxcode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-product row">
                                
                            </div>
                            <div class="form-product row">
                                <label for="customer" class="col-md-2 col-form-label text-md-right">{{ __('ลูกค้า') }}</label>

                                <div class="col-md-4">
                                    <input id="customer" type="text" class="form-control @error('customer') is-invalid @enderror"
                                        name="customer" value="{{ $product->customer }}" autocomplete="customer" autofocus>

                                    @error('customer')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="bagcode" class="col-md-2 col-form-label text-md-right">{{ __('รหัสถุง') }}</label>

                                <div class="col-md-4">
                                    <input id="bagcode" type="text" class="form-control @error('bagcode') is-invalid @enderror"
                                        name="bagcode" value="{{ $product->bagcode }}" autocomplete="bagcode" autofocus>

                                    @error('bagcode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-product row">
                                <label for="underboxcode" class="col-md-2 col-form-label text-md-right">{{ __('รหัสก้นกล่อง') }}</label>

                                <div class="col-md-4">
                                    <input id="underboxcode" type="text" class="form-control @error('underboxcode') is-invalid @enderror"
                                        name="underboxcode" value="{{ $product->underboxcode }}" autocomplete="underboxcode" autofocus>

                                    @error('underboxcode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="desc" class="col-md-2 col-form-label text-md-right">{{ __('Desc') }}</label>

                                <div class="col-md-4">
                                    <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror"
                                        name="desc" value="{{ $product->desc }}">

                                    @error('desc')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-product row">
                                <label for="wide" class="col-md-2 col-form-label text-md-right">{{ __('กว้าง (cm)') }}</label>

                                <div class="col-md-4">
                                    <input id="wide" type="text" class="form-control @error('wide') is-invalid @enderror"
                                        name="wide" value="{{ round($product->wide,2) }}" placeholder="ความกว้าง" autofocus>
                                         <input id="wide_er" type="text" class="form-control @error('wide_er') is-invalid @enderror"
                                        name="wide_er" value="{{ $product->wide_er }}" placeholder="ค่า+-"  autofocus>
                                    @error('wide')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="long" class="col-md-2 col-form-label text-md-right">{{ __('ยาว (cm)') }}</label>

                                <div class="col-md-4">
                                    <input id="long" type="text" class="form-control @error('long') is-invalid @enderror"
                                        name="long" value="{{ round($product->long,2) }}" placeholder="ความยาว"  autofocus>
                                     <input id="long_er" type="text" class="form-control @error('long_er') is-invalid @enderror"
                                        name="long_er" value="{{ $product->long_er }}" placeholder="ค่า+-"  autofocus>
                                    @error('long')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row">
                                <label for="height"
                                    class="col-md-2 col-form-label text-md-right">{{ __('สูง (cm)') }}</label>

                                <div class="col-md-4">
                                    <input id="height" type="text"
                                        class="form-control @error('height') is-invalid @enderror" name="height"
                                        value="{{ round($product->height,2) }}" placeholder="ความสูง"  autofocus>
                                    <input id="height_er" type="text"
                                        class="form-control @error('height_er') is-invalid @enderror" name="height_er"
                                        value="{{ $product->height_er }}" placeholder="ค่า+-"  autofocus>
                                    @error('height')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="numperbox"
                                    class="col-md-2 col-form-label text-md-right">{{ __('จำนวนถุงต่อกล่อง') }}</label>

                                <div class="col-md-4">
                                    <input id="numperbox" type="text"
                                        class="form-control @error('numperbox') is-invalid @enderror" name="numperbox"
                                        value="{{ $product->numperbox }}" autofocus>

                                    @error('numperbox')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-product row">
                                <label for="bagweight"
                                    class="col-md-2 col-form-label text-md-right">{{ __('หนักถุง (g)') }}</label>

                                <div class="col-md-4">
                                    <input id="bagweight" type="text"
                                        class="form-control @error('bagweight') is-invalid @enderror" name="bagweight"
                                        value="{{ $product->bagweight }}" autofocus>

                                    @error('bagweight')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="weight"
                                    class="col-md-2 col-form-label text-md-right">{{ __('หนัก (kg)') }}</label>

                                <div class="col-md-4">
                                    <input id="weight" type="text"
                                        class="form-control @error('weight') is-invalid @enderror" name="weight"
                                        value="{{ $product->weight }}" autofocus>

                                    @error('weight')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="bundelnumber"
                                    class="col-md-2 col-form-label text-md-right">{{ __('จำนวนกล่องต่อBundel') }}</label>

                                <div class="col-md-4">
                                    <input id="bundelnumber" type="text"
                                        class="form-control @error('bundelnumber') is-invalid @enderror" name="bundelnumber"
                                        value="{{ $product->bundelnumber }}" autofocus>

                                    @error('bundelnumber')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row">
                                <label for="note" class="col-md-2 col-form-label text-md-right">{{ __('Note') }}</label>

                                <div class="col-md-10">
                                    <input id="note" type="text" class="form-control @error('note') is-invalid @enderror"
                                        name="note" value="{{ $product->note }}">

                                    @error('note')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row">
                                <label for="lot_load_no" class="col-md-2 col-form-label text-md-right">{{ __('No.(LOT LOAD)') }}</label>

                                <div class="col-md-4">
                                    <input id="lot_load_no" type="text" class="form-control @error('lot_load_no') is-invalid @enderror"
                                        name="lot_load_no" value="{{ $product->lot_load_no }}" autocomplete="lot_load_no">

                                    @error('lot_load_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="lot_load_name" class="col-md-2 col-form-label text-md-right">{{ __('Name(LOT LOAD)') }}</label>

                                <div class="col-md-4">
                                    <input id="lot_load_name" type="text" class="form-control @error('lot_load_name') is-invalid @enderror"
                                        name="lot_load_name" value="{{ $product->lot_load_name }}" autocomplete="lot_load_name">

                                    @error('lot_load_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
