@extends('layouts.main')

@section('content')
    @if(Session::has('success'))
        <div><h3> {{ Session::get('success') }}</h3></div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('จัดการสินค้า') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('products.create') }}">สร้างสินค้าใหม่</a>
                        <a class="btn btn-success" href="{{ route('product_importView') }}">Upload</a>
                    </div>
                    
                    <form method="GET" action="{{ route('products.index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th>รายละเอียด กว้าง/ยาว/สูง/น้ำหนัก</th>
                                <th></th>
                            </tr>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ number_format($product->wide, 2, '.', ',') ?? '-' }} / {{ number_format($product->long , 2, '.', ',') ?? '-' }} / {{ number_format($product->height  , 2, '.', ',') ?? '-' }} / {{ number_format($product->weight , 2, '.', ',')  ?? '-' }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('products.show', $product->id) }}">แสดงข้อมูล</a>
                                        <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">แก้ไข</a>
                                        {{-- <a class="btn btn-danger" href="{{ route('products.destroy', $product->id) }}">ลบ</a> --}}
                                        <form action="{{ route('products.destroy', $product->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="pagination-wrapper"> {!! $products->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
