@extends('layouts.publicscan')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>สแกน คิวอาร์ โค๊ด ในจุดที่ถึง</h3>
            </div>
            <div class="col-12">
                <div id="reader" width="600px">
                </div>
            </div>
        </div>
    </div>
    @endsection
