@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('รายงาน') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('รายงานแผน Plan vs. Act') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('reports.loadplanvsactaction') }}" accept-charset="UTF-8"
                            class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('select_month') ? 'has-error' : '' }}">
                                    <label for="select_month" class="control-label">{{ 'เลือกเดือน' }}</label>
                                    {!! Form::select('select_month', 
                                    ['01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun',
                                    '07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec'],
                                    \Carbon\Carbon::now()->format('m'), 
                                    ['id'=>'select_month','class'=>'form-control','placeholder'=>'===Select===']) !!}
                                    {!! $errors->first('select_month', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('select_year') ? 'has-error' : '' }}">
                                    <label for="select_year" class="control-label">{{ 'ปี' }}</label>
                                    <input class="form-control" name="select_year" type="text" id="select_year"
                                        value="{{ \Carbon\Carbon::now()->format('Y') }}">
                                    {!! $errors->first('select_year', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group  col-md-12">
                                    <input class="btn btn-primary" type="submit" value="Export">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
