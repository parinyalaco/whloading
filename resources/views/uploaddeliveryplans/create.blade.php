@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการสินค้า') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('uploaddeliveryplans.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้างสินค้าใหม่') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('uploaddeliveryplans.store') }}" enctype="multipart/form-data">
                            @csrf


                            <div class="form-product row">
                                <label for="file" class="col-md-2 col-form-label text-md-right">{{ __('File Excel') }}</label>

                                <div class="col-md-4">
                                    <input id="file" type="file" class="form-control @error('file') is-invalid @enderror"
                                        name="file" required autofocus>

                                    @error('file')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-product row mb-0">
                                <div class="col-md-6 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
