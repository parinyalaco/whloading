@extends('layouts.main')

@section('content')
    @if(Session::has('success'))
        <div><h3> {{ Session::get('success') }}</h3></div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('จัด Upload Delivery Plan') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('uploaddeliveryplans.create') }}">Upload Delivery Plan</a>
                    </div>

                    <form method="GET" action="{{ route('uploaddeliveryplans.index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>วัน-เวลา</th>
                                <th>ชื่อ</th>
                                <th>จำนวนรายการ</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            @foreach ($uploaddeliveryplanms as $uploaddeliveryplanm)
                                <tr><td>{{ $uploaddeliveryplanm->created_at }}</td>
                                    <td>{{ $uploaddeliveryplanm->file_name }}</td>
                                    <td>{{ $uploaddeliveryplanm->uploaddeliveryplands()->count() }}</td>
                                    <td>{{ $uploaddeliveryplanm->status }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('uploaddeliveryplans.show', $uploaddeliveryplanm->id) }}">แสดงข้อมูล</a>
                                        {{-- <a class="btn btn-danger" href="{{ route('uploaddeliveryplans.destroy', $product->id) }}">ลบ</a> --}}
                                        <form action="{{ route('uploaddeliveryplans.destroy', $uploaddeliveryplanm->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="pagination-wrapper"> {!! $uploaddeliveryplanms->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
