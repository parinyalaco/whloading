@extends('layouts.main')

@section('content')
    @if(Session::has('success'))
        <div><h3> {{ Session::get('success') }}</h3></div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('จัด Upload Delivery Plan') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('uploaddeliveryplans.create') }}">Upload Delivery Plan</a>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Loading</th>
                                <th>Order</th>
                                <th>ทะเบียนหาง</th>
                                <th>ทะเบียนหัวส่งออก</th>
                                <th>หัวลาก</th>
                                <th>Container No</th>
                                <th>Seal No</th>
                                <th>พนักงานขับรถ1</th>
                                <th>พนักงานขับรถ2</th>
                                <th>status</th>
                            </tr>
                            @foreach ($uploadDeliveryPlanM->uploaddeliveryplands as $uploaddeliverypland)
                                <tr>
                                    <td>{{ $uploaddeliverypland->loading }}</td>
                                    <td>{{ $uploaddeliverypland->order }}</td>
                                    <td>{{ $uploaddeliverypland->license_plate_tail }}</td>
                                    <td>{{ $uploaddeliverypland->license_plate_head }}</td>
                                    <td>{{ $uploaddeliverypland->truck }}</td>
                                    <td>{{ $uploaddeliverypland->container_no }}</td>
                                    <td>{{ $uploaddeliverypland->seal_no }}</td>
                                    <td>{{ $uploaddeliverypland->driver1 }}</td>
                                    <td>{{ $uploaddeliverypland->driver2 }}</td>
                                    <td>{{ $uploaddeliverypland->status }}</td>
                                </tr>
                            @endforeach
                        </table>
                     </div>
                </div>
            </div>
        </div>
    </div>




@endsection
