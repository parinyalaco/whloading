@extends('layouts.main')
<style>
    label.cameraButton {
        display: inline-block;
        /* margin: 1em 0; */

        /* Styles to make it look like a button */
        padding: 0.5em;
        border: 1px solid #000;
        border-color: #EEE #CCC #CCC #EEE;
        background-color: #DDD;
    }

    /* Look like a clicked/depressed button */
    label.cameraButton:active {
        border-color: #CCC #EEE #EEE #CCC;
    }

    /* This is the part that actually hides the 'Choose file' text box for camera inputs */
    label.cameraButton input[accept*="camera"] {
        display: none;
    }
</style>

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading">
        <h3>{{ __('รายการ Load') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('list_load.create') }}">สร้างรายการใหม่</a>
                    </div>

                    <form method="GET" action="{{ route('list_load.index') }}" accept-charset="UTF-8" class="form-inline mx-2 my-lg-2 float-right" role="search">
                        <div class="input-group">
                            <input type="date" class="form-control" name="search_date" value="{{ request('search_date') }}">
                            <input type="text" class="form-control mx-2" name="search" placeholder="...shipment/order ขนส่ง..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 5%"></th>
                                <th style="width: 5%">วันที่โหลด</th>
                                <th style="width: 5%">shipment/order</th>
                                <th style="width: 5%">Customer PO& No.</th>
                                <th style="width: 5%">รูป</th>
                                <th style="width: 5%"></th>
                            </tr>
                            @foreach ($load_list as $key)
                                <tr>
                                    <td class="cell" style="vertical-align:middle;text-align: center;"
                                        @if(!empty($load_listD[$key->id]['pd']))
                                            data-toggle="collapse" data-bs-toggle="collapse" href="#det_{{ $key->id }}"
                                            aria-expanded="true" aria-controls="det_{{ $key->id }}"
                                        @endif>
                                        @if(!empty($load_listD[$key->id]['pd']))
                                            <a class="text-primary" href="#" title="show">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                                    <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                                                    <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                                                </svg>
                                            </a>
                                        @endif
                                    </td>
                                    <td>{{ date('d/m/Y',strtotime($key->list_date)) }}</td>
                                    <td>{{ $key->shipment_order }}</td>
                                    <td>{{ $key->customer_po }}</td>
                                    <td style="text-align: center">
                                        <input type="checkbox" disabled @if(!empty($key->list_img)) checked @endif>
                                    </td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('list_load.show', $key->id) }}">Detail</a>
                                        <a class="btn btn-primary" href="{{ route('list_load.edit', $key->id) }}">แก้ไข</a>
                                        @if(!empty($load_listD[$key->id]['pd']) && !empty($load_listD[$key->id]['img']))
                                            <a class="btn btn-info" href="{{ route('list_load.to_pdf', $key->id) }}">PDF</a>
                                        @endif
                                        <form method="POST" action="{{ route('list_load.capture',$key->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            <label class="cameraButton">รูปปิดตู้
                                                <input type="file" accept="image/*; capture=camera" name="file_webcam{{ $key->id }}" id="file_webcam{{ $key->id }}" onchange="this.form.submit()"/>
                                            </label>
                                        </form>
                                        <form action="{{ route('list_load.destroy', $key->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" onclick="return confirm('ข้อมูลทั้งหมดจะถูกลบ คุณแน่ใจมั้ย ?')">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                                @if(!empty($img_con[$key->id]['con_img']))
                                    <tr>
                                        <td colspan="7" class="hiddenRow">
                                            <table class="table table-success">
                                                <tr>
                                                    @php
                                                        $i = 0;
                                                    @endphp
                                                    @foreach ($img_con[$key->id]['con_img'] as $kimg => $vimg)
                                                        @php
                                                            $path = $img_con[$key->id]['con_img_path'][$kimg];
                                                            $type = pathinfo($path, PATHINFO_EXTENSION);
                                                            if(file_exists($path)){
                                                                $data = file_get_contents($path);
                                                                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                                                        @endphp
                                                        <td class="cell" style="text-align: center;">
                                                            <div id="div_{{ $kimg }}">
                                                                <a href="{{ $base64 }}" target="_blank">
                                                                    <img height="100px" src="{{ $base64 }}"
                                                                    id="img_m_{{ $kimg }}" />
                                                                </a>
                                                                <a class="btn app-btn-secondary" onclick="ConfirmMessage('m',{{ $kimg }})" >
                                                                     X
                                                                </a>
                                                            </div>
                                                        </td>
                                                        @php
                                                            }else{
                                                                                        echo "<td></td>";
                                                                                    }
                                                        @endphp
                                                    @endforeach
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                                @if(!empty($load_listD[$key->id]['pd']))
                                    <tr>
                                        <td colspan="7" class="hiddenRow">
                                            <div class="accordian-body collapse" id="det_{{ $key->id }}">
                                                <table class="table table-info">
                                                    <thead>
                                                        <tr>
                                                            <th class="cell" colspan="2"></th>
                                                            <th class="cell">Name</th>
                                                            <th class="cell">น้ำหนัก</th>
                                                            <th class="cell">จำนวน</th>
                                                            <th class="cell">น้ำหนักรวม</th>
                                                            <th class="cell">No.</th>
                                                            <th class="cell"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $c_pd = 0;
                                                        @endphp
                                                        @foreach ($load_listD[$key->id]['pd'] as $kpd => $vpd)
                                                            <tr>
                                                                <td class="cell" style="text-align: center;">
                                                                    @if(count($load_listD[$key->id]['sort'])>1)
                                                                        @if($load_listD[$key->id]['sort'][$kpd]==1)
                                                                            <a class="text-primary" title="down"
                                                                                onclick="to_sort('down', '{{ $load_listD[$key->id]['id'][$kpd] }}', '{{ $load_listD[$key->id]['sort'][$kpd] }}')">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                                                    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                                                </svg>
                                                                            </a>
                                                                        @elseif ($vpd==end($load_listD[$key->id]['pd']))
                                                                            <a class="text-primary" title="up"
                                                                                onclick="to_sort('up', '{{ $load_listD[$key->id]['id'][$kpd] }}', '{{ $load_listD[$key->id]['sort'][$kpd] }}')">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                                                                    <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                                                                </svg>
                                                                            </a>
                                                                        @else
                                                                            <div class="col">
                                                                                <div class="row">
                                                                                    <a class="text-primary" title="up"
                                                                                        onclick="to_sort('up', '{{ $load_listD[$key->id]['id'][$kpd] }}', '{{ $load_listD[$key->id]['sort'][$kpd] }}')">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                                                                            <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                                                                        </svg>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <a class="text-primary" title="down"
                                                                                    onclick="to_sort('down', '{{ $load_listD[$key->id]['id'][$kpd] }}', '{{ $load_listD[$key->id]['sort'][$kpd] }}')">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                                                                            <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                                                                        </svg>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td class="cell" style="text-align: center;">{{ $load_listD[$key->id]['sort'][$kpd] }}</td>
                                                                <td class="cell">{{ $load_listD[$key->id]['pd'][$kpd] }}</td>
                                                                <td class="cell" style="text-align: right;">{{ $load_listD[$key->id]['qty'][$kpd] }}</td>
                                                                <td class="cell" style="text-align: right;">{{ $load_listD[$key->id]['num'][$kpd] }}</td>
                                                                <td class="cell" style="text-align: right;">
                                                                    {{ number_format($load_listD[$key->id]['qty'][$kpd]*$load_listD[$key->id]['num'][$kpd],3) }}
                                                                </td>
                                                                <td class="cell" style="text-align:center;">
                                                                     {{ $load_listD[$key->id]['no'][$kpd] }}
                                                                </td>
                                                                <td class="cell" style="text-align: center;">
                                                                    <form method="POST" action="{{ url('/list_load/capture_pd/'.$key->id.'/'.$kpd) }}" enctype="multipart/form-data">
                                                                        @csrf
                                                                        <label class="cameraButton">รูปสินค้า
                                                                            <input type="file" accept="image/*; capture=camera" name="file_webcam_pd_{{ $key->id }}_{{ $kpd }}" id="file_webcam_pd_{{ $key->id }}_{{ $kpd }}" onchange="this.form.submit()"/>
                                                                        </label>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                            @if(!empty($load_listD[$key->id]['img'][$kpd]))
                                                                <tr>
                                                                    <td colspan="8" class="hiddenRow">
                                                                        <table class="table table-info">
                                                                            <tr>
                                                                                @foreach ($load_listD[$key->id]['img'][$kpd] as $kimg => $vimg)
                                                                                    @php
                                                                                        $path = $load_listD[$key->id]['img_path'][$kpd][$kimg];
                                                                                        $type = pathinfo($path, PATHINFO_EXTENSION);
                                                                                        if(file_exists($path)){
                                                                                        $data = file_get_contents($path);
                                                                                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                                                    @endphp
                                                                                    <td class="cell" style="text-align: center;">
                                                                                        <div id="divpd_{{ $kimg }}">
                                                                                            <a href="{{ $base64 }}" target="_blank">
                                                                                                <img height="100px" src="{{ $base64 }}"
                                                                                                id="img_d_{{ $kimg }}"/>
                                                                                            </a>
                                                                                            <a class="btn app-btn-secondary" onclick="ConfirmMessage('d',{{ $kimg }})" >
                                                                                                X
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                    @php
                                                                                    }else{
                                                                                        echo "<td></td>";
                                                                                    }
                                                                                    @endphp
                                                                                @endforeach
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                        <div class="pagination-wrapper"> {!! $load_list->appends(['search_date' => Request::get('search_date'), 'search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script language="JavaScript">
        function ConfirmMessage(status, id) {
            if (confirm("คุณต้องการลบรูปนี้ใช่มั้ย ?")) { // Clic sur OK
                var link_url1 = window.location.origin + window.location.pathname;
                if(status=='m'){
                    var link_url2 = "/del_capture?id="+id; //ใช้กับตัวจริง
                    console.log('div_'+id);
                    document.getElementById('div_'+id).style.display = "none";
                }else{
                    var link_url2 = "/del_capture_pd?id="+id; //ใช้กับตัว pd
                    console.log('divpd_'+id);
                    document.getElementById('divpd_'+id).style.display = "none";
                }
                link_url = link_url1+link_url2;
                console.log(link_url);
                $.get(link_url,function (data) {
                    alert(data)
                });
            }
        }
        function to_sort(up_down, id, sort) {
            // var link_url = window.location.origin+"/whloading/list_load/to_sort?type="+up_down+"&id="+id+"&sort="+sort;
            var link_url = window.location.origin+window.location.pathname+"/to_sort?type="+up_down+"&id="+id+"&sort="+sort;
            window.location.replace(link_url);
        }
    </script>
@endsection
