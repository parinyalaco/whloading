@extends('layouts.main')

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load.index') }}">Back</a>
        </div>
        <h3 class="col">{{ __('รายการ Load') }} -> Edit</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <form action="{{ route('list_load.update', $list_load->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-product row">
                                <label for="list_date" class="col-md-2 col-form-label text-md-right">{{ __('วันที่โหลด') }}</label>

                                <div class="col-md-4">
                                    <input id="list_date" type="date" class="form-control @error('name') is-invalid @enderror"
                                        name="list_date" value="@if(old('list_date')){{ old('list_date') }}@else{{ $list_load->list_date }}@endif"
                                        required autocomplete="list_date" autofocus>

                                    @error('list_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row my-2">
                                <label for="shipment_order" class="col-md-2 col-form-label text-md-right">{{ __('shipment/order') }}</label>

                                <div class="col-md-4">
                                    <input id="shipment_order" type="text" class="form-control @error('shipment_order') is-invalid @enderror"
                                        name="shipment_order" value="@if(old('shipment_order')){{ old('shipment_order') }}@else{{ $list_load->shipment_order }}@endif"
                                        required autocomplete="shipment_order" autofocus>

                                    @error('shipment_order')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row my-2">
                                <label for="customer_po" class="col-md-2 col-form-label text-md-right">{{ __('Customer PO No.') }}</label>

                                <div class="col-md-4">
                                    <input id="customer_po" type="text" class="form-control @error('customer_po') is-invalid @enderror"
                                        name="customer_po" value="{{ $list_load->customer_po }}" required autocomplete="customer_po" autofocus>

                                    @error('customer_po')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row my-2">
                                <label for="numperbox"
                                    class="col-md-2 col-form-label text-md-right">{{ __('รูปประกอบ') }}</label>

                                <div class="col-md-4">
                                    <input type="file" class="custom-file-input" accept="image/*;capture=camera"
                                        id='list_img' name='list_img' onchange="show_img(event)">
                                    {{-- <input type="hidden" id="old_img" name="old_img" value="{{ $list_load->list_img }}"> --}}
                                    <input type="hidden" id="chk_img" name="chk_img" value="{{ $list_load->list_img }}">

                                    @error('list_img')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                            @php
                                $fs = '';
                                if($list_load->list_img_path){
                                    $size_img = getimagesize($list_load->list_img_path);
                                    // echo $size_img[0].'--'.$size_img[1];
                                    // print_r($size_img);
                                    $i_w = $size_img[0];
                                    $i_h = $size_img[1];
                                    // onclick="zoom_img()"
                                    if($i_w<$i_h){
                                        // $ds = "height: 300px";
                                        $fs = 'height=300px';
                                    }else{
                                        // $ds = "width: 850px";
                                        $fs = 'width=200px';
                                    }
                                    $path = $list_load->list_img_path;
                                    $type = pathinfo($path, PATHINFO_EXTENSION);
                                    $data = file_get_contents($path);
                                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                }
                            @endphp
                            <div class="row my-2">
                                <div id="content">
                                    @if(!empty($list_load->list_img_path))

                                        <div class="alert alert-dismissible fade show" role="alert">
                                            <img id="output" name="output" {{ $fs }} src="{{ $base64 }}" />
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event)"></button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">

        var show_img = function(event) {
            console.log(event);
            if(file = event.target.files[0]){
                var size_show = '';
                let img = new Image()
                img.src = window.URL.createObjectURL(event.target.files[0])
                getBase64(file, function(base64Data){
                    // console.log("Base64 of file is", base64Data);
                    base64_path = base64Data;
                });

                console.log(img.src);
                img.onload = () => {
                    console.log('width : '+img.width+' height :'+img.height);
                    if(img.width>img.height)
                        size_show = 'width="20px" ';
                    else
                        size_show = 'height="300px" ';

                    var element = document.getElementById("content");
                    var add_att;
                    add_att = '<div class="alert alert-dismissible fade show" role="alert">';
                    // add_att += '<img id="output" src="'+URL.createObjectURL(event.target.files[0])+'" '+size_show+' onclick="zoom_img()"/>'
                    add_att += '<img id="output" src="'+base64_path+'" '+size_show+' onclick="zoom_img()"/>'
                    add_att += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event)"></button>';
                    add_att += '</div>';
                    element.innerHTML = add_att;

                    var element2 = document.getElementById("chk_img");
                    var element3 = document.getElementById("list_img");
                    element2.value = element3.value;
                }
            }
        };

        function getBase64 (file, callback) {
            const reader = new FileReader();
            reader.addEventListener('load', () => callback(reader.result));
            reader.readAsDataURL(file);
        }

        function del_img(event) {
            var element = document.getElementById("list_img");
            element.value = null;
            var element2 = document.getElementById("chk_img");
            element2.value = null;
        }

    </script>
@endsection
