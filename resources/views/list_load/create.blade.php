@extends('layouts.main')

@section('content')
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ Session::get('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="page-heading row">
        <div class="pull-right col-auto">
            <a class="btn btn-success" href="{{ route('list_load.index') }}">Back</a>
        </div>
        <h3 class="col">{{ __('รายการ Load') }} -> Create</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('list_load.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row my-2">
                                <label for="list_date" class="col-md-2 col-form-label text-md-right">{{ __('วันที่โหลด') }}</label>

                                <div class="col-md-4">
                                    <input id="list_date" type="date" class="form-control @error('name') is-invalid @enderror"
                                        name="list_date" value="{{ old('list_date') }}" required autocomplete="list_date" autofocus>

                                    @error('list_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row my-2">
                                <label for="shipment_order" class="col-md-2 col-form-label text-md-right">{{ __('shipment/order') }}</label>

                                <div class="col-md-4">
                                    <input id="shipment_order" type="text" class="form-control @error('shipment_order') is-invalid @enderror"
                                        name="shipment_order" value="{{ old('shipment_order') }}" required autocomplete="shipment_order" autofocus>

                                    @error('shipment_order')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row my-2">
                                <label for="customer_po" class="col-md-2 col-form-label text-md-right">{{ __('Customer PO No.') }}</label>

                                <div class="col-md-4">
                                    <input id="customer_po" type="text" class="form-control @error('customer_po') is-invalid @enderror"
                                        name="customer_po" value="{{ old('customer_po') }}" required autocomplete="customer_po" autofocus>

                                    @error('customer_po')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row my-2">
                                <label for="numperbox"
                                    class="col-md-2 col-form-label text-md-right">{{ __('รูปประกอบ') }}</label>

                                <div class="col-md-4">
                                    <input type="file" class="custom-file-input" accept="image/*;capture=camera"
                                        id='list_img' name='list_img' onchange="show_img(event)">

                                    @error('list_img')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>

                            <div class="row my-2">
                                <div id="content"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        var show_img = function(event) {
            console.log(event);
            if(file = event.target.files[0]){
                var size_show = '';
                let img = new Image()
                img.src = window.URL.createObjectURL(event.target.files[0])
                img.onload = () => {
                    console.log('width : '+img.width+' height :'+img.height);
                    if(img.width>img.height)
                        size_show = 'width="850px" ';
                    else
                        size_show = 'height="300px" ';

                    var element = document.getElementById("content");
                    var add_att;
                    add_att = '<div class="alert alert-dismissible fade show" role="alert">';
                    add_att += '<img id="output" src="'+URL.createObjectURL(event.target.files[0])+'" '+size_show+' onclick="zoom_img()"/>'
                    add_att += '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" onclick="del_img(event)"></button>';
                    add_att += '</div>';
                    element.innerHTML = add_att;

                    var element2 = document.getElementById("chk_img");
                    var element3 = document.getElementById("list_img");
                    element2.value = element3.value;
                }
            }
        };

        function del_img(event) {
            var element = document.getElementById("list_img");
            element.value = null;
        }
    </script>
@endsection
