<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        @LaravelDompdfThaiFont
        <style>
            body {
                font-family: "THSarabunNew";
            }
            .tb { border-collapse: collapse; width:100%; }
            .tb th, .tb td { border: solid 1px #000000; vertical-align: top;}
            .tbn th, .tb td { vertical-align: top;}
        </style>
        <title></title>
    </head>
    <body>
        <div class="page-content">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" style="text-align: center">
                            <strong>{{ $load_list->name }}&nbsp;Delivery&nbsp;Report</strong>
                        </div>
                        <div class="card-body">
                            <table style="width: 100%" class="tbn">
                                <tr style="vertical-align: top">
                                    <td style="width: 25%">
                                        <strong>Delivery&nbsp;Date</strong>&nbsp;:&nbsp;{{ date('dmY',strtotime($load_list->list_date)) }}
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td style="width: 25%">
                                        <strong>Order&nbsp;</strong>&nbsp;:&nbsp;{!! $load_list->shipment_order !!}
                                    </td>
                                </tr>                                
                                <tr style="vertical-align: top">
                                    <td style="width: 25%">
                                        <strong>Customer&nbsp;PO&nbsp;No.&nbsp;</strong>&nbsp;:&nbsp;{!! $load_list->customer_po !!}
                                    </td>
                                </tr>
                            </table>

                            <table style="width: 100%" class="tb">
                                <tr>
                                    <th style="width: 2%"></th>
                                    <th style="width: 35%">Product&nbsp;name</th>
                                    <th style="width: 20%">Product</th>
                                    <th style="width: 10%">Weight</th>
                                    <th style="width: 10%">Quantity</th>
                                    <th style="width: 10%">Unit</th>
                                    <th style="width: 13%">No.</th>
                                </tr>
                                @foreach($load_listD['pd_code'] as $key => $value)
                                    <tr>                                    
                                        <td style="text-align: center">{!! $load_listD['sort'][$key] !!}</td>
                                        <td>{!! $load_listD['pd_desc'][$key] !!}</td>
                                        <td>{!! $value !!}</td>
                                        <td style="text-align: right">{!! number_format($load_listD['qty'][$key],2) !!}</td>
                                        <td style="text-align: right">{!! number_format($load_listD['num'][$key]) !!}</td>
                                        <td>CAR</td>
                                        <td style="text-align: center">{!! $load_listD['no'][$key] !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="row" style="width: 100%">
                                <div class="col" style="float: left; text-align: right; width: 57%"><strong>Total</strong></div>
                                <div class="col" style="float: left; text-align: right; width: 10%"><strong>{!! number_format(array_sum($load_listD['qty']),2) !!}</strong></div>
                                <div class="col" style="float: left; text-align: right; width: 10%"><strong>{!! number_format(array_sum($load_listD['num'])) !!}</strong></div>                            
                                <div class="col" style="float: left; width: 10%"><strong>CAR</strong></div>
                            </div>
                            <div style="clear:both"></div>

                            <table style="width: 100%">
                                @foreach($load_listD['pd_code'] as $key => $value)
                                    <tr>
                                        <td style="width: 100%">No.{!! $load_listD['sort'][$key] !!}&nbsp;:&nbsp;{!! $load_listD['lot_load_no'][$key] !!}&nbsp;{!! $load_listD['lot_load_name'][$key] !!}&nbsp;No.&nbsp;{!! $load_listD['no'][$key] !!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width: 50%" class="tb">
                                                <tr>
                                                    <th style="width: 40%">Product</th>
                                                    <th style="width: 20%">Weight</th>
                                                    <th style="width: 20%">Quantity</th>
                                                    <th style="width: 20%">Unit</th>
                                                </tr>
                                                <tr>                                    
                                                    <td>{!! $value !!}</td>
                                                    <td style="text-align: right">{!! number_format($load_listD['qty'][$key],2) !!}</td>
                                                    <td style="text-align: right">{!! number_format($load_listD['num'][$key]) !!}</td>
                                                    <td>CAR</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @if(!empty($load_listD[$key]))
                                        <tr><td>PICTURE</td></tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        @foreach($load_listD[$key]['img_path'] as $kid => $vid)
                                                            @php
                                                                $logo = '';
                                                                $path = $vid;
                                                                $type = pathinfo($path, PATHINFO_EXTENSION);
                                                                $data = file_get_contents($path);
                                                                $logo = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                            @endphp
                                                            <td style="width: 20%"><img src="{{ $logo }}" width="150"/></td>
                                                        @endforeach
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
