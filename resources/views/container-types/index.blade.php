@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการประเภทตู้') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('container-types.create') }}">สร้างประเภทตู้ใหม่</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th>รายละเอียด</th>
                                <th></th>
                            </tr>
                            @foreach ($containertypes as $containertype)
                                <tr>
                                    <td>{{ $containertype->name }}</td>
                                    <td>{{ $containertype->desc }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('container-types.show', $containertype->id) }}">แสดง</a>
                                        <a class="btn btn-primary" href="{{ route('container-types.edit', $containertype->id) }}">แก้ไข</a>
                                        <form action="{{ route('container-types.destroy', $containertype->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
