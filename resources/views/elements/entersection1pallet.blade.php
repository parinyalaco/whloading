<div class="table-responsive-lg">
<table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">พาเลท No.</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">Lot</th>
                                    <th class="text-center">Exp Date.</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">Note</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sumtotal = 0;
                                @endphp
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get()
        as $item)
                                    <tr>

                                        <td>{{ $item->pallet_no }}</td>
                                        <td>{{ $item->productt->name ?? '' }}</td>
                                        <td>{{ $item->p_lot }}</td>
                                        <td>{{ $item->p_exp_date }}</td>
                                        <td>{{ $item->t_base }}</td>
                                        <td>{{ $item->t_height }}</td>
                                        <td>{{ $item->t_total }}</td>
                                        <td>{{ $item->all_total }}</td>
                                        <td>
                                            @php
                                                $sumtotal += $item->all_total;
                                            @endphp
                                            {{ $sumtotal }}</td>
                                        <td>{{ $item->note }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
</div>
