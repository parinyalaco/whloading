@foreach ($pics as $key => $value)
<label class="col-md-2 col-form-label text-md-right">{{ $key . '. ' . $value }}</label>
 <div class="col-md-4">
    @for ($i = 1; $i <= 8; $i++)
        <a href="{{ url("/loadings/uploadextimg/".$loadm->id."/".$key."/".$i) }}" target="_blank"> #{{$i}}
        @if (isset($imageData[$key][$i]))
            <img height="20px" src="{{ url($imageData[$key][$i]->loading_img_path) }}" alt="{{ $key . '. ' . $value }} #{{$i}}">
        @else
            ว่าง
        @endif
        </a>
    @endfor
 </div>
@endforeach
