@foreach ($pics as $key => $value)
    @php
        $load_pic = 'loading_img' . $key;
        $pic_path = 'loading_img_path' . $key;
        $pic_del = 'remove_path' . $key;
    @endphp
    <label for={{ $load_pic }} class="col-md-2 col-form-label text-md-right">{{ $key . '. ' . $value }}</label>

    <div class="col-md-4">
        {!! Form::file(
            $load_pic,
            $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png', 'capture' => 'camera'],
        ) !!}
        @if (!empty($loadm->$pic_path))
            <a href="{{ url($loadm->$pic_path) }}" target="_blank"><img height="20px"
                    src="{{ url($loadm->$pic_path) }}"></a>
            {!! Form::checkbox($pic_del, 'remove', false) !!} remove
        @endif

        @error($load_pic)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
@endforeach
