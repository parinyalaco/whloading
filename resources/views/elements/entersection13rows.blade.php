<div class="table-responsive-lg">
<table class="table table-bordered">
                            <thead>
                                <tr>

                                    <th colspan="6" class="w-35 text-center">แถวด้านซ้าย</th>
                                    <th colspan="5" class="w-35 text-center">แถวกลาง</th>
                                    <th colspan="5" class="w-35 text-center">แถวด้านขวา</th>
                                    <th rowspan="2" class="text-center">รวม</th>
                                    <th rowspan="2" class="text-center">ยอดรวม</th>
                                    <th rowspan="2" class="text-center">Note</th>
                                </tr>
                                <tr>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sumtotal = 0;
                                @endphp
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get()
        as $item)
                                    <tr>

                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productt->name ?? '' }}</td>
                                        <td>{{ $item->t_base }}</td>
                                        <td>{{ $item->t_height }}</td>
                                        <td>{{ $item->t_excess }}</td>
                                        <td>{{ $item->t_total }}</td>
                                        <td>{{ $item->productc->name ?? '' }}</td>
                                        <td>{{ $item->c_base }}</td>
                                        <td>{{ $item->c_height }}</td>
                                        <td>{{ $item->c_excess }}</td>
                                        <td>{{ $item->c_total }}</td>
                                        <td>{{ $item->productl->name ?? '' }}</td>
                                        <td>{{ $item->l_base }}</td>
                                        <td>{{ $item->l_height }}</td>
                                        <td>{{ $item->l_excess }}</td>
                                        <td>{{ $item->l_total }}</td>
                                        <td>{{ $item->all_total }}</td>
                                        <td>
                                            @php
                                                $sumtotal += $item->all_total;
                                            @endphp
                                            {{ $sumtotal }}</td>
                                        <td>{{ $item->note }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
</div>
