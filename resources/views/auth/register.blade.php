@extends('layouts.withoutlogin')

@section('content')

<div class="row h-100">
    <div class="col-12">
        <div id="auth-left">
                <a href="{{ url("/") }}"><img src="{{ asset('logo/laco_logo.png') }}" alt="Logo"></a>

            <h1 class="auth-title">ลงทะเบียน</h1>
<p class="auth-subtitle mb-5">กรอก Email และ Password</p>
            <form action="{{ route('register') }}" method="POST">
                @csrf
                <div class="row">
                <div class="col-6">
                <div class="form-group position-relative has-icon-left mb-4">
                    <input id='username' name='username' type="text" class="form-control form-control-xl" placeholder="username" required  autocomplete="off">
                    <input id='group_id' name='group_id' type="hidden" value='2'>
                    <div class="form-control-icon">
                        <i class="bi bi-person"></i>
                    </div>
                    @error('username')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                </div>
                
                <div class="col-6">
                <div class="form-group position-relative has-icon-left mb-4">
                    <input id='email'  name='email' type="text" class="form-control form-control-xl" placeholder="Email"  autocomplete="off" required>
                    <div class="form-control-icon">
                        <i class="bi bi-person"></i>
                    </div>
                    @error('email')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                 </div>
                 <div class="col-12">
                <div class="form-group position-relative has-icon-left mb-4">
                    <input id='name' name='name' type="text" class="form-control form-control-xl" placeholder="name"  autocomplete="off" required>
                    <div class="form-control-icon">
                        <i class="bi bi-person"></i>
                    </div>
                    @error('name')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                </div>
                <div class="col-6">
                <div class="form-group position-relative has-icon-left mb-4">
                    <input id='password'  name='password' type="password" class="form-control form-control-xl" placeholder="รหัสผ่าน"  autocomplete="off" required>
                    <div class="form-control-icon">
                        <i class="bi bi-shield-lock"></i>
                    </div>
                    @error('password')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                </div>
                <div class="col-6">
                <div class="form-group position-relative has-icon-left mb-4">
                    <input id='password_confirmation'  name='password_confirmation'  type="password" class="form-control form-control-xl" placeholder="รหัสผ่าน"  autocomplete="off" required>
                    <div class="form-control-icon">
                        <i class="bi bi-shield-lock"></i>
                    </div>
                </div>
                </div>
                <button class="btn btn-primary btn-block btn-lg shadow-lg mt-5">ลงทะเบียน</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-lg-7 d-none d-lg-block">
        <div id="auth-right">

        </div>
    </div>
</div>

@endsection