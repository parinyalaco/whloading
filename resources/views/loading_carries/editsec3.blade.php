@extends('layouts.main')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load ฝาก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loading_carries.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แผน Load ฝาก ส่วน3 #').$loadcarrym->id }}</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadcarrym->load_date}}</div>
                            <div class="col-md-3"><strong>คลังนอก</strong> : {{$loadcarrym->extwh->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Product</strong> : {{$loadcarrym->product->name ?? '-' }}</div>
                            <div class="col-md-3"><strong>น้ำหนักสินค้านำฝาก (kg)</strong> : {{$loadcarrym->total_weight}}</div>
                        </div>
                        <form action="{{ route('loading_carries.updatesec3', $loadcarrym->id) }}" method="POST">
                            @csrf

                            <div class="form-group row">
                                <label for="open_box" class="col-md-2 col-form-label text-md-right">{{ __('เวลาเปิดตู้') }}</label>

                                <div class="col-md-4">
                                    <input id="open_box" type="datetime-local" class="form-control @error('open_box') is-invalid @enderror"
                                        name="open_box" value="{{ $loadcarrym->open_box }}"  >

                                    @error('open_box')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="close_box" class="col-md-2 col-form-label text-md-right">{{ __('เวลาปิดตู้') }}</label>

                                <div class="col-md-4">
                                    <input id="close_box" type="datetime-local" class="form-control @error('close_box') is-invalid @enderror"
                                        name="close_box" value="{{ $loadcarrym->close_box }}"  >

                                    @error('close_box')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="open_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิเปิดตู้') }}</label>

                                <div class="col-md-4">
                                     <input id="open_temperature" type="number" class="form-control @error('open_temperature') is-invalid @enderror"
                                        name="open_temperature" value="{{ $loadcarrym->open_temperature }}"  >

                                    @error('open_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="first_pallet_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิสินค้า พาเลทแรก') }}</label>

                                <div class="col-md-4">
                                     <input id="first_pallet_temperature" type="number" class="form-control @error('first_pallet_temperature') is-invalid @enderror"
                                        name="first_pallet_temperature" value="{{ $loadcarrym->first_pallet_temperature }}"  >

                                    @error('first_pallet_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="delivery_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิปล่อยตู้') }}</label>

                                <div class="col-md-4">
                                     <input id="delivery_temperature" type="number" class="form-control @error('delivery_temperature') is-invalid @enderror"
                                        name="delivery_temperature" value="{{ $loadcarrym->delivery_temperature }}"  >

                                    @error('delivery_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="middle_pallet_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิสินค้า พาเลทกลางตู้') }}</label>

                                <div class="col-md-4">
                                     <input id="middle_pallet_temperature" type="number" class="form-control @error('middle_pallet_temperature') is-invalid @enderror"
                                        name="middle_pallet_temperature" value="{{ $loadcarrym->middle_pallet_temperature }}"  >

                                    @error('middle_pallet_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="boc_close_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิตู้ ขณะปิดตู้ เสียบปลั๊ก') }}</label>

                                <div class="col-md-4">
                                     <input id="boc_close_temperature" type="number" class="form-control @error('boc_close_temperature') is-invalid @enderror"
                                        name="boc_close_temperature" value="{{ $loadcarrym->boc_close_temperature }}"  >

                                    @error('boc_close_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="last_pallet_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิสินค้า พาเลทสุดท้าย') }}</label>

                                <div class="col-md-4">
                                     <input id="last_pallet_temperature" type="number" class="form-control @error('last_pallet_temperature') is-invalid @enderror"
                                        name="last_pallet_temperature" value="{{ $loadcarrym->last_pallet_temperature }}"  >

                                    @error('last_pallet_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                               <label for="boc_delivery_temperature" class="col-md-2 col-form-label text-md-right">{{ __('อุณหภูมิตู้ ขณะปล่อยตู้') }}</label>

                                <div class="col-md-4">
                                     <input id="boc_delivery_temperature" type="number" class="form-control @error('boc_delivery_temperature') is-invalid @enderror"
                                        name="boc_delivery_temperature" value="{{ $loadcarrym->delivery_temperature }}"  >

                                    @error('boc_delivery_temperature')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    $('.track_type_change').on('change',(event)=>{
        let typedata = event.target.value;
        if(typedata == "CONVOY" ){
            $("#truck_license_plate").prop('readonly', false);
        }else{
            $("#truck_license_plate").prop('readonly', true);
        }
    });

    $('.product_change').on('change',(event)=>{
        let id = event.target.value;
        $.get( "/products/getbyid/"+id, function( data ) {
            $( "#weight_per_product" ).val( data.weight );
            calwg();
        });


    },);

    $('.calweight').on('change',()=>{
        calwg();

    });

    const calwg = () => {
        console.log("calwg");
        let num_of_product = $('#num_of_product').val();
        let weight_per_product = $('#weight_per_product').val();

        if(num_of_product > 0 && weight_per_product > 0){
            $('#total_weight').val(num_of_product*weight_per_product);
        }else{
            $('#total_weight').val(0);
        }
    };

</script>
@endsection
