@extends('layouts.main')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load ฝาก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loading_carries.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>{{ __('แผน Load ฝาก File แนบ #').$loadcarrym->id }}</b></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ช้อมูลหลัก') }}</b></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadcarrym->load_date}}</div>
                            <div class="col-md-3"><strong>คลังนอก</strong> : {{$loadcarrym->extwh->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Product</strong> : {{$loadcarrym->product->name ?? '-' }}</div>
                            <div class="col-md-3"><strong>น้ำหนักสินค้านำฝาก (kg)</strong> : {{$loadcarrym->num_of_product}} X {{$loadcarrym->weight_per_product}} = {{$loadcarrym->total_weight}}</div>
                            <div class="col-md-3"><strong>Crop</strong> : {{$loadcarrym->crop->name}}</div>
                            <div class="col-md-3"><strong>Usage</strong> : {{$loadcarrym->usage ?? '-'}}</div>
                            <div class="col-md-3"><strong>ประเภทขนส่ง</strong> : {{$trackTypelist[$loadcarrym->track_type] ?? '-' }}</div>
                            <div class="col-md-3"><strong>ทะเบียน</strong> : {{$loadcarrym->truck_license_plate}} /  {{$loadcarrym->convoy_license_plate}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('พนักงาน') }}</b></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><strong>พนักงานหน้าตู้ 1</strong><br>{{$teams['Front'][0]->team->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>พนักงานหน้าตู้ 2</strong><br>{{$teams['Front'][1]->team->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>พนักงานท้ายตู้ 1</strong><br>{{$teams['Back'][0]->team->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>พนักงานท้ายตู้ 2</strong><br>{{$teams['Back'][1]->team->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>พนักงาน FL</strong><br>{{$teams['FL'][0]->team->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>พนักงานคุมโหลด</strong><br>{{$teams['Ctrl'][0]->team->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>พนักงานเขียนกระสอบ</strong><br>{{$teams['WRITE'][0]->team->name ?? '-'}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ข้อมูลตู้') }}</b></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><strong>เวลาเปิดตู้</strong> : {{ \Carbon\Carbon::parse($loadcarrym->open_box)->format('Y-m-d H:i') ?? '-'  }}</div>
                            <div class="col-md-3"><strong>เวลาปิดตู้</strong> : {{ \Carbon\Carbon::parse($loadcarrym->close_box)->format('Y-m-d H:i') ?? '-'  }}</div>
                            <div class="col-md-3"><strong>อุณหภูมิเปิดตู้</strong> : {{$loadcarrym->open_temperature ?? '-' }}</div>
                            <div class="col-md-3"><strong>อุณหภูมิสินค้า พาเลทแรก</strong> : {{$loadcarrym->first_pallet_temperature ?? '-' }}</div>
                            <div class="col-md-3"><strong>อุณหภูมิปล่อยตู้</strong> : {{$loadcarrym->delivery_temperature ?? '-' }}</div>
                            <div class="col-md-3"><strong>อุณหภูมิสินค้า พาเลทกลางตู้</strong> : {{$loadcarrym->middle_pallet_temperature ?? '-'}}</div>
                            <div class="col-md-3"><strong>อุณหภูมิตู้ ขณะปิดตู้ เสียบปลั๊ก</strong> : {{$loadcarrym->boc_close_temperature ?? '-' }}</div>
                            <div class="col-md-3"><strong>อุณหภูมิสินค้า พาเลทสุดท้าย</strong> : {{$loadcarrym->last_pallet_temperature ?? '-' }}</div>
                            <div class="col-md-3"><strong>อุณหภูมิตู้ ขณะปล่อยตู้</strong> : {{$loadcarrym->delivery_temperature ?? '-' }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ข้อมูลรูป 1') }}</b></div>
                        </div>
                        <div class="row">
                            @foreach ($picset1 as $key => $value)
                                @php
                                    $load_pic = 'loading_img'.$key;
                                    $pic_path = 'loading_img_path'.$key;
                                @endphp
                                <div class="col-md-3 border border-dark">
                                    @if (!empty($loadcarrym->$pic_path))


                                    <a href="{{ url($loadcarrym->$pic_path) }}" target="_blank">
                                        <img height="20px"
                                                    src="{{ url($loadcarrym->$pic_path) }}"></a>
                                    @endif
                                        <br/>
                                        <strong>{{ $key.'. '.$value }}</strong>
                                    </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ข้อมูลรูป 2') }}</b></div>
                        </div>
                        <div class="row">
                            @foreach ($picset2 as $key => $value)
                                @php
                                    $diffnum = 7;
                                    $load_pic = 'loading_img'.$key;
                                    $pic_path = 'loading_img_path'.$key;
                                @endphp
                                <div class="col-md-3 border border-dark">
                                    @if (!empty($loadcarrym->$pic_path))


                                    <a href="{{ url($loadcarrym->$pic_path) }}" target="_blank">
                                        <img height="20px"
                                                    src="{{ url($loadcarrym->$pic_path) }}"></a>
                                    @endif
                                        <br/>
                                        <strong>{{ ($key-$diffnum).'. '.$value }}</strong>
                                    </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ข้อมูลรูป 3') }}</b></div>
                        </div>
                        <div class="row">
                            @foreach ($picset3 as $key => $value)
                                @php
                                    $diffnum = 13;
                                    $load_pic = 'loading_img'.$key;
                                    $pic_path = 'loading_img_path'.$key;
                                @endphp
                                <div class="col-md-3 border border-dark">
                                    @if (!empty($loadcarrym->$pic_path))


                                    <a href="{{ url($loadcarrym->$pic_path) }}" target="_blank">
                                        <img height="20px"
                                                    src="{{ url($loadcarrym->$pic_path) }}"></a>
                                    @endif
                                        <br/>
                                        <strong>{{ ($key-$diffnum).'. '.$value }}</strong>
                                    </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ข้อมูลรูป 4') }}</b></div>
                        </div>
                        <div class="row">
                            @foreach ($picset4 as $key => $value)
                                @php
                                    $diffnum = 19;
                                    $load_pic = 'loading_img'.$key;
                                    $pic_path = 'loading_img_path'.$key;
                                @endphp
                                <div class="col-md-3 border border-dark">
                                    @if (!empty($loadcarrym->$pic_path))


                                    <a href="{{ url($loadcarrym->$pic_path) }}" target="_blank">
                                        <img class="img-thumbnail"
                                                    src="{{ url($loadcarrym->$pic_path) }}"></a>
                                    @endif
                                        <br/>
                                        <strong>{{ ($key-$diffnum).'. '.$value }}</strong>
                                    </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12"><b>{{ __('ข้อมูล File อื่นๆ') }}</b></div>
                        </div>
                        <div class="row">
                            @foreach ($fileset1 as $key => $value)
                                @php
                                    $diffnum = 23;
                                    $load_pic = 'loading_img'.$key;
                                    $pic_path = 'loading_img_path'.$key;
                                @endphp
                                <div class="col-md-3 border border-dark">
                                    @if (!empty($loadcarrym->$pic_path))


                                    <a href="{{ url($loadcarrym->$pic_path) }}" target="_blank">{{ url($loadcarrym->$load_pic) }}</a>
                                    @endif
                                        <br/>
                                        <strong>{{ ($key-$diffnum).'. '.$value }}</strong>
                                    </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    $('.track_type_change').on('change',(event)=>{
        let typedata = event.target.value;
        if(typedata == "CONVOY" ){
            $("#truck_license_plate").prop('readonly', false);
        }else{
            $("#truck_license_plate").prop('readonly', true);
        }
    });

    $('.product_change').on('change',(event)=>{
        let id = event.target.value;
        $.get( "/products/getbyid/"+id, function( data ) {
            $( "#weight_per_product" ).val( data.weight );
            calwg();
        });


    },);

    $('.calweight').on('change',()=>{
        calwg();

    });

    const calwg = () => {
        console.log("calwg");
        let num_of_product = $('#num_of_product').val();
        let weight_per_product = $('#weight_per_product').val();

        if(num_of_product > 0 && weight_per_product > 0){
            $('#total_weight').val(num_of_product*weight_per_product);
        }else{
            $('#total_weight').val(0);
        }
    };

</script>
@endsection
