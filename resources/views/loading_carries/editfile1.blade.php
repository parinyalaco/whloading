@extends('layouts.main')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load ฝาก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loading_carries.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แผน Load ฝาก File แนบ #').$loadcarrym->id }}</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadcarrym->load_date}}</div>
                            <div class="col-md-3"><strong>คลังนอก</strong> : {{$loadcarrym->extwh->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Product</strong> : {{$loadcarrym->product->name ?? '-' }}</div>
                            <div class="col-md-3"><strong>น้ำหนักสินค้านำฝาก (kg)</strong> : {{$loadcarrym->total_weight}}</div>
                        </div>
                        <form action="{{ route('loading_carries.updatefile1', $loadcarrym->id) }}" method="POST"  enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                            @foreach($pics as $key => $value)
                                    @php
                                        $remapid = 23;
                                        $load_pic = 'loading_img'.$key;
                                        $pic_path = 'loading_img_path'.$key;
                                        $pic_del = 'remove_path'.$key;
                                    @endphp
                                    <label for={{ $load_pic }}
                                        class="col-md-2 col-form-label text-md-right">{{ ($key-$remapid).'. '.$value }}</label>

                                    <div class="col-md-4">
                                        {!! Form::file($load_pic) !!}
                                        @if (!empty($loadcarrym->$pic_path))
                                            <a href="{{ url($loadcarrym->$pic_path) }}" target="_blank">{{ $loadcarrym->$load_pic }}</a>
                                            {!! Form::checkbox($pic_del, 'remove', false) !!} remove
                                        @endif

                                        @error($load_pic)
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    $('.track_type_change').on('change',(event)=>{
        let typedata = event.target.value;
        if(typedata == "CONVOY" ){
            $("#truck_license_plate").prop('readonly', false);
        }else{
            $("#truck_license_plate").prop('readonly', true);
        }
    });

    $('.product_change').on('change',(event)=>{
        let id = event.target.value;
        $.get( "/products/getbyid/"+id, function( data ) {
            $( "#weight_per_product" ).val( data.weight );
            calwg();
        });


    },);

    $('.calweight').on('change',()=>{
        calwg();

    });

    const calwg = () => {
        console.log("calwg");
        let num_of_product = $('#num_of_product').val();
        let weight_per_product = $('#weight_per_product').val();

        if(num_of_product > 0 && weight_per_product > 0){
            $('#total_weight').val(num_of_product*weight_per_product);
        }else{
            $('#total_weight').val(0);
        }
    };

</script>
@endsection
