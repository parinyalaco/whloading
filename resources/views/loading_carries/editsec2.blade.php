@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load ฝาก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loading_carries.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แผน Load ฝาก ส่วนแรก #').$loadcarrym->id }} สถานะ: {{ $loadcarrym->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadcarrym->load_date}}</div>
                            <div class="col-md-3"><strong>คลังนอก</strong> : {{$loadcarrym->extwh->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Product</strong> : {{$loadcarrym->product->name ?? '-' }}</div>
                            <div class="col-md-3"><strong>น้ำหนักสินค้านำฝาก (kg)</strong> : {{$loadcarrym->total_weight}}</div>
                        </div>
                        <form method="POST" action="{{ route('loading_carries.updatesec2', $loadcarrym->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="teams_front_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานหน้าตู้ 1') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Front'][0]))
                                        {{ Form::select('teams_front_1', $teamlistType['Front'], $teams['Front'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_front_1', $teamlistType['Front'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_front_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_back_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานท้ายตู้ 1') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Back'][0]))
                                        {{ Form::select('teams_back_1', $teamlistType['Back'], $teams['Back'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_back_1', $teamlistType['Back'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_back_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_front_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานหน้าตู้ 2') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Front'][1]))
                                        {{ Form::select('teams_front_2', $teamlistType['Front'], $teams['Front'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_front_2', $teamlistType['Front'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_front_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_back_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานท้ายตู้ 2') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Back'][1]))
                                        {{ Form::select('teams_back_2', $teamlistType['Back'], $teams['Back'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_back_2', $teamlistType['Back'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_back_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="teams_fl_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงาน FL') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['FL'][0]))
                                        {{ Form::select('teams_fl_1', $teamlistType['FL'], $teams['FL'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_fl_1', $teamlistType['FL'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_fl_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_ctrl_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานคุมโหลด') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Ctrl'][0]))
                                        {{ Form::select('teams_ctrl_1', $teamlistType['Ctrl'], $teams['Ctrl'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_ctrl_1', $teamlistType['Ctrl'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_ctrl_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_write_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานเขียนกระสอบ') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['WRITE'][0]))
                                        {{ Form::select('teams_write_1', $teamlistType['WRITE'], $teams['WRITE'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_write_1', $teamlistType['WRITE'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_write_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
