@extends('layouts.main')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load ฝาก') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    @if (Auth::user()->group->name == trim('Admin') ||
                            Auth::user()->group->name == trim('Superadmin') ||
                            Auth::user()->group->name == trim('Wh') ||
                            Auth::user()->group->name == trim('wh_sup'))
                        <div class="card-header">
                            <a class="btn btn-success" href="{{ route('loading_carries.create') }}"> สร้างแผน Load
                                ฝาก</a><br /><br />
                            <a class="btn @if (Request::get('status') == '') btn-warning @else btn-primary @endif"
                                href="{{ route('loading_carries.index') }}"> All </a>
                            <a class="btn @if (Request::get('status') == 'New') btn-warning @else btn-primary @endif"
                                href="{{ route('loading_carries.index', ['status' => 'New']) }}"> New
                            </a>
                            <a class="btn @if (Request::get('status') == 'Loading') btn-warning @else btn-primary @endif"
                                href="{{ route('loading_carries.index', ['status' => 'Loading']) }}"> Loading
                            </a>
                            <a class="btn @if (Request::get('status') == 'Loaded') btn-warning @else btn-primary @endif"
                                href="{{ route('loading_carries.index', ['status' => 'Loaded']) }}"> Loaded
                            </a>
                        </div>
                    @endif
                    <form method="GET" action="{{ url('/loading_carries/?status=' . $status) }}" accept-charset="UTF-8"
                        class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"
                                placeholder="ค้นหาจาก Order / ลูกค้า / สินค้า" value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body">

                        <table class="table table-bordered">
                            <tr>
                                @if (Auth::user()->group->name == trim('Admin') ||
                                        Auth::user()->group->name == trim('Superadmin') ||
                                        Auth::user()->username == trim('PIT') ||
                                        Auth::user()->username == trim('JJT'))
                                    <th>
                                        <a onclick="Myfunction()">
                                            <i class="bi bi-envelope"></i>
                                        </a>
                                    </th>
                                @endif
                                <th>Date</th>
                                <th>คลังนอก</th>
                                <th>Product</th>
                                <th>น้ำหนักสินค้านำฝาก (kg)</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            @foreach ($loadcarryms as $loadingcarrym)
                                <tr>
                                    @if (Auth::user()->group->name == trim('Admin') ||
                                            Auth::user()->group->name == trim('Superadmin') ||
                                            Auth::user()->username == trim('PIT') ||
                                            Auth::user()->username == trim('JJT'))
                                        <td>
                                            @if ($loadingcarrym->status == 'Loaded')
                                                <input type="checkbox" value="{{ $loadingcarrym->id }}"
                                                    id="chk_print[{{ $loadingcarrym->id }}]"
                                                    name="chk_print[{{ $loadingcarrym->id }}]">
                                            @endif
                                        </td>
                                    @endif
                                    <td>{{ $loadingcarrym->load_date }}</td>
                                    <td>{{ $loadingcarrym->extwh->name }}</td>
                                    <td>{{ $loadingcarrym->product->name }}</td>
                                    <td>{{ $loadingcarrym->total_weight }}</td>
                                    <td>{{ $loadingcarrym->status }}</td>
                                    <td>
                                        <a class="btn btn-primary mt-1"
                                            href="{{ route('loading_carries.show', $loadingcarrym->id) }}">ตรวจสอบข้อมูล</a><br />
                                        <a class="btn btn-warning mt-1"
                                            href="{{ route('loading_carries.edit', $loadingcarrym->id) }}">ข้อมูลส่วน 1</a>
                                        <a class="btn btn-warning mt-1"
                                            href="{{ route('loading_carries.editsec2', $loadingcarrym->id) }}">ข้อมูลส่วน 2</a>
                                        <a class="btn btn-warning mt-1"
                                            href="{{ route('loading_carries.editsec3', $loadingcarrym->id) }}">ข้อมูลส่วน 3</a><br />
                                        <a class="btn btn-primary mt-1"
                                            href="{{ route('loading_carries.editpic1', $loadingcarrym->id) }}">รูปภาพอุณหภูมิ</a>
                                            <a class="btn btn-primary mt-1"
                                            href="{{ route('loading_carries.editpic2', $loadingcarrym->id) }}">รูปภาพตู้ภายนอก</a>
                                        <a class="btn btn-primary mt-1"
                                            href="{{ route('loading_carries.editpic3', $loadingcarrym->id) }}">รูปภาพตู้ภายใน</a><br />
                                        <a class="btn btn-primary mt-1"
                                            href="{{ route('loading_carries.editpic4', $loadingcarrym->id) }}">รูปภาพอื่นๆ</a>
                                         <a class="btn btn-primary mt-1"
                                            href="{{ route('loading_carries.editfile1', $loadingcarrym->id) }}">ข้อมูล Files แนบ</a>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('loadings.load_reason') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea id="reason" class="form-control col" name="reason" required></textarea>
                                            <input type="hidden" id="loadm_id" class="form-control col" name="loadm_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="pagination-wrapper"> {!! $loadcarryms->appends(['status' => Request::get('status'), 'search' => Request::get('search')])->render() !!} </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <script type="text/javascript" src="{{ asset('assets/bootstrap/js/jquery-1.7.1.min.js') }}"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function Myfunction() {
            // var obj = "";
            // const values = Array
            // .from(document.querySelectorAll('input[type="checkbox"]'))
            // .filter((checkbox) => checkbox.checked)
            // .map((checkbox) => checkbox.value);

            // let mess='คุณต้องการส่งเมลล์ โดยมีใบโหลด \n';
            // for (const [key, value] of Object.entries(values)) {
            //     mess += ' Date:'+obj[value].load_date+'  Customer:'+obj[value].customer+'  Order :'+obj[value].order_no+'\n';
            // }
            // mess += 'ใช่หรือไม่';
            // console.log(values.length);
            // if(values.length>0){
            //     if (confirm(mess) == true) {
            //         var link_url = window.location.origin+window.location.pathname+"/sent_mail?load_m_id="+values;
            //         // var link_url = window.location.origin+"/whloading/loadings/sent_mail?load_m_id="+values;
            //         console.log(link_url);

            //         window.location.replace(link_url);
            //     }
            // }
        }
    </script>
    <script type="text/javascript">
        function show_input(num, order) {
            console.log(num + '--' + order);
            document.getElementById('loadm_id').value = num;
            let box = document.getElementById('test');
            box.innerText = 'กรุณาระบุเหตุผลที่ต้องการคืนค่า ใบ order ที่ ' + order;
        }
    </script>
@endsection
