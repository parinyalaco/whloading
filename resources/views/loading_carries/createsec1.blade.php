@extends('layouts.main')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load ฝาก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loading_carries.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้างแผน Load ฝาก') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('loading_carries.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="load_date" class="col-md-2 col-form-label text-md-right">{{ __('Loading Dae') }}</label>

                                <div class="col-md-4">
                                    <input id="load_date" type="date" class="form-control @error('load_date') is-invalid @enderror"
                                        name="load_date" value="{{ old('load_date') }}" required >

                                    @error('load_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="product_id" class="col-md-2 col-form-label text-md-right">{{ __('สินค้านำฝาก') }}</label>

                                <div class="col-md-4">

                                        {{ Form::select('product_id', $productlist , null, ['placeholder' => '==เลือก==', 'class' => 'form-control product_change']) }}

                                    @error('product_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ext_wh_id" class="col-md-2 col-form-label text-md-right">{{ __('คลังนอก (คลังเช่า)') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('ext_wh_id', $exWhlist , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('ext_wh_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="crop_id" class="col-md-2 col-form-label text-md-right">{{ __('Crop') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('crop_id', $croplist , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('crop_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="track_type_id" class="col-md-2 col-form-label text-md-right">{{ __('ประเภทขนส่ง') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('track_type', $trackTypelist , null, ['placeholder' => '==เลือก==', 'class' => 'form-control track_type_change']) }}

                                    @error('track_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="usage" class="col-md-2 col-form-label text-md-right">{{ __('Usage') }}</label>

                                <div class="col-md-4">
                                     {{ Form::select('usage', $usagelist , null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('usage')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                                <label for="truck_license_plate" class="col-md-2 col-form-label text-md-right">{{ __('ทะเบียนหัวลาก') }}</label>

                                <div class="col-md-4">
                                    <input id="truck_license_plate" type="text" class="form-control @error('truck_license_plate') is-invalid @enderror"
                                        name="truck_license_plate" value="{{ old('truck_license_plate') }}">

                                    @error('truck_license_plate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="num_of_product" class="col-md-2 col-form-label text-md-right">{{ __('ปริมาณสินค้านำฝาก') }}</label>

                                <div class="col-md-4">
                                    <input id="num_of_product" type="number" class="form-control calweight @error('num_of_product') is-invalid @enderror"
                                        name="num_of_product" value="{{ old('num_of_product') }}">

                                    @error('num_of_product')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                                <label for="convoy_license_plate" class="col-md-2 col-form-label text-md-right">{{ __('ทะเบียนหาง') }}</label>

                                <div class="col-md-4">
                                    <input id="convoy_license_plate" type="text" class="form-control  @error('convoy_license_plate') is-invalid @enderror"
                                        name="convoy_license_plate" value="{{ old('convoy_license_plate') }}">

                                    @error('convoy_license_plate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="weight_per_product" class="col-md-2 col-form-label text-md-right">{{ __('น้ำหนักสินค้านำฝาก') }}</label>

                                <div class="col-md-4">
                                    <input id="weight_per_product" type="number" class="form-control calweight @error('weight_per_product') is-invalid @enderror"
                                        name="weight_per_product" value="{{ old('weight_per_product') }}">

                                    @error('weight_per_product')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <input id="total_weight" type="number" class="form-control @error('total_weight') is-invalid @enderror"
                                        name="total_weight" value="{{ old('total_weight') }}">

                                    @error('total_weight')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    $('.track_type_change').on('change',(event)=>{
        let typedata = event.target.value;
        if(typedata == "CONVOY" ){
            $("#truck_license_plate").prop('readonly', false);
        }else{
            $("#truck_license_plate").prop('readonly', true);
        }
    });

    $('.product_change').on('change',(event)=>{
        let id = event.target.value;
        $.get( "/products/getbyid/"+id, function( data ) {
            $( "#weight_per_product" ).val( data.weight );
            calwg();
        });


    },);

    $('.calweight').on('change',()=>{
        calwg();

    });

    const calwg = () => {
        console.log("calwg");
        let num_of_product = $('#num_of_product').val();
        let weight_per_product = $('#weight_per_product').val();

        if(num_of_product > 0 && weight_per_product > 0){
            $('#total_weight').val(num_of_product*weight_per_product);
        }else{
            $('#total_weight').val(0);
        }
    };

</script>
@endsection
