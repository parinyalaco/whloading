@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการประเภทกล่องแตก') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('broken-types.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แสดงประเภทกล่องแตก') }}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <td>{{ $brokentype->name }}</td>
                            </tr>
                            <tr>
                                <th>รายละเอียด</th>
                                <td>{{ $brokentype->desc }}</td>
                            </tr>
                            <tr>
                                <th>ลำดับ</th>
                                <td>{{ $brokentype->seq }}</td>
                            </tr>
                            <tr>
                                <th>Action</th>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('broken-types.edit', $brokentype->id) }}">แก้ไข</a>
                                    <form action="{{ route('broken-types.destroy', $brokentype->id) }}" method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">ลบ</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
