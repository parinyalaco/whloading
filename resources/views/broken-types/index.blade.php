@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการประเภทกล่องแตก') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-success" href="{{ route('broken-types.create') }}">สร้างประเภทกล่องแตกใหม่</a>
                    </div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>ชื่อ</th>
                                <th>ลำดับ</th>
                                <th></th>
                            </tr>
                            @foreach ($brokentypes as $brokentype)
                                <tr>
                                    <td>{{ $brokentype->name }}</td>
                                    <td>{{ $brokentype->seq }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('broken-types.show', $brokentype->id) }}">แสดง</a>
                                        <a class="btn btn-primary" href="{{ route('broken-types.edit', $brokentype->id) }}">แก้ไข</a>
                                        <form action="{{ route('broken-types.destroy', $brokentype->id) }}" method="POST">

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
