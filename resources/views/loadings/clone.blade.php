@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }} สถานะ: {{ $loadm->status }} </div>
                    <div class="card-body">
                        <div class="row">
                            <form method="POST" action="{{ route('loadings.cloneAction', $loadm->id) }}" name="clonefrm" id="clonefrm">
                                @csrf
                                <input type="hidden" name="key" id="key" value="{{ Session::getId() }}" />
                                <input type="hidden" name="userid" id="userid" value="{{ Auth::user()->id }}" />
                                <input type="hidden" name="load_id" id="load_id" value="{{ $loadm->id }}" />
                                <div class="col-md-3">
                                    <label for="clonename">Clone Name </label>
                                    <input type="text" class="form-control" name="clonename" id="clonename" />
                                </div>
                                <div class="col-md-6">
                                    <label for="note">Note</label>
                                    <input type="text" class="form-control" name="note" id="note" />
                                </div>
                                <div class="col-md-3">
                                    <button id="clonebtn" type="submit" class="btn btn-primary">Clone</button>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{ $loadm->load_date }}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{ $loadm->customertype->name ?? '-' }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : {{ $loadm->customer }}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{ $loadm->order_no }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>วันที่ Load จริง</strong> : {{ $loadm->act_date }}</div>
                            <div class="col-md-4"><strong>หัวลาก</strong> : {{ $loadm->owner_type }}</div>
                            <div class="col-md-4"><strong>ทะเบียนหน้า</strong> : {{ $loadm->truck_license_plate }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>ทะเบียนหลัง</strong> : {{ $loadm->convoy_license_plate }}</div>
                            <div class="col-md-4"><strong>หมายเลขตู้</strong> : {{ $loadm->convoy_no }}</div>
                            <div class="col-md-4"><strong>หมายเลข Seal</strong> : {{ $loadm->seal_no }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>พนักงานหน้าตู้</strong> :
                                @if (isset($teams['Front']))
                                    @foreach ($teams['Front'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานหลังตู้</strong> :
                                @if (isset($teams['Back']))
                                    @foreach ($teams['Back'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงาน FL</strong> :
                                @if (isset($teams['FL']))
                                    @foreach ($teams['FL'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานคุมตู้</strong> :
                                @if (isset($teams['Ctrl']))
                                    @foreach ($teams['Ctrl'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานตรวจเช็คสินค้าก่อนโหลด</strong> :
                                @if (isset($teams['Bfload']))
                                    @foreach ($teams['Bfload'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานตรวจเช็คกล่องแตก</strong> :
                                @if (isset($teams['Bkbox']))
                                    @foreach ($teams['Bkbox'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>วันเวลา เปิดตู้</strong> : {{ date('Y-m-d H:i', strtotime($loadm->open_box)) }}
                            </div>
                            <div class="col-md-4">
                                <strong>วันเวลา ปิดตู้</strong> : {{ date('Y-m-d H:i', strtotime($loadm->close_box)) }}
                            </div>
                            @php
                                $dateTimeObject1 = date_create($loadm->open_box);
                                // $dateTimeObject1 = date_create('2022-07-19 20:52:00');
                                $dateTimeObject2 = date_create($loadm->close_box);

                                $difference = date_diff($dateTimeObject1, $dateTimeObject2);
                                // print_r($difference);
                            @endphp
                            <div class="col-md-4 @if ($dateTimeObject1 > $dateTimeObject2) text-danger @endif">
                                <strong>ใช้เวลารวม</strong> : @if ($dateTimeObject1 > $dateTimeObject2)
                                    -
                                @endif
                                {{ date('H:i', strtotime($difference->h . ':' . $difference->i)) }}
                                ชั่วโมง
                                {{-- <strong>ใช้เวลารวม</strong> : {{ $diff_time }} นาที --}}
                            </div>
                        </div>
                        @if (!empty($loadm->reason_box))
                            <div class="row" style="border:1px solid red;">
                                <div class="col-md-12">
                                    <strong>หมายเหตุ</strong> :
                                </div>
                                <div class="col-md-12 text-danger">
                                    {{ $loadm->reason_box }}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <strong>สายพานที่ใช้ในการโหลด</strong> : @if (!empty($loadm->conveyor_id))
                                    {{ $conveyor[$loadm->conveyor_id] }}@else-
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>ปิดการ Load โดย</strong> : @if (!empty($loadm->user_close))
                                    {{ $loadm->user->name }}@else-
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> <strong>กล่องแตก </strong> : จำนวน
                                @if ($loadm->broken_box > 0)
                                    {{ $loadm->broken_box }}
                                @else
                                    -
                                @endif
                                กล่อง
                                <strong>รายละเอียด : </strong> {{ $loadm->broken_box_case }}
                            </div>

                            @foreach ($loadm->loadbrokens as $loadbrokenObj)
                                <div class="col-md-4">
                                    <strong>{{ $loadbrokenObj->brokentype->name }}</strong> : {{ $loadbrokenObj->box }}
                                    กล่อง
                                </div>
                            @endforeach

                        </div>
                        <div class="row">
                            @foreach ($pics as $runkey => $pic)
                                @php
                                    $picparamname = 'loading_img_path' . $runkey;
                                @endphp
                                @if (!empty($loadm->$picparamname))
                                    <div class="col-md-3">
                                        {{-- @php
                                            $picparamname = 'loading_img_path'.$runkey
                                        @endphp
                                        @if (!empty($loadm->$picparamname)) --}}
                                        <a href="{{ url($loadm->$picparamname) }}" target="_blank">
                                            <img width="100px" src="{{ url($loadm->$picparamname) }}">
                                        </a>
                                        {{-- @else
                                            -
                                        @endif --}}
                                        <br />{{ $pic }}
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="6" class="w-25 text-center">แถวแนวขวาง</th>
                                    <th colspan="6" class="w-25 text-center">แถวแนวนอน</th>
                                    <th rowspan="2" class="text-center">รวม</th>
                                    <th rowspan="2" class="text-center">ยอดรวม</th>
                                    <th rowspan="2" class="text-center">Note</th>
                                </tr>
                                <tr>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sumtotal = 0;
                                @endphp
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                                    <tr>
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productt->name ?? '' }}</td>
                                        <td>{{ $item->t_base }}</td>
                                        <td>{{ $item->t_height }}</td>
                                        <td>@php
                                            $texcess = 0;
                                            if ($item->t_excess > 0) {
                                                $texcess += $item->t_excess;
                                            }
                                            if ($item->t_excess1 > 0) {
                                                $texcess += $item->t_excess1;
                                            }
                                            if ($item->t_excess2 > 0) {
                                                $texcess += $item->t_excess2;
                                            }
                                        @endphp
                                            {{ $texcess }}
                                        </td>
                                        <td>{{ $item->t_total }}</td>
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productl->name ?? '' }}</td>
                                        <td>{{ $item->l_base }}</td>
                                        <td>{{ $item->l_height }}</td>
                                        <td>@php
                                            $lexcess = 0;
                                            if ($item->l_excess > 0) {
                                                $lexcess += $item->l_excess;
                                            }
                                            if ($item->l_excess1 > 0) {
                                                $lexcess += $item->l_excess1;
                                            }
                                            if ($item->t_excess2 > 0) {
                                                $lexcess += $item->l_excess2;
                                            }
                                        @endphp
                                            {{ $lexcess }}
                                        </td>
                                        <td>{{ $item->l_total }}</td>
                                        <td>{{ $item->all_total }}</td>
                                        <td>
                                            @php
                                                $sumtotal += $item->all_total;
                                            @endphp
                                            {{ $sumtotal }}</td>
                                        <td>{{ $item->note }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
