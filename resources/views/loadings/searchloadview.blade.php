@extends('layouts.canvas')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
            <a class="btn btn-secondary"
                                            href="{{ route('loadings.exportLoadingData', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> PDF</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{ $loadm->load_date }}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{ $loadm->customertype->name ?? '-' }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : {{ $loadm->customer }}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{ $loadm->order_no }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>วันที่ Load จริง</strong> : {{ $loadm->act_date }}</div>
                            <div class="col-md-4"><strong>หัวลาก</strong> : {{ $loadm->owner_type }}</div>
                            <div class="col-md-4"><strong>ทะเบียนหน้า</strong> : {{ $loadm->truck_license_plate }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>ทะเบียนหลัง</strong> : {{ $loadm->convoy_license_plate }}</div>
                            <div class="col-md-4"><strong>หมายเลขตู้</strong> : {{ $loadm->convoy_no }}</div>
                            <div class="col-md-4"><strong>หมายเลข Seal</strong> : {{ $loadm->seal_no }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>ประเภทตู้</strong> : {{ $loadm->containertype->name ?? '-' }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>พนักงานหน้าตู้</strong> :
                                @if (isset($teams['Front']))
                                    @foreach ($teams['Front'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานหลังตู้</strong>
                                @if (isset($teams['Back']))
                                    @foreach ($teams['Back'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงาน FL</strong>
                                @if (isset($teams['FL']))
                                    @foreach ($teams['FL'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานคุมตู้</strong>
                                @if (isset($teams['Ctrl']))
                                    @foreach ($teams['Ctrl'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>

                            <div class="col-md-4"><strong>พนักงานตรวจเช็คสินค้าก่อนโหลด</strong>
                                @if (isset($teams['Bfload']))
                                    @foreach ($teams['Bfload'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานตรวจเช็คกล่องแตก</strong>
                                @if (isset($teams['Bkbox']))
                                    @foreach ($teams['Bkbox'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>วันเวลา เปิดตู้</strong> : {{ date('Y-m-d H:i', strtotime($loadm->open_box)) }}
                            </div>
                            <div class="col-md-4">
                                <strong>วันเวลา ปิดตู้</strong> : {{ date('Y-m-d H:i', strtotime($loadm->close_box)) }}
                            </div>
                            @php
                                $dateTimeObject1 = date_create($loadm->open_box);
                                $dateTimeObject2 = date_create($loadm->close_box);

                                $difference = date_diff($dateTimeObject1, $dateTimeObject2);
                            @endphp
                            <div class="col-md-4 @if ($dateTimeObject1 > $dateTimeObject2) text-danger @endif">
                                <strong>ใช้เวลารวม</strong> : @if ($dateTimeObject1 > $dateTimeObject2)
                                    -
                                @endif
                                {{ date('H:i', strtotime($difference->h . ':' . $difference->i)) }}
                                ชั่วโมง
                            </div>
                        </div>
                        @if (!empty($loadm->reason_box))
                            <div class="row" style="border:1px solid red;">
                                <div class="col-md-12">
                                    <strong>หมายเหตุ</strong> :
                                </div>
                                <div class="col-md-12 text-danger">
                                    {{ $loadm->reason_box }}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <strong>สายพานที่ใช้ในการโหลด</strong> : @if (!empty($loadm->conveyor_id))
                                    {{ $conveyor[$loadm->conveyor_id] }}@else-
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>ปิดการ Load โดย</strong> : @if (!empty($loadm->user_close))
                                    {{ $loadm->user->name }}@else-
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>กล่องแตก </strong> : จำนวน
                                @if ($loadm->broken_box > 0)
                                    {{ $loadm->broken_box }}
                                @else
                                    -
                                @endif
                                กล่อง <strong>รายละเอียด : </strong> {{ $loadm->broken_box_case }}
                            </div>

                            @foreach ($loadm->loadbrokens as $loadbrokenObj)
                                <div class="col-md-4">
                                    <strong>{{ $loadbrokenObj->brokentype->name }}</strong> : {{ $loadbrokenObj->box }}
                                    กล่อง
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            <div class="table-responsive-lg">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Loadin No</th>
                                            <th class="text-center">Product</th>
                                            <th class="text-center">No. To No.</th>
                                            <th class="text-center">Quantity Carton</th>
                                            <th class="text-center">Exp. Date</th>
                                            <th class="text-center">Stamping / LACO Batch</th>
                                            <th class="text-center">Remark</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($loadm->loadlots as $loadlot)
                                            <tr>
                                                <td class="text-center">No.{{ $loadlot->seq }}</td>
                                                <td class="text-center">{{ $loadlot->product->name }}</td>
                                                <td class="text-center">{{ $loadlot->start_no }} - {{ $loadlot->end_no }}
                                                </td>
                                                <td class="text-center">{{ $loadlot->quantity }}</td>
                                                <td class="text-center">{{ date('F d Y', strtotime($loadlot->exp_date)) }}
                                                </td>
                                                <td class="text-center">{{ date('Y.m.d', strtotime($loadlot->best_date)) }}
                                                    {{ $loadlot->lot }}</td>
                                                <td class="text-center">{{ $loadlot->remark }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            @foreach ($pics as $runkey => $pic)
                                @php
                                    $picparamname = 'loading_img_path' . $runkey;
                                @endphp
                                @if (!empty($loadm->$picparamname))
                                    <div class="col-md-3">
                                        {{-- @php
                                            $picparamname = 'loading_img_path'.$runkey
                                        @endphp
                                        @if (!empty($loadm->$picparamname)) --}}
                                        <a href="{{ url($loadm->$picparamname) }}" target="_blank">
                                            <img width="100px" src="{{ url($loadm->$picparamname) }}">
                                        </a>
                                        {{-- @else
                                            -
                                        @endif --}}
                                        <br />{{ $pic }}
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="table-responsive-lg">
                            <canvas id="my-house" width="600" height="{{ $loadm->loadds()->count() * 42 }}"></canvas>
                        </div>
                        @if ($loadm->type_loading == '3Rows')
                            @include('elements.entersection13rows')
                        @else
                            @include('elements.entersection1normal')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        @if ($loadm->type_loading == '3Rows')


            const canvas = document.getElementById('my-house');
            const ctx = canvas.getContext('2d');

            // Set line width
            ctx.lineWidth = 1;

            // box
            @php
                $maxx = 0;
                $maxy = 0;
            @endphp

            @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                @php
                    $lastx = 0;
                @endphp
                ctx.beginPath();
                ctx.fillStyle = "black";
                ctx.fillText('{{ $item->row }}', 5, {{ $item->row * 35 + 15 }});
                ctx.strokeStyle = "black";
                ctx.closePath();
                @if ($item->t_base > 0)
                    ctx.beginPath();
                    @for ($i = 1; $i <= $item->t_base; $i++)
                        ctx.strokeRect({{ $i * 25 }}, {{ $item->row * 35 }}, 20, 30);
                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->t_height }}', {{ $i * 25 + 5 }}, {{ $item->row * 35 + 15 }});

                        @php
                            $lastx = $i * 25;
                            if ($i * 25 > $maxx) {
                                $maxx = $i * 25;
                            }
                            if ($item->row * 35 > $maxy) {
                                $maxy = $item->row * 35;
                            }
                        @endphp
                    @endfor
                    ctx.strokeStyle = "black";
                    ctx.closePath();
                @endif



                @if ($item->t_excess > 0 || $item->t_excess1 > 0 || $item->t_excess2 > 0)
                    ctx.beginPath();
                    ctx.strokeStyle = "red";
                    ctx.strokeRect({{ $lastx + 25 }}, {{ $item->row * 35 }}, 20, 30);
                    ctx.fillStyle = "red";
                    @php
                        $texcesssum = 0;
                        if ($item->t_excess > 0) {
                            $texcesssum += $item->t_excess;
                        }
                        if ($item->t_excess1 > 0) {
                            $texcesssum += $item->t_excess1;
                        }
                        if ($item->t_excess2 > 0) {
                            $texcesssum += $item->t_excess2;
                        }
                    @endphp
                    ctx.fillText('{{ $texcesssum }}', {{ $lastx + 30 }}, {{ $item->row * 35 + 15 }});

                    ctx.closePath();
                    @php
                        $lastx = $lastx + 25;
                    @endphp
                @endif

                @if ($item->c_base > 0)
                    ctx.beginPath();
                    @for ($j = 1; $j <= $item->c_base; $j++)
                        ctx.strokeRect({{ $lastx - 5 + $j * 35 }}, {{ $item->row * 35 }}, 30, 20);
                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->l_height }}', {{ $lastx + $j * 35 + 10 }}, {{ $item->row * 35 + 15 }});

                        @php
                            if ($lastx - 5 + $j * 35 > $maxx) {
                                $maxx = $lastx - 5 + $j * 35;
                            }
                            if ($item->row * 35 > $maxy) {
                                $maxy = $item->row * 35;
                            }
                        @endphp
                    @endfor

                    @php
                        $lastx = $lastx - 5 + $item->c_base * 35;
                    @endphp
                    ctx.strokeStyle = "black";
                    ctx.closePath();
                @endif

                @if ($item->c_excess > 0 || $item->c_excess1 > 0 || $item->c_excess2 > 0)
                    ctx.beginPath();
                    ctx.strokeStyle = "red";
                    ctx.strokeRect({{ $lastx + 35 }}, {{ $item->row * 35 }}, 30, 20);
                    ctx.fillStyle = "red";
                    @php
                        $cexcesssum = 0;
                        if ($item->c_excess > 0) {
                            $cexcesssum += $item->c_excess;
                        }
                        if ($item->t_excess1 > 0) {
                            $cexcesssum += $item->c_excess1;
                        }
                        if ($item->c_excess2 > 0) {
                            $cexcesssum += $item->c_excess2;
                        }
                    @endphp
                    ctx.fillText('{{ $cexcesssum }}', {{ $lastx + 45 }}, {{ $item->row * 35 + 15 }});
                    ctx.closePath();
                    @php
                        $lastx = $lastx + 35;
                    @endphp
                @endif

                @if ($item->l_base > 0)
                    ctx.beginPath();
                    @for ($j = 1; $j <= $item->l_base; $j++)
                        ctx.strokeRect({{ $lastx - 5 + $j * 35 }}, {{ $item->row * 35 }}, 30, 20);
                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->l_height }}', {{ $lastx + $j * 35 + 10 }}, {{ $item->row * 35 + 15 }});

                        @php
                            if ($lastx - 5 + $j * 35 > $maxx) {
                                $maxx = $lastx - 5 + $j * 35;
                            }
                            if ($item->row * 35 > $maxy) {
                                $maxy = $item->row * 35;
                            }
                        @endphp
                    @endfor

                    @php
                        $lastx = $lastx - 5 + $item->l_base * 35;
                    @endphp
                    ctx.strokeStyle = "black";
                    ctx.closePath();
                @endif

                @if ($item->l_excess > 0 || $item->l_excess1 > 0 || $item->l_excess2 > 0)
                    ctx.beginPath();
                    ctx.strokeStyle = "red";
                    ctx.strokeRect({{ $lastx + 35 }}, {{ $item->row * 35 }}, 30, 20);
                    ctx.fillStyle = "red";
                    @php
                        $lexcesssum = 0;
                        if ($item->l_excess > 0) {
                            $lexcesssum += $item->l_excess;
                        }
                        if ($item->t_excess1 > 0) {
                            $lexcesssum += $item->l_excess1;
                        }
                        if ($item->l_excess2 > 0) {
                            $lexcesssum += $item->l_excess2;
                        }
                    @endphp
                    ctx.fillText('{{ $lexcesssum }}', {{ $lastx + 45 }}, {{ $item->row * 35 + 15 }});
                    ctx.closePath();
                    @php
                        $lastx = $lastx + 35;
                    @endphp
                @endif

                ctx.beginPath();
                ctx.strokeStyle = "blue";
                ctx.arc({{ $lastx + 50 }}, {{ $item->row * 35 + 10 }}, 14, 0, 2 * Math.PI);
                ctx.fillStyle = "blue";
                ctx.fillText('{{ $item->all_total }}', {{ $lastx + 45 }}, {{ $item->row * 35 + 15 }});
                ctx.closePath();
                ctx.stroke();
            @endforeach

            ctx.stroke();
        @else
            const canvas = document.getElementById('my-house');
            const ctx = canvas.getContext('2d');

            // Set line width
            ctx.lineWidth = 1;

            // box
            @php
                $maxx = 0;
                $maxy = 0;
            @endphp

            @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                @php
                    $lastx = 0;
                @endphp
                ctx.beginPath();
                ctx.fillStyle = "black";
                ctx.fillText('{{ $item->row }}', 5, {{ $item->row * 35 + 15 }});
                ctx.strokeStyle = "black";
                ctx.closePath();
                @if ($item->t_base > 0)
                    ctx.beginPath();
                    @for ($i = 1; $i <= $item->t_base; $i++)
                        ctx.strokeRect({{ $i * 25 }}, {{ $item->row * 35 }}, 20, 30);
                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->t_height }}', {{ $i * 25 + 5 }}, {{ $item->row * 35 + 15 }});

                        @php
                            $lastx = $i * 25;
                            if ($i * 25 > $maxx) {
                                $maxx = $i * 25;
                            }
                            if ($item->row * 35 > $maxy) {
                                $maxy = $item->row * 35;
                            }
                        @endphp
                    @endfor
                    ctx.strokeStyle = "black";
                    ctx.closePath();
                @endif

                @if ($item->t_excess > 0 || $item->t_excess1 > 0 || $item->t_excess2 > 0)
                    ctx.beginPath();
                    ctx.strokeStyle = "red";
                    ctx.strokeRect({{ $lastx + 25 }}, {{ $item->row * 35 }}, 20, 30);
                    ctx.fillStyle = "red";
                    @php
                        $texcesssum = 0;
                        if ($item->t_excess > 0) {
                            $texcesssum += $item->t_excess;
                        }
                        if ($item->t_excess1 > 0) {
                            $texcesssum += $item->t_excess1;
                        }
                        if ($item->t_excess2 > 0) {
                            $texcesssum += $item->t_excess2;
                        }
                    @endphp
                    ctx.fillText('{{ $texcesssum }}', {{ $lastx + 30 }}, {{ $item->row * 35 + 15 }});

                    ctx.closePath();
                    @php
                        $lastx = $lastx + 25;
                    @endphp
                @endif

                @if ($item->l_base > 0)
                    ctx.beginPath();
                    @for ($j = 1; $j <= $item->l_base; $j++)
                        ctx.strokeRect({{ $lastx - 5 + $j * 35 }}, {{ $item->row * 35 }}, 30, 20);
                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->l_height }}', {{ $lastx + $j * 35 + 10 }}, {{ $item->row * 35 + 15 }});

                        @php
                            if ($lastx - 5 + $j * 35 > $maxx) {
                                $maxx = $lastx - 5 + $j * 35;
                            }
                            if ($item->row * 35 > $maxy) {
                                $maxy = $item->row * 35;
                            }
                        @endphp
                    @endfor

                    @php
                        $lastx = $lastx - 5 + $item->l_base * 35;
                    @endphp
                    ctx.strokeStyle = "black";
                    ctx.closePath();
                @endif

                @if ($item->l_excess > 0 || $item->l_excess1 > 0 || $item->l_excess2 > 0)
                    ctx.beginPath();
                    ctx.strokeStyle = "red";
                    ctx.strokeRect({{ $lastx + 35 }}, {{ $item->row * 35 }}, 30, 20);
                    ctx.fillStyle = "red";
                    @php
                        $lexcesssum = 0;
                        if ($item->l_excess > 0) {
                            $lexcesssum += $item->l_excess;
                        }
                        if ($item->t_excess1 > 0) {
                            $lexcesssum += $item->l_excess1;
                        }
                        if ($item->l_excess2 > 0) {
                            $lexcesssum += $item->l_excess2;
                        }
                    @endphp
                    ctx.fillText('{{ $lexcesssum }}', {{ $lastx + 45 }}, {{ $item->row * 35 + 15 }});
                    ctx.closePath();
                    @php
                        $lastx = $lastx + 35;
                    @endphp
                @endif

                ctx.beginPath();
                ctx.strokeStyle = "blue";
                ctx.arc({{ $lastx + 50 }}, {{ $item->row * 35 + 10 }}, 14, 0, 2 * Math.PI);
                ctx.fillStyle = "blue";
                ctx.fillText('{{ $item->all_total }}', {{ $lastx + 45 }}, {{ $item->row * 35 + 15 }});
                ctx.closePath();
                ctx.stroke();
            @endforeach

            ctx.stroke();
        @endif
    </script>
@endsection
