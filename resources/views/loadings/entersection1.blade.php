@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }} สถานะ: {{ $loadm->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{ $loadm->load_date }}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{ $loadm->customertype->name ?? '-' }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : {{ $loadm->customer }}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{ $loadm->order_no }}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.entersection1action', $loadm->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="act_date"
                                    class="col-md-2 col-form-label text-md-right">{{ __('วันที่ Load จริง') }}</label>
                                <div class="col-md-4">
                                    @if (!empty($loadm->act_date))
                                        <input id="act_date" type="date"
                                            class="form-control @error('act_date') is-invalid @enderror" name="act_date"
                                            value="{{ $loadm->act_date }}" required>
                                    @else
                                        @if (!empty($currentDeliveryPlan->loading))
                                            <input id="act_date" type="date"
                                                class="form-control @error('act_date') is-invalid @enderror" name="act_date"
                                                value="{{ $currentDeliveryPlan->loading }}" required>
                                        @else
                                            <input id="act_date" type="date"
                                                class="form-control @error('act_date') is-invalid @enderror" name="act_date"
                                                value="{{ old('act_date') }}" required>
                                        @endif
                                    @endif


                                    @error('act_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="owner_type"
                                    class="col-md-2 col-form-label text-md-right">{{ __('หัวลาก') }}</label>

                                <div class="col-md-4">

                                    @if (!empty($loadm->owner_type))
                                        {{ Form::text('owner_type', $loadm->owner_type, ['placeholder' => 'หัวลาก', 'class' => 'form-control']) }}
                                    @else
                                        @if (!empty($currentDeliveryPlan->truck))
                                            {{ Form::text('owner_type', $currentDeliveryPlan->truck, ['placeholder' => 'หัวลาก', 'class' => 'form-control']) }}
                                        @else
                                            {{ Form::text('owner_type', null, ['placeholder' => 'หัวลาก', 'class' => 'form-control']) }}
                                        @endif
                                    @endif

                                    @error('owner_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="truck_license_plate"
                                    class="col-md-2 col-form-label text-md-right">{{ __('ทะเบียนหน้า') }}</label>

                                <div class="col-md-4">
                                    @if (!empty($loadm->truck_license_plate))
                                        {{ Form::text('truck_license_plate', $loadm->truck_license_plate, ['placeholder' => 'ทะเบียนหน้า', 'class' => 'form-control']) }}
                                    @else
                                        @if (!empty($currentDeliveryPlan->license_plate_head))
                                            {{ Form::text('truck_license_plate', $currentDeliveryPlan->license_plate_head, ['placeholder' => 'ทะเบียนหน้า', 'class' => 'form-control']) }}
                                        @else
                                            {{ Form::text('truck_license_plate', null, ['placeholder' => 'ทะเบียนหน้า', 'class' => 'form-control']) }}
                                        @endif
                                    @endif

                                    @error('truck_license_plate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="convoy_license_plate"
                                    class="col-md-2 col-form-label text-md-right">{{ __('ทะเบียนหลัง') }}</label>

                                <div class="col-md-4">
                                    @if (!empty($loadm->convoy_license_plate))
                                        {{ Form::text('convoy_license_plate', $loadm->convoy_license_plate, ['placeholder' => 'ทะเบียนหลัง', 'class' => 'form-control']) }}
                                    @else
                                        @if (!empty($currentDeliveryPlan->license_plate_tail))
                                            {{ Form::text('convoy_license_plate', $currentDeliveryPlan->license_plate_tail, ['placeholder' => 'ทะเบียนหลัง', 'class' => 'form-control']) }}
                                        @else
                                            {{ Form::text('convoy_license_plate', null, ['placeholder' => 'ทะเบียนหลัง', 'class' => 'form-control']) }}
                                        @endif
                                    @endif

                                    @error('convoy_license_plate')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="convoy_no"
                                    class="col-md-2 col-form-label text-md-right">{{ __('หมายเลขตู้') }}</label>

                                <div class="col-md-4">
                                    @if (!empty($loadm->convoy_no))
                                        {{ Form::text('convoy_no', $loadm->convoy_no, ['placeholder' => 'หมายเลขตู้', 'class' => 'form-control']) }}
                                    @else
                                        @if (!empty($currentDeliveryPlan->container_no))
                                            {{ Form::text('convoy_no', $currentDeliveryPlan->container_no, ['placeholder' => 'หมายเลขตู้', 'class' => 'form-control']) }}
                                        @else
                                            {{ Form::text('convoy_no', null, ['placeholder' => 'หมายเลขตู้', 'class' => 'form-control']) }}
                                        @endif
                                    @endif

                                    @error('convoy_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="seal_no"
                                    class="col-md-2 col-form-label text-md-right">{{ __('หมายเลข Seal') }}</label>

                                <div class="col-md-4">
                                    @if (!empty($loadm->seal_no))
                                        {{ Form::text('seal_no', $loadm->seal_no, ['placeholder' => 'หมายเลข Seal', 'class' => 'form-control']) }}
                                    @else
                                        @if (!empty($currentDeliveryPlan->seal_no))
                                            {{ Form::text('seal_no', $currentDeliveryPlan->seal_no, ['placeholder' => 'หมายเลข Seal', 'class' => 'form-control']) }}
                                        @else
                                            {{ Form::text('seal_no', null, ['placeholder' => 'หมายเลข Seal', 'class' => 'form-control']) }}
                                        @endif
                                    @endif

                                    @error('seal_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="container_type_id"
                                    class="col-md-2 col-form-label text-md-right">{{ __('ประเภทตู้') }}</label>

                                <div class="col-md-4">
                                    @if (!empty($loadm->container_type_id))
                                        {{ Form::select('container_type_id', $containertypelist, $loadm->container_type_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('container_type_id', $containertypelist, null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('container_type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </form>
                        @if ($loadm->type_loading == '3Rows')
                            @include('elements.entersection13rows')
                        @else
                            @if ($loadm->type_loading == 'Pallet')
                                @include('elements.entersection1pallet')
                            @else
                                @include('elements.entersection1normal')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
