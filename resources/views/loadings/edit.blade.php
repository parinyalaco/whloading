@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('แก้ไขแผน Load') }} #{{ $loadm->id }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('loadings.editaction',$loadm->id) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="load_date" class="col-md-2 col-form-label text-md-right">{{ __('Loading Dae') }}</label>

                                <div class="col-md-4">
                                    <input id="load_date" type="date" class="form-control @error('load_date') is-invalid @enderror"
                                        name="load_date" value="{{ $loadm->load_date }}" required >

                                    @error('load_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="customer_type_id" class="col-md-2 col-form-label text-md-right">{{ __('Customer Type') }}</label>

                                <div class="col-md-4">

                                        {{ Form::select('customer_type_id', $customertypelist , $loadm->customer_type_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                    @error('customer')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="customer" class="col-md-2 col-form-label text-md-right">{{ __('Customer') }}</label>

                                <div class="col-md-4">
                                    <input id="customer" type="text" class="form-control @error('customer') is-invalid @enderror"
                                        name="customer" value="{{ $loadm->customer }}">

                                    @error('customer')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="order_no" class="col-md-2 col-form-label text-md-right">{{ __('Order No.') }}</label>

                                <div class="col-md-4">
                                    <input id="order_no" type="text" class="form-control @error('order_no') is-invalid @enderror"
                                        name="order_no" value="{{ $loadm->order_no }}">

                                    @error('order_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="customer" class="col-md-2 col-form-label text-md-right">แผนโหลด</label>
                                <div class="col-md-4">
                                    @if ($loadm->type_loading == '3Rows')
                                        Normal
                                    @else
                                        พิเศษ
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
