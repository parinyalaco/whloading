@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }} สถานะ: {{ $loadm->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadm->load_date}}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{$loadm->customertype->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Customer</strong> : {{$loadm->customer}}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{$loadm->order_no}}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.entersection2action', $loadm->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="teams_front_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานหน้าตู้ 1') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Front'][0]))
                                        {{ Form::select('teams_front_1', $teamlistType['Front'], $teams['Front'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_front_1', $teamlistType['Front'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_front_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_front_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานหน้าตู้ 2') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Front'][1]))
                                        {{ Form::select('teams_front_2', $teamlistType['Front'], $teams['Front'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_front_2', $teamlistType['Front'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_front_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_back_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานหลังตู้ 1') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Back'][0]))
                                        {{ Form::select('teams_back_1', $teamlistType['Back'], $teams['Back'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_back_1', $teamlistType['Back'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_back_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_back_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานหลังตู้ 2') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Back'][1]))
                                        {{ Form::select('teams_back_2', $teamlistType['Back'], $teams['Back'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_back_2', $teamlistType['Back'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_back_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_fl_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงาน FL 1') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['FL'][0]))
                                        {{ Form::select('teams_fl_1', $teamlistType['FL'], $teams['FL'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_fl_1', $teamlistType['FL'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_fl_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_fl_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงาน FL 2') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['FL'][1]))
                                        {{ Form::select('teams_fl_2', $teamlistType['FL'], $teams['FL'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_fl_2', $teamlistType['FL'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_fl_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_ctrl_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานคุมตู้ 1') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Ctrl'][0]))
                                        {{ Form::select('teams_ctrl_1', $teamlistType['Ctrl'], $teams['Ctrl'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_ctrl_1', $teamlistType['Ctrl'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_ctrl_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_ctrl_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานคุมตู้ 2') }}</label>

                                <div class="col-md-4">
                                    @if (isset($teams['Ctrl'][1]))
                                        {{ Form::select('teams_ctrl_2', $teamlistType['Ctrl'], $teams['Ctrl'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_ctrl_2', $teamlistType['Ctrl'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_ctrl_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_bfload_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานตรวจเช็คสินค้าก่อนโหลด 1') }}</label>
                                <div class="col-md-4">
                                    @if (isset($teams['Bfload'][0]))
                                        {{ Form::select('teams_bfload_1', $teamlistType['Bfload'], $teams['Bfload'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_bfload_1', $teamlistType['Bfload'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_bfload_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_bfload_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานตรวจเช็คสินค้าก่อนโหลด 2') }}</label>
                                <div class="col-md-4">
                                    @if (isset($teams['Bfload'][1]))
                                        {{ Form::select('teams_bfload_2', $teamlistType['Bfload'], $teams['Bfload'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_bfload_2', $teamlistType['Bfload'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_bfload_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_bkbox_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานตรวจเช็คกล่องแตก 1') }}</label>
                                <div class="col-md-4">
                                    @if (isset($teams['Bkbox'][0]))
                                        {{ Form::select('teams_bkbox_1', $teamlistType['Bkbox'], $teams['Bkbox'][0]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_bkbox_1', $teamlistType['Bkbox'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_bkbox_1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="teams_bkbox_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('พนักงานตรวจเช็คกล่องแตก 2') }}</label>
                                <div class="col-md-4">
                                    @if (isset($teams['Bkbox'][1]))
                                        {{ Form::select('teams_bkbox_2', $teamlistType['Bkbox'], $teams['Bkbox'][1]->team_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select('teams_bkbox_2', $teamlistType['Bkbox'], null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif

                                    @error('teams_bkbox_2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </form>
                        @if ($loadm->type_loading == '3Rows')
                            @include('elements.entersection13rows')
                        @else
                            @if ($loadm->type_loading == 'Pallet')
                                @include('elements.entersection1pallet')
                            @else
                                @include('elements.entersection1normal')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
