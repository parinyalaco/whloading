@extends('layouts.entersection3')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }} สถานะ: {{ $loadm->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{ $loadm->load_date }}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{ $loadm->customertype->name ?? '-' }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : {{ $loadm->customer }}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{ $loadm->order_no }}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.entersection4action', $loadm->id) }}"
                            accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            @for ($i = 1; $i < 10; $i++)
                                <div class="form-group row">
                                <div class="col-md-1"><label for="">ลำดับที่ {{ $i }}</label></div>
                                <div class="col-md-2">
                                    @if (isset($loadlots[$i]))
                                        {{ Form::select($i.'_product_id',$productlist, $loadlots[$i]->product_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::select($i.'_product_id',$productlist, null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_start_no" type="number" value="{{$loadlots[$i]->start_no}}" placeholder='เริ่ม'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_start_no" type="number" value="" placeholder='เริ่ม'>
                                    @endif
                                     TO
                                    @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_end_no" type="number" value="{{$loadlots[$i]->end_no}}" placeholder='ถึง'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_end_no" type="number" value="" placeholder='ถึง' >
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_quantity" type="number" value="{{$loadlots[$i]->quantity}}" placeholder='จำนวน'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_quantity" type="number" value="" placeholder='จำนวน'>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    Exp.Date
                                   @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_exp_date" type="date" value="{{$loadlots[$i]->exp_date}}" placeholder='Exp.Date'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_exp_date" type="date" value="" placeholder='Exp.Date'>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    Best Buy
                                    @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_best_date" type="date" value="{{$loadlots[$i]->best_date}}" placeholder='Best Buy'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_best_date" type="date" value="" placeholder='Best Buy'>
                                    @endif
                                </div>
                                <div class="col-md-1">
                                    LACO BATCH
                                    @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_lot" type="text" value="{{$loadlots[$i]->lot}}" placeholder='BATCH'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_lot" type="text" value="" placeholder='BATCH'>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    @if (isset($loadlots[$i]))
                                        <input class="form-control" name="{{ $i }}_remark" type="text" value="{{$loadlots[$i]->remark}}" placeholder='remark'>
                                    @else
                                        <input class="form-control" name="{{ $i }}_remark" type="text" value="" placeholder='remark'>
                                    @endif
                                </div>
                            </div>
                            @endfor

                            <div class="form-group row">
                                <div class="col-md-6"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function Myfunction() {
            // var st = document.getElementsByName("open_box");     //ไม่แสดงค่า value
            var st = document.getElementById("open_box");
            var ed = document.getElementById("close_box");
            // console.log(st.value+'  -  '+ed.value);

            var startTime = Date.parse(document.querySelector("#open_box").value);
            var endTime =  Date.parse(document.querySelector("#close_box").value);
            console.log(startTime+'  -  '+endTime);
            if(startTime && endTime){
                if(startTime<endTime){
                    var cal_time = Math.abs(startTime - endTime) / 1000;
                    var cal_day = Math.floor( cal_time / 86400);
                    cal_time -= cal_day * 86400;
                    var cal_hour = Math.floor(cal_time / 3600) % 24;
                    cal_time -= cal_hour * 3600;
                    var cal_minutes = Math.floor(cal_time / 60) % 60;
                    // var cal_time = (((Math.abs( startTime - endTime ) / 1000)/60)/60);
                    console.log(cal_day + " day "+cal_hour+" hour "+cal_minutes+" minutes.");
                    if(cal_hour>=3 || cal_day>0){
                        alert("เวลาโหลดควรน้อยกว่า 3 ชั่วโมงค่ะ( ใช้เวลาไป "+cal_day + " day "+cal_hour+" hour "+cal_minutes+" minutes."+" )")
                    }
                }else{
                    alert("วันเวลาปิดตู้ มากกว่า/เท่ากับ วันเวลาเปิดตู้ค่ะ")

                }

            }
        }
    </script>
@endsection
