@extends('layouts.maindp')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้างPlan พาเลท / Slip sheet') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadm->load_date}}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{$loadm->customertype->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Customer</strong> : {{$loadm->customer}}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{$loadm->order_no}}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.planloadaction', $loadm->id) }}">
                            @csrf

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>พาเลท No</th>
                                        <th>Product</th>
                                        <th>ฐาน</th>
                                        <th>สูง</th>
                                        <th>ฝาก</th>
                                        <th>Lot</th>
                                        <th>Exp. Date</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @for ($i = 1; $i <= 30; $i++)
                                    <tr>
                                        <td>
                                            {{ Form::Text('pallet_no_' . $i, null, ['placeholder' => 'พาเลท No', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::select('p_product_id_' . $i, $productlist, null, ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker' ,'data-live-search'=>'true']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('p_base' . $i, null, ['placeholder' => 'ฐาน', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('p_height' . $i, null, ['placeholder' => 'สูง', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::number('p_excess' . $i, null, ['placeholder' => 'ฝาก', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::Text('p_lot_' . $i, null, ['placeholder' => 'Lot', 'class' => 'form-control']) }}
                                        </td>
                                        <td>
                                            {{ Form::Date('p_exp_date_' . $i, null, ['placeholder' => 'Exp Date', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Generate') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
