@extends('layouts.planload')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้างPlan Load') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4"><strong>Load Date</strong> : {{$loadd->loadm->load_date}}</div>
                            <div class="col-md-4"><strong>Customer</strong> : {{$loadd->loadm->customer}}</div>
                            <div class="col-md-4"><strong>Order No</strong> : {{$loadd->loadm->order_no}}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.editdetailaction',$loadd->id) }}">
                            @csrf

                            {{ Form::hidden('load_m_id' , $loadd->loadm->id, ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                            {{ Form::hidden('main_type' , 'Normal', ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                            <table class="table table-bordered">
                                <thead>
                                    <tr>

                                        <th colspan="2"> แถวแนวขวาง</th>

                                    </tr>
                                    <tr>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>พาเลท No</td>
                                        <td>
                                            {{ Form::number('row', $loadd->row,
                                            ['placeholder' => '0', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>พาเลท No</td>
                                        <td>
                                            {{ Form::text('pallet_no', $loadd->pallet_no,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('t_product_m_id', $productlist, $loadd->t_product_m_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker' ,'data-live-search'=>'true']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฐาน</td>
                                        <td>
                                            {{ Form::number('t_base' ,  $loadd->t_base,
                                            ['placeholder' => 'ฐาน',
                                            'class' => 'form-control calsum',
                                            'id' => 't_base']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('t_height' , $loadd->t_height,
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum',
                                            'id' => 't_height']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝาก</td>
                                        <td>
                                            {{ Form::number('t_excess' , $loadd->t_excess,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 't_excess']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lot</td>
                                        <td>
                                            {{ Form::text('p_lot', $loadd->p_lot,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Exp Date</td>
                                        <td>
                                            {{ Form::date('p_exp_date', $loadd->p_exp_date,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('t_product1_id', $productlist, $loadd->t_product1_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                            {{ Form::number('t_excess1' , $loadd->t_excess1,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess1']) }}
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('t_product2_id', $productlist, $loadd->t_product2_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                            {{ Form::number('t_excess2' , $loadd->t_excess2,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess2']) }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>รวม</td>
                                        <td>
                                            {{ Form::number('t_total' , $loadd->t_total, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'t_total']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>รวมทั้งหมด</td>
                                        <td>
                                            {{ Form::number('all_total' , $loadd->all_total, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'all_total']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Note</td>
                                        <td>
                                            {{ Form::text('note' , $loadd->note, ['placeholder' => 'note', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('สร้าง') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
