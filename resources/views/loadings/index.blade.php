@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    @if (Auth::user()->group->name == trim('Admin') ||
                            Auth::user()->group->name == trim('Superadmin') ||
                            Auth::user()->group->name == trim('Wh') ||
                            Auth::user()->group->name == trim('wh_sup'))
                        <div class="card-header">
                            <a class="btn btn-success" href="{{ route('loadings.create') }}"> สร้างแผน Load</a><br /><br />
                            <a class="btn @if (Request::get('status') == '') btn-warning @else btn-primary @endif"
                                href="{{ route('loadings.index') }}"> All </a>
                            <a class="btn @if (Request::get('status') == 'New') btn-warning @else btn-primary @endif"
                                href="{{ route('loadings.index', ['status' => 'New']) }}"> New
                            </a>
                            <a class="btn @if (Request::get('status') == 'Loading') btn-warning @else btn-primary @endif"
                                href="{{ route('loadings.index', ['status' => 'Loading']) }}"> Loading
                            </a>
                            <a class="btn @if (Request::get('status') == 'Loaded') btn-warning @else btn-primary @endif"
                                href="{{ route('loadings.index', ['status' => 'Loaded']) }}"> Loaded
                            </a>
                            <a class="btn @if (Request::get('status') == 'Finish') btn-warning @else btn-primary @endif"
                                href="{{ route('loadings.index', ['status' => 'Finish']) }}"> Finish
                            </a>
                        </div>
                    @endif
                    <form method="GET" action="{{ url('/loadings/?status=' . $status) }}" accept-charset="UTF-8"
                        class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"
                                placeholder="ค้นหาจาก Order / ลูกค้า / สินค้า" value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body table-responsive-lg d-none d-sm-block">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                @if (Auth::user()->group->name == trim('Admin') ||
                                        Auth::user()->group->name == trim('Superadmin') ||
                                        Auth::user()->username == trim('PIT') ||
                                        Auth::user()->username == trim('JJT'))
                                    <th>
                                        <a onclick="Myfunction()">
                                            <i class="bi bi-envelope"></i>
                                        </a>
                                    </th>
                                @endif
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Order</th>
                                <th>Product</th>
                                <th>Details</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            @foreach ($loadms as $loadm)
                                <tr>
                                    @if (Auth::user()->group->name == trim('Admin') ||
                                            Auth::user()->group->name == trim('Superadmin') ||
                                            Auth::user()->username == trim('PIT') ||
                                            Auth::user()->username == trim('JJT'))
                                        <td>
                                            @if ($loadm->status == 'Loaded')
                                                <input type="checkbox" value="{{ $loadm->id }}"
                                                    id="chk_print[{{ $loadm->id }}]"
                                                    name ="chk_print[{{ $loadm->id }}]">
                                            @endif
                                        </td>
                                    @endif
                                    <td>{{ $loadm->load_date }}</td>
                                    <td>{{ $loadm->customertype->name ?? '-' }} \ {{ $loadm->customer }}</td>
                                    <td>{{ $loadm->order_no }}</td>
                                    <td>
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($loadm->loadds as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                    </td>
                                    <td>{{ $loadm->owner_type }}</td>
                                    <td>{{ $loadm->status }}
                                        @if (
                                            $loadm->status == 'Loading' &&
                                                (Auth::user()->group->name == trim('Admin') ||
                                                    Auth::user()->group->name == trim('Superadmin') ||
                                                    Auth::user()->group->name == trim('Wh') ||
                                                    Auth::user()->group->name == trim('wh_sup')))
                                            <a class="btn btn-primary"
                                                href="{{ route('loadings.entercountermanual', $loadm->id) }}">
                                                <i class="bi bi-file-post-fill"></i> Manual
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if (Auth::user()->group->name == trim('Admin') ||
                                                Auth::user()->group->name == trim('Superadmin') ||
                                                Auth::user()->group->name == trim('Wh') ||
                                                Auth::user()->group->name == trim('wh_sup'))
                                            @php
                                                $flag1 = false;
                                                $flag2 = false;
                                                $flag3 = false;
                                            @endphp
                                            @if ($loadm->status == 'New')
                                                @if (!empty($products))
                                                    <a class="btn btn-primary"
                                                        href="{{ route('loadings.changestage', [$loadm->id, 'Loading']) }}">เริ่ม
                                                        Loading</a>
                                                @endif


                                                <a class="btn btn-warning"
                                                    href="{{ route('loadings.edit', $loadm->id) }}">Edit</a>
                                                @if (empty($products))
                                                    <a class="btn btn-warning"
                                                        href="{{ route('loadings.usemasterplan', $loadm->id) }}">Use Master
                                                        Plan</a>
                                                    <a class="btn btn-primary"
                                                        href="{{ route('loadings.planload', $loadm->id) }}">Plan Load</a>
                                                @endif
                                            @else
                                                @if ($loadm->status == 'Loading')
                                                    <a class="btn btn-primary"
                                                        href="{{ route('loadings.realtime', $loadm->id) }}">
                                                        <i class="bi bi-file-bar-graph-fill"></i> Realtime Data
                                                    </a>
                                                    @if (
                                                        !empty($loadm->act_date) &&
                                                            !empty($loadm->owner_type) &&
                                                            !empty($loadm->truck_license_plate) &&
                                                            !empty($loadm->convoy_license_plate) &&
                                                            !empty($loadm->convoy_no) &&
                                                            !empty($loadm->seal_no) &&
                                                            !empty($loadm->container_type_id))
                                                        <a class="btn btn-success"
                                                            href="{{ route('loadings.entersection1', $loadm->id) }}">
                                                            <i class="bi bi-check-circle-fill"></i> ใส่ข้อมูลส่วนที่ 2
                                                        </a>
                                                        @php
                                                            $flag1 = true;
                                                        @endphp
                                                    @else
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.entersection1', $loadm->id) }}">ใส่ข้อมูลส่วนที่
                                                            2</a>
                                                    @endif

                                                    @if (
                                                        $loadm->loadteams()->where('team_type', 'Front')->count() > 1 &&
                                                            $loadm->loadteams()->where('team_type', 'Back')->count() > 1 &&
                                                            $loadm->loadteams()->where('team_type', 'FL')->count() > 0 &&
                                                            $loadm->loadteams()->where('team_type', 'Bkbox')->count() > 0 &&
                                                            $loadm->loadteams()->where('team_type', 'Ctrl')->count() > 0)
                                                        @php
                                                            $flag2 = true;
                                                        @endphp
                                                        <a class="btn btn-success"
                                                            href="{{ route('loadings.entersection2', $loadm->id) }}">
                                                            <i class="bi bi-check-circle-fill"></i> ใส่ข้อมูลส่วนที่
                                                            3</a>
                                                    @else
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.entersection2', $loadm->id) }}">ใส่ข้อมูลส่วนที่
                                                            3</a>
                                                    @endif


                                                    {{-- <a class="btn btn-primary"
                                                        href="{{ route('loadings.entersection4', $loadm->id) }}">ใส่ข้อมูลส่วนที่
                                                        4</a> --}}


                                                    @if (
                                                        !empty($loadm->open_box) &&
                                                            !empty($loadm->close_box) &&
                                                            !empty($loadm->loading_img1) &&
                                                            !empty($loadm->loading_img5) &&
                                                            !empty($loadm->loading_img6) &&
                                                            !empty($loadm->loading_img8) &&
                                                            !empty($loadm->loading_img9) &&
                                                            (!empty($loadm->loading_img20) || !empty($loadm->loading_img21) || !empty($loadm->loading_img22)))
                                                        @php
                                                            $flag3 = true;
                                                        @endphp
                                                        <a class="btn btn-success"
                                                            href="{{ route('loadings.entersection3', $loadm->id) }}">
                                                            <i class="bi bi-check-circle-fill"></i> ใส่ข้อมูลปิดตู้</a>
                                                    @else
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.entersection3', $loadm->id) }}">ใส่ข้อมูลปิดตู้</a>
                                                    @endif

                                                    @if ($flag1 && $flag2 && $flag3)
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.closeload', $loadm->id) }}">ปิดการ
                                                            Load</a>
                                                    @endif
                                                @endif
                                            @endif

                                            {{-- <br /> --}}
                                        @endif

                                        <a class="btn btn-secondary"
                                            href="{{ route('loadings.planloadview', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> Image</a>

                                        @if ($loadm->status == 'Loaded')
                                            <a class="btn btn-secondary" href="{{ route('answers.index', $loadm->id) }}"><i
                                                    class="bi bi-camera-fill"></i> Check Point</a>'
                                            <a class="btn btn-info" href="{{ route('answers.createAll', $loadm->id) }}"><i
                                                    class="bi bi-camera-fill"></i> ใส่ข้อมูลเดินทาง</a>
                                            <a class="btn btn-primary"
                                                href="{{ route('loadings.changestage', [$loadm->id, 'Finish']) }}"><i
                                                    class="bi bi-check-circle-fill"></i> ส่งตู้เรียบร้อย</a>
                                        @endif

                                        @if ($loadm->status == 'Finish')
                                            <a class="btn btn-secondary" href="{{ route('answers.index', $loadm->id) }}"><i
                                                    class="bi bi-camera-fill"></i> Check Point</a>
                                        @endif

                                        @if (Auth::user()->group->name == trim('Admin') ||
                                                Auth::user()->group->name == trim('Superadmin') ||
                                                Auth::user()->group->name == trim('Wh') ||
                                                Auth::user()->group->name == trim('wh_sup'))
                                            <a class="btn btn-info" href="{{ route('loadings.show', $loadm->id) }}"><i
                                                    class="bi bi-columns-gap"></i> Show</a>
                                            @if ($loadm->status == 'Loaded')
                                                <button type="button" class="btn btn-warning"
                                                    onclick="show_input({{ $loadm->id }},'{{ $loadm->order_no }}')"
                                                    data-bs-toggle="modal" data-bs-target="#exampleModal"
                                                    data-mdb-whatever="{{ $loadm->id }}">
                                                    คืนสถานะ
                                                </button>
                                            @endif

                                            @if ($flag1)
                                                <a class="btn btn-info"
                                                    href="{{ route('reports.loadingdoc', $loadm->id) }}"><i
                                                        class="bi bi-file-earmark-spreadsheet-fill"></i> เอกสาร Load</a>
                                                <a class="btn btn-info"
                                                    href="{{ route('reports.masterplan', $loadm->id) }}"><i
                                                        class="bi bi-file-earmark-spreadsheet-fill"></i> Masterplan
                                                    Excel</a>
                                            @endif
                                            @if (
                                                $loadm->status == 'Loaded' &&
                                                    (Auth::user()->group->name == trim('Admin') or Auth::user()->group->name == trim('Superadmin')))
                                                <a class="btn btn-primary"
                                                    href="{{ route('loadings.clone', $loadm->id) }}">
                                                    <i class="bi bi-file-post-fill"></i> Clone
                                                </a>
                                            @endif

                                            @if (Auth::user()->group->name == 'Admin' or
                                                    Auth::user()->group->name == trim('Superadmin') or
                                                    Auth::user()->group->name == 'wh_sup')
                                                <br /><a class="btn btn-danger"
                                                    href="{{ route('loadings.destroy', $loadm->id) }}"><i
                                                        class="bi bi-trash-fill"></i> DELETE</a>
                                            @endif
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('loadings.load_reason') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea id="reason" class="form-control col" name="reason" required></textarea>
                                            <input type="hidden" id="loadm_id" class="form-control col"
                                                name="loadm_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="pagination-wrapper"> {!! $loadms->appends(['status' => Request::get('status'), 'search' => Request::get('search')])->render() !!} </div>

                    </div>

                    <div class="card-body table-responsive-lg d-block d-sm-none">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                @if (Auth::user()->group->name == trim('Admin') ||
                                        Auth::user()->group->name == trim('Superadmin') ||
                                        Auth::user()->username == trim('PIT') ||
                                        Auth::user()->username == trim('JJT'))
                                    <th>
                                        <a onclick="Myfunction()">
                                            <i class="bi bi-envelope"></i>
                                        </a>
                                    </th>
                                @endif
                                <th>Data</th>
                            </tr>
                            @foreach ($loadms as $loadm)
                                <tr>
                                    @if (Auth::user()->group->name == trim('Admin') ||
                                            Auth::user()->group->name == trim('Superadmin') ||
                                            Auth::user()->username == trim('PIT') ||
                                            Auth::user()->username == trim('JJT'))
                                        <td>
                                            @if ($loadm->status == 'Loaded')
                                                <input type="checkbox" value="{{ $loadm->id }}"
                                                    id="chk_print[{{ $loadm->id }}]"
                                                    name ="chk_print[{{ $loadm->id }}]">
                                            @endif
                                        </td>
                                    @endif
                                    <td>{{ $loadm->load_date }}
                                        <br />
                                        {{ $loadm->customertype->name ?? '-' }} \ {{ $loadm->customer }}<br />
                                        {{ $loadm->order_no }}<br />
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($loadm->loadds as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                        <br />{{ $loadm->owner_type }}<br />{{ $loadm->status }}
                                        @if (
                                            $loadm->status == 'Loading' &&
                                                (Auth::user()->group->name == trim('Admin') ||
                                                    Auth::user()->group->name == trim('Superadmin') ||
                                                    Auth::user()->group->name == trim('Wh') ||
                                                    Auth::user()->group->name == trim('wh_sup')))
                                            <a class="btn btn-primary"
                                                href="{{ route('loadings.entercountermanual', $loadm->id) }}">
                                                <i class="bi bi-file-post-fill"></i> Manual
                                            </a>
                                        @endif
                                        <br />
                                        @if (Auth::user()->group->name == trim('Admin') ||
                                                Auth::user()->group->name == trim('Superadmin') ||
                                                Auth::user()->group->name == trim('Wh') ||
                                                Auth::user()->group->name == trim('wh_sup'))
                                            @php
                                                $flag1 = false;
                                                $flag2 = false;
                                                $flag3 = false;
                                            @endphp
                                            @if ($loadm->status == 'New')
                                                @if (!empty($products))
                                                    <a class="btn btn-primary"
                                                        href="{{ route('loadings.changestage', [$loadm->id, 'Loading']) }}">เริ่ม
                                                        Loading</a>
                                                @endif


                                                <a class="btn btn-warning"
                                                    href="{{ route('loadings.edit', $loadm->id) }}">Edit</a>
                                                @if (empty($products))
                                                    <a class="btn btn-warning"
                                                        href="{{ route('loadings.usemasterplan', $loadm->id) }}">Use
                                                        Master Plan</a>
                                                    <a class="btn btn-primary"
                                                        href="{{ route('loadings.planload', $loadm->id) }}">Plan Load</a>
                                                @endif
                                            @else
                                                @if ($loadm->status == 'Loading')
                                                    <a class="btn btn-primary"
                                                        href="{{ route('loadings.realtime', $loadm->id) }}">
                                                        <i class="bi bi-file-bar-graph-fill"></i> Realtime Data
                                                    </a>
                                                    @if (
                                                        !empty($loadm->act_date) &&
                                                            !empty($loadm->owner_type) &&
                                                            !empty($loadm->truck_license_plate) &&
                                                            !empty($loadm->convoy_license_plate) &&
                                                            !empty($loadm->convoy_no) &&
                                                            !empty($loadm->seal_no) &&
                                                            !empty($loadm->container_type_id))
                                                        <a class="btn btn-success"
                                                            href="{{ route('loadings.entersection1', $loadm->id) }}">
                                                            <i class="bi bi-check-circle-fill"></i> ใส่ข้อมูลส่วนที่ 2
                                                        </a>
                                                        @php
                                                            $flag1 = true;
                                                        @endphp
                                                    @else
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.entersection1', $loadm->id) }}">ใส่ข้อมูลส่วนที่
                                                            2</a>
                                                    @endif

                                                    @if (
                                                        $loadm->loadteams()->where('team_type', 'Front')->count() > 1 &&
                                                            $loadm->loadteams()->where('team_type', 'Back')->count() > 1 &&
                                                            $loadm->loadteams()->where('team_type', 'FL')->count() > 0 &&
                                                            $loadm->loadteams()->where('team_type', 'Bkbox')->count() > 0 &&
                                                            $loadm->loadteams()->where('team_type', 'Ctrl')->count() > 0)
                                                        @php
                                                            $flag2 = true;
                                                        @endphp
                                                        <a class="btn btn-success"
                                                            href="{{ route('loadings.entersection2', $loadm->id) }}">
                                                            <i class="bi bi-check-circle-fill"></i> ใส่ข้อมูลส่วนที่
                                                            3</a>
                                                    @else
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.entersection2', $loadm->id) }}">ใส่ข้อมูลส่วนที่
                                                            3</a>
                                                    @endif

                                                    @if (
                                                        !empty($loadm->open_box) &&
                                                            !empty($loadm->close_box) &&
                                                            !empty($loadm->loading_img1) &&
                                                            !empty($loadm->loading_img5) &&
                                                            !empty($loadm->loading_img6) &&
                                                            !empty($loadm->loading_img8) &&
                                                            !empty($loadm->loading_img9) &&
                                                            (!empty($loadm->loading_img20) || !empty($loadm->loading_img21) || !empty($loadm->loading_img22)))
                                                        @php
                                                            $flag3 = true;
                                                        @endphp
                                                        <a class="btn btn-success"
                                                            href="{{ route('loadings.entersection3', $loadm->id) }}">
                                                            <i class="bi bi-check-circle-fill"></i> ใส่ข้อมูลปิดตู้</a>
                                                    @else
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.entersection3', $loadm->id) }}">ใส่ข้อมูลปิดตู้</a>
                                                    @endif

                                                    @if ($flag1 && $flag2 && $flag3)
                                                        <a class="btn btn-primary"
                                                            href="{{ route('loadings.closeload', $loadm->id) }}">ปิดการ
                                                            Load</a>
                                                    @endif
                                                @endif
                                            @endif

                                            {{-- <br /> --}}
                                        @endif

                                        <a class="btn btn-secondary"
                                            href="{{ route('loadings.planloadview', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> Image</a>

                                        @if ($loadm->status == 'Loaded')
                                            <a class="btn btn-secondary"
                                                href="{{ route('answers.index', $loadm->id) }}">Check Point</a>
                                        @endif

                                        @if (Auth::user()->group->name == trim('Admin') ||
                                                Auth::user()->group->name == trim('Superadmin') ||
                                                Auth::user()->group->name == trim('Wh') ||
                                                Auth::user()->group->name == trim('wh_sup'))
                                            <a class="btn btn-info" href="{{ route('loadings.show', $loadm->id) }}"><i
                                                    class="bi bi-columns-gap"></i> Show</a>
                                            @if ($loadm->status == 'Loaded')
                                                <button type="button" class="btn btn-warning"
                                                    onclick="show_input({{ $loadm->id }},'{{ $loadm->order_no }}')"
                                                    data-bs-toggle="modal" data-bs-target="#exampleModal"
                                                    data-mdb-whatever="{{ $loadm->id }}">
                                                    คืนสถานะ
                                                </button>
                                            @endif

                                            @if ($flag1)
                                                <a class="btn btn-info"
                                                    href="{{ route('reports.loadingdoc', $loadm->id) }}">เอกสาร Load</a>
                                                <a class="btn btn-info"
                                                    href="{{ route('reports.masterplan', $loadm->id) }}">Masterplan
                                                    Excel</a>
                                            @endif
                                            @if (
                                                $loadm->status == 'Loaded' &&
                                                    (Auth::user()->group->name == trim('Admin') or Auth::user()->group->name == trim('Superadmin')))
                                                <a class="btn btn-primary"
                                                    href="{{ route('loadings.clone', $loadm->id) }}">
                                                    <i class="bi bi-file-post-fill"></i> Clone
                                                </a>
                                            @endif

                                            @if (Auth::user()->group->name == 'Admin' or
                                                    Auth::user()->group->name == trim('Superadmin') or
                                                    Auth::user()->group->name == 'wh_sup')
                                                <br /><a class="btn btn-danger"
                                                    href="{{ route('loadings.destroy', $loadm->id) }}">DELETE</a>
                                            @endif
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('loadings.load_reason') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea id="reason" class="form-control col" name="reason" required></textarea>
                                            <input type="hidden" id="loadm_id" class="form-control col"
                                                name="loadm_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pagination-wrapper  table-responsive-lg "> {!! $loadms->appends(['status' => Request::get('status'), 'search' => Request::get('search')])->render() !!} </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <script type="text/javascript" src="{{ asset('assets/bootstrap/js/jquery-1.7.1.min.js') }}"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function Myfunction() {
            var obj = <?php echo json_encode($to_alert); ?>;
            const values = Array
                .from(document.querySelectorAll('input[type="checkbox"]'))
                .filter((checkbox) => checkbox.checked)
                .map((checkbox) => checkbox.value);

            let mess = 'คุณต้องการส่งเมลล์ โดยมีใบโหลด \n';
            for (const [key, value] of Object.entries(values)) {
                mess += ' Date:' + obj[value].load_date + '  Customer:' + obj[value].customer + '  Order :' + obj[value]
                    .order_no + '\n';
            }
            mess += 'ใช่หรือไม่';
            console.log(values.length);
            if (values.length > 0) {
                if (confirm(mess) == true) {
                    var link_url = window.location.origin + window.location.pathname + "/sent_mail?load_m_id=" + values;
                    // var link_url = window.location.origin+"/whloading/loadings/sent_mail?load_m_id="+values;
                    console.log(link_url);

                    window.location.replace(link_url);
                }
            }
        }
    </script>
    <script type="text/javascript">
        function show_input(num, order) {
            console.log(num + '--' + order);
            document.getElementById('loadm_id').value = num;
            let box = document.getElementById('test');
            box.innerText = 'กรุณาระบุเหตุผลที่ต้องการคืนค่า ใบ order ที่ ' + order;
        }
    </script>
@endsection
