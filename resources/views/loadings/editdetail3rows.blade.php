@extends('layouts.planload')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้างPlan Load') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4"><strong>Load Date</strong> : {{$loadd->loadm->load_date}}</div>
                            <div class="col-md-4"><strong>Customer</strong> : {{$loadd->loadm->customer}}</div>
                            <div class="col-md-4"><strong>Order No</strong> : {{$loadd->loadm->order_no}}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.editdetailaction', $loadd->id) }}">
                            @csrf

                            <table class="table table-bordered">
                                <thead>
                                    <tr>

                                        <th colspan="2"> แถวด้านซ้าย</th>

                                        <th colspan="2"> แถวกลาง</th>

                                        <th colspan="2"> แถวด้านขวา</th>
                                    </tr>
                                    <tr>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('t_product_m_id', $productlist, $loadd->t_product_m_id, ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true",'id'=>'t_product_m_id' ]) }}
                                        </td>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('c_product_m_id', $productlist, trim($loadd->c_product_m_id), ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true",'id'=>'t_product_m_id' ]) }}
                                        </td>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('l_product_m_id', $productlist, trim($loadd->l_product_m_id), ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true",'id'=>'t_product_m_id' ]) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>แถว</td>
                                        <td>
                                            {{ $loadd->row }}
                                        </td>
                                        <td>แถว</td>
                                        <td>
                                            {{ $loadd->row }}
                                        </td>
                                        <td>แถว</td>
                                        <td>
                                            {{ $loadd->row }}
                                        </td>
                                    </tr>
                                    <tr><td>ฐาน</td>
                                        <td>
                                            {{ Form::number('t_base' , $loadd->t_base,
                                            ['placeholder' => 'ฐาน', 'class' => 'form-control calsum','id'=>'t_base']) }}
                                        </td>
                                        <td>ฐาน</td>
                                        <td>
                                            {{ Form::number('c_base' , $loadd->c_base,
                                            ['placeholder' => 'ฐาน', 'class' => 'form-control calsum','id'=>'c_base']) }}
                                        </td>
                                        <td>ฐาน</td>
                                        <td>
                                            {{ Form::number('l_base' , $loadd->l_base,
                                            ['placeholder' => 'ฐาน', 'class' => 'form-control calsum','id'=>'l_base']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('t_height' , $loadd->t_height,
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum','id'=>'t_height']) }}
                                        </td>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('c_height' , $loadd->c_height,
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum','id'=>'c_height']) }}
                                        </td>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('l_height' , $loadd->l_height,
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum','id'=>'l_height']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('t_product_id', $productlist, $loadd->t_product_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                        </td>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('c_product_id', $productlist, $loadd->c_product_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                        </td>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('l_product_id', $productlist, $loadd->l_product_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true"]) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝาก1</td>
                                        <td>
                                            {{ Form::number('t_excess' , $loadd->t_excess,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess']) }}
                                        </td>
                                        <td>ฝาก1</td>
                                        <td>
                                            {{ Form::number('c_excess' , $loadd->c_excess,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'c_excess']) }}
                                        </td>
                                        <td>ฝาก1</td>
                                        <td>
                                            {{ Form::number('l_excess' , $loadd->l_excess,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum','id'=>'l_excess']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('t_product1_id', $productlist, $loadd->t_product1_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                        </td>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('c_product1_id', $productlist, $loadd->c_product1_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                        </td>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('l_product1_id', $productlist, $loadd->l_product1_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝาก2</td>
                                        <td>
                                            {{ Form::number('t_excess1' , $loadd->t_excess1,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess1']) }}
                                        </td>
                                        <td>ฝาก2</td>
                                        <td>
                                            {{ Form::number('c_excess1' , $loadd->c_excess1,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'c_excess1']) }}
                                        </td>
                                        <td>ฝาก2</td>
                                        <td>
                                            {{ Form::number('l_excess1' , $loadd->l_excess1,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum','id'=>'l_excess1']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝากสินค้า3</td>
                                        <td>
                                            {{ Form::select('t_product2_id', $productlist, $loadd->t_product2_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true"]) }}
                                        </td>
                                        <td>ฝากสินค้า3</td>
                                        <td>
                                            {{ Form::select('c_product2_id', $productlist, $loadd->c_product2_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true"]) }}
                                        </td>
                                        <td>ฝากสินค้า3</td>
                                        <td>
                                            {{ Form::select('l_product2_id', $productlist, $loadd->l_product2_id,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝาก3</td>
                                        <td>
                                            {{ Form::number('t_excess2' , $loadd->t_excess2,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess2']) }}
                                        </td>
                                        <td>ฝาก3</td>
                                        <td>
                                            {{ Form::number('c_excess2' , $loadd->c_excess2,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'c_excess2']) }}
                                        </td>
                                        <td>ฝาก3</td>
                                        <td>
                                            {{ Form::number('l_excess2' , $loadd->l_excess2,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum','id'=>'l_excess2']) }}
                                        </td>
                                    </tr>   1
                                    <tr>
                                        <td>รวม</td>
                                        <td>
                                            {{ Form::number('t_total' , $loadd->t_total,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control','readonly'=>true,'id'=>'t_total']) }}
                                        </td>
                                        <td>รวม</td>
                                        <td>
                                            {{ Form::number('c_total' , $loadd->c_total,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control','readonly'=>true,'id'=>'c_total']) }}
                                        </td>
                                        <td>รวม</td>
                                        <td>
                                            {{ Form::number('l_total' , $loadd->l_total,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control','readonly'=>true,'id'=>'l_total']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>รวมทั้งหมด</td>
                                        <td colspan="5">
                                            {{ Form::number('all_total' , $loadd->all_total, ['placeholder' => 'ฝาก', 'class' => 'form-control','readonly'=>true,'id'=>'all_total']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Note</td>
                                        <td colspan="5">
                                            {{ Form::text('note' , $loadd->note, ['placeholder' => 'note', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('แก้ไข') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
