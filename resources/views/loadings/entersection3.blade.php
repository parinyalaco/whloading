@extends('layouts.entersection3')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }} สถานะ: {{ $loadm->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{ $loadm->load_date }}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{ $loadm->customertype->name ?? '-' }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : {{ $loadm->customer }}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{ $loadm->order_no }}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.entersection3action', $loadm->id) }}"
                            accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="teams_front_1"
                                    class="col-md-2 col-form-label text-md-right">{{ __('วันเวลา เปิดตู้') }}</label>

                                <div class="col-md-4">
                                    <input class="form-control" id="open_box" name="open_box" type="dateTime-local" onchange="Myfunction()" onkeydown="return false"
                                    value="@if(!empty($loadm->open_box)){{ date('Y-m-d\TH:i', strtotime($loadm->open_box)) }}@endif">
                                    {{-- @if(!empty(old('open_box')))
                                        {{ Form::input('dateTime-local', 'open_box', date('Y-m-d\TH:i', strtotime(old('open_box'))), ['id' => 'game-date-time-text', 'class' => 'form-control']) }}
                                    @elseif (!empty($loadm->open_box))
                                        {{ Form::input('dateTime-local', 'open_box', date('Y-m-d\TH:i', strtotime($loadm->open_box)), ['id' => 'game-date-time-text', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::input('dateTime-local', 'open_box', null, ['id' => 'game-date-time-text', 'class' => 'form-control', 'onkeydown' => 'return false', 'onchange' => 'Myfunction()']) }}
                                    @endif --}}

                                    @error('open_box')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                                <label for="teams_front_2"
                                    class="col-md-2 col-form-label text-md-right">{{ __('วันเวลา ปิดตู้') }}</label>

                                <div class="col-md-4">
                                    <input class="form-control" id="close_box" name="close_box" type="dateTime-local" onchange="Myfunction()" onkeydown = 'return false'
                                    value="@if(!empty($loadm->close_box)){{ date('Y-m-d\TH:i', strtotime($loadm->close_box)) }}@endif">
                                    {{-- @if(!empty(old('close_box')))
                                        {{ Form::input('dateTime-local', 'close_box', date('Y-m-d\TH:i', strtotime(old('close_box'))), ['id' => 'game-date-time-text', 'class' => 'form-control']) }}
                                    @elseif (!empty($loadm->close_box))
                                        {{ Form::input('dateTime-local', 'close_box', date('Y-m-d\TH:i', strtotime($loadm->close_box)), ['id' => 'game-date-time-text', 'class' => 'form-control']) }}
                                    @else
                                        {{ Form::input('dateTime-local', 'close_box', null, ['id' => 'game-date-time-text', 'class' => 'form-control']) }}
                                    @endif --}}

                                    @error('open_box')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <label for="teams_front_2"
                                    class="col-md-12 col-form-label text-md-right">{{ __('หมายเหตุ (ชี้แจงการใช้เวลา เปิด-ปิดตู้ มากกว่าปกติ)') }}</label>

                                <div class="col-md-12 py-2">
                                    <textarea class="form-control" id="reason_box" name="reason_box">{{ $loadm->reason_box }}</textarea>
                                </div>

                                @foreach ($brokentypelist as $key => $brokentype)
                                    <label for="broken_box"
                                        class="col-md-3 col-form-label text-md-right">{{ $brokentype }}</label>

                                    <div class="col-md-1">
                                        @if (isset($brokens[$key]))
                                            {{ Form::number('broken_type' . $key, $brokens[$key]->box, ['placeholder' => '0', 'class' => 'form-control brokenall']) }}

                                        @else
                                            {{ Form::number('broken_type' . $key, null, ['placeholder' => '0', 'class' => 'form-control brokenall']) }}

                                        @endif

                                        @error('broken_box')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @endforeach
                                {{-- <div class="col-md-12 py-2"> --}}
                                    <div class="row py-2">
                                        <label for="broken_box_case"
                                            class="col-md-3 col-form-label text-md-right">{{ __('รวม') }}</label>

                                        <div class="col-md-3">
                                            @if (!empty($loadm->broken_box))
                                                {{ Form::number('broken_box', $loadm->broken_box, ['placeholder' => '0', 'class' => 'form-control' , 'id' => 'broken_box' , 'readonly'=>'true']) }}

                                            @else
                                                {{ Form::number('broken_box', null, ['placeholder' => '0', 'class' => 'form-control', 'id' => 'broken_box' , 'readonly'=>'true']) }}

                                            @endif

                                            @error('broken_box')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label for="broken_box_case"
                                            class="col-md-3 col-form-label text-md-right">{{ __('รายละเอียดกล่องแตก') }}</label>

                                        <div class="col-md-9">
                                            @if (!empty($loadm->broken_box_case))
                                                {{ Form::text('broken_box_case', $loadm->broken_box_case, ['placeholder' => 'รายละเอียดกล่องแตก', 'class' => 'form-control']) }}

                                            @else
                                                {{ Form::text('broken_box_case', null, ['placeholder' => 'รายละเอียดกล่องแตก', 'class' => 'form-control']) }}

                                            @endif

                                            @error('teams_front_4')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                {{-- </div> --}}
                                <div class='row py-2'>
                                    <label for="broken_box_case"
                                        class="col-md-3 col-form-label text-md-right">{{ __('น้ำหนักตู้ (kg)') }}</label>
                                    <div class="col-md-3">
                                        @if (!empty($loadm->box_weight))
                                            {{ Form::number('box_weight', $loadm->box_weight, ['placeholder' => '0', 'class' => 'form-control' , 'id' => 'box_weight' ]) }}

                                        @else
                                            {{ Form::number('box_weight', null, ['placeholder' => '0', 'class' => 'form-control', 'id' => 'box_weight' ]) }}

                                        @endif

                                        @error('box_weight')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <label for="broken_box_case"
                                            class="col-md-3 col-form-label text-md-right">{{ __('สายพานที่ใช้ในการโหลด') }}</label>
                                    <div class="col-md-3">
                                        @if (!empty($loadm->conveyor_id))
                                            {{ Form::select('conveyor_id', $conveyor, $loadm->conveyor_id, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                        @else
                                            {{ Form::select('conveyor_id', $conveyor, null, ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}

                                        @endif

                                            @error('box_weight')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                </div>

                                @include('elements.imageinternal')

                                <div class="col-md-6"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </form>
                        @if ($loadm->type_loading == "3Rows")
                            @include('elements.entersection13rows')
                        @else
                            @include('elements.entersection1normal')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function Myfunction() {
            // var st = document.getElementsByName("open_box");     //ไม่แสดงค่า value
            var st = document.getElementById("open_box");
            var ed = document.getElementById("close_box");
            // console.log(st.value+'  -  '+ed.value);

            var startTime = Date.parse(document.querySelector("#open_box").value);
            var endTime =  Date.parse(document.querySelector("#close_box").value);
            console.log(startTime+'  -  '+endTime);
            if(startTime && endTime){
                if(startTime<endTime){
                    var cal_time = Math.abs(startTime - endTime) / 1000;
                    var cal_day = Math.floor( cal_time / 86400);
                    cal_time -= cal_day * 86400;
                    var cal_hour = Math.floor(cal_time / 3600) % 24;
                    cal_time -= cal_hour * 3600;
                    var cal_minutes = Math.floor(cal_time / 60) % 60;
                    // var cal_time = (((Math.abs( startTime - endTime ) / 1000)/60)/60);
                    console.log(cal_day + " day "+cal_hour+" hour "+cal_minutes+" minutes.");
                    if(cal_hour>=3 || cal_day>0){
                        alert("เวลาโหลดควรน้อยกว่า 3 ชั่วโมงค่ะ( ใช้เวลาไป "+cal_day + " day "+cal_hour+" hour "+cal_minutes+" minutes."+" )")
                    }
                }else{
                    alert("วันเวลาปิดตู้ มากกว่า/เท่ากับ วันเวลาเปิดตู้ค่ะ")

                }

            }
        }
    </script>
@endsection
