<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        @LaravelDompdfThaiFont
        <style>
        body {
            font-family: "THSarabunNew";
        }
            .tb { border-collapse: collapse; width:100%; }
            .tb th, .tb td { border: solid 1px #000000; vertical-align: top;}

            .tbn th, .tb td { vertical-align: top;}
        </style>
        <title></title>
    </head>
    <body>
        <div class="page-content">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        {{-- {{ asset('fonts/THSarabunNew Bold.ttf') }} --}}
                        <div class="card-header"><strong>&nbsp;Load&nbsp;สถานะ</strong>&nbsp;:&nbsp;{{ $loadm->status }}</div>
                        <div class="card-body">
                            <table style="width: 100%" class="tbn">
                                <tr style="vertical-align: top">
                                    <td style="width: 25%">
                                        <strong>Load&nbsp;Date</strong>&nbsp;:&nbsp;{{$loadm->load_date}}</td>
                                    <td style="width: 25%">
                                        <strong>Customer&nbsp;Type</strong>&nbsp;:&nbsp;{!! $pdf_val[0] !!}
                                    </td>
                                    <td style="width: 25%">
                                        <strong>Customer</strong>&nbsp;:&nbsp;{!! $pdf_val[1] !!}</td>
                                    <td style="width: 25%">
                                        <strong>Order&nbsp;No</strong>&nbsp;:&nbsp;{!! $pdf_val[2] !!}</td>
                                </tr>
                            </table>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 33%"><strong>วันที่&nbsp;Load&nbsp;จริง</strong>&nbsp;:&nbsp;{{ $loadm->act_date }}</td>
                                    <td style="width: 33%"><strong>หัวลาก</strong>&nbsp;:&nbsp;{!! $pdf_val[3] !!}</td>
                                    <td style="width: 33%"><strong>ทะเบียนหน้า</strong>&nbsp;:&nbsp;{!! $pdf_val[4] !!}</td>
                                </tr>
                                <tr>
                                    <td><strong>ทะเบียนหลัง</strong>&nbsp;:&nbsp;{!! $pdf_val[5] !!}</td>
                                    <td><strong>หมายเลขตู้</strong>&nbsp;:&nbsp;{!! $pdf_val[6] !!}</td>
                                    <td><strong>หมายเลข&nbsp;Seal</strong>&nbsp;:&nbsp;{!! $pdf_val[7] !!}</td>
                                </tr>
                                <tr>
                                    <td><strong>ประเภทตู้</strong>&nbsp;:&nbsp;{!! $pdf_val[12] !!}</td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>พนักงานหน้าตู้</strong>&nbsp;:&nbsp;@if (isset($teams['Front']))
                                        @foreach ($staff['Front'] as $item=>$name)
                                            <br />
                                            {!! $name !!}
                                        @endforeach
                                        @else-@endif
                                    </td>
                                    <td><strong>พนักงานหลังตู้</strong>&nbsp;:&nbsp;@if (isset($teams['Back']))
                                            @foreach ($staff['Back'] as $item=>$name)
                                                <br />
                                                {!! $name !!}
                                            @endforeach
                                            @else-@endif
                                    </td>
                                    <td><strong>พนักงาน&nbsp;FL</strong>&nbsp;:&nbsp;@if (isset($teams['FL']))
                                            @foreach ($staff['FL'] as $item=>$name)
                                                <br />
                                                {!! $name !!}
                                            @endforeach
                                            @else-@endif
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>พนักงานคุมตู้</strong>&nbsp;:&nbsp;@if (isset($teams['Ctrl']))
                                            @foreach ($staff['Ctrl'] as $item=>$name)
                                                <br />
                                                {!! $name !!}
                                            @endforeach
                                            @else-@endif
                                    </td>
                                    <td><strong>พนักงานตรวจเช็คสินค้าก่อนโหลด</strong>&nbsp;:&nbsp;@if (isset($teams['Bfload']))
                                            @foreach ($staff['Bfload'] as $item=>$name)
                                                <br />
                                                {!! $name !!}
                                            @endforeach
                                            @else-@endif
                                    </td>
                                    <td><strong>พนักงานตรวจเช็คกล่องแตก</strong>&nbsp;:&nbsp;@if (isset($teams['Bkbox']))
                                            @foreach ($staff['Bkbox'] as $item=>$name)
                                                <br />
                                                {!! $name !!}
                                            @endforeach
                                            @else-@endif
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td><strong>วันเวลา&nbsp;เปิดตู้</strong>&nbsp;:&nbsp;{{ date('Y-m-d', strtotime($loadm->open_box)) }}&nbsp;{{ date('H:i', strtotime($loadm->open_box)) }}</td>
                                    <td><strong>วันเวลา&nbsp;ปิดตู้</strong>&nbsp;:&nbsp;{{ date('Y-m-d', strtotime($loadm->close_box)) }}&nbsp;{{ date('H:i', strtotime($loadm->close_box)) }}</td>
                                    @php
                                        $dateTimeObject1 = date_create($loadm->open_box);
                                        $dateTimeObject2 = date_create($loadm->close_box);

                                        $difference = date_diff($dateTimeObject1, $dateTimeObject2);
                                    @endphp
                                    <td><strong>ใช้เวลารวม</strong>&nbsp;:&nbsp;@if($dateTimeObject1 > $dateTimeObject2)'-'@else{{ date('H:i', strtotime($difference->h.':'.$difference->i)) }}@endif&nbsp;ชั่วโมง</td>
                                </tr>
                                @if(!empty($loadm->reason_box))
                                    <tr style="vertical-align: top">
                                        <td colspan="3" style="border:1px solid red;">
                                        {{-- <div class="row" > --}}
                                            <div class="row"><strong>หมายเหตุ</strong>&nbsp;:&nbsp;</div>
                                            <div style="color: red;">{!! $pdf_val[8] !!}</div>
                                        {{-- </div> --}}
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td colspan="3">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <strong>สายพานที่ใช้ในการโหลด</strong>&nbsp;:&nbsp;{!! $pdf_val[9] !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <strong>ปิดการ&nbsp;Load&nbsp;โดย</strong>&nbsp;:&nbsp;{!! $pdf_val[10] !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"><strong>กล่องแตก</strong>&nbsp;:&nbsp;จำนวน&nbsp;@if($loadm->broken_box > 0){{ $loadm->broken_box }}@else-@endif&nbsp;กล่อง</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><strong>รายละเอียด</strong>&nbsp;:&nbsp;{!! $pdf_val[11] !!}</td>
                                    {{-- {{ $loadm->broken_box_case }} --}}
                                </tr>
                                @php
                                    $count_brok = 0;
                                    $count_pic = 0;
                                @endphp
                                @foreach ($loadm->loadbrokens as $loadbrokenObj)
                                    @php
                                        $count_brok++;
                                    @endphp
                                    @if($count_brok%3==1)<tr style="vertical-align: top">@endif
                                        <td>
                                            <strong>
                                                @php
                                                    $ep = explode(' ',$loadbrokenObj->brokentype->name);
                                                @endphp
                                                @foreach($ep as $key => $value){{ $value  ?? '' }}&nbsp;@endforeach</strong>&nbsp;:&nbsp;{{ $loadbrokenObj->box }}&nbsp;กล่อง
                                        </td>
                                        @if($count_brok%3==0 || $count_brok==count($loadm->loadbrokens))</tr>@endif
                                @endforeach
                                @foreach ($pics as $runkey => $pic)
                                    @php
                                        $picparamname = 'loading_img_path'.$runkey
                                    @endphp
                                    @if (!empty($loadm->$picparamname))
                                        @php
                                            $count_pic++;
                                        @endphp
                                        @if($count_pic%3==1)<tr>@endif
                                        <td>
                                            <div class="col">
                                                <div class="row">

                                                    @php
                                                        $logo = '';
                                                        $chk_path = url($loadm->$picparamname);
                                                        $path = public_path($loadm->$picparamname);
                                                        $type = pathinfo($path, PATHINFO_EXTENSION);
                                                        $data = file_get_contents($path);
                                                        $logo = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                    @endphp
                                                    <img src="{{ $logo }}" width="150" />

                                                </div>
                                                <div class="row">
                                                    @php
                                                        $ep = explode(' ',$pic);
                                                    @endphp
                                                    @foreach($ep as $key => $value){{ $value  ?? '' }}&nbsp;@endforeach
                                                </div>
                                            </div>
                                        </td>
                                        @if($count_pic%3==0 || $count_pic==count($pics))</tr>@endif
                                    @endif
                                @endforeach
                            </table>
                            <table class="tb">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Loadin No</th>
                                            <th class="text-center">Product</th>
                                            <th class="text-center">No. To No.</th>
                                            <th class="text-center">Quantity Carton</th>
                                            <th class="text-center">Exp. Date</th>
                                            <th class="text-center">Stamping / LACO Batch</th>
                                            <th class="text-center">Remark</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($loadm->loadlots as $loadlot)
                                            <tr>
                                                <td class="text-center">No.{{ $loadlot->seq }}</td>
                                                <td class="text-center">{{ $loadlot->product->name }}</td>
                                                <td class="text-center">{{ $loadlot->start_no }} - {{ $loadlot->end_no }}
                                                </td>
                                                <td class="text-center">{{ $loadlot->quantity }}</td>
                                                <td class="text-center">{{ date('F d Y', strtotime($loadlot->exp_date)) }}
                                                </td>
                                                <td class="text-center">{{ date('Y.m.d', strtotime($loadlot->best_date)) }}
                                                    {{ $loadlot->lot }}</td>
                                                <td class="text-center">{{ $loadlot->remark }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            <table class="tb">
                                <thead>
                                    <tr class="text-center">
                                        <th colspan="6">แถวแนวขวาง</th>
                                        <th colspan="6">แถวแนวนอน</th>
                                        <th rowspan="2">รวม</th>
                                        <th rowspan="2">ยอดรวม</th>
                                        <th rowspan="2" >Note</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>แถว</th>
                                        <th>Product</th>
                                        <th>ฐาน</th>
                                        <th>สูง</th>
                                        <th>ฝาก</th>
                                        <th>รวม</th>
                                        <th>แถว</th>
                                        <th>Product</th>
                                        <th>ฐาน</th>
                                        <th>สูง</th>
                                        <th>ฝาก</th>
                                        <th>รวม</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sumtotal = 0;
                                    @endphp
                                    @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                                        <tr style="text-align:right">
                                            <td style="text-align: center">{{ $item->row }}</td>
                                            <td style="text-align: left">{{ $item->productt->name ?? ''}}</td>
                                            <td>{{ $item->t_base }}</td>
                                            <td>{{ $item->t_height }}</td>
                                            <td>@php
                                                $texcess = 0;
                                                if($item->t_excess > 0){
                                                    $texcess += $item->t_excess;
                                                }
                                                if($item->t_excess1 > 0){
                                                    $texcess += $item->t_excess1;
                                                }
                                                if($item->t_excess2 > 0){
                                                    $texcess += $item->t_excess2;
                                                }
                                                @endphp
                                                {{ $texcess }}
                                            </td>
                                            <td>{{ $item->t_total }}</td>
                                            <td style="text-align: center">{{ $item->row }}</td>
                                            <td style="text-align:left">{{ $item->productl->name ?? ''}}</td>
                                            <td>{{ $item->l_base }}</td>
                                            <td>{{ $item->l_height }}</td>
                                            <td>@php
                                                $lexcess = 0;
                                                if($item->l_excess > 0){
                                                    $lexcess += $item->l_excess;
                                                }
                                                if($item->l_excess1 > 0){
                                                    $lexcess += $item->l_excess1;
                                                }
                                                if($item->t_excess2 > 0){
                                                    $lexcess += $item->l_excess2;
                                                }
                                                @endphp
                                                {{ $lexcess }}
                                            </td>
                                            <td>{{ $item->l_total }}</td>
                                            <td>{{ $item->all_total }}</td>
                                            <td>
                                                @php
                                                    $sumtotal += $item->all_total;
                                                @endphp
                                                {{ $sumtotal }}</td>
                                            <td>{{ $item->note }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table style="width: 100%" class="tbn">
                                <tr style="vertical-align: top">
                                    <td>
                                        <strong>หมายเหตุ</strong>
                                    </td>
                                </tr>
                                @php
                                    $l_row = array('l_excess'=>'lproduct', 'l_excess1'=>'l1product', 'l_excess2'=>'l2product');
                                    $r_row = array('t_excess'=>'tproduct', 't_excess1'=>'t1product', 't_excess2'=>'t2product');
                                @endphp
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                                    @foreach ($l_row as $key=>$value)
                                        @if($item->$key)
                                            <tr>
                                                <td style="width: 10%"></td>
                                                <td style="width: 90%">
                                                    แถวแนวนอน&nbsp;แถวที่&nbsp;{{ $item->row }}&nbsp;:&nbsp;{{ $item->$value->name ?? ''}}&nbsp;จำนวน&nbsp;{{ $item->$key }}&nbsp;กล่อง
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                                    @foreach ($r_row as $key=>$value)
                                        @if($item->$key)
                                            <tr>
                                                <td style="width: 10%"></td>
                                                <td style="width: 90%">
                                                    แถวแนวขวาง&nbsp;แถวที่&nbsp;{{ $item->row }}&nbsp;:&nbsp;{{ $item->$value->name ?? ''}}&nbsp;จำนวน&nbsp;{{ $item->$key }}&nbsp;กล่อง
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
