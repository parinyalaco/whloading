@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Use Mastert Plan to Plan Load') }} สถานะ: {{ $loadm->status }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadm->load_date}}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{$loadm->customertype->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Customer</strong> : {{$loadm->customer}}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{$loadm->order_no}}</div>
                        </div>
                         <form method="POST" action="{{ route('loadings.usemasterplanaction',$loadm->id) }}">
                            @csrf

                            <div class="form-group row mb-0">
                                <div class="col-md-3 ">
                                    <label for="master_plan_id"> Master Plan </label>
                                    {{ Form::select('master_plan_id',$masterplanlist, null, ['placeholder' => '==เลือก==', 'class' => 'form-control','required'=>true]) }}
                                </div>
                                 <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Use Master Plan') }}
                                    </button>
                                </div>
                            </div>
                         </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
