@extends('layouts.planload')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('สร้างPlan Load') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4"><strong>Load Date</strong> : {{$loadm->load_date}}</div>
                            <div class="col-md-4"><strong>Customer</strong> : {{$loadm->customer}}</div>
                            <div class="col-md-4"><strong>Order No</strong> : {{$loadm->order_no}}</div>
                        </div>
                        <form method="POST" action="{{ route('loadings.createdetailaction',$loadm->id) }}">
                            @csrf

                            {{ Form::hidden('load_m_id' , $loadm->id, ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                            {{ Form::hidden('main_type' , 'Normal', ['placeholder' => 'แถว', 'class' => 'form-control']) }}
                            <table class="table table-bordered">
                                <thead>
                                    <tr>

                                        <th colspan="2"> แถวแนวขวาง</th>

                                    </tr>
                                    <tr>
                                        <th class="w-25">รายการ</th>
                                        <th>ข้อมูล</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>พาเลท No</td>
                                        <td>
                                            {{ Form::text('pallet_no', null,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สินค้า</td>
                                        <td>
                                            {{ Form::select('t_product_m_id', $productlist, null,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control selectpicker' ,'data-live-search'=>'true']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฐาน</td>
                                        <td>
                                            {{ Form::number('t_base' , null,
                                            ['placeholder' => 'ฐาน',
                                            'class' => 'form-control calsum',
                                            'id' => 't_base']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สูง</td>
                                        <td>
                                            {{ Form::number('t_height' , null,
                                            ['placeholder' => 'สูง', 'class' => 'form-control calsum',
                                            'id' => 't_height']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ฝาก</td>
                                        <td>
                                            {{ Form::number('t_excess' , null,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control calsum',
                                            'id' => 't_excess']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lot</td>
                                        <td>
                                            {{ Form::text('p_lot', null,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Exp Date</td>
                                        <td>
                                            {{ Form::date('p_exp_date', null,
                                            ['placeholder' => '==เลือก==', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>ฝากสินค้า1</td>
                                        <td>
                                            {{ Form::select('t_product1_id', $productlist, null,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                            {{ Form::number('t_excess1' , null,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess1']) }}
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>ฝากสินค้า2</td>
                                        <td>
                                            {{ Form::select('t_product2_id', $productlist, null,
                                            ['placeholder' => '==เลือก==', 'class' => 'selectpicker form-control', 'data-live-search'=>"true" ]) }}
                                            {{ Form::number('t_excess2' , null,
                                            ['placeholder' => 'ฝาก', 'class' => 'form-control  calsum','id'=>'t_excess2']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>รวม</td>
                                        <td>
                                            {{ Form::number('t_total' , null, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'t_total']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>รวมทั้งหมด</td>
                                        <td>
                                            {{ Form::number('all_total' , null, ['placeholder' => '0', 'class' => 'form-control','readonly'=>true,'id'=>'all_total']) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Note</td>
                                        <td>
                                            {{ Form::text('note' , null, ['placeholder' => 'note', 'class' => 'form-control']) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group row mb-0">
                                <div class="col-md-3 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('สร้าง') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
