@extends('layouts.realtime')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
    </div>
    <div class="page-content">
        <input type="hidden" id="loadmid" value="{{$loadm->id}}" >
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ __('Plan Load') }} สถานะ: {{ $loadm->status }} </h3>
                                <h1>
                                    <span id="realcounter" >@if (isset($loadm->loadcounting->all_counting ))
                                        {{ number_format($loadm->loadcounting->all_counting,0,'.',',') }}
                                    @else
                                        0
                                    @endif</span> / <span>{{ number_format($loadm->countingbox()['all'],0,'.',',') }}</span>
                                    
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
