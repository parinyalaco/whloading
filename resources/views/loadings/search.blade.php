@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('ค้นหา Loading') }}</h3>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            @if (Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('error') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card">
                    <form method="GET" action="{{ url('/loadings/search') }}" accept-charset="UTF-8"
                        class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">

                            <label for="sdate" class="p-2">วันที่ Load</label>
                            <input type="date" class="form-control" name="sdate" placeholder="ค้นหาจาก"
                                value="{{ request('sdate') }}">
                            <label for="edate" class="p-2">ถึง</label>
                            <input type="date" class="form-control" name="edate" placeholder="ถึง"
                                value="{{ request('edate') }}">
                            <label for="customer" class="p-2">Customer</label>
                            <input type="text" class="form-control" name="customer" placeholder="Customer"
                                value="{{ request('customer') }}">
                            <label for="order" class="p-2">Order</label>
                            <input type="text" class="form-control" name="order" placeholder="Order"
                                value="{{ request('order') }}">
                            <span class="input-group-append p-2">
                                <button class="btn btn-secondary" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                    </form>
                    <div class="card-body table-responsive-lg d-none d-sm-block">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Order</th>
                                <th>Product</th>
                                <th>Details</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            @foreach ($loadms as $loadm)
                                <tr>
                                    <td>{{ $loadm->load_date }}</td>
                                    <td>{{ $loadm->customertype->name ?? '-' }} \ {{ $loadm->customer }}</td>
                                    <td>{{ $loadm->order_no }}</td>
                                    <td>
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($loadm->loadds as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                    </td>
                                    <td>{{ $loadm->owner_type }}</td>
                                    <td>{{ $loadm->status }}</td>
                                    <td>

                                        <a class="btn btn-secondary"
                                            href="{{ route('loadings.searchview', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> Image</a>
<a class="btn btn-secondary"
                                            href="{{ route('loadings.exportLoadingData', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> PDF</a>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('loadings.load_reason') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea id="reason" class="form-control col" name="reason" required></textarea>
                                            <input type="hidden" id="loadm_id" class="form-control col" name="loadm_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="pagination-wrapper"> {!! $loadms->appends(request()->input())->render()  !!} </div>

                    </div>

                    <div class="card-body table-responsive-lg d-block d-sm-none">
                        <table class="table table-bordered table-responsive">
                            <tr>
                                <th>Data</th>
                            </tr>
                            @foreach ($loadms as $loadm)
                                <tr>
                                    <td>{{ $loadm->load_date }}
                                        <br />
                                        {{ $loadm->customertype->name ?? '-' }} \ {{ $loadm->customer }}<br />
                                        {{ $loadm->order_no }}<br />
                                        @php
                                            $products = [];
                                        @endphp
                                        @foreach ($loadm->loadds as $item)
                                            @if (isset($item->productt->name))
                                                @if (isset($item->productt->name) && isset($products[$item->productt->name]))
                                                    @php
                                                        $products[$item->productt->name]['count'] = $products[$item->productt->name]['count'] + $item->t_base * $item->t_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productt->name]['count'] = +($item->t_base * $item->t_height);
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->productl->name))
                                                @if (isset($item->productl->name) && isset($products[$item->productl->name]))
                                                    @php
                                                        $products[$item->productl->name]['count'] = $products[$item->productl->name]['count'] + $item->l_base * $item->l_height;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->productl->name]['count'] = $item->l_base * $item->l_height;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->tproduct->name))
                                                @if (isset($item->tproduct->name) && isset($products[$item->tproduct->name]))
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $products[$item->tproduct->name]['count'] + $item->t_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->tproduct->name]['count'] = $item->t_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t1product->name))
                                                @if (isset($item->t1product->name) && isset($products[$item->t1product->name]))
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $products[$item->t1product->name]['count'] + $item->t_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t1product->name]['count'] = $item->t_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->t2product->name))
                                                @if (isset($item->t2product->name) && isset($products[$item->t2product->name]))
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $products[$item->t2product->name]['count'] + $item->t_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->t2product->name]['count'] = $item->t_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->lproduct->name))
                                                @if (isset($item->lproduct->name) && isset($products[$item->lproduct->name]))
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $products[$item->lproduct->name]['count'] + $item->l_excess;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->lproduct->name]['count'] = $item->l_excess;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l1product->name))
                                                @if (isset($item->l1product->name) && isset($products[$item->l1product->name]))
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $products[$item->l1product->name]['count'] + $item->l_excess1;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l1product->name]['count'] = $item->l_excess1;
                                                    @endphp
                                                @endif
                                            @endif
                                            @if (isset($item->l2product->name))
                                                @if (isset($item->l2product->name) && isset($products[$item->l2product->name]))
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $products[$item->l2product->name]['count'] + $item->l_excess2;
                                                    @endphp
                                                @else
                                                    @php
                                                        $products[$item->l2product->name]['count'] = $item->l_excess2;
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                        @foreach ($products as $product => $counter)
                                            {{ $product }} จำนวน {{ $counter['count'] }} กล่อง <br />
                                        @endforeach
                                        <br />{{ $loadm->owner_type }}<br />{{ $loadm->status }}
<br />
                                        <a class="btn btn-secondary"
                                            href="{{ route('loadings.searchview', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> Image</a>
<a class="btn btn-secondary"
                                            href="{{ route('loadings.exportLoadingData', $loadm->id) }}"><i
                                                class="bi bi-columns-gap"></i> PDF</a>


                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="{{ route('loadings.load_reason') }}" method="POST">
                                    @csrf
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                            <div id="test"></div>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea id="reason" class="form-control col" name="reason" required></textarea>
                                            <input type="hidden" id="loadm_id" class="form-control col"
                                                name="loadm_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pagination-wrapper  table-responsive-lg "> {!! $loadms->appends(request()->input())->render() !!} </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <script type="text/javascript" src="{{ asset('assets/bootstrap/js/jquery-1.7.1.min.js') }}"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function Myfunction() {
            var obj = <?php echo json_encode($to_alert); ?>;
            const values = Array
                .from(document.querySelectorAll('input[type="checkbox"]'))
                .filter((checkbox) => checkbox.checked)
                .map((checkbox) => checkbox.value);

            let mess = 'คุณต้องการส่งเมลล์ โดยมีใบโหลด \n';
            for (const [key, value] of Object.entries(values)) {
                mess += ' Date:' + obj[value].load_date + '  Customer:' + obj[value].customer + '  Order :' + obj[value]
                    .order_no + '\n';
            }
            mess += 'ใช่หรือไม่';
            console.log(values.length);
            if (values.length > 0) {
                if (confirm(mess) == true) {
                    var link_url = window.location.origin + window.location.pathname + "/sent_mail?load_m_id=" + values;
                    // var link_url = window.location.origin+"/whloading/loadings/sent_mail?load_m_id="+values;
                    console.log(link_url);

                    window.location.replace(link_url);
                }
            }
        }
    </script>
    <script type="text/javascript">
        function show_input(num, order) {
            console.log(num + '--' + order);
            document.getElementById('loadm_id').value = num;
            let box = document.getElementById('test');
            box.innerText = 'กรุณาระบุเหตุผลที่ต้องการคืนค่า ใบ order ที่ ' + order;
        }
    </script>
@endsection
