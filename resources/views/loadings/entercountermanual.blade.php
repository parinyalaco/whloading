@extends('layouts.main')

@section('content')

    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }} สถานะ: {{ $loadm->status }}</div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('loadings.entercountermanualAction', $loadm->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="accept_counting"
                                    class="col-md-2 col-form-label text-md-right">{{ __('จำนวนกล่องดี') }}</label>
                                <div class="col-md-4">
                                    @if (!empty($loadm->loadcounting->accept_counting))
                                        <input id="accept_counting" type="number"
                                            class="form-control @error('accept_counting') is-invalid @enderror" name="accept_counting"
                                            value="{{ $loadm->loadcounting->accept_counting }}" >
                                    @else
                                        <input id="accept_counting" type="number"
                                            class="form-control @error('accept_counting') is-invalid @enderror" name="accept_counting"
                                            value="0" >
                                    @endif


                                    @error('act_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <label for="reject_counting"
                                    class="col-md-2 col-form-label text-md-right">{{ __('จำนวนกล่องเสีย') }}</label>
                                <div class="col-md-4">
                                    @if (!empty($loadm->loadcounting->reject_counting))
                                        <input id="reject_counting" type="number"
                                            class="form-control @error('reject_counting') is-invalid @enderror" name="reject_counting"
                                            value="{{ $loadm->loadcounting->reject_counting }}" >
                                    @else
                                        <input id="reject_counting" type="number"
                                            class="form-control @error('reject_counting') is-invalid @enderror" name="reject_counting"
                                            value="0" >
                                    @endif


                                    @error('act_date')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12"><input class="btn btn-success" type="submit" value="บันทึก"></div>
                            </div>
                        </form>
                         <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{$loadm->load_date}}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{$loadm->customertype->name ?? '-'}}</div>
                            <div class="col-md-3"><strong>Customer</strong> : {{$loadm->customer}}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{$loadm->order_no}}</div>
                        </div>
                        <div class="table-responsive-lg">
                        <table class="table table-bordered">
                            <thead>
                                <tr>

                                    <th colspan="6" class="w-35 text-center">แถวแนวขวาง</th>
                                    <th colspan="6" class="w-35 text-center">แถวแนวนอน</th>
                                    <th rowspan="2" class="text-center">รวม</th>
                                    <th rowspan="2" class="text-center">ยอดรวม</th>
                                    <th rowspan="2" class="text-center">Note</th>
                                </tr>
                                <tr>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sumtotal = 0;
                                @endphp
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get()
        as $item)
                                    <tr>

                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productt->name ?? '' }}</td>
                                        <td>{{ $item->t_base }}</td>
                                        <td>{{ $item->t_height }}</td>
                                        <td>{{ $item->t_excess }}</td>
                                        <td>{{ $item->t_total }}</td>
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->productl->name ?? '' }}</td>
                                        <td>{{ $item->l_base }}</td>
                                        <td>{{ $item->l_height }}</td>
                                        <td>{{ $item->l_excess }}</td>
                                        <td>{{ $item->l_total }}</td>
                                        <td>{{ $item->all_total }}</td>
                                        <td>
                                            @php
                                                $sumtotal += $item->all_total;
                                            @endphp
                                            {{ $sumtotal }}</td>
                                        <td>{{ $item->note }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
