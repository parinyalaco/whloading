@extends('layouts.canvas')

@section('content')
    <div class="page-heading">
        <h3>{{ __('จัดการแผน Load') }}</h3>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('loadings.index') }}">Back</a>
        </div>
    </div>
    <div class="page-content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Plan Load') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3"><strong>Load Date</strong> : {{ $loadm->load_date }}</div>
                            <div class="col-md-3"><strong>Customer Type</strong> : {{ $loadm->customertype->name ?? '-' }}
                            </div>
                            <div class="col-md-3"><strong>Customer</strong> : {{ $loadm->customer }}</div>
                            <div class="col-md-3"><strong>Order No</strong> : {{ $loadm->order_no }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>วันที่ Load จริง</strong> : {{ $loadm->act_date }}</div>
                            <div class="col-md-4"><strong>หัวลาก</strong> : {{ $loadm->owner_type }}</div>
                            <div class="col-md-4"><strong>ทะเบียนหน้า</strong> : {{ $loadm->truck_license_plate }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>ทะเบียนหลัง</strong> : {{ $loadm->convoy_license_plate }}</div>
                            <div class="col-md-4"><strong>หมายเลขตู้</strong> : {{ $loadm->convoy_no }}</div>
                            <div class="col-md-4"><strong>หมายเลข Seal</strong> : {{ $loadm->seal_no }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><strong>พนักงานหน้าตู้</strong> :
                                @if (isset($teams['Front']))
                                    @foreach ($teams['Front'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานหลังตู้</strong>
                                @if (isset($teams['Back']))
                                    @foreach ($teams['Back'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงาน FL</strong>
                                @if (isset($teams['FL']))
                                    @foreach ($teams['FL'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานคุมตู้</strong>
                                @if (isset($teams['Ctrl']))
                                    @foreach ($teams['Ctrl'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>

                            <div class="col-md-4"><strong>พนักงานตรวจเช็คสินค้าก่อนโหลด</strong>
                                @if (isset($teams['Bfload']))
                                    @foreach ($teams['Bfload'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                            <div class="col-md-4"><strong>พนักงานตรวจเช็คกล่องแตก</strong>
                                @if (isset($teams['Bkbox']))
                                    @foreach ($teams['Bkbox'] as $item)
                                        <br />{{ $item->team->name ?? '-' }}
                                    @endforeach
                                @else
                                    -
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>วันเวลา เปิดตู้</strong> : {{ date('Y-m-d H:i', strtotime($loadm->open_box)) }}
                            </div>
                            <div class="col-md-4">
                                <strong>วันเวลา ปิดตู้</strong> : {{ date('Y-m-d H:i', strtotime($loadm->close_box)) }}
                            </div>
                            @php
                                $dateTimeObject1 = date_create($loadm->open_box);
                                $dateTimeObject2 = date_create($loadm->close_box);

                                $difference = date_diff($dateTimeObject1, $dateTimeObject2);
                            @endphp
                            <div class="col-md-4 @if ($dateTimeObject1 > $dateTimeObject2) text-danger @endif">
                                <strong>ใช้เวลารวม</strong> : @if ($dateTimeObject1 > $dateTimeObject2)
                                    -
                                @endif
                                {{ date('H:i', strtotime($difference->h . ':' . $difference->i)) }}
                                ชั่วโมง
                            </div>
                        </div>
                        @if (!empty($loadm->reason_box))
                            <div class="row" style="border:1px solid red;">
                                <div class="col-md-12">
                                    <strong>หมายเหตุ</strong> :
                                </div>
                                <div class="col-md-12 text-danger">
                                    {{ $loadm->reason_box }}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <strong>สายพานที่ใช้ในการโหลด</strong> : @if (!empty($loadm->conveyor_id))
                                    {{ $conveyor[$loadm->conveyor_id] }}@else-
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>ปิดการ Load โดย</strong> : @if (!empty($loadm->user_close))
                                    {{ $loadm->user->name }}@else-
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>กล่องแตก </strong> : จำนวน
                                @if ($loadm->broken_box > 0)
                                    {{ $loadm->broken_box }}
                                @else
                                    -
                                @endif
                                กล่อง <strong>รายละเอียด : </strong> {{ $loadm->broken_box_case }}
                            </div>

                            @foreach ($loadm->loadbrokens as $loadbrokenObj)
                                <div class="col-md-4">
                                    <strong>{{ $loadbrokenObj->brokentype->name }}</strong> : {{ $loadbrokenObj->box }}
                                    กล่อง
                                </div>
                            @endforeach
                        </div>

                        <div class="row">
                            @foreach ($pics as $runkey => $pic)
                                @php
                                    $picparamname = 'loading_img_path' . $runkey;
                                @endphp
                                @if (!empty($loadm->$picparamname))
                                    <div class="col-md-3">
                                        {{-- @php
                                            $picparamname = 'loading_img_path'.$runkey
                                        @endphp
                                        @if (!empty($loadm->$picparamname)) --}}
                                        <a href="{{ url($loadm->$picparamname) }}" target="_blank">
                                            <img width="100px" src="{{ url($loadm->$picparamname) }}">
                                        </a>
                                        {{-- @else
                                            -
                                        @endif --}}
                                        <br />{{ $pic }}
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <canvas id="my-house" width="600" height="{{ $loadm->loadds()->count() * 42 }}"></canvas>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    {{-- <th rowspan="2" class="text-center"><a class="btn btn-sm btn-primary"
                                            href="{{ route('loadings.createdetail', $loadm->id) }}">สร้าง</a></th> --}}
                                    <th rowspan="2" class="text-center">Product</th>
                                    <th colspan="5" class="w-25 text-center">แถวแนวขวาง</th>
                                    <th colspan="5" class="w-25 text-center">แถวแนวนอน</th>
                                    <th rowspan="2" class="text-center">รวม</th>
                                    <th rowspan="2" class="text-center">ยอดรวม</th>
                                    <th rowspan="2" class="text-center">Note</th>
                                </tr>
                                <tr>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                    <th class="text-center">แถว</th>
                                    <th class="text-center">ฐาน</th>
                                    <th class="text-center">สูง</th>
                                    <th class="text-center">ฝาก</th>
                                    <th class="text-center">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sumtotal = 0;
                                @endphp
                                @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
                                    <tr>
                                        {{-- <td>
                                            <a class="btn btn-sm btn-primary"
                                                href="{{ route('loadings.editdetail', $item->id) }}">Edit</a>
                                            <a class="btn btn-sm btn-danger"
                                                href="{{ route('loadings.deletedetail', $item->id) }}">Delete</a>
                                        </td> --}}
                                        <td>{{ $item->product->name }}
                                            ({{ $item->product->wide }}/{{ $item->product->long }}/{{ $item->product->height }})
                                        </td>
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->t_base }}</td>
                                        <td>{{ $item->t_height }}</td>
                                        <td>{{ $item->t_excess }}</td>
                                        <td>{{ $item->t_total }}</td>
                                        <td>{{ $item->row }}</td>
                                        <td>{{ $item->l_base }}</td>
                                        <td>{{ $item->l_height }}</td>
                                        <td>{{ $item->l_excess }}</td>
                                        <td>{{ $item->l_total }}</td>
                                        <td>{{ $item->all_total }}</td>
                                        <td>
                                            @php
                                                $sumtotal += $item->all_total;
                                            @endphp
                                            {{ $sumtotal }}</td>
                                        <td>{{ $item->note }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        const canvas = document.getElementById('my-house');
        const ctx = canvas.getContext('2d');

        // Set line width
        ctx.lineWidth = 1;



        // box
        @php
            $maxx = 0;
            $maxy = 0;
            $ptStartX = 4;
                $ptStartY = 4;
                $ptNextY = 0;
        @endphp

        @foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $item)
            @php
                $lastx = 0;

                $ptNextX = 0;
            @endphp

            @if ($item->l_base > 0)
                ctx.beginPath();
                @php
                    $spinFlag = false;
                @endphp
                @for ($j = 0; $j < $item->l_base; $j++)
                    @if ($item->note == 'SPIN')
                        ctx.strokeRect({{ $ptNextX + $ptStartX }},
                            {{ $ptNextY + $ptStartY }},
                            {{ $item->product->long }}, {{ $item->product->wide }});

                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->l_height }}',
                            {{ $ptNextX + ($item->product->long / 2) - 5 }},
                            {{ $ptNextY + ($item->product->wide / 2) + 3 }});

                        @php

                            if ($lastx - 5 + $j * $item->product->long > $maxx) {
                                $maxx = $lastx - 5 + $j * $item->product->long;
                            }
                            if ($item->row * $item->product->wide > $maxy) {
                                $maxy = $item->row * $item->product->wide;
                            }
                            $spinFlag = true;
                            $spinheight = $item->product->long;

                            $ptNextX += $item->product->long;
                        @endphp
                    @else
                        @if ($spinFlag)
                            ctx.strokeRect({{ $ptNextX + $ptStartX }},
                                {{ $ptNextY + $ptStartY }},
                                {{ $item->product->wide }}, {{ $item->product->long }});
                            @php
                                $spinFlag = false;
                            @endphp
                        @else
                            ctx.strokeRect({{ $ptNextX + $ptStartX }},
                                {{ $ptNextY + $ptStartY}},
                                {{ $item->product->wide }}, {{ $item->product->long }});
                        @endif



                        ctx.fillStyle = "black";
                        ctx.fillText('{{ $item->l_height }}',
                            {{ $ptNextX + $ptStartX + ($item->product->wide / 2) - 5 }},
                            {{ $ptNextY + $ptStartY + ($item->product->long / 2) + 3 }});

                        @php
                            if ($lastx - 5 + $j * $item->product->wide > $maxx) {
                                $maxx = $lastx - 5 + $j * $item->product->wide;
                            }
                            if ($item->row * $item->product->long > $maxy) {
                                $maxy = $item->row * $item->product->long;
                            }
                            $ptNextX += $item->product->wide;
                        @endphp
                    @endif
                @endfor

                @php
                    if ($spinFlag){
                        $ptNextY += $item->product->wide;
                    }else{
                        $ptNextY += $item->product->long;
                    }


                    $lastx = $lastx - 5 + $item->l_base * $item->product->wide;
                @endphp
                ctx.strokeStyle = "black";
                ctx.closePath();
            @endif

            @if ($item->l_excess > 0 || $item->l_excess1 > 0 || $item->l_excess2 > 0)
                ctx.beginPath();
                ctx.strokeStyle = "red";
                ctx.strokeRect({{ $lastx + $item->product->wide }}, {{ $item->row * $item->product->long }},
                    {{ $item->product->wide }}, {{ $item->product->long }});
                ctx.fillStyle = "red";
                @php
                    $lexcesssum = 0;
                    if ($item->l_excess > 0) {
                        $lexcesssum += $item->l_excess;
                    }
                    if ($item->t_excess1 > 0) {
                        $lexcesssum += $item->l_excess1;
                    }
                    if ($item->l_excess2 > 0) {
                        $lexcesssum += $item->l_excess2;
                    }
                @endphp
                ctx.fillText('{{ $lexcesssum }}', {{ $lastx + $item->product->wide + $item->product->wide / 2 }},
                    {{ $item->row * $item->product->long + $item->product->long / 2 }});
                ctx.closePath();
                @php
                    $lastx = $lastx + $item->product->wide;
                @endphp
            @endif



            // ctx.beginPath();
            // ctx.strokeStyle = "blue";
            // ctx.arc({{ $lastx + 50 }}, {{ $item->row * $item->product->long + 1 }}, 14, 0, 2 * Math.PI);
            // ctx.fillStyle = "blue";
            // ctx.fillText('{{ $item->all_total }}', {{ $lastx + $item->product->long }},
            //     {{ $item->row * $item->product->wide + 1 }});
            // ctx.closePath();
            // ctx.stroke();
        @endforeach
        ctx.beginPath();
        ctx.strokeStyle = "blue";
        ctx.strokeRect(2, 2,
            228, 1154);
        ctx.fillStyle = "black";
        ctx.closePath();
        ctx.stroke();
    </script>
@endsection
