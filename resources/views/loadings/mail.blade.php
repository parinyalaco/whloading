<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รายงาน</title>
    <style>
        /* table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        } */
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    
</head>
<body style="font-family: Cordia New; font-size:160%;">
    <p><strong>TO..ALL</strong></p>
    <p><strong>รายงานการ Load สินค้า โดยมีรายการดังนี้</strong></p>    
    <p style="text-indent: 2.5em;">
        <table style="width: 80%">
            <thead>
                <tr style="text-align: center; background-color:#b1d3f5">
                    <th class="cell"></th>
                    <th class="cell">วันที่</th>
                    <th class="cell">Customer</th>
                    <th class="cell">Order</th> 
                    <th class="cell">ใช้เวลา</th>
                    <th class="cell">หมายเหตุ</th>                       
                    <th class="cell">ไฟล์แนบ</th>
                </tr>
            </thead>
            <tbody>           
                @if(!empty($load_m))
                    @php
                        $id = 0;
                    @endphp
                    @foreach ($load_m as $kload)
                        <tr>
                            <td class="cell">{{ $id+1 }}</td>
                            <td class="cell" style="text-align: center;">{{ $kload->load_date }}</td>
                            <td class="cell" style="text-align: left;">{{ $kload->customertype->name ?? '-' }}</td>
                            <td class="cell" style="text-align: left;">{{ $kload->order_no }}</td>
                            @php
                                $dateTimeObject1 = date_create($kload->open_box); 
                                $dateTimeObject2 = date_create($kload->close_box); 
                                
                                $difference = date_diff($dateTimeObject1, $dateTimeObject2); 
                            @endphp
                            <td>@if($dateTimeObject1 > $dateTimeObject2)'-'@else{{ date('H:i', strtotime($difference->h.':'.$difference->i)) }} ชั่วโมง@endif</td>
                            <td class="cell" style="text-align: left;">{{ $kload->reason_box }}</td>
                            <td class="cell" style="text-align: left;">{{ $file_all[$id] }}</td>
                        </tr>  
                        @php
                            $id++;
                        @endphp                                                         
                    @endforeach
                @endif
            </tbody>
        </table>
    </p>
    <br>
    
    <p><strong>Laco Warehouse System</strong><br/></p>
    
</body>

</html>
