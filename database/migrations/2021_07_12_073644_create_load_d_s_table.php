<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('load_m_id');
            $table->integer('product_id');
            $table->string('main_type');
            $table->integer('row');
            $table->integer('t_base')->nullable();
            $table->integer('t_height')->nullable();
            $table->integer('t_excess')->nullable();
            $table->integer('t_product_id')->nullable();
            $table->integer('t_total')->nullable();
            $table->integer('l_product_m_id')->nullable();
            
            $table->integer('l_base')->nullable();
            $table->integer('l_height')->nullable();
            $table->integer('l_excess')->nullable();
            $table->integer('l_product_id')->nullable();
            $table->integer('l_total')->nullable();
            $table->integer('all_total')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_d_s');
    }
}
