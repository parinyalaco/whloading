<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_m_s', function (Blueprint $table) {
            $table->id();
            $table->date('load_date');
            $table->string('customer')->nullable();
            $table->string('order_no')->nullable();
            $table->string('owner_type')->nullable();
            $table->string('truck_license_plate')->nullable();
            $table->string('convoy_license_plate')->nullable();
            $table->string('convoy_no')->nullable();
            $table->string('seal_no')->nullable();
            $table->datetime('open_box')->nullable();
            $table->datetime('close_box')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_m_s');
    }
}
