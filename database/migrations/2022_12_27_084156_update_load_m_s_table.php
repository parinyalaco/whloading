<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLoadMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_m_s',
            function (Blueprint $table) {
                $table->integer('container_type_id')->nullable()->constrained('container_types');
                $table->string('loading_img20')->nullable();
                $table->string('loading_img_path20')->nullable();
                $table->string('loading_img21')->nullable();
                $table->string('loading_img_path21')->nullable();
                $table->string('loading_img22')->nullable();
                $table->string('loading_img_path22')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_m_s', 'container_type_id')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                // $table->dropForeign(['container_type_id']);
                $table->dropColumn('container_type_id');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img20')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img20');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img_path20')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path20');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img21')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img21');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img_path21')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path21');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img22')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img22');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img_path22')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path22');
            });
        }
    }
}
