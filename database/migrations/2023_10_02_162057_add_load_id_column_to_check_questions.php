<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoadIdColumnToCheckQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_questions', function (Blueprint $table) {
            $table->string('required_flag')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('check_questions', 'required_flag')) {
            Schema::table('check_questions', function (Blueprint $table) {
                $table->dropColumn('required_flag');
            });
        }
    }
}
