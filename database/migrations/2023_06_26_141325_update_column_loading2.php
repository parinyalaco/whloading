<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnLoading2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_d_s',
            function (Blueprint $table) {
                $table->integer('c_product_id')->nullable();
                $table->integer('c_base')->nullable();
                $table->integer('c_height')->nullable();
                $table->integer('c_total')->nullable();
                $table->integer('c_product_m_id')->nullable();
                $table->integer('c_product1_id')->nullable();
                $table->integer('c_product2_id')->nullable();
                $table->integer('c_excess')->nullable();
                $table->integer('c_excess1')->nullable();
                $table->integer('c_excess2')->nullable();
                $table->string('t_box_style',100)->nullable();
                $table->string('tx_box_style',100)->nullable();
                $table->string('l_box_style',100)->nullable();
                $table->string('lx_box_style',100)->nullable();
                $table->string('c_box_style', 100)->nullable();
                $table->string('cx_box_style', 100)->nullable();
            }
        );



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $columnsArray =['c_product_id','c_base','c_height','c_total','c_product_m_id',
        'c_product1_id','c_product2_id','c_excess','c_excess1','c_excess2','t_box_style',
        'tx_box_style','l_box_style','lx_box_style','c_box_style', 'cx_box_style'];

        foreach ($columnsArray as $key => $value) {
            if (Schema::hasColumn('load_d_s', $value)) {
                Schema::table('load_d_s', function (Blueprint $table, $value) {
                    $table->dropColumn($value);
                });
            }
        }

    }
}
