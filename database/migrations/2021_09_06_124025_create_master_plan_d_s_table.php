<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterPlanDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_plan_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('master_plan_m_id');
            $table->integer('product_id')->nullable();
            $table->string('main_type');
            $table->integer('row');
            $table->integer('t_product_m_id')->nullable();
            $table->integer('t_base')->nullable();
            $table->integer('t_height')->nullable();
            $table->integer('t_excess')->nullable();
            $table->integer('t_product_id')->nullable();
            $table->integer('t_total')->nullable();
            $table->integer('l_product_m_id')->nullable();
            $table->integer('l_base')->nullable();
            $table->integer('l_height')->nullable();
            $table->integer('l_excess')->nullable();
            $table->integer('l_product_id')->nullable();
            $table->integer('l_total')->nullable();
            $table->integer('all_total')->nullable();
            $table->string('note')->nullable();
            $table->integer('t_product1_id')->nullable();
            $table->integer('t_product2_id')->nullable();
            $table->integer('l_product1_id')->nullable();
            $table->integer('l_product2_id')->nullable();
            $table->integer('t_excess1')->nullable();
            $table->integer('t_excess2')->nullable();
            $table->integer('l_excess1')->nullable();
            $table->integer('l_excess2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_plan_d_s');
    }
}
