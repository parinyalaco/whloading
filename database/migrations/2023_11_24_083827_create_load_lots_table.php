<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_lots', function (Blueprint $table) {
            $table->id();
            $table->integer('load_m_id');
            $table->integer('product_id');
            $table->integer('seq')->nullable();
            $table->integer('start_no')->nullable();
            $table->integer('end_no')->nullable();
            $table->integer('quantity')->nullable();
            $table->date('exp_date')->nullable();
            $table->date('best_date')->nullable();
            $table->string('lot')->nullable();
            $table->string('remark')->nullable();
            $table->string('status', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_lots');
    }
}
