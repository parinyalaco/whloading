<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadDeliveryPlanDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_delivery_plan_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('upload_delivery_plan_m_id');
            $table->date('loading');
            $table->string('order');
            $table->string('license_plate_tail');
            $table->string('license_plate_head');
            $table->string('truck');
            $table->string('container_no');
            $table->string('seal_no');
            $table->string('driver1');
            $table->string('driver2');
            $table->string('status',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_delivery_plan_d_s');
    }
}
