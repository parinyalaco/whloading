<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table(
            'products',
            function (Blueprint $table) {
                $table->string('customer')->nullable();
                $table->integer('numperbox')->nullable();
                $table->float('bagweight')->nullable();
                $table->string('underboxcode')->nullable();
                $table->float('errorheight')->nullable();
                $table->float('errorwidth')->nullable();
                $table->float('errorlong')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('products', 'customer')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('customer');
            });
        }
        if (Schema::hasColumn('products', 'numperbox')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('numperbox');
            });
        }
        if (Schema::hasColumn('products', 'bagweight')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('bagweight');
            });
        }
        if (Schema::hasColumn('products', 'underboxcode')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('underboxcode');
            });
        }
        if (Schema::hasColumn('products', 'errorheight')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('errorheight');
            });
        }
        if (Schema::hasColumn('products', 'errorwidth')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('errorwidth');
            });
        }
        if (Schema::hasColumn('products', 'errorlong')) {
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('errorlong');
            });
        }
    }
}
