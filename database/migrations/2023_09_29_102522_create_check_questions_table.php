<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_questions', function (Blueprint $table) {
            $table->id();
            $table->integer('check_set_id');
            $table->string('name');
            $table->integer('seq');
            $table->string('type_question');
            $table->string('ans1_column')->nullable();
            $table->string('ans2_column')->nullable();
            $table->string('ans3_column')->nullable();
            $table->string('desc')->nullable();
            $table->string('status', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_questions');
    }
}
