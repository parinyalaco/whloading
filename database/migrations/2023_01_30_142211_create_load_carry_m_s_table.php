<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadCarryMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_carry_m_s', function (Blueprint $table) {
            $table->id();
            $table->date('load_date');
            $table->date('act_date')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('ext_wh_id');
            $table->integer('crop_id')->nullable();
            $table->string('track_type',50)->nullable();
            $table->integer('num_of_product')->nullable();
            $table->float('weight_per_product')->nullable();
            $table->float('total_weight')->nullable();
            $table->string('truck_license_plate')->nullable();
            $table->string('convoy_license_plate')->nullable();
            $table->string('usage', 100)->nullable();
            $table->datetime('open_box')->nullable();
            $table->datetime('close_box')->nullable();
            $table->float('open_temperature')->nullable();
            $table->float('delivery_temperature')->nullable();
            $table->float('boc_close_temperature')->nullable();
            $table->float('boc_delivery_temperature')->nullable();
            $table->float('first_pallet_temperature')->nullable();
            $table->float('middle_pallet_temperature')->nullable();
            $table->float('last_pallet_temperature')->nullable();
            $table->string('note')->nullable();
            $table->string('status',50);
            $table->float('broken_box')->nullable();
            $table->string('broken_box_case')->nullable();
            $table->string('loading_img1')->nullable();
            $table->string('loading_img_path1')->nullable();
            $table->string('loading_img2')->nullable();
            $table->string('loading_img_path2')->nullable();
            $table->string('loading_img3')->nullable();
            $table->string('loading_img_path3')->nullable();
            $table->string('loading_img4')->nullable();
            $table->string('loading_img_path4')->nullable();
            $table->string('loading_img5')->nullable();
            $table->string('loading_img_path5')->nullable();
            $table->string('loading_img6')->nullable();
            $table->string('loading_img_path6')->nullable();
            $table->string('loading_img7')->nullable();
            $table->string('loading_img_path7')->nullable();
            $table->string('loading_img8')->nullable();
            $table->string('loading_img_path8')->nullable();
            $table->string('loading_img9')->nullable();
            $table->string('loading_img_path9')->nullable();
            $table->string('loading_img10')->nullable();
            $table->string('loading_img_path10')->nullable();
            $table->string('loading_img11')->nullable();
            $table->string('loading_img_path11')->nullable();
            $table->string('loading_img12')->nullable();
            $table->string('loading_img_path12')->nullable();
            $table->string('loading_img13')->nullable();
            $table->string('loading_img_path13')->nullable();
            $table->string('loading_img14')->nullable();
            $table->string('loading_img_path14')->nullable();
            $table->string('loading_img15')->nullable();
            $table->string('loading_img_path15')->nullable();
            $table->string('loading_img16')->nullable();
            $table->string('loading_img_path16')->nullable();
            $table->string('loading_img17')->nullable();
            $table->string('loading_img_path17')->nullable();
            $table->string('loading_img18')->nullable();
            $table->string('loading_img_path18')->nullable();
            $table->string('loading_img19')->nullable();
            $table->string('loading_img_path19')->nullable();
            $table->string('loading_img20')->nullable();
            $table->string('loading_img_path20')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_carry_m_s');
    }
}
