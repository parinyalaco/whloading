<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addimagecolumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_carry_m_s',
            function (Blueprint $table) {
                $table->string('loading_img21')->nullable();
                $table->string('loading_img_path21')->nullable();
                $table->string('loading_img22')->nullable();
                $table->string('loading_img_path22')->nullable();
                $table->string('loading_img23')->nullable();
                $table->string('loading_img_path23')->nullable();
                $table->string('loading_img24')->nullable();
                $table->string('loading_img_path24')->nullable();
                $table->string('loading_img25')->nullable();
                $table->string('loading_img_path25')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_carry_m_s', 'loading_img21')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img21');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img22')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img22');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img23')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img23');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img24')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img24');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img25')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img25');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img_path21')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path21');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img_path22')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path22');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img_path23')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path23');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img_path24')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path24');
            });
        }
        if (Schema::hasColumn('load_carry_m_s', 'loading_img_path25')) {
            Schema::table('load_carry_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path25');
            });
        }
    }
}
