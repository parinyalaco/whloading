<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLoadD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('load_d_s', function (Blueprint $table) {
            $table->string('pallet_no')->nullable();
            $table->string('p_lot')->nullable();
            $table->date('p_exp_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_d_s', 'pallet_no')) {
            Schema::table('load_d_s', function (Blueprint $table) {
                $table->dropColumn('pallet_no');
            });
        }
        if (Schema::hasColumn('load_d_s', 'p_lot')) {
            Schema::table('load_d_s', function (Blueprint $table) {
                $table->dropColumn('p_lot');
            });
        }
        if (Schema::hasColumn('load_d_s', 'p_exp_date')) {
            Schema::table('load_d_s', function (Blueprint $table) {
                $table->dropColumn('p_exp_date');
            });
        }
    }
}
