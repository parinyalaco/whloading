<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadingImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loading_images', function (Blueprint $table) {
            $table->id();
            $table->integer('load_m_id');
            $table->integer('seq');
            $table->string('typename');
            $table->integer('typeseq');
            $table->string('loading_img');
            $table->string('loading_img_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loading_images');
    }
}
