<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListLoadDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_load_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('list_load_m_id');
            $table->integer('sort')->nullable();
            $table->integer('st_location_id');
            $table->integer('st_type_id');
            $table->integer('product_id');
            $table->string('storage_bin');
            $table->string('pallet_no');
            $table->string('batch');
            $table->date('gr_date');
            $table->date('mfg_date');
            $table->date('exp_date');
            $table->integer('laco_batch_id');
            $table->string('box_no');
            $table->integer('shipment_id');
            $table->integer('ur_qty');
            $table->integer('block_qty')->default(0);
            $table->integer('qi_qty')->default(0);
            $table->integer('available_stock');
            $table->integer('status_id');
            $table->integer('usage_id');
            $table->string('sales_doc')->nullable();
            $table->string('dales_doc_item')->nullable();
            $table->string('inspection_los')->nullable();
            $table->text('note')->nullable();
            $table->string('list_img')->nullable();
            $table->string('list_img_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_load_d_s');
    }
}
