<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add2fileAtPlanM extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_m_s',
            function (Blueprint $table) {
                $table->string('loading_img11')->nullable();
                $table->string('loading_img_path11')->nullable();
                $table->string('loading_img12')->nullable();
                $table->string('loading_img_path12')->nullable();
                $table->string('loading_img13')->nullable();
                $table->string('loading_img_path13')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_m_s', 'loading_img11')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img11');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img_path11')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path11');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img12')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img12');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img_path12')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path12');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img13')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img13');
            });
        }
        if (Schema::hasColumn('load_m_s', 'loading_img_path13')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('loading_img_path13');
            });
        }
    }
}
