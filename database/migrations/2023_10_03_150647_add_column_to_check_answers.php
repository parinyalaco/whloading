<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCheckAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_answers', function (Blueprint $table) {
            $table->integer('check_ans_m_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('check_answers', 'check_ans_m_id')) {
            Schema::table('check_answers', function (Blueprint $table) {
                $table->dropColumn('check_ans_m_id');
            });
        }
    }
}
