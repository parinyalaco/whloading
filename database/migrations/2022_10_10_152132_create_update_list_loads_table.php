<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateListLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'list_load_m_s',
            function (Blueprint $table) {                
                $table->string('customer_po')->nullable();
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
        Schema::table(
            'list_load_d_s',
            function (Blueprint $table) {                
                $table->foreignId('user_id')->nullable()->constrained('users');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('list_load_m_s', 'customer_po')) {
            Schema::table('list_load_m_s', function (Blueprint $table) {
                $table->dropColumn('customer_po');
            });
        }
        if (Schema::hasColumn('list_load_m_s', 'user_id')) {
            Schema::table('list_load_m_s', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
        if (Schema::hasColumn('list_load_d_s', 'user_id')) {
            Schema::table('list_load_d_s', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        }
    }
}
