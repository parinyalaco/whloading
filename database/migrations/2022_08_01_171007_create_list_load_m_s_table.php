<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListLoadMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_load_m_s', function (Blueprint $table) {
            $table->id();
            $table->date('list_date');
            $table->string('shipment_order');
            $table->string('container_no');
            $table->string('seal_no');
            $table->string('supplier_tran');
            $table->string('car_head');
            $table->string('car_detail');
            $table->string('driver_name');
            $table->dateTime('return_datetime');
            $table->float('weight_null')->nullable();
            $table->float('weight_load')->nullable();
            $table->float('net_weight')->nullable();
            $table->string('list_img')->nullable();
            $table->string('list_img_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_load_m_s');
    }
}
