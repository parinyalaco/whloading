<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addfilecolumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_carry_m_s',
            function (Blueprint $table) {
                $table->string('loading_img26')->nullable();
                $table->string('loading_img_path26')->nullable();
                $table->string('loading_img27')->nullable();
                $table->string('loading_img_path27')->nullable();
                $table->string('loading_img28')->nullable();
                $table->string('loading_img_path28')->nullable();
                $table->string('loading_img29')->nullable();
                $table->string('loading_img_path29')->nullable();
                $table->string('loading_img30')->nullable();
                $table->string('loading_img_path30')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        for ($i=26; $i <= 30 ; $i++) {
            if (Schema::hasColumn('load_carry_m_s', 'loading_img'.$i)) {
                Schema::table('load_carry_m_s', function (Blueprint $table,$i) {
                    $table->dropColumn('loading_img'.$i);
                });
            }
            if (Schema::hasColumn('load_carry_m_s', 'loading_img_path'.$i)) {
                Schema::table('load_carry_m_s', function (Blueprint $table,$i) {
                    $table->dropColumn('loading_img_path'.$i);
                });
            }
        }

    }
}
