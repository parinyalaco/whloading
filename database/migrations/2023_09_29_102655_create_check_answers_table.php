<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('check_point_id');
            $table->integer('check_questions_id');
            $table->integer('seq');
            $table->string('type_question');
            $table->string('ans1_column')->nullable();
            $table->string('ans2_column')->nullable();
            $table->string('ans3_column')->nullable();
            $table->float('custom_f_1')->nullable();
            $table->float('custom_f_2')->nullable();
            $table->float('custom_f_3')->nullable();
            $table->string('custom_s_1')->nullable();
            $table->string('custom_s_2')->nullable();
            $table->string('custom_s_3')->nullable();
            $table->text('custom_t_1')->nullable();
            $table->text('custom_t_2')->nullable();
            $table->text('custom_t_3')->nullable();
            $table->string('status', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_answers');
    }
}
