<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateLoadMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_m_s',
            function (Blueprint $table) {
                $table->integer('conveyor_id')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_m_s', 'conveyor_id')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('conveyor_id');
            });
        }
    }
}
