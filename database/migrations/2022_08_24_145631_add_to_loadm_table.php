<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToLoadmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_m_s',
            function (Blueprint $table) {
                $table->integer('user_close')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_m_s', 'user_close')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('user_close');
            });
        }
    }
}
