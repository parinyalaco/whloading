<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnLoading extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'load_m_s',
            function (Blueprint $table) {
                $table->string('type_loading',100)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('load_m_s', 'type_loading')) {
            Schema::table('load_m_s', function (Blueprint $table) {
                $table->dropColumn('type_loading');
            });
        }
    }
}
