<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoadIdColumnToCheckAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('check_answers', function (Blueprint $table) {
            $table->integer('load_m_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('check_answers', 'load_m_id')) {
            Schema::table('check_answers', function (Blueprint $table) {
                $table->dropColumn('load_m_id');
            });
        }
    }
}
