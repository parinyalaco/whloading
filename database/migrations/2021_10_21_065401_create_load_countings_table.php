<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadCountingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('load_countings', function (Blueprint $table) {
            $table->id();
            $table->integer('load_m_id');
            $table->integer('all_counting');
            $table->integer('reject_counting')->nullable();
            $table->integer('accept_counting')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('load_countings');
    }
}
