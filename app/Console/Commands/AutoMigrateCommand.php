<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\LoadM;
use App\Models\LoadingImage;

class AutoMigrateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automigrate:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command automigrate:images use to migraate image old style to new version';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $pics = array(
            1 => "รูปวัดแถวแนวยาว",
            2 => "รูปวัดแถวแนวขวาง",
            3 => "รูปวัดแถวที่ 10",
            4 => "รูปวัดแถวที่ 20",
            5 => "รูปป้ายโหลด",
            6 => "รูปท้ายตู้",
            7 => "รูปสินค้าตัวอย่าง",
            8 => "รูปการใส่ถุงลม",
            9 => "รูปล็อค SEAL",
            10 => "รูปท้ายรถหลังล็อค Seal",
            11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
            12 => "ใบ Lot โหลด",
            13 => "Plan โหลด(สำหรับเก็บ plan ที่มีการเซ็นชื่อ)",
            14 => "แถวสุดท้าย สินค้า Product 1",
            15 => "แถวสุดท้าย สินค้า Product 2",
            16 => "แถวสุดท้าย  สินค้า Product 3",
            17 => "รูปปัญหาที่พบในการโหลด รูปที่ 1",
            18 => "รูปปัญหาที่พบในการโหลด รูปที่ 2  ",
            19 => "รูปปัญหาที่พบในการโหลด รูปที่ 3",
            20 => "รูปการตรวจตู้ 1",
            21 => "รูปการตรวจตู้ 2",
            22 => "รูปการตรวจตู้ 3",
        );

        $allLoadData = LoadM::all();

        $transaction = 1;

        foreach ($allLoadData as $loadMObj) {
            $imageTmps = [];
            foreach ($pics as $pickey => $picvalue) {
                $imageName = "loading_img". $pickey;
                $imagePath = "loading_img_path" . $pickey;
                if(isset($loadMObj->$imageName) && isset($loadMObj->$imagePath)){
                    echo $transaction;
                    echo " : ".$loadMObj->order_no ;
                    echo " : " . $loadMObj->$imageName;
                    echo " : " . $loadMObj->$imagePath . "\n";
                    $transaction++;

                    $imageTmp = [];
                    $imageTmp['load_m_id'] = $loadMObj->id;
                    $imageTmp['loading_img'] = $loadMObj->$imageName;
                    $imageTmp['loading_img_path'] = $loadMObj->$imagePath;
                    $imageTmp['seq'] = $pickey;
                    $imageTmp['typename'] =  $picvalue;
                    $imageTmp['typeseq'] = 1;
                    $imageTmp['created_at'] = date('Y-m-d H:i:s');
                    $imageTmp['updated_at'] = date('Y-m-d H:i:s');

                    $imageTmps[] = $imageTmp;
                }
            }
            LoadingImage::insert($imageTmps);
        }

        return 0;
    }
}

