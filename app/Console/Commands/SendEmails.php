<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\LoadingsController;
use App\Models\LoadM;
use Illuminate\Http\Request;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WHLoad:AutoReportPlanLoad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail auto plan load';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_send = date ("Y-m-d", strtotime("-1 day"));
        // $date_send = date ("Y-m-d", strtotime("-1 day", strtotime('2021-08-26')));
        // dd($date_send);
        $loadm = LoadM::where('load_date', $date_send)->where('status', 'Loaded')->orderBy('open_box')->get();
        $load_id = '';
        if(count($loadm)>0){
            foreach ($loadm as $key) {
                $load_id .= $key->id.',';
            }
            // $load_id = "20735,20736,20737,220738,20739,20740";
            // dd($load_id);
            $controller = new LoadingsController();
            $content = new Request();
            $content->load_m_id = $load_id;
            $controller->sent_mail($content);
        }else{
            echo 'No Data!!';
        }
        
    }
}
