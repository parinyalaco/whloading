<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoadM;
use App\Models\CheckSet;

class CheckPointPublicController extends Controller
{
    public function scan(){
        return view('publics.scan');
    }

    public function create($loading_id, Request $request)
    {
        $location = $request->get('location');
        $loadm = LoadM::findOrFail($loading_id);

        $checkset = CheckSet::where('status', 'Active')->first();
        return view('checkanswers.create', compact('loadm', 'location', 'checkset'));
    }


    private function _resizeTextAdd($src, $name, $desc, $newWidth, $txt)
    {

        $img = imagecreatefromjpeg($src);

        $info = getimagesize($src);
        $mime = $info['mime'];

        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Unknown image type.');
        }

        $img = $image_create_func($src);
        list($width, $height) = getimagesize($src);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        $tmp = imagerotate($tmp, 270, 0);

        $dir = dirname(realpath(__FILE__));
        $sep = DIRECTORY_SEPARATOR;
        $fontFile = config('myconfig.font_link');
        // echo $fontFile;
        //$fontFile = realpath("arial.ttf"); //replace with your font
        $fontSize = 24;
        $fontColor = imagecolorallocate($tmp, 255, 255, 255);
        $black = imagecolorallocate($tmp, 0, 0, 0);
        $white = imagecolorallocate($tmp, 255, 255, 255);
        $angle = 0;

        $iWidth = imagesx($tmp);
        $iHeight = imagesy($tmp);

        $tSize = imagettfbbox($fontSize, $angle, $fontFile, $txt);
        $tWidth = max([$tSize[2], $tSize[4]]) - min([$tSize[0], $tSize[6]]);
        $tHeight = max([$tSize[5], $tSize[7]]) - min([$tSize[1], $tSize[3]]);

        // text is placed in center you can change it by changing $centerX, $centerY values
        $centerX = CEIL(($iWidth - $tWidth)) - 50;
        $centerX = $centerX < 0 ? 0 : $centerX;
        $centerY = CEIL(($iHeight - $tHeight)) - 50;
        $centerY = $centerY < 0 ? 0 : $centerY;

        imagettftext($tmp, $fontSize, $angle, $centerX, $centerY, $white, $fontFile, $txt);
        $image_save_func($tmp, "$desc$name");

        imagedestroy($img);
        imagedestroy($tmp);

        if (file_exists($src)) {
            unlink($src);
        }
    }
}
