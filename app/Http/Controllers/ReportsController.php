<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Models\LoadM;
use App\Models\PlanShip;
use App\Models\BrokenType;

use App\Models\CheckAnswer;
use App\Models\CheckAnsM;
use App\Models\CheckPoint;

use Illuminate\Support\Facades\DB;


class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadingdoc($load_m_id)
    {

        $loadm = LoadM::findOrFail($load_m_id);

        if ($loadm->type_loading == "3Rows") {
            $teams = array();
            foreach ($loadm->loadteams as $loadteamObj) {
                $teams[$loadteamObj->team_type][] = $loadteamObj;
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();


            $sheet->setCellValueByColumnAndRow(1, 1, "เอกสารบันทึกการ Load สินค้าแช่แข็ง");
            $sheet->mergeCellsByColumnAndRow(1, 1, 16, 1);

            $maintable = array();
            $maintable['start'] = 'B2';
            $maintable['start2'] = 'I2';
            $maintable['start3'] = 'P2';

            $sheet->setCellValueByColumnAndRow(2, 2, "แถวด้านซ้ายตู้");
            $sheet->mergeCellsByColumnAndRow(2, 2, 7, 2);

            $sheet->setCellValueByColumnAndRow(2, 3, "แถว");
            $sheet->setCellValueByColumnAndRow(3, 3, "Product");
            $sheet->setCellValueByColumnAndRow(4, 3, "แนว");
            $sheet->setCellValueByColumnAndRow(5, 3, "ฐาน");
            $sheet->setCellValueByColumnAndRow(6, 3, "สูง");
            $sheet->setCellValueByColumnAndRow(7, 3, "ฝาก");

            $sheet->setCellValueByColumnAndRow(9, 2, "แถวตรงกลาง");
            $sheet->mergeCellsByColumnAndRow(9, 2, 14, 2);

            $sheet->setCellValueByColumnAndRow(9, 3, "แถว");
            $sheet->setCellValueByColumnAndRow(10, 3, "Product");
            $sheet->setCellValueByColumnAndRow(11, 3, "แนว");
            $sheet->setCellValueByColumnAndRow(12, 3, "ฐาน");
            $sheet->setCellValueByColumnAndRow(13, 3, "สูง");
            $sheet->setCellValueByColumnAndRow(14, 3, "ฝาก");

            $sheet->setCellValueByColumnAndRow(16, 2, "แถวตรงกลาง");
            $sheet->mergeCellsByColumnAndRow(16, 2, 21, 2);

            $sheet->setCellValueByColumnAndRow(16, 3, "แถว");
            $sheet->setCellValueByColumnAndRow(17, 3, "Product");
            $sheet->setCellValueByColumnAndRow(18, 3, "แนว");
            $sheet->setCellValueByColumnAndRow(19, 3, "ฐาน");
            $sheet->setCellValueByColumnAndRow(20, 3, "สูง");
            $sheet->setCellValueByColumnAndRow(21, 3, "ฝาก");
            $sheet->setCellValueByColumnAndRow(22, 2, "ยอดรวม");
            $sheet->mergeCellsByColumnAndRow(22, 2,22, 3);

            $sheet->getStyle('A1:K3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            //ฝั่งขวา
            $sheet->setCellValueByColumnAndRow(24, 2, "Order no.");
            $sheet->setCellValueByColumnAndRow(25, 2, $loadm->order_no);

            $sheet->setCellValueByColumnAndRow(24, 4, "วันที่ทำการ Load");
            $sheet->setCellValueByColumnAndRow(25, 4, $loadm->act_date);
            $sheet->setCellValueByColumnAndRow(24, 5, "หัวลาก");
            $sheet->setCellValueByColumnAndRow(25, 5, $loadm->owner_type);
            $sheet->setCellValueByColumnAndRow(24, 6, "ทะเบียนหน้า");
            $sheet->setCellValueByColumnAndRow(25, 6, $loadm->truck_license_plate);
            $sheet->setCellValueByColumnAndRow(24, 7, "ทะเบียนหลัง");
            $sheet->setCellValueByColumnAndRow(25, 7, $loadm->convoy_license_plate);
            $sheet->setCellValueByColumnAndRow(24, 8, "หมายเลขตู้");
            $sheet->setCellValueByColumnAndRow(25, 8, $loadm->convoy_no);
            $sheet->setCellValueByColumnAndRow(24, 9, "หมายเลข Seal");
            $sheet->setCellValueByColumnAndRow(25, 9, $loadm->seal_no);
            $sheet->setCellValueByColumnAndRow(24, 10, "ประเภทตู้");
            $sheet->setCellValueByColumnAndRow(25, 10, $loadm->containertype->name ?? '-');

            $sheet->setCellValueByColumnAndRow(24, 12, "ผลิตภัณฑ์");
            $sheet->setCellValueByColumnAndRow(25, 12, "จำนวน/กล่อง");


            $startrow = 4;

            $totalbox = array();
            $totalline = 0;
            $notestr = array();
            foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $loaddobj) {

                //  print_r($totalbox);

                $sheet->setCellValueByColumnAndRow(2, $startrow, $loaddobj->row);

                if (($loaddobj->t_base > 0) && ($loaddobj->t_height > 0)) {
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $loaddobj->productt->name);
                    $sheet->setCellValueByColumnAndRow(4, $startrow, $loaddobj->t_box_style);
                    $sheet->setCellValueByColumnAndRow(5, $startrow, $loaddobj->t_base);
                    $sheet->setCellValueByColumnAndRow(6, $startrow, $loaddobj->t_height);

                    //$totalline += $loaddobj->t_total;

                    if (isset($totalbox[$loaddobj->productt->name])) {
                        $totalbox[$loaddobj->productt->name] += ($loaddobj->t_base *  $loaddobj->t_height);
                    } else {
                        $totalbox[$loaddobj->productt->name] = ($loaddobj->t_base *  $loaddobj->t_height);
                    }
                }
                if ($loaddobj->t_excess > 0 || $loaddobj->t_excess1 > 0 || $loaddobj->t_excess2 > 0) {
                    $ttotal = 0;
                    if ($loaddobj->t_excess > 0) {
                        $ttotal += $loaddobj->t_excess;
                        //$totalline += $loaddobj->t_excess;
                        $notestr['T'][$loaddobj->row][] =  " : " . $loaddobj->tproduct->name . " จำนวน " . $loaddobj->t_excess . " กล่อง";

                        if (isset($totalbox[$loaddobj->tproduct->name])) {
                            $totalbox[$loaddobj->tproduct->name] += $loaddobj->t_excess;
                        } else {
                            $totalbox[$loaddobj->tproduct->name] = $loaddobj->t_excess;
                        }
                    }
                    if ($loaddobj->t_excess1 > 0) {
                        $ttotal += $loaddobj->t_excess1;
                        //$totalline += $loaddobj->t_excess1;
                        $notestr['T'][$loaddobj->row][] =  " : " . $loaddobj->t1product->name . " จำนวน " . $loaddobj->t_excess1 . " กล่อง";

                        if (isset($totalbox[$loaddobj->t1product->name])) {
                            $totalbox[$loaddobj->t1product->name] += $loaddobj->t_excess1;
                        } else {
                            $totalbox[$loaddobj->t1product->name] = $loaddobj->t_excess1;
                        }
                    }
                    if ($loaddobj->t_excess2 > 0) {
                        $ttotal += $loaddobj->t_excess2;
                        //$totalline += $loaddobj->t_excess1;
                        $notestr['T'][$loaddobj->row][] =  " : " . $loaddobj->t2product->name . " จำนวน " . $loaddobj->t_excess2 . " กล่อง";

                        if (isset($totalbox[$loaddobj->t2product->name])) {
                            $totalbox[$loaddobj->t2product->name] += $loaddobj->t_excess2;
                        } else {
                            $totalbox[$loaddobj->t2product->name] = $loaddobj->t_excess2;
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(7, $startrow, $ttotal);
                }

                $sheet->setCellValueByColumnAndRow(9, $startrow, $loaddobj->row);
                if (($loaddobj->c_base > 0) && ($loaddobj->c_height > 0)) {
                    $sheet->setCellValueByColumnAndRow(10, $startrow, $loaddobj->productc->name);
                    $sheet->setCellValueByColumnAndRow(11, $startrow, $loaddobj->c_box_style);
                    $sheet->setCellValueByColumnAndRow(12, $startrow, $loaddobj->c_base);
                    $sheet->setCellValueByColumnAndRow(13, $startrow, $loaddobj->c_height);

                    //$totalline += $loaddobj->l_total;

                    if (isset($totalbox[$loaddobj->productc->name])) {
                        $totalbox[$loaddobj->productc->name] += ($loaddobj->c_base *  $loaddobj->c_height);
                    } else {
                        $totalbox[$loaddobj->productc->name] = ($loaddobj->c_base *  $loaddobj->c_height);
                    }
                }

                if ($loaddobj->c_excess > 0 || $loaddobj->c_excess1 > 0 || $loaddobj->c_excess2 > 0
                ) {

                    $ctotal = 0;
                    if ($loaddobj->c_excess > 0) {
                        $ctotal += $loaddobj->c_excess;
                        // print(" : " . $loaddobj->id);
                        //$totalline += $loaddobj->l_excess;
                        // dd($loaddobj->lproduct->name);
                        //print(" : " . $loaddobj->lproduct->name . " จำนวน " . $loaddobj->l_excess . " กล่อง");
                        $notestr['C'][$loaddobj->row][] =  " : " . $loaddobj->cproduct->name . " จำนวน " . $loaddobj->c_excess . " กล่อง";
                        // dd($notestr);

                        if (isset($totalbox[$loaddobj->cproduct->name])) {
                            $totalbox[$loaddobj->cproduct->name] += $loaddobj->c_excess;
                        } else {
                            $totalbox[$loaddobj->cproduct->name] = $loaddobj->c_excess;
                        }
                    }
                    if ($loaddobj->c_excess1 > 0) {
                        $ctotal += $loaddobj->c_excess1;
                        //$totalline += $loaddobj->l_excess1;
                        $notestr['C'][$loaddobj->row][] =  " : " . $loaddobj->c1product->name . " จำนวน " . $loaddobj->c_excess1 . " กล่อง";

                        if (isset($totalbox[$loaddobj->c1product->name])) {
                            $totalbox[$loaddobj->c1product->name] += $loaddobj->c_excess1;
                        } else {
                            $totalbox[$loaddobj->c1product->name] = $loaddobj->c_excess1;
                        }
                    }
                    if ($loaddobj->c_excess2 > 0) {
                        $ctotal += $loaddobj->c_excess2;
                        //$totalline += $loaddobj->l_excess2;
                        $notestr['C'][$loaddobj->row][] =  " : " . $loaddobj->c2product->name . " จำนวน " . $loaddobj->c_excess2 . " กล่อง";

                        if (isset($totalbox[$loaddobj->c2product->name])) {
                            $totalbox[$loaddobj->c2product->name] += $loaddobj->c_excess2;
                        } else {
                            $totalbox[$loaddobj->c2product->name] = $loaddobj->c_excess2;
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(14,
                        $startrow,
                        $ltotal
                    );
                }


                $sheet->setCellValueByColumnAndRow(16, $startrow, $loaddobj->row);
                if (($loaddobj->l_base > 0) && ($loaddobj->l_height > 0)) {
                    $sheet->setCellValueByColumnAndRow(17, $startrow, $loaddobj->productl->name);
                    $sheet->setCellValueByColumnAndRow(18, $startrow, $loaddobj->l_box_style);
                    $sheet->setCellValueByColumnAndRow(19, $startrow, $loaddobj->l_base);
                    $sheet->setCellValueByColumnAndRow(20, $startrow, $loaddobj->l_height);

                    //$totalline += $loaddobj->l_total;

                    if (isset($totalbox[$loaddobj->product->name])) {
                        $totalbox[$loaddobj->product->name] += ($loaddobj->l_base *  $loaddobj->l_height);
                    } else {
                        $totalbox[$loaddobj->product->name] = ($loaddobj->l_base *  $loaddobj->l_height);
                    }
                }

                if ($loaddobj->l_excess > 0 || $loaddobj->l_excess1 > 0 || $loaddobj->l_excess2 > 0) {

                    $ltotal = 0;
                    if ($loaddobj->l_excess > 0) {
                        $ltotal += $loaddobj->l_excess;
                        // print(" : " . $loaddobj->id);
                        //$totalline += $loaddobj->l_excess;
                        // dd($loaddobj->lproduct->name);
                        //print(" : " . $loaddobj->lproduct->name . " จำนวน " . $loaddobj->l_excess . " กล่อง");
                        $notestr['L'][$loaddobj->row][] =  " : " . $loaddobj->lproduct->name . " จำนวน " . $loaddobj->l_excess . " กล่อง";
                        // dd($notestr);

                        if (isset($totalbox[$loaddobj->lproduct->name])) {
                            $totalbox[$loaddobj->lproduct->name] += $loaddobj->l_excess;
                        } else {
                            $totalbox[$loaddobj->lproduct->name] = $loaddobj->l_excess;
                        }
                    }
                    if ($loaddobj->l_excess1 > 0) {
                        $ltotal += $loaddobj->l_excess1;
                        //$totalline += $loaddobj->l_excess1;
                        $notestr['L'][$loaddobj->row][] =  " : " . $loaddobj->l1product->name . " จำนวน " . $loaddobj->l_excess1 . " กล่อง";

                        if (isset($totalbox[$loaddobj->l1product->name])) {
                            $totalbox[$loaddobj->l1product->name] += $loaddobj->l_excess1;
                        } else {
                            $totalbox[$loaddobj->l1product->name] = $loaddobj->l_excess1;
                        }
                    }
                    if ($loaddobj->l_excess2 > 0) {
                        $ltotal += $loaddobj->l_excess2;
                        //$totalline += $loaddobj->l_excess2;
                        $notestr['L'][$loaddobj->row][] =  " : " . $loaddobj->l2product->name . " จำนวน " . $loaddobj->l_excess2 . " กล่อง";

                        if (isset($totalbox[$loaddobj->l2product->name])) {
                            $totalbox[$loaddobj->l2product->name] += $loaddobj->l_excess2;
                        } else {
                            $totalbox[$loaddobj->l2product->name] = $loaddobj->l_excess2;
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(21, $startrow, $ltotal);
                }

                $totalline += $loaddobj->all_total;

                $sheet->setCellValueByColumnAndRow(22, $startrow, $totalline);

                $startrow++;
            }

            //    dd($totalbox);

            if ($startrow < 37) {
                $startrow = 37;
            }


            $maintable['end'] = 'G' . ($startrow - 1);

            $sheet->getStyle($maintable['start'] . ':' . $maintable['end'])
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));


            $maintable['end2'] = 'N' . ($startrow - 1);

            $sheet->getStyle($maintable['start2'] . ':' . $maintable['end2'])
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $maintable['end3'] = 'V' . ($startrow - 1);

            $sheet->getStyle($maintable['start3'] . ':' . $maintable['end3'])
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('V2:V' . ($startrow))
                ->getBorders()
                ->getRight()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
                ->setColor(new Color('000000'));


            //Footer part
            $starFooter = $startrow;
            $starFooter++;
            $sheet->getStyle('B' . $starFooter . ':V' . $starFooter)
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
                ->setColor(new Color('000000'));

            $sheet->setCellValueByColumnAndRow(2,  $starFooter, "หมายเหตุ");
            $startdatanote = $starFooter + 1;
            if (!empty($notestr)) {
                foreach ($notestr as $notetype => $notetopobj) {
                    foreach ($notetopobj as $linenote => $noteinnerobj) {
                        foreach ($noteinnerobj as $key => $noteitemobj) {
                            if ($notetype == 'L') {
                                $sheet->setCellValueByColumnAndRow(3,  $startdatanote, "แถวแนวยาว แถวที่ " . $linenote . $noteitemobj);
                            } else {
                                $sheet->setCellValueByColumnAndRow(3,  $startdatanote, "แถวแนวขวาง แถวที่ " . $linenote . $noteitemobj);
                            }
                            $startdatanote++;
                        }
                    }
                }
            }

            $starFooter++;

            $sheet->setCellValueByColumnAndRow(10,  $starFooter, "ผู้จัดทำ plan load :..................................................................................");
            $sheet->mergeCellsByColumnAndRow(10, $starFooter, 16, $starFooter);
            $starFooter++;
            $starFooter++;
            $starFooter++;
            $sheet->setCellValueByColumnAndRow(10,  $starFooter, "ผู้ตรวจสอบ plan load..............................................................................");
            $sheet->mergeCellsByColumnAndRow(10, $starFooter, 16, $starFooter);

            $starFooter++;

            if ($startdatanote > $starFooter) {
                $starFooter = $startdatanote + 1;
            }
            $sheet->getStyle('B' . $starFooter . ':V' . $starFooter)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
                ->setColor(new Color('000000'));


            $startproductrow = 13;
            $totalall = 0;
            foreach ($totalbox as $key => $value) {
                $sheet->setCellValueByColumnAndRow(24,  $startproductrow, $key);
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $value);
                $totalall += $value;
                $startproductrow++;
            }
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "Total");
            $sheet->setCellValueByColumnAndRow(25, $startproductrow, $totalall);

            $sheet->getStyle('O' . $startproductrow)
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('O' . $startproductrow)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE)
                ->setColor(new Color('000000'));

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานโหลดหน้าตู้ 1");
            if (isset($teams['Front'][0])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['Front'][0]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานโหลดหน้าตู้ 2");
            if (isset($teams['Front'][1])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['Front'][1]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานโหลดหลังตู้ 1");
            if (isset($teams['Back'][0])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['Back'][0]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานโหลดหลังตู้ 2");
            if (isset($teams['Back'][1])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['Back'][1]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานเช็คกล่องแตก");
            if (isset($teams['Bkbox'][0])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['Bkbox'][0]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานเช็คสินค้าก่อนโหลด");
            if (isset($teams['Bfload'][0])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['Bfload'][0]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงาน FL");
            if (isset($teams['FL'][0])) {
                $sheet->setCellValueByColumnAndRow(25, $startproductrow, $teams['FL'][0]->team->name);
            }

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "____________________________________");
            $sheet->mergeCellsByColumnAndRow(24, $startproductrow, 25, $startproductrow);
            $startproductrow++;
            if (isset($teams['Ctrl'][0])) {
                $sheet->setCellValueByColumnAndRow(24, $startproductrow, "( " . $teams['Ctrl'][0]->team->name . " )");
            } else {
                $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "(__________________________________)");
            }
            $sheet->mergeCellsByColumnAndRow(24, $startproductrow, 25, $startproductrow);
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานควบคุมการโหลด");
            $sheet->mergeCellsByColumnAndRow(24, $startproductrow, 25, $startproductrow);
            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "____________________________________");
            $sheet->mergeCellsByColumnAndRow(24, $startproductrow, 25, $startproductrow);
            $startproductrow++;
            if (isset($teams['Ctrl'][1])) {
                $sheet->setCellValueByColumnAndRow(24, $startproductrow, "( " . $teams['Ctrl'][1]->team->name . " )");
            } else {
                $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "(__________________________________)");
            }
            $sheet->mergeCellsByColumnAndRow(24, $startproductrow, 25, $startproductrow);
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(24,  $startproductrow, "พนักงานควบคุมการโหลด");
            $sheet->mergeCellsByColumnAndRow(24, $startproductrow, 25, $startproductrow);

            $writer = new Xlsx($spreadsheet);

            $filename = "loadingdoc-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            return response()->download('storage/' . $filename);
        } elseif($loadm->type_loading == "Pallet") {



            $teams = array();
            foreach ($loadm->loadteams as $loadteamObj) {
                $teams[$loadteamObj->team_type][] = $loadteamObj;
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();


            $sheet->setCellValueByColumnAndRow(1, 1, "เอกสารบันทึกการ Load สินค้าแช่แข็ง");
            $sheet->mergeCellsByColumnAndRow(1, 1, 12, 1);

            $maintable = array();
            $maintable['start'] = 'B2';

            $sheet->setCellValueByColumnAndRow(2, 2, "พาเลท No");
            $sheet->mergeCellsByColumnAndRow(2, 2, 2, 3);

            $sheet->setCellValueByColumnAndRow(3, 2, "Product");
            $sheet->mergeCellsByColumnAndRow(3, 2, 3, 3);

            $sheet->setCellValueByColumnAndRow(4, 2, "Lot No");
            $sheet->mergeCellsByColumnAndRow(4, 2, 4, 3);

            $sheet->setCellValueByColumnAndRow(5, 2, "Exp.Date");
            $sheet->mergeCellsByColumnAndRow(5, 2, 5, 3);

            $sheet->setCellValueByColumnAndRow(6, 2, "การจัดเรียงในพาเลท");
            $sheet->mergeCellsByColumnAndRow(6, 2, 8, 2);

            $sheet->setCellValueByColumnAndRow(6, 3, "ฐาน");
            $sheet->setCellValueByColumnAndRow(7, 3, "สูง");
            $sheet->setCellValueByColumnAndRow(8, 3, "รวม/กล่อง");

            $sheet->getStyle('A1:K3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            //ฝั่งขวา
            $sheet->setCellValueByColumnAndRow(11, 2, "Order no.");
            $sheet->setCellValueByColumnAndRow(12, 2, $loadm->order_no);

            $sheet->setCellValueByColumnAndRow(11, 4, "วันที่ทำการ Load");
            $sheet->setCellValueByColumnAndRow(12, 4, $loadm->act_date);
            $sheet->setCellValueByColumnAndRow(11, 5, "หัวลาก");
            $sheet->setCellValueByColumnAndRow(12, 5, $loadm->owner_type);
            $sheet->setCellValueByColumnAndRow(11, 6, "ทะเบียนหน้า");
            $sheet->setCellValueByColumnAndRow(12, 6, $loadm->truck_license_plate);
            $sheet->setCellValueByColumnAndRow(11, 7, "ทะเบียนหลัง");
            $sheet->setCellValueByColumnAndRow(12, 7, $loadm->convoy_license_plate);
            $sheet->setCellValueByColumnAndRow(11, 8, "หมายเลขตู้");
            $sheet->setCellValueByColumnAndRow(12, 8, $loadm->convoy_no);
            $sheet->setCellValueByColumnAndRow(11, 9, "หมายเลข Seal");
            $sheet->setCellValueByColumnAndRow(12, 9, $loadm->seal_no);
            $sheet->setCellValueByColumnAndRow(11, 10, "ประเภทตู้");
            $sheet->setCellValueByColumnAndRow(12, 10, $loadm->containertype->name ?? '-');

            $sheet->setCellValueByColumnAndRow(11, 12, "ผลิตภัณฑ์");
            $sheet->setCellValueByColumnAndRow(12, 12, "จำนวน/กล่อง");

            $startrow = 4;

            $totalbox = array();
            $productList = [];
            $totalline = 0;
            $notestr = array();
            foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $loaddobj) {

                $sheet->setCellValueByColumnAndRow(2, $startrow, $loaddobj->pallet_no);
                $sheet->setCellValueByColumnAndRow(3, $startrow, $loaddobj->productt->name);
                $sheet->setCellValueByColumnAndRow(4, $startrow, $loaddobj->p_lot);
                $sheet->setCellValueByColumnAndRow(5, $startrow, $loaddobj->p_exp_date);

                $sheet->setCellValueByColumnAndRow(6, $startrow, $loaddobj->t_base);
                $sheet->setCellValueByColumnAndRow(7, $startrow, $loaddobj->t_height);
                $sheet->setCellValueByColumnAndRow(8, $startrow, $loaddobj->all_total);

                if(isset($productList[$loaddobj->productt->name])){
                    $productList[$loaddobj->productt->name] += $loaddobj->all_total;

                }else{
                    $productList[$loaddobj->productt->name] = $loaddobj->all_total;
                }
                $totalline += $loaddobj->all_total;
                $startrow++;
            }

            $rightRow = 13;
            foreach($productList as $productname => $totalproduct){
                $sheet->setCellValueByColumnAndRow(11, $rightRow, $productname);
                $sheet->setCellValueByColumnAndRow(12, $rightRow, $totalproduct);
                $rightRow++;
            }
            $sheet->setCellValueByColumnAndRow(11, $rightRow, "Total");
            $sheet->setCellValueByColumnAndRow(12, $rightRow, $totalline);


            //    dd($totalbox);

            if ($startrow < 37) {
                $startrow = 37;
            }

            // Add an image to the spreadsheet
            $imagePath = public_path('imgs/pallet40.png');
            $drawing = new Drawing();
            $drawing->setName('Sample image');
            $drawing->setDescription('Sample image');
            $drawing->setPath($imagePath);
            $drawing->setCoordinates('A25');
            // $drawing->setHeight(5);
            $drawing->setWidth(500);
            $drawing->setWorksheet($sheet);

            $imagePath = public_path('imgs/pallet20.png');
            $drawing = new Drawing();
            $drawing->setName('Sample image');
            $drawing->setDescription('Sample image');
            $drawing->setPath($imagePath);
            $drawing->setCoordinates('J25');
            // $drawing->setHeight(5);
            $drawing->setWidth(350);
            $drawing->setWorksheet($sheet);

            $starFooter = 35;

            $sheet->setCellValueByColumnAndRow(2,  $starFooter, "หมายเหตุ");
            $sheet->setCellValueByColumnAndRow(2,  ++$starFooter, "1.ตู้ขนาด 40 ฟุต กำหนดให้โหลดสินค้าแบบพาเลท จำนวน 20 พาเลทต่อ 1 ตู้");
            $sheet->setCellValueByColumnAndRow(10,  $starFooter, "ผู้จัดทำ plan load :..................................................................................");
            $sheet->setCellValueByColumnAndRow(2,  ++$starFooter, "2.ตู้ขนาด 20 ฟุต กำหนดให้โหลดสินค้าแบบพาเลท จำนวน 9 พาเลทต่อ 1 ตู้");
            $sheet->setCellValueByColumnAndRow(10,  $starFooter+3, "ผู้ตรวจสอบ plan load..............................................................................");



            $sheet->getStyle('B2:H22')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new Color('000000'));

            $sheet->getStyle('j2:j23')
            ->getBorders()
            ->getLeft()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new Color('000000'));

            $sheet->getStyle('K2:L2')
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('K4:L10')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new Color('000000'));

            $callast = 12 + sizeof($productList)+1;
            $sheet->getStyle('K12:L'. $callast)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new Color('000000'));

            $sheet->getStyle('A24:P24')
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('I24:I33')
            ->getBorders()
            ->getLeft()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));


            $sheet->getStyle('A34:P34')
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('A42:P42')
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $writer = new Xlsx($spreadsheet);

            $filename = "loadingpalletdoc-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            return response()->download('storage/' . $filename)->deleteFileAfterSend(true);
        } else {



            $teams = array();
            foreach ($loadm->loadteams as $loadteamObj) {
                $teams[$loadteamObj->team_type][] = $loadteamObj;
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();


            $sheet->setCellValueByColumnAndRow(1, 1, "เอกสารบันทึกการ Load สินค้าแช่แข็ง");
            $sheet->mergeCellsByColumnAndRow(1, 1, 16, 1);

            $maintable = array();
            $maintable['start'] = 'B2';

            $sheet->setCellValueByColumnAndRow(2, 2, "แถวแนวขวาง");
            $sheet->mergeCellsByColumnAndRow(2, 2, 6, 2);

            $sheet->setCellValueByColumnAndRow(2, 3, "แถว");
            $sheet->setCellValueByColumnAndRow(3, 3, "Product");
            $sheet->setCellValueByColumnAndRow(4, 3, "ฐาน");
            $sheet->setCellValueByColumnAndRow(5, 3, "สูง");
            $sheet->setCellValueByColumnAndRow(6, 3, "ฝาก");

            $sheet->setCellValueByColumnAndRow(7, 2, "แถวแนวยาว");
            $sheet->mergeCellsByColumnAndRow(7, 2, 10, 2);

            $sheet->setCellValueByColumnAndRow(7, 3, "แถว");
            $sheet->setCellValueByColumnAndRow(8, 3, "ฐาน");
            $sheet->setCellValueByColumnAndRow(9, 3, "สูง");
            $sheet->setCellValueByColumnAndRow(10, 3, "ฝาก");
            $sheet->setCellValueByColumnAndRow(11, 3, "ยอดรวม");

            $sheet->getStyle('A1:K3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            //ฝั่งขวา
            $sheet->setCellValueByColumnAndRow(14, 2, "Order no.");
            $sheet->setCellValueByColumnAndRow(15, 2, $loadm->order_no);

            $sheet->setCellValueByColumnAndRow(14, 4, "วันที่ทำการ Load");
            $sheet->setCellValueByColumnAndRow(15, 4, $loadm->act_date);
            $sheet->setCellValueByColumnAndRow(14, 5, "หัวลาก");
            $sheet->setCellValueByColumnAndRow(15, 5, $loadm->owner_type);
            $sheet->setCellValueByColumnAndRow(14, 6, "ทะเบียนหน้า");
            $sheet->setCellValueByColumnAndRow(15, 6, $loadm->truck_license_plate);
            $sheet->setCellValueByColumnAndRow(14, 7, "ทะเบียนหลัง");
            $sheet->setCellValueByColumnAndRow(15, 7, $loadm->convoy_license_plate);
            $sheet->setCellValueByColumnAndRow(14, 8, "หมายเลขตู้");
            $sheet->setCellValueByColumnAndRow(15, 8, $loadm->convoy_no);
            $sheet->setCellValueByColumnAndRow(14, 9, "หมายเลข Seal");
            $sheet->setCellValueByColumnAndRow(15, 9, $loadm->seal_no);
            $sheet->setCellValueByColumnAndRow(14, 10, "ประเภทตู้");
            $sheet->setCellValueByColumnAndRow(15, 10, $loadm->containertype->name ?? '-');

            $sheet->setCellValueByColumnAndRow(14, 12, "ผลิตภัณฑ์");
            $sheet->mergeCellsByColumnAndRow(14, 12, 15, 12);
            $sheet->setCellValueByColumnAndRow(16, 12, "จำนวน/กล่อง");


            $startrow = 4;

            $totalbox = array();
            $totalline = 0;
            $notestr = array();
            foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $loaddobj) {

                //  print_r($totalbox);

                $sheet->setCellValueByColumnAndRow(2, $startrow, $loaddobj->row);

                if (($loaddobj->t_base > 0) && ($loaddobj->t_height > 0)) {
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $loaddobj->productt->name);
                    $sheet->setCellValueByColumnAndRow(4, $startrow, $loaddobj->t_base);
                    $sheet->setCellValueByColumnAndRow(5, $startrow, $loaddobj->t_height);

                    //$totalline += $loaddobj->t_total;

                    if (isset($totalbox[$loaddobj->productt->name])) {
                        $totalbox[$loaddobj->productt->name] += ($loaddobj->t_base *  $loaddobj->t_height);
                    } else {
                        $totalbox[$loaddobj->productt->name] = ($loaddobj->t_base *  $loaddobj->t_height);
                    }
                }
                if ($loaddobj->t_excess > 0 || $loaddobj->t_excess1 > 0 || $loaddobj->t_excess2 > 0) {
                    $ttotal = 0;
                    if ($loaddobj->t_excess > 0) {
                        $ttotal += $loaddobj->t_excess;
                        //$totalline += $loaddobj->t_excess;
                        $notestr['T'][$loaddobj->row][] =  " : " . $loaddobj->tproduct->name . " จำนวน " . $loaddobj->t_excess . " กล่อง";

                        if (isset($totalbox[$loaddobj->tproduct->name])) {
                            $totalbox[$loaddobj->tproduct->name] += $loaddobj->t_excess;
                        } else {
                            $totalbox[$loaddobj->tproduct->name] = $loaddobj->t_excess;
                        }
                    }
                    if ($loaddobj->t_excess1 > 0) {
                        $ttotal += $loaddobj->t_excess1;
                        //$totalline += $loaddobj->t_excess1;
                        $notestr['T'][$loaddobj->row][] =  " : " . $loaddobj->t1product->name . " จำนวน " . $loaddobj->t_excess1 . " กล่อง";

                        if (isset($totalbox[$loaddobj->t1product->name])) {
                            $totalbox[$loaddobj->t1product->name] += $loaddobj->t_excess1;
                        } else {
                            $totalbox[$loaddobj->t1product->name] = $loaddobj->t_excess1;
                        }
                    }
                    if ($loaddobj->t_excess2 > 0) {
                        $ttotal += $loaddobj->t_excess2;
                        //$totalline += $loaddobj->t_excess1;
                        $notestr['T'][$loaddobj->row][] =  " : " . $loaddobj->t2product->name . " จำนวน " . $loaddobj->t_excess2 . " กล่อง";

                        if (isset($totalbox[$loaddobj->t2product->name])) {
                            $totalbox[$loaddobj->t2product->name] += $loaddobj->t_excess2;
                        } else {
                            $totalbox[$loaddobj->t2product->name] = $loaddobj->t_excess2;
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(6, $startrow, $ttotal);
                }
                $sheet->setCellValueByColumnAndRow(7, $startrow, $loaddobj->row);
                if (($loaddobj->l_base > 0) && ($loaddobj->l_height > 0)) {
                    $sheet->setCellValueByColumnAndRow(3, $startrow, $loaddobj->productl->name);
                    $sheet->setCellValueByColumnAndRow(8, $startrow, $loaddobj->l_base);
                    $sheet->setCellValueByColumnAndRow(9, $startrow, $loaddobj->l_height);

                    //$totalline += $loaddobj->l_total;

                    if (isset($totalbox[$loaddobj->product->name])) {
                        $totalbox[$loaddobj->product->name] += ($loaddobj->l_base *  $loaddobj->l_height);
                    } else {
                        $totalbox[$loaddobj->product->name] = ($loaddobj->l_base *  $loaddobj->l_height);
                    }
                }

                if ($loaddobj->l_excess > 0 || $loaddobj->l_excess1 > 0 || $loaddobj->l_excess2 > 0) {

                    $ltotal = 0;
                    if ($loaddobj->l_excess > 0) {
                        $ltotal += $loaddobj->l_excess;
                        // print(" : " . $loaddobj->id);
                        //$totalline += $loaddobj->l_excess;
                        // dd($loaddobj->lproduct->name);
                        //print(" : " . $loaddobj->lproduct->name . " จำนวน " . $loaddobj->l_excess . " กล่อง");
                        $notestr['L'][$loaddobj->row][] =  " : " . $loaddobj->lproduct->name . " จำนวน " . $loaddobj->l_excess . " กล่อง";
                        // dd($notestr);

                        if (isset($totalbox[$loaddobj->lproduct->name])) {
                            $totalbox[$loaddobj->lproduct->name] += $loaddobj->l_excess;
                        } else {
                            $totalbox[$loaddobj->lproduct->name] = $loaddobj->l_excess;
                        }
                    }
                    if ($loaddobj->l_excess1 > 0) {
                        $ltotal += $loaddobj->l_excess1;
                        //$totalline += $loaddobj->l_excess1;
                        $notestr['L'][$loaddobj->row][] =  " : " . $loaddobj->l1product->name . " จำนวน " . $loaddobj->l_excess1 . " กล่อง";

                        if (isset($totalbox[$loaddobj->l1product->name])) {
                            $totalbox[$loaddobj->l1product->name] += $loaddobj->l_excess1;
                        } else {
                            $totalbox[$loaddobj->l1product->name] = $loaddobj->l_excess1;
                        }
                    }
                    if ($loaddobj->l_excess2 > 0) {
                        $ltotal += $loaddobj->l_excess2;
                        //$totalline += $loaddobj->l_excess2;
                        $notestr['L'][$loaddobj->row][] =  " : " . $loaddobj->l2product->name . " จำนวน " . $loaddobj->l_excess2 . " กล่อง";

                        if (isset($totalbox[$loaddobj->l2product->name])) {
                            $totalbox[$loaddobj->l2product->name] += $loaddobj->l_excess2;
                        } else {
                            $totalbox[$loaddobj->l2product->name] = $loaddobj->l_excess2;
                        }
                    }

                    $sheet->setCellValueByColumnAndRow(10, $startrow, $ltotal);
                }

                $totalline += $loaddobj->all_total;

                $sheet->setCellValueByColumnAndRow(11, $startrow, $totalline);

                $startrow++;
            }

            //    dd($totalbox);

            if ($startrow < 37) {
                $startrow = 37;
            }


            $maintable['end'] = 'K' . ($startrow - 1);

            $sheet->getStyle($maintable['start'] . ':' . $maintable['end'])
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('L2:L' . ($startrow))
                ->getBorders()
                ->getRight()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
                ->setColor(new Color('000000'));

            //Footer part
            $starFooter = $startrow;
            $starFooter++;
            $sheet->getStyle('B' . $starFooter . ':P' . $starFooter)
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
                ->setColor(new Color('000000'));

            $sheet->setCellValueByColumnAndRow(2,  $starFooter, "หมายเหตุ");
            $startdatanote = $starFooter + 1;
            if (!empty($notestr)) {
                foreach ($notestr as $notetype => $notetopobj) {
                    foreach ($notetopobj as $linenote => $noteinnerobj) {
                        foreach ($noteinnerobj as $key => $noteitemobj) {
                            if ($notetype == 'L') {
                                $sheet->setCellValueByColumnAndRow(3,  $startdatanote, "แถวแนวยาว แถวที่ " . $linenote . $noteitemobj);
                            } else {
                                $sheet->setCellValueByColumnAndRow(3,  $startdatanote, "แถวแนวขวาง แถวที่ " . $linenote . $noteitemobj);
                            }
                            $startdatanote++;
                        }
                    }
                }
            }

            $starFooter++;

            $sheet->setCellValueByColumnAndRow(10,  $starFooter, "ผู้จัดทำ plan load :..................................................................................");
            $sheet->mergeCellsByColumnAndRow(10, $starFooter, 16, $starFooter);
            $starFooter++;
            $starFooter++;
            $starFooter++;
            $sheet->setCellValueByColumnAndRow(10,  $starFooter, "ผู้ตรวจสอบ plan load..............................................................................");
            $sheet->mergeCellsByColumnAndRow(10, $starFooter, 16, $starFooter);

            $starFooter++;

            if ($startdatanote > $starFooter) {
                $starFooter = $startdatanote + 1;
            }
            $sheet->getStyle('B' . $starFooter . ':P' . $starFooter)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
                ->setColor(new Color('000000'));


            $startproductrow = 13;
            $totalall = 0;
            foreach ($totalbox as $key => $value) {
                $sheet->setCellValueByColumnAndRow(14,  $startproductrow, $key);
                $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 15, $startproductrow);
                $sheet->setCellValueByColumnAndRow(16, $startproductrow, $value);
                $totalall += $value;
                $startproductrow++;
            }
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "Total");
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 15, $startproductrow);
            $sheet->setCellValueByColumnAndRow(16, $startproductrow, $totalall);

            $sheet->getStyle('P' . $startproductrow)
                ->getBorders()
                ->getTop()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $sheet->getStyle('P' . $startproductrow)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE)
                ->setColor(new Color('000000'));

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานโหลดหน้าตู้ 1");
            if (isset($teams['Front'][0])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['Front'][0]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานโหลดหน้าตู้ 2");
            if (isset($teams['Front'][1])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['Front'][1]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานโหลดหลังตู้ 1");
            if (isset($teams['Back'][0])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['Back'][0]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานโหลดหลังตู้ 2");
            if (isset($teams['Back'][1])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['Back'][1]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานเช็คกล่องแตก");
            if (isset($teams['Bkbox'][0])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['Bkbox'][0]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานเช็คสินค้าก่อนโหลด");
            if (isset($teams['Bfload'][0])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['Bfload'][0]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงาน FL");
            if (isset($teams['FL'][0])) {
                $sheet->setCellValueByColumnAndRow(15, $startproductrow, $teams['FL'][0]->team->name);
            }
            $sheet->mergeCellsByColumnAndRow(15, $startproductrow, 16, $startproductrow);

            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "____________________________________");
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 16, $startproductrow);
            $startproductrow++;
            if (isset($teams['Ctrl'][0])) {
                $sheet->setCellValueByColumnAndRow(14, $startproductrow, "( " . $teams['Ctrl'][0]->team->name . " )");
            } else {
                $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "(__________________________________)");
            }
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 16, $startproductrow);
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานควบคุมการโหลด");
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 16, $startproductrow);
            $startproductrow++;
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "____________________________________");
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 16, $startproductrow);
            $startproductrow++;
            if (isset($teams['Ctrl'][1])) {
                $sheet->setCellValueByColumnAndRow(14, $startproductrow, "( " . $teams['Ctrl'][1]->team->name . " )");
            } else {
                $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "(__________________________________)");
            }
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 16, $startproductrow);
            $startproductrow++;
            $sheet->setCellValueByColumnAndRow(14,  $startproductrow, "พนักงานควบคุมการโหลด");
            $sheet->mergeCellsByColumnAndRow(14, $startproductrow, 16, $startproductrow);

            $writer = new Xlsx($spreadsheet);

            $filename = "loadingdoc-" . date('yymmdd-hi') . ".xlsx";

            $writer->save('storage/' . $filename);

            return response()->download('storage/' . $filename);
        }
    }

    public function masterplan($load_m_id)
    {

        $loadm = LoadM::findOrFail($load_m_id);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValueByColumnAndRow(2, 1, "Master Plan");
        $sheet->mergeCellsByColumnAndRow(2, 1, 15, 1);

        $sheet->setCellValueByColumnAndRow(2, 2, "แถวแนวขวาง");
        $sheet->mergeCellsByColumnAndRow(2, 2, 7, 2);

        $sheet->setCellValueByColumnAndRow(2, 3, "แถว");
        $sheet->setCellValueByColumnAndRow(3, 3, "สินค้า");
        $sheet->setCellValueByColumnAndRow(4, 3, "ฐาน");
        $sheet->setCellValueByColumnAndRow(5, 3, "สูง");
        $sheet->setCellValueByColumnAndRow(6, 3, "ฝาก");
        $sheet->setCellValueByColumnAndRow(7, 3, "รวม");



        $sheet->setCellValueByColumnAndRow(8, 2, "แถวแนวยาว");
        $sheet->mergeCellsByColumnAndRow(8, 2, 13, 2);

        $sheet->setCellValueByColumnAndRow(8, 3, "แถว");
        $sheet->setCellValueByColumnAndRow(9, 3, "สินค้า");
        $sheet->setCellValueByColumnAndRow(10, 3, "ฐาน");
        $sheet->setCellValueByColumnAndRow(11, 3, "สูง");
        $sheet->setCellValueByColumnAndRow(12, 3, "ฝาก");
        $sheet->setCellValueByColumnAndRow(13, 3, "รวม");

        $sheet->setCellValueByColumnAndRow(14, 2, "รวมทั้งหมด");
        $sheet->mergeCellsByColumnAndRow(14, 2, 14, 3);

        $sheet->setCellValueByColumnAndRow(15, 2, "Note");
        $sheet->mergeCellsByColumnAndRow(15, 2, 15, 3);



        $startrow = 4;

        $totalbox = array();

        foreach ($loadm->loadds()->orderBy('row', 'asc')->get() as $loaddobj) {
            $sheet->setCellValueByColumnAndRow(2, $startrow, $loaddobj->row);
            if (($loaddobj->t_base > 0) && ($loaddobj->t_height > 0)) {
                $sheet->setCellValueByColumnAndRow(3, $startrow, $loaddobj->productt->name);
                $sheet->setCellValueByColumnAndRow(4, $startrow, $loaddobj->t_base);
                $sheet->setCellValueByColumnAndRow(5, $startrow, $loaddobj->t_height);

                if (isset($totalbox[$loaddobj->product->name])) {
                    $totalbox[$loaddobj->product->name] += ($loaddobj->t_base * $loaddobj->t_height);
                } else {
                    $totalbox[$loaddobj->product->name] = ($loaddobj->t_base * $loaddobj->t_height);
                }

                if ($loaddobj->t_excess > 0 || $loaddobj->t_excess1 > 0 || $loaddobj->t_excess2 > 0) {

                    $texcess = 0;
                    if ($loaddobj->t_excess > 0) {
                        $texcess += $loaddobj->t_excess;
                    }
                    if ($loaddobj->t_excess1 > 0) {
                        $texcess += $loaddobj->t_excess1;
                    }
                    if ($loaddobj->t_excess2 > 0) {
                        $texcess += $loaddobj->t_excess2;
                    }

                    $sheet->setCellValueByColumnAndRow(6, $startrow, $texcess);

                    if (isset($totalbox[$loaddobj->tproduct->name])) {
                        $totalbox[$loaddobj->tproduct->name] += $texcess;
                    } else {
                        $totalbox[$loaddobj->tproduct->name] = $texcess;
                    }
                }


                $sheet->setCellValueByColumnAndRow(7, $startrow, $loaddobj->t_total);
            }
            $sheet->setCellValueByColumnAndRow(8, $startrow, $loaddobj->row);
            if (($loaddobj->l_base > 0) && ($loaddobj->l_height > 0)) {
                $sheet->setCellValueByColumnAndRow(9, $startrow, $loaddobj->productl->name);
                $sheet->setCellValueByColumnAndRow(10, $startrow, $loaddobj->l_base);
                $sheet->setCellValueByColumnAndRow(11, $startrow, $loaddobj->l_height);

                if (isset($totalbox[$loaddobj->product->name])) {
                    $totalbox[$loaddobj->product->name] += ($loaddobj->l_base * $loaddobj->l_height);
                } else {
                    $totalbox[$loaddobj->product->name] = ($loaddobj->l_base * $loaddobj->l_height);
                }

                if ($loaddobj->l_excess > 0 || $loaddobj->l_excess1 > 0 || $loaddobj->l_excess2 > 0) {

                    $lexcess = 0;
                    if ($loaddobj->l_excess > 0) {
                        $lexcess += $loaddobj->l_excess;
                    }
                    if ($loaddobj->l_excess1 > 0) {
                        $lexcess += $loaddobj->l_excess1;
                    }
                    if ($loaddobj->l_excess2 > 0) {
                        $lexcess += $loaddobj->l_excess2;
                    }

                    $sheet->setCellValueByColumnAndRow(12, $startrow, $lexcess);

                    if (isset($totalbox[$loaddobj->lproduct->name])) {
                        $totalbox[$loaddobj->lproduct->name] += $lexcess;
                    } else {
                        $totalbox[$loaddobj->lproduct->name] = $lexcess;
                    }
                }

                $sheet->setCellValueByColumnAndRow(13, $startrow, $loaddobj->l_total);
            }
            $sheet->setCellValueByColumnAndRow(14, $startrow, $loaddobj->all_total);

            $note = $loaddobj->note;
            if (!empty($loaddobj->t_product_id) &&  $loaddobj->t_excess > 0) {
                $note .= " \n" . $loaddobj->tproduct->name . ' จำนวน ' . $loaddobj->t_excess;
            }
            if (!empty($loaddobj->t_product1_id) &&  $loaddobj->t_excess1 > 0) {
                $note .= " \n" . $loaddobj->t1product->name . ' จำนวน ' . $loaddobj->t_excess1;
            }
            if (!empty($loaddobj->t_product2_id) &&  $loaddobj->t_excess2 > 0) {
                $note .= " \n" . $loaddobj->t2product->name . ' จำนวน ' . $loaddobj->t_excess2;
            }
            if (!empty($loaddobj->l_product_id) &&  $loaddobj->l_excess > 0) {
                $note .= " \n" . $loaddobj->lproduct->name . ' จำนวน ' . $loaddobj->l_excess;
            }
            if (!empty($loaddobj->l_product1_id) &&  $loaddobj->l_excess1 > 0) {
                $note .= " \n" . $loaddobj->l1product->name . ' จำนวน ' . $loaddobj->l_excess1;
            }
            if (!empty($loaddobj->l_product2_id) &&  $loaddobj->l_excess2 > 0) {
                $note .= " \n" . $loaddobj->l2product->name . ' จำนวน ' . $loaddobj->l_excess2;
            }

            $sheet->setCellValueByColumnAndRow(15, $startrow, $note);
            $startrow++;
        }

        $spreadsheet
            ->getActiveSheet()
            ->getStyle('B2:O' . 5)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_MEDIUM)
            ->setColor(new Color('000000'));

        $spreadsheet
            ->getActiveSheet()
            ->getStyle('B1')
            ->getFont()
            ->setSize(14)->setBold(true);

        $spreadsheet->getActiveSheet()->getStyle('B1:M3')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setWrapText(true);

        $writer = new Xlsx($spreadsheet);

        $filename = "masterplan-" . date('yymmdd-hi') . ".xlsx";

        $writer->save('storage/' . $filename);

        return response()->download('storage/' . $filename);
    }

    public function exportdata()
    {
        return view('reports.exportdata');
    }

    public function exportdataAction(Request $request)
    {
        $requestData = $request->all();
        //dd($requestData);

        $loadms = LoadM::whereBetween('act_date', [$requestData['from_date'], $requestData['to_date']])->get();

        //dd($loadms);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        //Excel Generate
        $arraySet = array(
            'load_date' => 'Load Date',
            'customertype-name' => 'Customer Type',
            'order_no' => 'Order No.',
            'act_date' => 'วันที่ Load จริง',
            'owner_type' => 'หัวลาก',
            'truck_license_plate' => 'ทะเบียนหน้า',
            'convoy_license_plate' => 'ทะเบียนหลัง',
            'convoy_no' => 'หมายเลขตู้',
            'seal_no' => 'หมายเลข Seal',
            'loadteams-team-name-Front' => 'พนักงานหน้าตู้',
            'loadteams-team-name-Back' => 'พนักงานหลังตู้',
            'loadteams-team-name-FL' => 'พนักงาน FL',
            'loadteams-team-name-Ctrl' => 'พนักงานคุมตู้',
            'loadteams-team-name-Bfload' => 'พนักงานตรวจสอบสินค้าก่อนโหลด',
            'loadteams-team-name-Bkbox' => 'พนักงานตรวจเช็คกล่องแตก',
            'open_box' => 'วันเวลาเปิดตู้',
            'close_box' => 'วันเวลาปิดตู้',
            'broken_box' => 'กล่องแตก',
        );

        //Head
        $loopColumn = 1;
        foreach ($arraySet as $key => $value) {
            $sheet->setCellValueByColumnAndRow($loopColumn, 1, $value);
            $loopColumn++;
        }



        $rowstart = 2;
        $styleAllArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        foreach ($loadms as $loadmObj) {

            $loopColumn = 1;
            foreach ($arraySet as $key => $value) {
                if (isset($loadmObj->$key)) {
                    $sheet->setCellValueByColumnAndRow($loopColumn, $rowstart, $loadmObj->$key);
                } else {
                    $keyar = explode("-", $key);
                    //dd($keyar);
                    if (sizeof($keyar) == 2) {
                        $step1 = $keyar[0];
                        $step2 = $keyar[1];
                        $sheet->setCellValueByColumnAndRow($loopColumn, $rowstart, $loadmObj->$step1->$step2);
                    } elseif (sizeof($keyar) == 4) {
                        $step1 = $keyar[0];
                        $step2 = $keyar[1];
                        $step3 = $keyar[2];
                        $step4 = $keyar[3];
                        $tempname = "";
                        $nameCount = 1;
                        foreach ($loadmObj->$step1()->where('team_type', $step4)->get() as $Objteam) {
                            if (isset($Objteam->$step2->$step3)) {
                                if ($nameCount == 1) {
                                    $tempname .= $Objteam->$step2->$step3;
                                } else {
                                    $tempname .= " , " . $Objteam->$step2->$step3;
                                }
                                $nameCount++;
                            }
                        }
                        //    / dd($tempname);
                        $sheet->setCellValueByColumnAndRow($loopColumn, $rowstart, $tempname);
                    }
                }
                $loopColumn++;
            }
            $rowstart++;
        }


        $sheet->getStyle('A1:R1')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('92D050');

        foreach (range('A', 'R') as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $sheet->getStyle('A1:R' . ($rowstart - 1))
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)
            ->setColor(new Color('000000'));

        $writer = new Xlsx($spreadsheet);

        $filename = "exportdata-" . date('yymmdd-hi') . ".xlsx";

        $writer->save('storage/' . $filename);

        return response()->download('storage/' . $filename);
    }

    public function loadPlanVsAct()
    {
        return view('reports.load_plan_vs_act');
    }
    public function loadPlanVsActAction(Request $request)
    {
        $requestData = $request->all();

        $firstdate = $requestData['select_year'] . "-" . $requestData['select_month'] . "-01";
        $lastdate = date("Y-m-t", strtotime($firstdate));

        $datadiff = date('d', strtotime($lastdate) - strtotime($firstdate));

        $lastcolumn = $this->_columnLetter($datadiff + 1);

        $dataactrw = DB::table('load_m_s')
            ->select(DB::raw('act_date, status, count(id) as planno'))
            ->whereBetween("act_date", [$firstdate, $lastdate])
            ->where('status', 'Loaded')
            ->groupBy(DB::raw('act_date, status'))
            ->orderBy('act_date', 'asc')
            ->get();

        $actdata = array();
        foreach ($dataactrw as $actobj) {
            $actdata[$actobj->act_date] = $actobj;
        }

        $dataplanrw = PlanShip::whereBetween("plan_date", [$firstdate, $lastdate])->get();

        $plandata = array();
        foreach ($dataplanrw as $planobj) {
            $plandata[$planobj->plan_date] = $planobj;
        }


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Plan Vs Act");

        $sheet->setCellValueByColumnAndRow(1, 1, 'Date');
        $sheet->mergeCellsByColumnAndRow(1, 1, 1, 3);
        $sheet->setCellValueByColumnAndRow(1, 4, 'Plan');
        $sheet->setCellValueByColumnAndRow(1, 5, 'Actual');

        $sheet->setCellValueByColumnAndRow(1, 7, 'Sum Plan');
        $sheet->setCellValueByColumnAndRow(1, 8, 'Sum Actual');
        $sheet->setCellValueByColumnAndRow(1, 9, 'Plan');
        $sheet->setCellValueByColumnAndRow(1, 10, 'Actual');
        $sheet->setCellValueByColumnAndRow(1, 11, 'Sum Diff');

        $rundate = date("Y-m-d", strtotime($firstdate));
        $topcolumn = date("F Y", strtotime($firstdate));

        $loopcolumn = 2;
        $plansum = 0;
        $actsum = 0;
        do {
            $label1 = date("l", strtotime($rundate))[0];
            $label2 = date("d", strtotime($rundate));
            $sheet->setCellValueByColumnAndRow($loopcolumn, 2, $label1);
            $sheet->setCellValueByColumnAndRow($loopcolumn, 3, $label2);

            $planvalue = 0;
            $actvalue = 0;

            if (isset($plandata[$rundate])) {
                $sheet->setCellValueByColumnAndRow($loopcolumn, 4, $plandata[$rundate]->plan_num_ship);
                $planvalue = $plandata[$rundate]->plan_num_ship;
                $plansum += $plandata[$rundate]->plan_num_ship;
            } else {
                $sheet->setCellValueByColumnAndRow($loopcolumn, 4, 0);
            }
            if (isset($actdata[$rundate])) {
                $sheet->setCellValueByColumnAndRow($loopcolumn, 5, $actdata[$rundate]->planno);
                $actvalue = $actdata[$rundate]->planno;
                $actsum += $actdata[$rundate]->planno;
            } else {
                $sheet->setCellValueByColumnAndRow($loopcolumn, 5, 0);
            }

            //  $sheet->setCellValueByColumnAndRow($loopcolumn, 6, ($planvalue - $actvalue));
            $sheet->setCellValueByColumnAndRow($loopcolumn, 7, ($plansum));
            $sheet->setCellValueByColumnAndRow($loopcolumn, 8, ($actsum));
            if ($plansum > 0) {
                $sheet->setCellValueByColumnAndRow($loopcolumn, 9, ("=" . $plansum . "/sum(B4:" . $lastcolumn . "4)"));
            }
            if ($actsum > 0) {
                $sheet->setCellValueByColumnAndRow($loopcolumn, 10, ("=" . $actsum . "/sum(B5:" . $lastcolumn . "5)"));
            }
            $sheet->setCellValueByColumnAndRow($loopcolumn, 11, ($plansum - $actsum));

            $rundate = date("Y-m-d", strtotime("+1 day", strtotime($rundate)));
            $loopcolumn++;
        } while ($lastdate >= $rundate);

        $sheet->getStyle("B9:" . $lastcolumn . "10")
            ->getNumberFormat()->setFormatCode('0.00%');

        $sheet->setCellValueByColumnAndRow(2, 1, $topcolumn);
        $sheet->mergeCellsByColumnAndRow(2, 1, $loopcolumn - 1, 1);

        $lastColumnLabel = $this->_columnLetter($loopcolumn - 1);

        $sheet
            ->getStyle('A1:' . $lastColumnLabel  . '11')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_MEDIUM)
            ->setColor(new Color('000000'));


        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(1);
        $sheet2 = $spreadsheet->getActiveSheet(1);

        $sheet2->setTitle("กล่องแตก");

        $loadmdatas = LoadM::whereBetween("act_date", [$firstdate, $lastdate])->where('status', 'Loaded')->get();

        $databrokenBox = array();
        foreach ($loadmdatas as $loadmdata) {
            $databrokenBox[$loadmdata->act_date][$loadmdata->order_no]['data'] = $loadmdata;
            foreach ($loadmdata->loadbrokens()->get() as $loadbroken) {
                $databrokenBox[$loadmdata->act_date][$loadmdata->order_no]['brokenbox'][$loadbroken->broken_type_id] = $loadbroken->box;
            }
        }

        $firstdate = $requestData['select_year'] . "-" . $requestData['select_month'] . "-01";
        $lastdate = date("Y-m-t", strtotime($firstdate));

        $rundate = date("Y-m-d", strtotime($firstdate));

        $brokentypes = BrokenType::orderBy('seq', 'ASC')->pluck('name', 'id');

        $sheet2->setCellValueByColumnAndRow(2, 1, "วันที่");
        $sheet2->setCellValueByColumnAndRow(3, 1, "Order");

        $colnum = 4;
        foreach ($brokentypes as $brokentype) {
            $sheet2->setCellValueByColumnAndRow($colnum, 1, $brokentype);
            $colnum++;
        }

        $rowrun = 2;
        do {

            $sheet2->setCellValueByColumnAndRow(2, $rowrun, ($rundate));
            $flagjump = true;
            if (isset($databrokenBox[$rundate])) {
                foreach ($databrokenBox[$rundate] as $orderkey => $loadObj) {
                    $sheet2->setCellValueByColumnAndRow(3, $rowrun, $orderkey);
                    $loopcol = 4;
                    foreach ($brokentypes as $brokentypekey => $brokentypeval) {
                        if (isset($databrokenBox[$rundate][$orderkey]['brokenbox'][$brokentypekey])) {
                            $sheet2->setCellValueByColumnAndRow($loopcol, $rowrun, $databrokenBox[$rundate][$orderkey]['brokenbox'][$brokentypekey]);
                        }
                        $loopcol++;
                    }
                    $rowrun++;
                    $flagjump = false;
                }
            }

            $rundate = date("Y-m-d", strtotime("+1 day", strtotime($rundate)));
            if ($flagjump) {
                $rowrun++;
            }
        } while ($lastdate >= $rundate);

        $sheet2->setCellValueByColumnAndRow(2, $rowrun, 'Total');
        $loopcol = 4;
        foreach ($brokentypes as $brokentypekey => $brokentypeval) {
            $labelcase = $this->_columnLetter($loopcol);
            $sheet2->setCellValueByColumnAndRow($loopcol, $rowrun, "=sum(" . $labelcase . "2:" . $labelcase . ($rowrun - 1) . ")");
            $loopcol++;
        }
        $rowrun++;
        $lastc = $this->_columnLetter($loopcol - 1);
        $loopcol = 4;
        $firstc = $this->_columnLetter($loopcol);

        foreach ($brokentypes as $brokentypekey => $brokentypeval) {

            $labelcase = $this->_columnLetter($loopcol);
            $sheet2->setCellValueByColumnAndRow($loopcol, $rowrun, "=(" . $labelcase . ($rowrun - 1) . ")/sum(" . $firstc . ($rowrun - 1) . ":" . $lastc . ($rowrun - 1) . ")");
            $loopcol++;
        }

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(2);
        $sheet3 = $spreadsheet->getActiveSheet(2);

        $sheet3->setTitle("เวลาทำงาน");

        $sheet3->setCellValueByColumnAndRow(2, 1, "วันที่");
        $sheet3->setCellValueByColumnAndRow(3, 1, "Order");
        $sheet3->setCellValueByColumnAndRow(4, 1, "Start Time");
        $sheet3->setCellValueByColumnAndRow(5, 1, "End Time");
        $sheet3->setCellValueByColumnAndRow(6, 1, "เวลา Load นาที");
        $sheet3->setCellValueByColumnAndRow(7, 1, "จำนวนสินค้า (กล่อง)");
        $sheet3->setCellValueByColumnAndRow(8, 1, "Targert เวลาโหลด/ตู้ (จำนวน(กล่อง) x เวลา STD (1 กล่อง = 4 วินาที)");
        $sheet3->setCellValueByColumnAndRow(9, 1, "เวลาที่ใช้ในการโหลดจริง (วินาที)");
        $sheet3->setCellValueByColumnAndRow(10, 1, "Time STD (4 Sec: CAR)");
        $sheet3->setCellValueByColumnAndRow(11, 1, "Actual (4 Sec: CAR)");

        $firstdate = $requestData['select_year'] . "-" . $requestData['select_month'] . "-01";
        $lastdate = date("Y-m-t", strtotime($firstdate));

        $rundate = date("Y-m-d", strtotime($firstdate));

        $rowrun = 2;

        do {

            $sheet3->setCellValueByColumnAndRow(2, $rowrun, ($rundate));
            $flagjump = true;
            if (isset($databrokenBox[$rundate])) {
                foreach ($databrokenBox[$rundate] as $orderkey => $loadObj) {
                    $sheet3->setCellValueByColumnAndRow(3, $rowrun, $orderkey);
                    $sheet3->setCellValueByColumnAndRow(4, $rowrun, $loadObj['data']->open_box);
                    $sheet3->setCellValueByColumnAndRow(5, $rowrun, $loadObj['data']->close_box);

                    $timemins = (strtotime($loadObj['data']->close_box) - strtotime($loadObj['data']->open_box)) / 60;

                    $sheet3->setCellValueByColumnAndRow(6, $rowrun, $timemins);

                    $totalbox = 0;
                    foreach ($loadObj['data']->loadds()->get() as $loadd) {
                        //dd($loadd);
                        $totalbox = $totalbox + $loadd->all_total;
                    }

                    $sheet3->setCellValueByColumnAndRow(7, $rowrun, $totalbox);
                    $sheet3->setCellValueByColumnAndRow(8, $rowrun, '=G' . $rowrun . "*4");
                    $sheet3->setCellValueByColumnAndRow(9, $rowrun, '=F' . $rowrun . "*60");
                    $sheet3->setCellValueByColumnAndRow(10, $rowrun, 4);
                    $sheet3->setCellValueByColumnAndRow(11, $rowrun, '=I' . $rowrun . "/G" . $rowrun);


                    $rowrun++;
                    $flagjump = false;
                }
            }

            $rundate = date("Y-m-d", strtotime("+1 day", strtotime($rundate)));
            if ($flagjump) {
                $rowrun++;
            }
        } while ($lastdate >= $rundate);

        $writer = new Xlsx($spreadsheet);

        $filename = "exportplanvsactdata-" . date('yymmdd-hi') . ".xlsx";

        $writer->save('storage/' . $filename);

        return response()->download('storage/' . $filename);
    }

    private function _columnLetter($c)
    {
        $c = intval($c);
        if ($c <= 0) return '';

        $letter = '';

        while ($c != 0) {
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }

        return $letter;
    }

    public function exportCheckPoint()
    {
        return view('reports.export_check_point');
    }

    public function exportCheckPointAction(Request $request){
        $requestData = $request->all();
       // dd($requestData);

        $databaseObj = DB::table('load_m_s')
        ->leftJoin('upload_delivery_plan_d_s','load_m_s.order_no', 'upload_delivery_plan_d_s.order')
        ->select(DB::raw('load_m_s.id,load_m_s.load_date,
load_m_s.order_no,
load_m_s.owner_type,
load_m_s.convoy_no,
load_m_s.seal_no,
load_m_s.truck_license_plate,
load_m_s.convoy_license_plate,
upload_delivery_plan_d_s.driver1,
upload_delivery_plan_d_s.driver2'))
        ->whereBetween("load_m_s.load_date", [$requestData['from_date'], $requestData['to_date']])
            ->where('load_m_s.status', 'Loaded')
            ->orderBy('load_m_s.load_date', 'asc');

        $data = $databaseObj->get();
        $datalist = $databaseObj->pluck('id','id');

        $chkAnsMsRaw = CheckAnsM::whereIn('load_m_id', $datalist)->get();

        $chkAnsMs = array();
        foreach($chkAnsMsRaw as $chkAnsMObj){
            $chkAnsMs[$chkAnsMObj->load_m_id][$chkAnsMObj->check_point_id] = $chkAnsMObj;
        }



        $checkpoints = CheckPoint::orderBy('id','asc')->get();

        // dd($checkpoints);

        $filename = $this->_exportCheckPointExcel($requestData,$data, $chkAnsMs, $checkpoints);

        return response()->download('storage/' . $filename);

     //   $loadms = LoadM::whereBetween('act_date', [$requestData['from_date'], $requestData['to_date']])->get();

    }

    private function _exportCheckPointExcel($requestData,$data,$chkAnsMs, $checkpoints)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $questionTempId = 2;
        $questionWeightId = 5;
       // dd($chkAnsMs);

        //Head
        $sheet->setCellValueByColumnAndRow(1, 2, "รายงานการบันทึกอุณหภูมิและน้ำหนักตู้โหลดส่งออก ปี " .  (intval(substr($requestData['from_date'], 0, 4)) + 543));
        $sheet->mergeCellsByColumnAndRow(1, 2, 9 + (2*sizeof($checkpoints)), 2);
        $sheet->getStyle('A2')->getFont()->setBold(true);
        $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2')->getAlignment()->setVertical('center');

        $sheet->setCellValueByColumnAndRow(1, 4, "ข้อมูลการโหลดส่งออก");
        $sheet->mergeCellsByColumnAndRow(1, 4, 9, 4);
        $sheet->getStyle('A4')->getFont()->setBold(true);
        $sheet->getStyle('A4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A4')->getAlignment()->setVertical('center');

        $sheet->setCellValueByColumnAndRow(10, 4, "รายงานการบันทึกอุณหภูมิ");
        $sheet->mergeCellsByColumnAndRow(10, 4, (10 + sizeof($checkpoints)-1)  , 4);
        $formatcolumnlabel = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(10);
        $sheet->getStyle($formatcolumnlabel.'4')->getFont()->setBold(true);
        $sheet->getStyle($formatcolumnlabel . '4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle($formatcolumnlabel . '4')->getAlignment()->setVertical('center');

        $sheet->setCellValueByColumnAndRow(((10 + sizeof($checkpoints))), 4, "รายงานการบันทึกน้ำหนัก");
        $sheet->mergeCellsByColumnAndRow(((10 + sizeof($checkpoints))), 4,( (10 + sizeof($checkpoints)) + sizeof($checkpoints)-1), 4);
        $formatcolumnlabel = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(((10 + sizeof($checkpoints))));
        $sheet->getStyle($formatcolumnlabel . '4')->getFont()->setBold(true);
        $sheet->getStyle($formatcolumnlabel . '4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle($formatcolumnlabel . '4')->getAlignment()->setVertical('center');

        $sheet->setCellValueByColumnAndRow(1, 5, "วันที่ Load");
        $sheet->setCellValueByColumnAndRow(2, 5, "Order");
        $sheet->setCellValueByColumnAndRow(3, 5, "หัวลาก");
        $sheet->setCellValueByColumnAndRow(4, 5, "เบอร์ตู้");
        $sheet->setCellValueByColumnAndRow(5, 5, "เบอร์ซีล");
        $sheet->setCellValueByColumnAndRow(6, 5, "ทะเบียนหัวลาก");
        $sheet->setCellValueByColumnAndRow(7, 5, "ทะเบียนหางลาก");
        $sheet->setCellValueByColumnAndRow(8, 5, "พนักงานขับรถคนที่1");
        $sheet->setCellValueByColumnAndRow(9, 5, "พนักงานขับรถคนที่2");

        $runLoop = 10;
        foreach($checkpoints as $checkpoint){
            $sheet->setCellValueByColumnAndRow($runLoop, 5, $checkpoint->name);
            $runLoop++;
        }
        foreach ($checkpoints as $checkpoint) {
            $sheet->setCellValueByColumnAndRow($runLoop, 5, $checkpoint->name);
            $runLoop++;
        }

        $formatcolumnlabel = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($runLoop-1);
        $sheet->getStyle('A5:' . $formatcolumnlabel . '5')->getFont()->setBold(true);
        $sheet->getStyle('A5:' . $formatcolumnlabel . '5')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A5:' . $formatcolumnlabel . '5')->getAlignment()->setVertical('center');


        $sheet->getStyle('A4:' .$formatcolumnlabel . '5')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
        $sheet->getStyle('A4:' .$formatcolumnlabel . '5')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        //Data
        $runLine = 6;
        foreach($data as $lineData){
            $sheet->setCellValueByColumnAndRow(1, $runLine, $lineData->load_date);
            $sheet->setCellValueByColumnAndRow(2, $runLine, $lineData->order_no);
            $sheet->setCellValueByColumnAndRow(3, $runLine, $lineData->owner_type);
            $sheet->setCellValueByColumnAndRow(4, $runLine, $lineData->convoy_no);
            $sheet->setCellValueByColumnAndRow(5, $runLine, $lineData->seal_no);
            $sheet->setCellValueByColumnAndRow(6, $runLine, $lineData->truck_license_plate);
            $sheet->setCellValueByColumnAndRow(7, $runLine, $lineData->convoy_license_plate);
            $sheet->setCellValueByColumnAndRow(8, $runLine, $lineData->driver1);
            $sheet->setCellValueByColumnAndRow(9, $runLine, $lineData->driver2);

            if(isset($chkAnsMs[$lineData->id])){
                $runLoop = 10;
                foreach ($checkpoints as $checkpoint) {
                    $sheet->setCellValueByColumnAndRow($runLoop, $runLine, "-");
                    if(isset($chkAnsMs[$lineData->id][$checkpoint->id])){
                        foreach($chkAnsMs[$lineData->id][$checkpoint->id]->checkanswers as $checkanswer){
                            if($checkanswer->check_questions_id == $questionTempId){
                                $column =  $checkanswer->ans1_column;
                                $sheet->setCellValueByColumnAndRow($runLoop, $runLine, $checkanswer->$column);
                            }
                        }
                    }
                    $runLoop++;
                }
                foreach ($checkpoints as $checkpoint) {
                    $sheet->setCellValueByColumnAndRow($runLoop, $runLine, "-");
                    if (isset($chkAnsMs[$lineData->id][$checkpoint->id])) {
                        foreach ($chkAnsMs[$lineData->id][$checkpoint->id]->checkanswers as $checkanswer) {
                            if ($checkanswer->check_questions_id == $questionWeightId) {
                                $column =  $checkanswer->ans1_column;
                                $sheet->setCellValueByColumnAndRow($runLoop, $runLine, $checkanswer->$column);
                            }
                        }
                    }
                    $runLoop++;
                }
            }
            $runLine++;
        }

        for ($i = 1; $i < $runLoop; $i++) {
            $columnID = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($i);
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->getStyle('A6:' . $formatcolumnlabel . ($runLine-1))->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        $writer = new Xlsx($spreadsheet);

        $filename = "checkpoint-data-" . date('yymmdd-hi') . ".xlsx";

        $writer->save('storage/' . $filename);

        return $filename;
    }

}
