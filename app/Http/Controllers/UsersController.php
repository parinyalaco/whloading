<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Group;
use Illuminate\Http\Request;
use Auth;

use Carbon\Carbon;
use DB;
use SimpleXLSX;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware('admin');
    // }

    public function index()
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin')) ) {
            $perPage = 25;
            $users = User::latest()->paginate($perPage);
            return view('users.index', compact('users'));
        }

        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            $groups = Group::pluck('name', 'id');
            return view('users.create',compact('groups'));
        }

        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            $requestData = $request->all();
            User::create($requestData);

            return redirect('userdatas')->with('flash_message', ' added!');
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            $user = User::findOrFail($id);
            return view('users.view', compact('user'));
        }

        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            $user = User::findOrFail($id);
            $groups = Group::pluck('name','id');
            return view('users.edit', compact('user', 'groups'));
        }

        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            $requestData = $request->all();

            $user = User::findOrFail($id);
            // $requestData['password'] = bcrypt($request->password);
            $user->update($requestData);

            return redirect('userdatas')->with('flash_message', ' updated!');
        }

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            User::destroy($id);

            return redirect('userdatas')->with('flash_message', ' deleted!');
        }

        return redirect('/');
    }

    public function changepass(){
        return view('users.changepass');
    }

    public function changepassAction(Request $request){
        $requestData = $request->all();

        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $user->password = $requestData['password'];
        $user->update();
        return redirect('/')->with('flash_message', ' auth!');
    }

    public function importExportView()
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
            return view('users.import');
        }

        return redirect('/');
    }

    public function import(Request $request)
    {
        if (Auth::user() && (Auth::user()->group->name == trim('Admin') || Auth::user()->group->name == trim('Superadmin'))) {
        // dd($id);
            set_time_limit(0);
            if ($request->hasFile('file_upload')) {
                $tmpfolder = md5(time());
                $zipfile = $request->file('file_upload');
                $uploadname = $zipfile->getClientOriginalName();
                $name = md5($zipfile->getClientOriginalName() . time()) . '.' . $zipfile->getClientOriginalExtension();
                $destinationPath = public_path('storage/upload/' . $tmpfolder);
                $zipfile->move($destinationPath, $name);

                $uploadpath = 'upload/' . $tmpfolder  . "/" . $name;
                $uploadfile = $destinationPath  . "/" . $name;
                // dd($uploadfile);
                $tmpUploadD = array();
                if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                    $user_name = array();
                    $list_user =array();
                    $check_user =array();
                    $err_user =array();
                    $sort_user =array();
                    $unique_user =array();

                    $user = User::get();
                    foreach($user as $key){
                        $user_name[] = $key->username;
                    }
                    // print_r($user_name);

                    $group = Group::get();
                    foreach($group as $key){
                        $group_name[$key->name] = $key->id;
                    }
                    // dd($group_name);

                    foreach ($xlsx->rows() as $r => $row) {
                        if ($r > 0) {
                            $list_user[] = $row[1];
                            $list_name[] = $row[2];
                            $check_user[$row[1]]['row'][] = $r+1;
                            $check_user[$row[1]]['name'][] = $row[2];
                        }
                    }
                    // dd($check_user);
                    //เช็คข้อมูลซ้ำในไฟล์
                    foreach ($check_user as $count => $row) {
                        if(count($check_user[$count]['row']) > 1) {
                            // echo $count."---</br>";
                            $err_user[$check_user[$count]['row'][0]]['row'] = $check_user[$count]['row'][0];
                            $err_user[$check_user[$count]['row'][0]]['name'] = $count." ".$check_user[$count]['name'][0];
                            $err_user[$check_user[$count]['row'][0]]['note'] = 'พบสินค้านี้ในบรรทัดที่';

                            foreach ($check_user[$count]['row'] as $kid => $vrow) {
                                $err_user[$check_user[$count]['row'][0]]['note'] .= ' '.$vrow;
                            }
                        }
                    }
                    // dd($err_user);

                    //เช็คสินค้าซ้ำใน db
                    $unique_user = array_unique($list_user);
                    // dd($unique_user);
                    foreach ($unique_user as $klist=>$vlist) {
                        // echo   $vlist."</br>";
                        $check_array = array_search($vlist, $user_name);
                        // echo "---</br>";
                        if(strlen($check_array)>0){
                            // echo $err_user[$check_user[$vlist]['row'][0]]['row'].'ค้นพบคำว่า Toshiba</br>';
                            $err_user[$check_user[$vlist]['row'][0]]['row'] = $check_user[$vlist]['row'][0];
                            $err_user[$check_user[$vlist]['row'][0]]['name'] = $vlist." ".$list_name[$klist];
                            if(!empty($err_user[$check_user[$vlist]['row'][0]]['note'])){
                                $err_user[$check_user[$vlist]['row'][0]]['note'] .= ', ข้อมูลนี้มีอยู่แล้ว';
                            }else{
                                $err_user[$check_user[$vlist]['row'][0]]['note'] = 'ข้อมูลนี้มีอยู่แล้ว';
                            }

                        }
                    }
                    // dd($err_user);

                    //เรียงลำดับ array
                    // echo count($list_user)."</br>";
                    for($i=1;$i<=count($list_user)+1;$i++){
                        // print_r($err_user[$i]);
                        // echo $i."--</br>";
                        if(!empty($err_user[$i])){
                            // print_r($err_user[$i])."</br>";
                            $sort_user[$i]['sort'] = $i;
                            $sort_user[$i]['name'] = $err_user[$i]['name'];
                            $sort_user[$i]['note'] = $err_user[$i]['note'];
                        }
                    }
                    // dd($sort_user);

                    if(!empty($sort_user)){
                        return view('users.err_import',compact('sort_user'));
                        // return redirect('product_err')->with('error',"Can't upload");
                    }else{
                        $user_load = array();
                        foreach ($xlsx->rows() as $r => $row) {
                            if ($r > 0) {
                                // $requestData = $request->all();
                                $user_load['username'] = $row[1];
                                $user_load['name'] = $row[2];
                                $user_load['email'] = $row[3];
                                $user_load['password'] = $row[4];
                                $user_load['group_id'] = $group_name[$row[5]];
                                // dd($product_load);
                                $query_user = User::create($user_load);
                            }
                        }
                        return redirect('userdatas')->with('success','Updated successfully');
                    }
                } else {
                    echo SimpleXLSX::parseError();
                }
            }
        }

        return redirect('/');
    }
}
