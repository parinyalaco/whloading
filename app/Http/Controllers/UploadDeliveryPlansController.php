<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\UploadDeliveryPlanM;
use App\Models\UploadDeliveryPlanD;
use App\Models\CurrentDeliveryPlan;
use SimpleXLSX;

class UploadDeliveryPlansController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;
        if (!empty($keyword)) {
            $uploaddeliveryplanms = UploadDeliveryPlanM::where('file_name', 'like', '%' . $keyword . '%')
                ->latest()
                ->paginate($perPage);
        } else {
            $uploaddeliveryplanms = UploadDeliveryPlanM::latest()
                ->paginate($perPage);
        }
        return view('uploaddeliveryplans.index', compact('uploaddeliveryplanms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('uploaddeliveryplans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(0);
        $validatedData = $request->validate([
            'file'  => 'required|mimes:xls,xlsx'
        ]);
        if ($request->hasFile('file')) {
            //ลบข้อมูลเก่า
            $list_id = $request->list_load_m_id;

            $completedirectory = 'storage/upload/';
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }

            $zipfile = $request->file('file');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path($completedirectory . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            $tmpUploadDeliveryPlanM = [];
            $tmpUploadDeliveryPlanM['file_name'] = $uploadname;
            $tmpUploadDeliveryPlanM['file_path'] = $uploadpath;
            $tmpUploadDeliveryPlanM['filename_path'] = $uploadfile;
            $tmpUploadDeliveryPlanM['status'] = 'Uploaded';

            $uploadDeliveryPlanM = UploadDeliveryPlanM::create($tmpUploadDeliveryPlanM);

            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                //                  4 => "Loading"
                //                   8 => "Order"
                //                     93 => "ทะเบียนหาง"
                //   96 => "ทะเบียนหัวส่งออก"
                //   103 => "หัวลาก"
                //   108 => "Container No"
                //   109 => "Seal No"
                //   112 => "พนักงานขับรถ1"
                //   113 => "พนักงานขับรถ2"
                foreach ($xlsx->rows() as $r => $row) {
                    if ($r > 0) {
                        $tmpUploadDeliveryPlanD = [];
                        $tmpUploadDeliveryPlanD['upload_delivery_plan_m_id'] =  $uploadDeliveryPlanM->id;
                        //$tmpUploadDeliveryPlanD['loading'] = $row[4];
                        $tmpUploadDeliveryPlanD['loading'] = date('Y-m-d', strtotime($row[4]));
                        $tmpUploadDeliveryPlanD['order'] =  $row[8];
                        $tmpUploadDeliveryPlanD['license_plate_tail'] =  $row[93];
                        $tmpUploadDeliveryPlanD['license_plate_head'] =  $row[96];
                        $tmpUploadDeliveryPlanD['truck'] =  $row[103];
                        $tmpUploadDeliveryPlanD['container_no'] =  $row[108];
                        $tmpUploadDeliveryPlanD['seal_no'] =  $row[109];
                        $tmpUploadDeliveryPlanD['driver1'] =  $row[112];
                        $tmpUploadDeliveryPlanD['driver2'] =  $row[113];
                        $tmpUploadDeliveryPlanD['status'] = 'Uploaded';

                        $chkCount = UploadDeliveryPlanD::where('order', $tmpUploadDeliveryPlanD['order'])
                            ->where('upload_delivery_plan_m_id', $tmpUploadDeliveryPlanD['upload_delivery_plan_m_id'])
                            ->count();
                        if ($chkCount == 0) {
                            UploadDeliveryPlanD::create($tmpUploadDeliveryPlanD);
                        }
                    }
                }
            }

            $this->_import2current($uploadDeliveryPlanM->id);
        }

        return redirect(route('uploaddeliveryplans.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $uploadDeliveryPlanM = UploadDeliveryPlanM::findOrFail($id);
        return view('uploaddeliveryplans.view', compact('uploadDeliveryPlanM'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CurrentDeliveryPlan::where('upload_delivery_plan_m_id', $id)->delete();
        UploadDeliveryPlanD::where('upload_delivery_plan_m_id', $id)->delete();
        UploadDeliveryPlanM::destroy($id);

        return redirect('uploaddeliveryplans')->with('flash_message', ' deleted!');
    }

    private function _import2current($upload_delivery_plan_m_id)
    {
        $uploadDeliveryPlanM = UploadDeliveryPlanM::findOrFail($upload_delivery_plan_m_id);
        foreach ($uploadDeliveryPlanM->uploaddeliveryplands as $uploaddeliverypland) {
            $chkCount = CurrentDeliveryPlan::where('order', $uploaddeliverypland->order)
                ->count();
            if ($chkCount == 0) {
                $tmpCurrentDeliveryPlan = [];
                $tmpCurrentDeliveryPlan['upload_delivery_plan_m_id'] =  $uploaddeliverypland->upload_delivery_plan_m_id;
                //$tmpUploadDeliveryPlanD['loading'] = $row[4];
                $tmpCurrentDeliveryPlan['loading'] = $uploaddeliverypland->loading;
                $tmpCurrentDeliveryPlan['order'] =  $uploaddeliverypland->order;
                $tmpCurrentDeliveryPlan['license_plate_tail'] =  $uploaddeliverypland->license_plate_tail;
                $tmpCurrentDeliveryPlan['license_plate_head'] =  $uploaddeliverypland->license_plate_head;
                $tmpCurrentDeliveryPlan['truck'] =  $uploaddeliverypland->truck;
                $tmpCurrentDeliveryPlan['container_no'] =  $uploaddeliverypland->container_no;
                $tmpCurrentDeliveryPlan['seal_no'] =  $uploaddeliverypland->seal_no;
                $tmpCurrentDeliveryPlan['driver1'] =  $uploaddeliverypland->driver1;
                $tmpCurrentDeliveryPlan['driver2'] =  $uploaddeliverypland->driver2;
                $tmpCurrentDeliveryPlan['status'] = 'Active';

                CurrentDeliveryPlan::create($tmpCurrentDeliveryPlan);

                $uploaddeliverypland->status = 'Active';
                $uploaddeliverypland->update();
            }
        }
        $uploadDeliveryPlanM->status = 'Active';
        $uploadDeliveryPlanM->update();
        // return redirect(route('uploaddeliveryplans.index'));
    }
}
