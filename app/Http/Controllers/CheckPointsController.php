<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CheckPoint;

class CheckPointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'check_points';
    public $statusList = [
        'Active' => 'Active',
        'Inactive' => 'Inactive'
    ];


    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $checkpoints = CheckPoint::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $checkpoints = CheckPoint::latest()->paginate($perPage);
        }

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('admin.checkpoints.index', compact('checkpoints', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $statusList = $this->statusList;
        return view('admin.checkpoints.create', compact('mainnav', 'namenav', 'statusList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:check_points',
        ]);

        CheckPoint::create($requestData);

        return redirect('admin/check_points')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $checkpoint = CheckPoint::findOrFail($id);
        return view('admin.checkpoints.view', compact('checkpoint', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $checkpoint = CheckPoint::findOrFail($id);

        $statusList = $this->statusList;
        return view('admin.checkpoints.edit', compact('checkpoint', 'mainnav', 'namenav', 'statusList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $checkpoint = CheckPoint::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:check_points,name,' . $checkpoint->id,
        ]);


        $checkpoint->update($requestData);

        return redirect('admin/check_points')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countcandelete = CheckPoint::findOrFail($id)->checkanswers()->count();

        if ($countcandelete == 0) {
            CheckPoint::destroy($id);
            return redirect('admin/check_points')->with('flash_message', ' deleted!');
        } else {
            return back()->with('error', 'Delete not successfully');
        }
    }

    public function qrcode(){

        $checkpoints = CheckPoint::all();
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('admin.checkpoints.qrcode', compact('checkpoints', 'mainnav', 'namenav'));
    }
}
