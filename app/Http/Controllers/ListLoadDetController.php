<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\StLocation;
use App\Models\StType;
use App\Models\LacoBatch;
use App\Models\Shipment;
use App\Models\ListStatus;
use App\Models\ListLoadM;
use App\Models\ListLoadD;
use App\Models\ListLoadSort;
use App\Models\ImgPd;

use Session;
use SimpleXLSX;
use Storage;
use Auth;

class ListLoadDetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('HaveListLoadID');
    }
    
    public function index(Request $request)
    {
        $list_id = Session::get('sess_lld');
        $keyword = $request->get('search');
        $keywordP = $request->get('search_pallet');
        if(!empty($list_id)){
            $list_load = ListLoadM::findOrFail($list_id);
            
            $pd = array();
            $pd_data = array();
            $pd = ListLoadD::join('products', 'list_load_d_s.product_id', '=', 'products.id')
                ->select('list_load_d_s.product_id', 'products.name', 'products.desc_list')->orderBy('products.name')->get();
            foreach ($pd as $key) {
                $pd_data[$key->product_id]['name'] = $key->name;
                $pd_data[$key->product_id]['desc_list'] = $key->desc_list;
            }

            $ll_det = array();
            $ll_det = new ListLoadD;
            $ll_det = $ll_det->where('list_load_m_id', $list_id);
            if (!empty($keyword)) {
                $ll_det = $ll_det->where('product_id', $keyword );
            }
            if (!empty($keywordP)) {
                $ll_det = $ll_det->where('pallet_no', 'like', '%' . $keywordP . '%');
            }
            $ll_det = $ll_det->orderBy('sort')->get();
            return view('list_load_det.index', compact('list_load','ll_det','pd_data'));
        }else{
            return redirect('list_load');
        }
    }
    
    public function create()
    {
        $base_data = array();
        $arr = array();
        $base_data_id = array();
        $col_name = array();
        $list_load = array();

        $arr_data = $this->arr_data();
        $base_data = $arr_data[0];
        $arr = $arr_data[1];
        $base_data_id = $arr_data[2];
        $col_name = $arr_data[3];
        $list_load = $arr_data[4];
        
        return view('list_load_det.create',compact('list_load', 'base_data', 'arr', 'col_name', 'base_data_id'));
    }

    public function store(Request $request)
    {
        $list_id = Session::get('sess_lld');

        $base_data = array();
        $arr = array();
        $base_data_id = array();

        $arr_data = $this->arr_data();
        $base_data = $arr_data[0];
        $arr = $arr_data[1];
        $base_data_id = $arr_data[2];

        $requestData = $request->all();
        
        $chk = ListLoadD::where('list_load_m_id', $list_id)
            ->where(function($query)  use ($requestData) {
                $query->where('pallet_no', $requestData['pallet_no'])
                      ->orWhere('batch', $requestData['batch']);
            })
            ->count();
        // dd($chk);
        if($chk == 0){
            $requestData['list_load_m_id'] = $list_id;                
            $requestData['user_id'] = Auth::user()->id;  
            ListLoadD::create($requestData);

            $to_sort = $this->save_sort($list_id, $requestData);

            return redirect('list_load_det')->with('success','Create successfully');
        }else{
            return back()->with('error','Pallet No. หรือ Batch ซ้ำกับข้อมูลที่มีอยู่');
        }  
    }

    public function show($id)
    {
        $list_load = ListLoadD::findOrFail($id);  

        $base_data = array();
        $arr = array();

        $arr_data = $this->arr_data();
        $base_data = $arr_data[0];
        $arr = $arr_data[1];

        return view('list_load_det.show', compact('list_load', 'base_data'));
    }

    public function edit($id)
    {
        $list_loadD = ListLoadD::findOrFail($id);

        $base_data = array();
        $arr = array();
        $base_data_id = array();
        $col_name = array();
        $list_load = array();

        $arr_data = $this->arr_data();
        $base_data = $arr_data[0];
        $arr = $arr_data[1];
        $base_data_id = $arr_data[2];
        $col_name = $arr_data[3];
        $list_load = $arr_data[4];
        // dd($base_data_id);

        return view('list_load_det.edit', compact('list_load', 'base_data', 'arr', 'col_name', 'base_data_id', 'list_loadD'));
    }

    public function update(Request $request, $id)
    {
        $list_id = Session::get('sess_lld');
        $list_loadD = ListLoadD::findOrFail($id);

        $requestData = $request->all();

        $chk = ListLoadD::where('list_load_m_id', $list_id)->whereNotIn('id',[$id])
            ->where(function($query)  use ($requestData) {
                $query->where('pallet_no', $requestData['pallet_no'])
                      ->orWhere('batch', $requestData['batch']);
            })
            ->count();
        if($chk == 0){
            $requestData['list_load_m_id'] = $list_id;
            if($requestData['chk_img'] <> $list_loadD->list_img){
                if(!empty($list_loadD->list_img)){
                    if (file_exists($list_loadD->list_img_path)) {
                        unlink($list_loadD->list_img_path);
                    }
                }
                if(empty($requestData['chk_img'])){    
                //     $requestData['list_img'] = $list_id;
                //     $requestData['list_img_path'] = $list_id;
                // }else{
                    $requestData['list_img'] = null;
                    $requestData['list_img_path'] = null;
                }
            }
            $list_loadD->update($requestData);

            $to_sort = $this->save_sort($list_id, $requestData);

            return redirect('list_load_det')->with('success','Update successfully');
        }else{
            return back()->with('error','Pallet No. หรือ Batch ซ้ำกับข้อมูลที่มีอยู่');
        }
    }

    public function destroy($id)
    {
        $list_loadD = ListLoadD::findOrFail($id);
        $m_id = $list_loadD->list_load_m_id;
        $p_id = $list_loadD->product_id;
        // $dir = array('webcam_pd', 'webcam_det');

        //ยังไม่ลบเพราะมีสินค้าอยู่ ต้องดูว่าไม่เหลือสินค้าแล้ว
        $c_lld = ListLoadD::where('list_load_m_id', $m_id)->where('product_id', $p_id)->whereNotIn('id', [$id])->selectRaw('sum(ur_qty) AS num_pd')->get();
        if(!empty($c_lld[0]['num_pd'])){    
            $to_save = array();
            $to_save['quantity'] = $c_lld[0]['num_pd'];
            ListLoadSort::where('list_load_m_id', $m_id)->where('product_id', $p_id)->update($to_save);     
        }else{
            // $dirname = 'images/webcam_pd/'.$m_id.'/'.$p_id;
            $dirname = config('my.directory.lot_load.pdf').$m_id.'/'.$p_id.'/';
            $this->deleteDirectory($dirname);
            ImgPd::where('list_load_m_id', $m_id)->where('product_id', $p_id)->delete();

            ListLoadSort::where('list_load_m_id', $m_id)->where('product_id', $p_id)->delete();
        }
        // $dirname = 'images/webcam_det/'.$m_id.'/'.$id;
        $dirname = config('my.directory.lot_load.detail').$m_id.'/'.$id.'/';
        $this->deleteDirectory($dirname);
        ListLoadD::destroy($id);

        return back()->with('success','Delete successfully');
    }
    
    public function importExportView()
    {
        $list_id = Session::get('sess_lld');
        $list_load = ListLoadM::findOrFail($list_id);
        return view('list_load_det.import', compact('list_load'));
    }

    public function arr_data()
    {
        $base_data = array();
        $base_data_id = array();

        $loc = array();
        $loc = StLocation::get();                
        foreach($loc as $key){
            $base_data['st_location_id'][$key->name] = $key->id;    
            $base_data_id['st_location_id'][$key->id] = $key->name;                
        } 
        
        $type = array();
        $type = StType::get();                
        foreach($type as $key){
            $base_data['st_type_id'][$key->name] = $key->id;    
            $base_data_id['st_type_id'][$key->id] = $key->name;                
        }
        
        $pd = array();
        $pd = Product::orderBy('name')->get();                
        foreach($pd as $key){
            $base_data['product_id']['name'][$key->name] = $key->id; 
            if(!empty($key->desc_list))    $base_data['product_id']['desc_list'][$key->desc_list] = $key->id;

            $base_data_id['product_id']['name'][$key->id] = $key->name;
            if(!empty($key->desc_list))    $base_data_id['product_id']['desc_list'][$key->id] = $key->desc_list;
        }
        
        $lb = array();
        $lb = LacoBatch::get();                
        foreach($lb as $key){
            $base_data['laco_batch_id'][$key->name] = $key->id;    
            $base_data_id['laco_batch_id'][$key->id] = $key->name;                
        }

        $sm = array();
        $sm = Shipment::get();                
        foreach($sm as $key){
            $base_data['shipment_id'][$key->name] = $key->id;    
            $base_data_id['shipment_id'][$key->id] = $key->name;               
        }  

        $st = array();
        $st = ListStatus::get();                
        foreach($st as $key){
            $base_data['status_id'][$key->name] = $key->id;      
            $base_data_id['status_id'][$key->id] = $key->name;                 
        }
        
        $arr['all'] = ['st_location_id', 'st_type_id', 'storage_bin', 'pallet_no', 'product_id', 
                    '', 'batch', 'gr_date', 'mfg_date', 'exp_date', 
                    'laco_batch_id', 'box_no', 'shipment_id', 'ur_qty', 'block_qty', 
                    'qi_qty', 'available_stock', 'status_id', 'usage_id', 'sales_doc', 
                    'dales_doc_item', 'inspection_los', 'note', 'sort'];
        $arr['str'] = ['storage_bin', 'pallet_no', 'batch', 'box_no', 'sales_doc', 'dales_doc_item', 'inspection_los', 'note'];
        $arr['int'] = ['ur_qty', 'block_qty', 'qi_qty', 'available_stock', 'sort'];
        $arr['date'] = ['gr_date', 'mfg_date', 'exp_date'];  
        $arr['link'] = ['st_location_id', 'st_type_id', 'product_id', 'laco_batch_id', 'shipment_id', 'status_id', 'usage_id'];   

        $col_name  = ['Storage Location', 'Storage Type', 'Storage Bin', 'Pallet No.', 'Material', 
            'Material Description','Batch', 'GR Date', 'MFG', 'Exp. Date', 
            'LACO Batch', 'Box no. Interval1', 'Shipment No.', 'UR Qty', 'Block Qty', 
            'QI Qty', 'Available stock', 'Status Micro', 'Usage', 'Sales Document', 
            'Sales Document Item', 'Inspection Los no', 'Note', 'ลำดับโหลดขึ้นตู้'];
        // dd($base_data['st_location_id']);

        $list_id = Session::get('sess_lld');
        $list_load = ListLoadM::findOrFail($list_id);

        return [$base_data, $arr, $base_data_id, $col_name, $list_load];
    }

    public function import(Request $request)
    {
        set_time_limit(0);
        $validatedData = $request->validate([
            'file_upload'  => 'required|mimes:xls,xlsx'
        ]);
        if ($request->hasFile('file_upload')) {
            //ลบข้อมูลเก่า
            $list_id = $request->list_load_m_id;

            $completedirectory = 'storage/upload/'; 
            $tmpfolder = date('Ymd');
            if (!is_dir($completedirectory . '/' . $tmpfolder)) {
                mkdir($completedirectory . '/' . $tmpfolder, 0777, true);
            }  

            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '-upload.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path($completedirectory . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = $completedirectory . "/" . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;

            $tmpUploadD = array();
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $base_data = array();
                $arr = array();                

                $arr_data = $this->arr_data();
                $base_data = $arr_data[0];
                $arr = $arr_data[1];

                $excel_data = array();
                $chk_unique =array(); 
                if(count($xlsx->rows()[0])==23){
                    foreach ($xlsx->rows() as $r => $row) {                    
                        if ($r > 0) {
                            if(!empty($row[0]) && trim($row[0]) != 'Storage Location'){ 
                                $excel_data[] = $row[0].'฿'.$row[1].'฿'.$row[2].'฿'.$row[3].'฿'.$row[4].'฿'.$row[5].'฿'.$row[6].'฿'.$row[7].'฿'.$row[8].'฿'.$row[9].'฿'.$row[10].'฿'.$row[11].'฿'.$row[12].'฿'.$row[13].'฿'.$row[14].'฿'.$row[15].'฿'.$row[16].'฿'.$row[17].'฿'.$row[18].'฿'.$row[19].'฿'.$row[20].'฿'.$row[21].'฿'.$row[22];
                                if(empty($chk_unique['count'][$row[3]][$row[6]])){
                                    $chk_unique['count'][$row[3]][$row[6]] = 1;
                                    $chk_unique['row'][$row[3]][$row[6]] = $r+1;
                                    $chk_unique['pd'][$row[3]][$row[6]] = $row[4].' '.$row[5];
                                }else{
                                    $chk_unique['count'][$row[3]][$row[6]] += 1;
                                    $chk_unique['row'][$row[3]][$row[6]] .= ', '.($r+1);
                                }    
                                
                            }
                        }
                    } 
                }else{
                    return back()->with('error','จำนวนคอลัมน์ไม่ถูกต้อง');
                }               
                // dd($chk_unique);

                //เช็คสินค้าซ้ำในไฟล์
                $i = 0;
                $err_load =array(); 
                foreach ($chk_unique['count'] as $kp => $vp) {
                    foreach ($vp as $kb => $vb) {
                        if($vb > 1) {
                            $err_load[$i]['row'] = $chk_unique['row'][$kp][$kb];
                            $err_load[$i]['product'] = $chk_unique['pd'][$kp][$kb];
                        }
                        $i++;
                    }
                }
                // dd($err_load);

                if(!empty($err_load)){
                    return view('list_load_det.err_import',compact('err_load'));
                }else{
                    $ll_det = array();
                    $ll_det = ListLoadD::where('list_load_m_id', $list_id)->count();
                    if($ll_det>0){
                        // $dirname = 'images/webcam_det/'.$list_id;
                        $dir = array('pdf', 'detail');
                        foreach($dir as $key=>$val){
                            $dirname = 'c:/project/whloading/data/'.$val.'/'.$list_id;
                            $this->deleteDirectory($dirname);
                        }

                        ImgPd::where('list_load_m_id', $list_id)->delete();
                        ListLoadD::where('list_load_m_id', $list_id)->delete();
                        ListLoadSort::where('list_load_m_id', $list_id)->delete();
                    }
                    foreach ($excel_data as $k => $v) { 
                        $exp = explode('฿', $v);
                        // dd($exp);
                        $to_save = array();
                        $to_save['list_load_m_id'] = $list_id;                  
                        $to_save['user_id'] = Auth::user()->id;   
                        foreach ($arr['all'] as $karr => $varr) {
                            $save_det = array();
                            if($varr=='st_location_id'){
                                if(empty($base_data[$varr][trim($exp[0])])){
                                    $save_det['name'] = trim($exp[0]);
                                    $loc_id = StLocation::create($save_det)->id; 
                                    $base_data[$varr][trim($exp[0])] = $loc_id;
                                }else{
                                    $loc_id = $base_data[$varr][trim($exp[0])];
                                }
                                $to_save[$varr] = $loc_id;
                            }elseif($varr=='st_type_id'){
                                if(empty($base_data[$varr][trim($exp[1])])){
                                    $save_det['name'] = trim($exp[1]);
                                    $type_id = StType::create($save_det)->id; 
                                    $base_data[$varr][trim($exp[1])] = $type_id;
                                }else{
                                    $type_id = $base_data[$varr][trim($exp[1])];
                                }
                                $to_save[$varr] = $type_id;
                            }elseif($varr=='product_id'){
                                $save_det['name'] = trim($exp[4]);
                                $save_det['desc_list'] = trim($exp[5]);
                                if(empty($base_data[$varr]['name'][trim($exp[4])])){                                    
                                    $pd_id = Product::create($save_det)->id; 
                                    $base_data[$varr][trim($exp[4])] = $pd_id;
                                }else{
                                    $pd_id = $base_data[$varr]['name'][trim($exp[4])];
                                    if(empty($base_data[$varr]['desc_list'][trim($exp[5])])){
                                        $pd_update = Product::findOrFail($pd_id); 
                                        $pd_update->update($save_det);
                                    }                                    
                                }
                                $to_save[$varr] = $pd_id;
                            }elseif($varr=='laco_batch_id'){
                                if(empty($base_data[$varr][trim($exp[10])])){
                                    $save_det['name'] = trim($exp[10]);
                                    $lc_b_id = LacoBatch::create($save_det)->id; 
                                    $base_data[$varr][trim($exp[10])] = $lc_b_id;
                                }else{
                                    $lc_b_id = $base_data[$varr][trim($exp[10])];
                                }
                                $to_save[$varr] = $lc_b_id;  
                            }elseif($varr=='shipment_id'){
                                if(empty($base_data[$varr][trim($exp[12])])){
                                    $save_det['name'] = trim($exp[12]);
                                    $sm_id = Shipment::create($save_det)->id; 
                                    $base_data[$varr][trim($exp[12])] = $sm_id;
                                }else{
                                    $sm_id = $base_data[$varr][trim($exp[12])];
                                }
                                $to_save[$varr] = $sm_id;  
                            }elseif($varr=='status_id'){
                                if(empty($base_data[$varr][trim($exp[17])])){
                                    $save_det['name'] = trim($exp[17]);
                                    $ls_id = ListStatus::create($save_det)->id; 
                                    $base_data[$varr][trim($exp[17])] = $ls_id;
                                }else{
                                    $ls_id = $base_data[$varr][trim($exp[17])];
                                }
                                $to_save[$varr] = $ls_id; 
                            }elseif($varr=='usage_id'){
                                if(empty($base_data['status_id'][trim($exp[18])])){
                                    $save_det['name'] = trim($exp[18]);
                                    $ls_id = ListStatus::create($save_det)->id; 
                                    $base_data['status_id'][trim($exp[18])] = $ls_id;
                                }else{
                                    $ls_id = $base_data['status_id'][trim($exp[18])];
                                }
                                $to_save[$varr] = $ls_id; 
                            }else{
                                if(!empty($varr) && !empty($exp[$karr])){
                                    if($karr == 7 || $karr == 8 || $karr == 9){
                                        $date_exp = array();
                                        if(strtotime($exp[$karr])){
                                            $to_save[$varr] = date('Y-m-d', strtotime($exp[$karr]));
                                        }else{
                                            $date_exp = explode('/', trim($exp[$karr]));
                                            if(count($date_exp)==0){
                                                $date_exp = explode('.', trim($exp[$karr]));
                                            }
                                            if(count($date_exp)>0){
                                                $to_save[$varr] = date('Y-m-d', strtotime($date_exp[2].'-'.$date_exp[1].'-'.$date_exp[0]));
                                            }
                                        }
                                    }else{
                                        $to_save[$varr] = trim($exp[$karr]);
                                    }
                                }       

                            } 
                        }                        
                        // dd($to_save);
                        ListLoadD::create($to_save);  
                    }

                    $to_save = array();
                    $to_save['list_load_m_id'] = $list_id;                    
                    $to_save['user_id'] = Auth::user()->id;  
                    $sort = 0;
                    $ll_det = ListLoadD::where('list_load_m_id', $list_id)
                        ->selectRaw('product_id, sum(ur_qty) AS num_pd')
                        ->groupBy('product_id')->get();
                    foreach ($ll_det as $key) {                        
                        $to_save['product_id'] = $key->product_id;
                        $to_save['sort'] = ++$sort;
                        $to_save['quantity'] = $key->num_pd;
                        ListLoadSort::create($to_save); 
                    }

                    return redirect('list_load_det')->with('success','Upload successfully');
                }
            } else {
                echo SimpleXLSX::parseError();
            }   
        }     
    }

    function deleteDirectory($path){
        if(is_dir($path)){
            $files = array_diff(scandir($path), array('.','..'));   
            foreach($files as $file){
                echo "$path/$file".'->';
                echo is_dir("$path/$file").'</br>';
                if(is_dir($path.'/'.$file)){
                    $this->deleteDirectory("$path/$file");
                }
                if (file_exists("$path/$file")) {unlink("$path/$file");}
            }
            return rmdir($path);
        }
    }       

    public function capture(Request $request,$id)
    {
        if ($request->hasFile('file_webcam'.$id)) {
            $list_loadD = ListLoadD::findOrFail($id);
            $zipfile = $request->file('file_webcam'.$id);

            $fileName = uniqid() . '.jpg';
                
            // $completedirectory = 'images/webcam_det/'.$list_loadD->list_load_m_id.'/'.$id.'/'; 
            $completedirectory = config('my.directory.lot_load.detail').$list_loadD->list_load_m_id.'/'.$id.'/'; 
            if (!is_dir($completedirectory)) {
                mkdir($completedirectory, 0777, true);
            }else{
                if (file_exists($list_loadD->list_img_path))
                    unlink($list_loadD->list_img_path);
            }
            
            $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
            $size=GetimageSize($zipfile);
            $height=round($width*$size[1]/$size[0]);
            $images_orig = ImageCreateFromJPEG($zipfile);
            $photoX = ImagesX($images_orig);
            $photoY = ImagesY($images_orig);
            $images_fin = ImageCreateTrueColor($width, $height);
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
            ImageJPEG($images_fin,$completedirectory.$fileName);
            ImageDestroy($images_orig);
            ImageDestroy($images_fin);

            if(empty($list_loadD->sort)){
                $list_loadD_sort = ListLoadD::where('list_load_m_id', $list_loadD->list_load_m_id)->orderBy('sort', 'desc')->limit(1)->get();
                if(is_null($list_loadD_sort[0]['sort']))    $sort = 1;
                else    $sort = $list_loadD_sort[0]['sort']+1;
                $requestData['sort'] = $sort;
            }
            $requestData['list_img'] = $fileName;
            $requestData['list_img_path'] = $completedirectory.$fileName;
            $list_loadD->update($requestData);
            return back()->with('success','บันทึกรูปภาพแล้ว');
        }
    }

    public function save_sort($list_id, $requestData)
    {
        //ถ้าไม่มีสินค้าตัวไหนแล้วต้องลบทิ้งทั้งรูปและ sort 
        $exp = array();
        $ll_det = ListLoadD::where('list_load_m_id', $list_id)->selectRaw("product_id, sum(ur_qty) AS num_pd")->groupBy('product_id')->get();
        foreach ($ll_det as $key) {
            $exp[] = $key->product_id;
            $qty[$key->product_id] = $key->num_pd;
        }
        $ll_sort = ListLoadSort::where('list_load_m_id', $list_id)->whereNotIn('product_id', $exp)->get();
        if(count($ll_sort)>0){
            foreach ($ll_sort as $key) {
                ListLoadSort::where('id', $key->id)->delete();
                $dirname = config('my.directory.lot_load.pdf').$list_id.'/'.$key->product_id;
                $this->deleteDirectory($dirname);
            }
        }        
        //เรียงตัวที่เหลือใหม่
        $sort = 0;
        $ll_sort = ListLoadSort::where('list_load_m_id', $list_id)->orderBy('sort')->get();
        if(count($ll_sort)>0){
            foreach ($ll_sort as $key) {
                if (($kpd = array_search($key->product_id, $exp)) !== false) {
                    unset($exp[$kpd]);
                }
                $set_sort = array();
                $set_sort['sort'] = ++$sort;
                $set_sort['quantity'] = $qty[$key->product_id];
                $set_sort['user_id'] = Auth::user()->id;
                ListLoadSort::where('id', $key->id)->update($set_sort);
            }
        }
        if(count($exp)>0){
            foreach ($exp as $key => $value) {
                $to_save = array();
                $to_save['list_load_m_id'] = $list_id;                      
                $to_save['product_id'] = $value;  
                $to_save['sort'] = ++$sort; 
                $to_save['quantity'] = $qty[$value];
                $to_save['user_id'] = Auth::user()->id;  
                ListLoadSort::create($to_save); 
            }
        }
    }
}
