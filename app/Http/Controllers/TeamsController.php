<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use SimpleXLSX;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'teams';

    public function index(Request $request)
    {
        $perPage = 25;
        $teams = new Team();

        if (!empty($request->get('search'))) {
            $teams = $teams->where('name', 'like', '%' . $request->get('search') . '%');
        }
        if (!empty($request->get('status'))) {
            $teams = $teams->where('status', $request->get('status'));
        }
        $teams = $teams->latest()->paginate($perPage);

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('teams.index', compact('teams', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('teams.create', compact('mainnav', 'namenav'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        Team::create($requestData);

        return redirect('teams')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $team = Team::findOrFail($id);
        return view('teams.view', compact('team', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $team = Team::findOrFail($id);
        return view('teams.edit', compact('team', 'mainnav', 'namenav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $team = Team::findOrFail($id);
        $team->update($requestData);

        return redirect('teams')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Team::destroy($id);
        $set_status['status'] = 'Inactive';
        $team = Team::findOrFail($id);
        $team->update($set_status);

        return redirect('teams')->with('flash_message', ' deleted!');
    }

    public function importExportView()
    {
       return view('teams.import');
    }

    public function import(Request $request)
    {
        // dd($id);
        set_time_limit(0);
        if ($request->hasFile('file_upload')) {
            $tmpfolder = md5(time());
            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path('storage/upload/' . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = 'upload/' . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;
            // dd($uploadfile);
            $tmpUploadD = array();
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $team_name = array();
                $list_team =array();
                $check_team =array();
                $err_team =array();
                $sort_team =array();
                $unique_team =array();

                $team = Team::get();
                foreach($team as $key){
                    $team_name[] = $key->name." ".$key->type;
                }
                // print_r($team_name);

                // $typeloadingteam = array(
                //     'Front' => 'พนักงานหน้าตู้',
                //     'Back' => 'พนักงานหลังตู้',
                //     'FL' => 'พนักงาน FL',
                //     'Ctrl' => 'พนักงานคุมโหลด',
                //     'Bfload' => 'พนักงานตรวจเช็คสินค้าก่อนโหลด',
                //     'Bkbox' => 'พนักงานตรวจเช็คกล่องแตก');

                $typeloadingteam = array('Front', 'Back', 'FL', 'Ctrl', 'Bfload', 'Bkbox','WRITE');

                foreach ($xlsx->rows() as $r => $row) {
                    if ($r > 0) {
                        // $list_team[] = $row[1];
                        $list_name[] = $row[2];
                        $list_team[] = $row[1]." ".$row[2];
                        $check_team[$row[1]." ".$row[2]][] = $r+1;
                        // $check_team[$row[1]]['row'][] = $r+1;
                        // $check_team[$row[1]]['name'][] = $row[2];
                    }
                }
                // dd($check_team);

                //เช็ค type ในไฟล์
                foreach ($list_name as $count => $name) {
                    // foreach ($name as $type => $row) {
                        $check_array = array_search($name, $typeloadingteam);
                        // echo "---</br>";
                        if(strlen($check_array)==0){
                            // echo $count."---</br>";
                            $err_team[$check_team[$list_team[$count]][0]]['row'] = $check_team[$list_team[$count]][0];
                            $err_team[$check_team[$list_team[$count]][0]]['name'] = $list_team[$count];
                            $err_team[$check_team[$list_team[$count]][0]]['note'] = 'ข้อมูล type ไม่สอดคล้องในบรรทัดที่';

                            foreach ($check_team[$list_team[$count]] as $kid => $vrow) {
                                $err_team[$check_team[$list_team[$count]][0]]['note'] .= ' '.$vrow;
                            }
                        }
                    // }
                }
                // dd($err_team);

                //เช็คข้อมูลซ้ำในไฟล์
                foreach ($check_team as $count => $name) {
                    // foreach ($name as $type => $row) {
                        if(count($check_team[$count]) > 1) {
                            // echo $count."---</br>";
                            $err_team[$check_team[$count][0]]['row'] = $check_team[$count][0];
                            $err_team[$check_team[$count][0]]['name'] = $count;

                            $note = 'พบข้อมูลพนักงานซ้ำนี้ในบรรทัดที่';
                            foreach ($check_team[$count] as $kid => $vrow) {
                                $note .= ' '.$vrow;
                            }

                            if(!empty($err_team[$check_team[$count][0]]['note'])){
                                $err_team[$check_team[$count][0]]['note'] .= ', '.$note;
                            }else{
                                $err_team[$check_team[$count][0]]['note'] = $note;
                            }
                        }
                    // }
                }
                // dd($err_team);

                //เช็คสินค้าซ้ำใน db
                $unique_team = array_unique($list_team);
                // dd($unique_user);
                foreach ($unique_team as $klist=>$vlist) {
                    // echo   $vlist."</br>";
                    $check_array = array_search($vlist, $team_name);
                    // echo "---</br>";
                    if(strlen($check_array)>0){
                        // echo $err_user[$check_user[$vlist]['row'][0]]['row'].'ค้นพบคำว่า Toshiba</br>';
                        $err_team[$check_team[$vlist][0]]['row'] = $check_team[$vlist][0];
                        $err_team[$check_team[$vlist][0]]['name'] = $vlist;
                        if(!empty($err_team[$check_team[$vlist][0]]['note'])){
                            $err_team[$check_team[$vlist][0]]['note'] .= ', ข้อมูลนี้มีอยู่แล้ว';
                        }else{
                            $err_team[$check_team[$vlist][0]]['note'] = 'ข้อมูลนี้มีอยู่แล้ว';
                        }
                    }
                }
                // dd($err_team);

                //เรียงลำดับ array
                // echo count($list_user)."</br>";
                for($i=1;$i<=count($list_team)+1;$i++){
                    // print_r($err_user[$i]);
                    // echo $i."--</br>";
                    if(!empty($err_team[$i])){
                        // print_r($err_user[$i])."</br>";
                        $sort_team[$i]['sort'] = $i;
                        $sort_team[$i]['name'] = $err_team[$i]['name'];
                        $sort_team[$i]['note'] = $err_team[$i]['note'];
                    }
                }
                // dd($sort_team);

                if(!empty($sort_team)){
                    return view('teams.err_import',compact('sort_team'));
                }else{
                    $team_load = array();
                    foreach ($xlsx->rows() as $r => $row) {
                        if ($r > 0) {
                            // $requestData = $request->all();
                            $team_load['name'] = $row[1];
                            $team_load['type'] = $row[2];
                            $team_load['status'] = 'Active';
                            // dd($product_load);
                            $query_team = Team::create($team_load);
                        }
                    }
                    return redirect('teams')->with('success','Updated successfully');
                }
            } else {
                echo SimpleXLSX::parseError();
            }
        }
    }
}
