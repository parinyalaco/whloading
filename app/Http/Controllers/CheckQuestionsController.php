<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CheckSet;
use App\Models\CheckQuestion;

class CheckQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $mainnav = 'BaseData';
    public $namenav = 'check_questions';
    public $statusList = [
        'Active' => 'Active',
        'Inactive' => 'Inactive'
    ];
    public $typeQuestionList = [
        'TEXT' => 'TEXT',
        'IMAGE' => 'IMAGE',
        'LONGTEXT' => 'LONGTEXT'
    ];

    public function index($check_set_id,Request $request)
    {
        $checkset = CheckSet::findOrFail($check_set_id);

        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $checkquestions = CheckQuestion::where('name', 'like', '%' . $keyword . '%')
                ->where('check_set_id', $check_set_id)
                ->paginate($perPage);
        } else {
            $checkquestions = CheckQuestion::where('check_set_id', $check_set_id)->latest()->paginate($perPage);
        }

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('admin.checkquestions.index', compact('checkquestions', 'mainnav', 'namenav', 'checkset'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($check_set_id)
    {
        $checkset = CheckSet::findOrFail($check_set_id);
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $statusList = $this->statusList;
        $typeQuestionList = $this->typeQuestionList;
        return view('admin.checkquestions.create', compact('mainnav', 'namenav','statusList', 'typeQuestionList', 'checkset'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($check_set_id, Request $request)
    {
        $requestData = $request->all();
        $requestData['check_set_id'] = $check_set_id;
        CheckQuestion::create($requestData);

        return redirect('admin/'. $check_set_id.'/check_questions')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($check_set_id, $id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $checkquestion = CheckQuestion::findOrFail($id);
        return view('admin.checkquestions.view', compact('checkquestion', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($check_set_id, $id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $checkquestion = CheckQuestion::findOrFail($id);
        $typeQuestionList = $this->typeQuestionList;

        $statusList = $this->statusList;
        return view('admin.checkquestions.edit', compact('checkquestion', 'mainnav', 'namenav', 'statusList', 'typeQuestionList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $check_set_id, $id)
    {
        $requestData = $request->all();


        $checkquestion = CheckQuestion::findOrFail($id);


        $checkquestion->update($requestData);

        return redirect('admin/'. $check_set_id.'/check_questions')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($check_set_id, $id)
    {
        $countcandelete = CheckQuestion::findOrFail($id)->checkanswers()->count();

        if ($countcandelete == 0) {
            CheckQuestion::destroy($id);
            return redirect('admin/'. $check_set_id.'/check_questions')->with('flash_message', ' deleted!');
        } else {
            return back()->with('error', 'Delete not successfully');
        }
    }
}
