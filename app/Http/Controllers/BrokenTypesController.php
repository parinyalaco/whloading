<?php

namespace App\Http\Controllers;

use App\Models\BrokenType;
use Illuminate\Http\Request;

class BrokenTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'broken-types';    

    public function index()
    {
        $perPage = 25;
        $brokentypes = BrokenType::orderBy('seq','asc')->paginate($perPage);

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('broken-types.index', compact('brokentypes', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('broken-types.create', compact('mainnav', 'namenav'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        BrokenType::create($requestData);

        return redirect('broken-types')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $brokentype = BrokenType::findOrFail($id);
        return view('broken-types.view', compact('brokentype', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $brokentype = BrokenType::findOrFail($id);
        return view('broken-types.edit', compact('brokentype', 'mainnav', 'namenav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $brokentype = BrokenType::findOrFail($id);
        $brokentype->update($requestData);

        return redirect('broken-types')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BrokenType::destroy($id);

        return redirect('broken-types')->with('flash_message', ' deleted!');
    }
}
