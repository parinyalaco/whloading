<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use Illuminate\Http\Request;

class CustomerTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'customer-types';


    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $perPage = 25;
        $customertypes = CustomerType::latest()->paginate($perPage);

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('customer-types.index', compact('customertypes', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('customer-types.create', compact('mainnav', 'namenav'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        CustomerType::create($requestData);

        return redirect('customer-types')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $customertype = CustomerType::findOrFail($id);
        return view('customer-types.view', compact('customertype', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $customertype = CustomerType::findOrFail($id);
        return view('customer-types.edit', compact('customertype', 'mainnav', 'namenav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $customertype = CustomerType::findOrFail($id);
        $customertype->update($requestData);

        return redirect('customer-types')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustomerType::destroy($id);

        return redirect('customer-types')->with('flash_message', ' deleted!');
    }
}
