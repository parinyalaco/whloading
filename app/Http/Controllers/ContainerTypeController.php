<?php

namespace App\Http\Controllers;

use App\Models\ContainerType;
use Illuminate\Http\Request;

class ContainerTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $mainnav = 'BaseData';
    public $namenav = 'container-types';    

    public function index()
    {
        $perPage = 25;
        $containertypes = ContainerType::where('active','Active')->orderBy('name','asc')->paginate($perPage);

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('container-types.index', compact('containertypes', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        return view('container-types.create', compact('mainnav', 'namenav'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        ContainerType::create($requestData);

        return redirect('container-types')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;

        $containertype = ContainerType::findOrFail($id);
        return view('container-types.view', compact('containertype', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        
        $containertype = ContainerType::findOrFail($id);
        return view('container-types.edit', compact('containertype', 'mainnav', 'namenav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $containertype = ContainerType::findOrFail($id);
        $containertype->update($requestData);

        return redirect('container-types')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //ContainerType::destroy($id);

        $containertype = ContainerType::findOrFail($id);
        $set_active['active'] = 'Inactive';
        $containertype->update($set_active);

        return redirect('container-types')->with('flash_message', ' deleted!');
    }
}
