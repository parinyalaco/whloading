<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasterPlanM;
use App\Models\MasterPlanD;
use App\Models\Product;
use App\Models\CustomerType;

class MasterPlansController extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        $masterplanObj = new MasterPlanM();
        if(!empty($keyword)){
            $productslist = Product::where('name','like','%'.$keyword.'%')->pluck('id','id');
            $masterplanidlist = MasterPlanD::whereIn('t_product_m_id', $productslist)->orWhereIn('l_product_m_id', $productslist)->pluck('master_plan_m_id', 'master_plan_m_id');
            $masterplanObj = $masterplanObj->whereIn('id', $masterplanidlist)->orWhere('name','like', '%' . $keyword . '%');

        }

        $masterplans = $masterplanObj->latest()->paginate($perPage);

        return view('master-plans.index', compact('masterplans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customertypelist = CustomerType::pluck('name', 'id');
        $productlist = Product::pluck('name', 'id');
        return view('master-plans.create', compact( 'productlist', 'customertypelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $tmpPlanMasterM = array();
        $tmpPlanMasterM['name'] = $requestData['name'];
        $tmpPlanMasterM['customer_type_id'] = $requestData['customer_type_id'];
        $tmpPlanMasterM['customer'] = $requestData['customer'];
        $tmpPlanMasterM['note'] = $requestData['note'];
        $tmpPlanMasterM['status'] = $requestData['status'];

        $masterplanm = MasterPlanM::create($tmpPlanMasterM);

        $id = $masterplanm->id;
        for ($i = 1; $i <= 10; $i++) {
            if (!empty($requestData['t_product_id_' . $i]) || !empty($requestData['l_product_id_' . $i])) {
                if (
                    !empty($requestData['t_product_id_' . $i])
                    && !empty($requestData['t_from_line' . $i])
                    && !empty($requestData['t_to_line' . $i])
                    && !empty($requestData['t_base' . $i])
                    && !empty($requestData['t_height' . $i])
                ) {
                    if ($requestData['t_from_line' . $i] <=  $requestData['t_to_line' . $i]) {
                        $tloopdata = array();
                        for ($it = $requestData['t_from_line' . $i]; $it <= $requestData['t_to_line' . $i]; $it++) {
                            $tmp = array();
                            $tmp['master_plan_m_id'] = $id;
                            $tmp['product_id'] = $requestData['t_product_id_' . $i];
                            $tmp['t_product_m_id'] = $requestData['t_product_id_' . $i];
                            $tmp['main_type'] = 'Normal';
                            $tmp['row'] = $it;
                            $tmp['t_base'] = $requestData['t_base' . $i];
                            $tmp['t_height'] = $requestData['t_height' . $i];
                            $tmp['t_total'] = $tmp['t_base'] * $tmp['t_height'];

                            $chk = MasterPlanD::where('master_plan_m_id', $id)
                            ->where('row', $tmp['row'])
                                ->first();

                            if (!empty($chk)) {
                                $chk->update($tmp);
                                $chk->recalculate($chk->id);
                            } else {
                                $chk2 = MasterPlanD::create($tmp);
                                $chk2->recalculate($chk2->id);
                            }
                        }
                    }
                }
                if (
                    !empty($requestData['l_product_id_' . $i])
                    && !empty($requestData['l_from_line' . $i])
                    && !empty($requestData['l_to_line' . $i])
                    && !empty($requestData['l_base' . $i])
                    && !empty($requestData['l_height' . $i])
                ) {
                    if ($requestData['l_from_line' . $i] <=  $requestData['l_to_line' . $i]) {
                        $lloopdata = array();
                        for ($il = $requestData['l_from_line' . $i]; $il <= $requestData['l_to_line' . $i]; $il++) {
                            $tmp = array();
                            $tmp['master_plan_m_id'] = $id;
                            $tmp['product_id'] = $requestData['l_product_id_' . $i];
                            $tmp['l_product_m_id'] = $requestData['l_product_id_' . $i];
                            $tmp['main_type'] = 'Normal';
                            $tmp['row'] = $il;
                            $tmp['l_base'] = $requestData['l_base' . $i];
                            $tmp['l_height'] = $requestData['l_height' . $i];
                            $tmp['l_total'] = $tmp['l_base'] * $tmp['l_height'];

                            $chk = MasterPlanD::where('master_plan_m_id', $id)
                            ->where('row', $tmp['row'])
                                ->first();

                            if (!empty($chk)) {
                                $chk->update($tmp);
                                $chk->recalculate($chk->id);
                            } else {
                                $chk2 = MasterPlanD::create($tmp);
                                $chk2->recalculate($chk2->id);
                            }
                        }
                    }
                }
            }
        }
        return redirect('master-plans/' . $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterplanm = MasterPlanM::findOrFail($id);
        return view('master-plans.view', compact('masterplanm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterplanm = MasterPlanM::findOrFail($id);
        $customertypelist = CustomerType::pluck('name', 'id');
        return view('master-plans.edit', compact('masterplanm', 'customertypelist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $masterplanm = MasterPlanM::findOrFail($id);
        $masterplanm->update($requestData);

        return redirect('master-plans')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MasterPlanD::where('master_plan_m_id', $id)->delete();

        MasterPlanM::destroy($id);

        return redirect('master-plans')->with('flash_message', ' deleted!');
    }

    public function createdetail($master_plan_m_id)
    {
        $masterplanm = MasterPlanM::findOrFail($master_plan_m_id);
        $productlist = Product::pluck('name', 'id');
        return view('master-plans.createdetail', compact('masterplanm', 'productlist'));
    }

    public function createdetailAction(Request $request, $id)
    {
        $requestData = $request->all();
        $requestData['product_id'] = $requestData['t_product_m_id'];
        if (!empty($requestData['l_product_m_id'])) {
            $requestData['product_id'] = $requestData['l_product_m_id'];
        }
        MasterPlanD::create($requestData);
        return redirect('master-plans/' . $id);
    }

    public function editdetail($id){
        $masterpland = MasterPlanD::findOrFail($id);
        $productlist = Product::pluck('name', 'id');
        return view('master-plans.editdetail', compact('masterpland', 'productlist'));
    }

    public function editdetailAction(Request $request, $id)
    {
        $requestData = $request->all();
        $requestData['product_id'] = $requestData['t_product_m_id'];
        if (!empty($requestData['l_product_m_id'])) {
            $requestData['product_id'] = $requestData['l_product_m_id'];
        }
        $masterpland = MasterPlanD::findOrFail($id);
        $masterpland->update($requestData);
        return redirect('master-plans/' . $masterpland->master_plan_m_id);
    }

    public function deletedetail($id)
    {
        $master_plan_m_id = MasterPlanD::findOrFail($id)->master_plan_m_id;

        MasterPlanD::destroy($id);

        return redirect('master-plans/' . $master_plan_m_id);
    }
}
