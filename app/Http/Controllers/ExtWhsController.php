<?php

namespace App\Http\Controllers;

use App\Models\ExtWh;
use App\Models\LoadCarryM;
use Illuminate\Http\Request;

class ExtWhsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'ext_whs';
    public $statusList = [
        'Active' => 'Active',
        'Inactive' => 'Inactive'
    ];

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $ext_whs = ExtWh::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $ext_whs = ExtWh::latest()->paginate($perPage);
        }

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('ext_whs.index', compact('ext_whs', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $statusList = $this->statusList;
        return view('ext_whs.create', compact('mainnav', 'namenav', 'statusList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:ext_whs',
        ]);

        ExtWh::create($requestData);

        return redirect('ext_whs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $ext_wh = ExtWh::findOrFail($id);
        return view('ext_whs.view', compact('ext_wh', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $ext_wh = ExtWh::findOrFail($id);

        $statusList = $this->statusList;
        return view('ext_whs.edit', compact('ext_wh', 'mainnav', 'namenav', 'statusList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $ext_wh = ExtWh::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:ext_whs,name,' . $ext_wh->id,
        ]);


        $ext_wh->update($requestData);

        return redirect('ext_whs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countcandelete = LoadCarryM::where('ext_wh_id', $id)
        ->count();

        if ($countcandelete == 0) {
            ExtWh::destroy($id);
            return redirect('ext_whs')->with('flash_message', ' deleted!');
        } else {
            return back()->with('error', 'Delete not successfully');
        }
    }
}
