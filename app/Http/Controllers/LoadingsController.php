<?php

namespace App\Http\Controllers;

use App\Models\BrokenType;
use Illuminate\Http\Request;
use App\Models\LoadM;
use App\Models\Product;
use App\Models\LoadD;
use App\Models\LoadTeam;
use App\Models\Team;
use App\Models\CustomerType;
use App\Models\LoadBroken;
use App\Models\MasterPlanM;
use App\Models\MasterPlanD;
use App\Models\LoadCounting;
use App\Models\LoadCountingLog;
use App\Models\reason;
use App\Models\ContainerType;
use App\Models\CurrentDeliveryPlan;
use App\Models\LoadLot;
use App\Models\LoadingImage;

use Auth;
use PDF;
use App\Mail\PlanLoad;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class LoadingsController extends Controller
{
    public $PUB_PICS = [
        1 => "รูปวัดแถวแนวยาว",
        2 => "รูปวัดแถวแนวขวาง",
        3 => "รูปวัดแถวที่ 10",
        4 => "รูปวัดแถวที่ 20",
        5 => "รูปป้ายโหลด",
        6 => "รูปท้ายตู้",
        7 => "รูปสินค้าตัวอย่าง",
        8 => "รูปการใส่ถุงลม",
        9 => "รูปล็อค SEAL",
        10 => "รูปท้ายรถหลังล็อค Seal",
        11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
        12 => "ใบ Lot โหลด",
        13 => "Plan โหลด(สำหรับเก็บ plan ที่มีการเซ็นชื่อ)",
        14 => "แถวสุดท้าย สินค้า Product 1",
        15 => "แถวสุดท้าย สินค้า Product 2",
        16 => "แถวสุดท้าย  สินค้า Product 3",
        17 => "รูปปัญหาที่พบในการโหลด รูปที่ 1",
        18 => "รูปปัญหาที่พบในการโหลด รูปที่ 2  ",
        19 => "รูปปัญหาที่พบในการโหลด รูปที่ 3",
        20 => "รูปการตรวจตู้ 1",
        21 => "รูปการตรวจตู้ 2",
        22 => "รูปการตรวจตู้ 3",
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list(Request $request)
    {
        if (Auth::user()->group->name == 'View') $status = 'Loaded';
        else   $status = $request->get('status');

        if (Auth::user()->group->name == 'Driver') {
            return redirect(route('drivers.index'));
        }
        $keyword = $request->get('search');
        $perPage = 10;
        $loadmObj = new LoadM();
        if (!empty($status)) {
            $loadmObj = $loadmObj->where('status', $status);
        }
        if (!empty($keyword)) {

            $productid = Product::where('name', 'like', '%' . $keyword . '%')
                ->orWhere('desc', 'like', '%' . $keyword . '%')
                ->orWhere('boxcode', 'like', '%' . $keyword . '%')
                ->orWhere('bagcode', 'like', '%' . $keyword . '%')
                ->pluck('id', 'id');

            $loadmid = LoadD::whereIn('t_product_id', $productid)
                ->orWhereIn('l_product_id', $productid)
                ->pluck('load_m_id', 'load_m_id');

            $loadmObj = $loadmObj->where('order_no', 'like', '%' . $keyword . '%')
                ->orWhere('customer', 'like', '%' . $keyword . '%')
                ->orWhereIn('id', $loadmid);
        }
        $loadms = $loadmObj->latest()->paginate($perPage);
        // $to_js = $loadmObj->get();
        $to_alert = array();
        foreach ($loadms as $key) {
            $to_alert[$key->id]['load_date'] = $key->load_date;
            $cus = $key->customertype->name ?? '-';
            $to_alert[$key->id]['customer'] = $cus . '/' . $key->customer;
            $to_alert[$key->id]['order_no'] = $key->order_no;
        }

        return view('loadings.index', compact('loadms', 'status', 'to_alert'));
    }

    public function create()
    {
        $customertypelist = CustomerType::pluck('name', 'id');
        return view('loadings.create', compact('customertypelist'));
    }

    public function createAction(Request $request)
    {
        $requestData = $request->all();
        $requestData['status'] = 'New';
        $loadm = LoadM::create($requestData);

        return redirect('loadings/planload/' . $loadm->id);
    }

    public function show($id)
    {
        // $loadm = LoadM::findOrFail($id);
        // $teams = array();
        // foreach ($loadm->loadteams as $loadteamObj) {
        //     $teams[$loadteamObj->team_type][] = $loadteamObj;
        // }
        // $pics = array(
        //     1 => "รูปวัดแถวแนวยาว",
        //     2 => "รูปวัดแถวแนวขวาง",
        //     3 => "รูปวัดแถวที่ 10",
        //     4 => "รูปวัดแถวที่ 20",
        //     5 => "รูปป้ายโหลด",
        //     6 => "รูปท้ายตู้",
        //     7 => "รูปสินค้าตัวอย่าง",
        //     8 => "รูปการใส่ถุงลม",
        //     9 => "รูปล็อค SEAL",
        //     10 => "รูปท้ายรถหลังล็อคSeal",
        //     11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
        //     12 => "ใบ Lot โหลด",
        //     13 => "Plan โหลด(สำหรับเก็บ plan ที่มีการเซ็นชื่อ)",
        // );

        $loadm = array();
        $teams = array();
        $pics = array();

        $loadm = LoadM::findOrFail($id);

        if ($loadm->type_loading == "3Rows") {
            $test = $this->to_show3rows($id);
            // dd($test);
            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];
            // dd($teams);
            return view('loadings.view3rows', compact('loadm', 'teams', 'pics', 'conveyor'));
        } elseif ($loadm->type_loading == "Pallet") {
            $test = $this->to_show($id);
            // dd($test);
            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];
            // dd($teams);
            return view('loadings.viewpallet', compact('loadm', 'teams', 'pics', 'conveyor'));
        } else {
            $test = $this->to_show($id);
            // dd($test);
            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];
            // dd($teams);
            return view('loadings.view', compact('loadm', 'teams', 'pics', 'conveyor'));
        }
    }

    public function edit($id)
    {
        $loadm = LoadM::findOrFail($id);
        $customertypelist = CustomerType::pluck('name', 'id');
        return view('loadings.edit', compact('loadm', 'customertypelist'));
    }

    public function editAction(Request $request, $id)
    {
        $requestData = $request->all();
        $loadm = LoadM::findOrFail($id);
        $loadm->update($requestData);

        return redirect('loadings?status=' . $loadm->status);
    }

    public function delete($id)
    {
        LoadBroken::where('load_m_id', $id)->delete();
        LoadTeam::where('load_m_id', $id)->delete();
        LoadD::where('load_m_id', $id)->delete();

        LoadM::destroy($id);

        return redirect('loadings');
    }

    public function planload($id)
    {
        $loadm = LoadM::findOrFail($id);
        $productlist = Product::pluck('name', 'id');
        if ($loadm->type_loading == "3Rows") {
            return view('loadings.planload3line', compact('loadm', 'productlist'));
        } else {
            if ($loadm->type_loading == "Pallet") {
                return view('loadings.planloadpallet', compact('loadm', 'productlist'));
            } else {
                return view('loadings.planload', compact('loadm', 'productlist'));
            }
        }
    }

    public function planloadAction(Request $request, $id)
    {
        $requestData = $request->all();
        $loadm = LoadM::findOrFail($id);
        if ($loadm->type_loading == "3Rows") {
            for ($i = 1; $i <= 20; $i++) {
                if (!empty($requestData['t_product_id_' . $i]) || !empty($requestData['l_product_id_' . $i]) || !empty($requestData['c_product_id_' . $i])) {
                    if (
                        !empty($requestData['t_product_id_' . $i])
                        && !empty($requestData['t_box_style' . $i])
                        && !empty($requestData['t_from_line' . $i])
                        && !empty($requestData['t_to_line' . $i])
                        && !empty($requestData['t_base' . $i])
                        && !empty($requestData['t_height' . $i])
                    ) {
                        if ($requestData['t_from_line' . $i] <=  $requestData['t_to_line' . $i]) {
                            $tloopdata = array();
                            for ($it = $requestData['t_from_line' . $i]; $it <= $requestData['t_to_line' . $i]; $it++) {
                                $tmp = array();
                                $tmp['load_m_id'] = $id;
                                $tmp['product_id'] = $requestData['t_product_id_' . $i];
                                $tmp['t_product_m_id'] = $requestData['t_product_id_' . $i];
                                $tmp['t_box_style'] = $requestData['t_box_style' . $i];
                                $tmp['main_type'] = 'Normal';
                                $tmp['row'] = $it;
                                $tmp['t_base'] = $requestData['t_base' . $i];
                                $tmp['t_height'] = $requestData['t_height' . $i];
                                $tmp['t_excess'] = $requestData['t_excess' . $i];

                                if($tmp['t_excess'] > 0){
                                    $tmp['t_total'] = ($tmp['t_base'] * $tmp['t_height']) + $tmp['t_excess'];
                                }else{
                                    $tmp['t_total'] = $tmp['t_base'] * $tmp['t_height'];
                                }

                                $chk = LoadD::where('load_m_id', $id)
                                    ->where('row', $tmp['row'])
                                    ->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                    $chk->recalculate3rows($chk->id);
                                } else {
                                    $chk2 = LoadD::create($tmp);
                                    $chk2->recalculate3rows($chk2->id);
                                }
                            }
                        }
                    }
                    if (
                        !empty($requestData['l_product_id_' . $i])
                        && !empty($requestData['l_box_style' . $i])
                        && !empty($requestData['l_from_line' . $i])
                        && !empty($requestData['l_to_line' . $i])
                        && !empty($requestData['l_base' . $i])
                        && !empty($requestData['l_height' . $i])
                    ) {
                        if ($requestData['l_from_line' . $i] <=  $requestData['l_to_line' . $i]) {
                            $lloopdata = array();
                            for ($il = $requestData['l_from_line' . $i]; $il <= $requestData['l_to_line' . $i]; $il++) {
                                $tmp = array();
                                $tmp['load_m_id'] = $id;
                                $tmp['product_id'] = $requestData['l_product_id_' . $i];
                                $tmp['l_product_m_id'] = $requestData['l_product_id_' . $i];
                                $tmp['l_box_style'] = $requestData['l_box_style' . $i];
                                $tmp['main_type'] = 'Normal';
                                $tmp['row'] = $il;
                                $tmp['l_base'] = $requestData['l_base' . $i];
                                $tmp['l_height'] = $requestData['l_height' . $i];
                                $tmp['l_excess'] = $requestData['l_excess' . $i];

                                if($tmp['l_excess'] > 0){
                                    $tmp['l_total'] = ($tmp['l_base'] * $tmp['l_height']) + $tmp['l_excess'];
                                }else{
                                    $tmp['l_total'] = $tmp['l_base'] * $tmp['l_height'];
                                }

                                $chk = LoadD::where('load_m_id', $id)
                                    ->where('row', $tmp['row'])
                                    ->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                    $chk->recalculate3rows($chk->id);
                                } else {
                                    $chk2 = LoadD::create($tmp);
                                    $chk2->recalculate3rows($chk2->id);
                                }
                            }
                        }
                    }
                    if (
                        !empty($requestData['c_product_id_' . $i])
                        && !empty($requestData['c_box_style' . $i])
                        && !empty($requestData['c_from_line' . $i])
                        && !empty($requestData['c_to_line' . $i])
                        && !empty($requestData['c_base' . $i])
                        && !empty($requestData['c_height' . $i])
                    ) {
                        if ($requestData['c_from_line' . $i] <=  $requestData['c_to_line' . $i]) {
                            $lloopdata = array();
                            for ($il = $requestData['c_from_line' . $i]; $il <= $requestData['c_to_line' . $i]; $il++) {
                                $tmp = array();
                                $tmp['load_m_id'] = $id;
                                $tmp['product_id'] = $requestData['c_product_id_' . $i];
                                $tmp['c_product_m_id'] = $requestData['c_product_id_' . $i];
                                $tmp['c_box_style'] = $requestData['c_box_style' . $i];
                                $tmp['main_type'] = 'Normal';
                                $tmp['row'] = $il;
                                $tmp['c_base'] = $requestData['c_base' . $i];
                                $tmp['c_height'] = $requestData['c_height' . $i];
                                $tmp['c_excess'] = $requestData['c_excess' . $i];
                                if($tmp['c_excess'] > 0){
                                    $tmp['c_total'] = ($tmp['c_base'] * $tmp['c_height']) + $tmp['c_excess'];
                                }else{
                                    $tmp['c_total'] = $tmp['c_base'] * $tmp['c_height'];
                                }

                                $chk = LoadD::where('load_m_id', $id)
                                    ->where('row', $tmp['row'])
                                    ->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                    $chk->recalculate3rows($chk->id);
                                } else {
                                    $chk2 = LoadD::create($tmp);
                                    $chk2->recalculate3rows($chk2->id);
                                }
                            }
                        }
                    }
                }
            }
            return redirect('loadings/show/' . $id);
        } elseif ($loadm->type_loading == "Pallet") {
            for ($i = 1; $i <= 30; $i++) {
                if (
                    !empty($requestData['pallet_no_' . $i])
                    || !empty($requestData['p_product_id_' . $i])
                    || !empty($requestData['p_base_' . $i])
                    || !empty($requestData['p_height_' . $i])
                    || !empty($requestData['p_lot_' . $i])
                    || !empty($requestData['p_exp_date_' . $i])
                ) {


                    $tmp = array();
                    $tmp['load_m_id'] = $id;
                    $tmp['pallet_no'] = $requestData['pallet_no_' . $i];
                    $tmp['product_id'] = $requestData['p_product_id_' . $i];
                    $tmp['t_product_m_id'] = $requestData['p_product_id_' . $i];
                    $tmp['main_type'] = 'Normal';
                    $tmp['row'] = $i;
                    $tmp['t_base'] = $requestData['p_base' . $i];
                    $tmp['t_height'] = $requestData['p_height' . $i];
                    $tmp['t_excess'] = $requestData['p_excess' . $i];
                    $tmp['p_lot'] = $requestData['p_lot_' . $i];
                    $tmp['p_exp_date'] = $requestData['p_exp_date_' . $i];
                    if($tmp['t_excess'] > 0){
                        $tmp['t_total'] = ($tmp['t_base'] * $tmp['t_height']) + $tmp['t_excess'];
                    }else{
                        $tmp['t_total'] = $tmp['t_base'] * $tmp['t_height'];
                    }

                    $chk = LoadD::where('load_m_id', $id)
                        ->where('row', $tmp['row'])
                        ->first();

                    if (!empty($chk)) {
                        $chk->update($tmp);
                        $chk->recalculate($chk->id);
                    } else {
                        $chk2 = LoadD::create($tmp);
                        $chk2->recalculate($chk2->id);
                    }
                }
            }
            return redirect('loadings/show/' . $id);
        } else {
            for ($i = 1; $i <= 10; $i++) {
                if (!empty($requestData['t_product_id_' . $i]) || !empty($requestData['l_product_id_' . $i])) {
                    if (
                        !empty($requestData['t_product_id_' . $i])
                        && !empty($requestData['t_from_line' . $i])
                        && !empty($requestData['t_to_line' . $i])
                        && !empty($requestData['t_base' . $i])
                        && !empty($requestData['t_height' . $i])
                    ) {
                        if ($requestData['t_from_line' . $i] <=  $requestData['t_to_line' . $i]) {
                            $tloopdata = array();
                            for ($it = $requestData['t_from_line' . $i]; $it <= $requestData['t_to_line' . $i]; $it++) {
                                $tmp = array();
                                $tmp['load_m_id'] = $id;
                                $tmp['product_id'] = $requestData['t_product_id_' . $i];
                                $tmp['t_product_m_id'] = $requestData['t_product_id_' . $i];
                                $tmp['main_type'] = 'Normal';
                                $tmp['row'] = $it;
                                $tmp['t_base'] = $requestData['t_base' . $i];
                                $tmp['t_height'] = $requestData['t_height' . $i];
                                $tmp['t_excess'] = $requestData['t_excess' . $i];
                                if($tmp['t_excess'] > 0){
                                    $tmp['t_total'] = ($tmp['t_base'] * $tmp['t_height']) + $tmp['t_excess'];
                                }else{
                                    $tmp['t_total'] = $tmp['t_base'] * $tmp['t_height'];
                                }

                                $chk = LoadD::where('load_m_id', $id)
                                    ->where('row', $tmp['row'])
                                    ->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                    $chk->recalculate($chk->id);
                                } else {
                                    $chk2 = LoadD::create($tmp);
                                    $chk2->recalculate($chk2->id);
                                }
                            }
                        }
                    }
                    if (
                        !empty($requestData['l_product_id_' . $i])
                        && !empty($requestData['l_from_line' . $i])
                        && !empty($requestData['l_to_line' . $i])
                        && !empty($requestData['l_base' . $i])
                        && !empty($requestData['l_height' . $i])
                    ) {
                        if ($requestData['l_from_line' . $i] <=  $requestData['l_to_line' . $i]) {
                            $lloopdata = array();
                            for ($il = $requestData['l_from_line' . $i]; $il <= $requestData['l_to_line' . $i]; $il++) {
                                $tmp = array();
                                $tmp['load_m_id'] = $id;
                                $tmp['product_id'] = $requestData['l_product_id_' . $i];
                                $tmp['l_product_m_id'] = $requestData['l_product_id_' . $i];
                                $tmp['main_type'] = 'Normal';
                                $tmp['row'] = $il;
                                $tmp['l_base'] = $requestData['l_base' . $i];
                                $tmp['l_height'] = $requestData['l_height' . $i];
                                $tmp['l_excess'] = $requestData['l_excess' . $i];
                                if($tmp['l_excess'] > 0){
                                    $tmp['l_total'] = ($tmp['l_base'] * $tmp['l_height']) + $tmp['l_excess'];
                                }else{
                                    $tmp['l_total'] = $tmp['l_base'] * $tmp['l_height'];
                                }

                                $chk = LoadD::where('load_m_id', $id)
                                    ->where('row', $tmp['row'])
                                    ->first();

                                if (!empty($chk)) {
                                    $chk->update($tmp);
                                    $chk->recalculate($chk->id);
                                } else {
                                    $chk2 = LoadD::create($tmp);
                                    $chk2->recalculate($chk2->id);
                                }
                            }
                        }
                    }
                }
            }
            return redirect('loadings/show/' . $id);
        }
    }

    public function createdetail($id)
    {
        $loadm = LoadM::findOrFail($id);
        $productlist = Product::pluck('name', 'id');

        if ($loadm->type_loading == "3Rows") {
            return view('loadings.createdetail3rows', compact('loadm', 'productlist'));
        } elseif ($loadm->type_loading == "Pallet") {
            return view('loadings.createdetailpallet', compact('loadm', 'productlist'));
        } else {
            return view('loadings.createdetail', compact('loadm', 'productlist'));
        }
    }

    public function createdetailAction(Request $request, $id)
    {
        $loadm = LoadM::findOrFail($id);

        $lastrow = 1;
        $lastdrow = $loadm->loadds()->orderBy('row', 'desc')->first();
        if(!empty($lastdrow)){
            $lastrow = $lastdrow->row + 1;
        }


        if ($loadm->type_loading == "3Rows") {
            $requestData = $request->all();
            $requestData['product_id'] = $requestData['t_product_m_id'];
            if (!empty($requestData['l_product_m_id'])) {
                $requestData['product_id'] = $requestData['l_product_m_id'];
            }
            $requestData['row'] = $lastrow;
            LoadD::create($requestData);
        } elseif ($loadm->type_loading == "Pallet") {
            $requestData = $request->all();
            $lastDD = $loadm->loadds()->orderBy('row', 'desc')->first();
            $requestData['product_id'] = $requestData['t_product_m_id'];
            $requestData['all_total'] = $requestData['t_total'];
            $requestData['row'] = 1;
            if (!empty($lastDD)) {
                $requestData['row'] = $lastDD->row + 1;
            }
            LoadD::create($requestData);
        } else {
            $requestData = $request->all();
            $requestData['product_id'] = $requestData['t_product_m_id'];
            if (!empty($requestData['l_product_m_id'])) {
                $requestData['product_id'] = $requestData['l_product_m_id'];
            }
            $requestData['row'] = $lastrow;
            LoadD::create($requestData);
        }


        return redirect('loadings/show/' . $id);
    }

    public function editdetail($load_d_id)
    {
        $loadd = LoadD::findOrFail($load_d_id);
        $productlist = Product::pluck('name', 'id');

        if ($loadd->loadm->type_loading == "3Rows") {
            return view('loadings.editdetail3rows', compact('loadd', 'productlist'));
        } elseif ($loadd->loadm->type_loading == "Pallet") {
            return view('loadings.editdetailpallet', compact('loadd', 'productlist'));
        } else {
            return view('loadings.editdetail', compact('loadd', 'productlist'));
        }
    }

    public function editdetailAction(Request $request, $load_d_id)
    {
        $requestData = $request->all();

        $loadd = LoadD::findOrFail($load_d_id);
        $loadd->update($requestData);

        if ($loadd->loadm->type_loading == "3Rows") {
            $requestData['product_id'] = $requestData['t_product_m_id'];
            if (!empty($requestData['l_product_m_id'])) {
                $requestData['product_id'] = $requestData['l_product_m_id'];
            }

            $loadd->recalculate3rows($load_d_id);
        } elseif ($loadd->loadm->type_loading == "Pallet") {
            $requestData['product_id'] = $requestData['t_product_m_id'];
            $requestData['all_total'] = $requestData['t_total'];
            $loadd->recalculate($load_d_id);
        } else {
            $requestData['product_id'] = $requestData['t_product_m_id'];
            if (!empty($requestData['l_product_m_id'])) {
                $requestData['product_id'] = $requestData['l_product_m_id'];
            }
            $loadd->recalculate($load_d_id);
        }
        return redirect('loadings/show/' . $loadd->load_m_id);
    }

    public function deletedetail($load_d_id)
    {
        $loadmid = LoadD::findOrFail($load_d_id)->load_m_id;

        LoadD::destroy($load_d_id);

        return redirect('loadings/show/' . $loadmid);
    }

    public function planloadview($id)
    {
        $loadm = array();
        $teams = array();
        $pics = array();

        $test = $this->to_show($id);
        // dd($test);
        if (!empty($test[0]))    $loadm = $test[0];
        if (!empty($test[1]))    $teams = $test[1];
        if (!empty($test[2]))    $pics = $test[2];
        if (!empty($test[3]))    $conveyor = $test[3];


        if (isset($loadm) && $loadm->type_loading == "Pallet") {
            return view('loadings.planloadviewpallet', compact('loadm', 'teams', 'pics', 'conveyor'));
        } else {
            return view('loadings.planloadview', compact('loadm', 'teams', 'pics', 'conveyor'));
        }
    }

    public function changestage($id, $stage)
    {
        $loadm = LoadM::findOrFail($id);
        $loadm->status = $stage;
        $loadm->update();
        return redirect('loadings?status=' . $stage);
    }

    public function entersection1($id)
    {
        $loadm = LoadM::findOrFail($id);
        $containertypelist = ContainerType::where('active', 'Active')->pluck('name', 'id');

        $currentDeliveryPlan = CurrentDeliveryPlan::where('order', $loadm->order_no)->where('loading', $loadm->load_date)->first();
        // dd($currentDeliveryPlan);
        return view('loadings.entersection1', compact('loadm', 'containertypelist', 'currentDeliveryPlan'));
    }

    public function entersection1Action(Request $request, $id)
    {
        $requestData = $request->all();
        $loadm = LoadM::findOrFail($id);
        // dd($requestData);
        $loadm->update($requestData);
        return redirect('loadings?status=' . $loadm->status);
    }

    public function entersection2($id)
    {
        $loadm = LoadM::findOrFail($id);

        $teamlist = Team::where('status', 'Active')->pluck('name', 'id');
        $teamlistRw = Team::where('status', 'Active')->get();
        $teamlistType = array();
        foreach ($teamlistRw as $teamlistObj) {
            $teamlistType[$teamlistObj->type][$teamlistObj->id] = $teamlistObj->name;
        }
        // dd($teamlistType);

        $teams = array();
        foreach ($loadm->loadteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }
        // /dd($teamlistType);

        if ($loadm->type_loading == "3Rows") {
            return view('loadings.entersection2', compact('loadm', 'teams', 'teamlist', 'teamlistType'));
        } elseif (
            $loadm->type_loading == "Pallet"
        ) {
            return view('loadings.entersection2pallet', compact('loadm', 'teams', 'teamlist', 'teamlistType'));
        } else {
            return view('loadings.entersection2', compact('loadm', 'teams', 'teamlist', 'teamlistType'));
        }
    }

    public function entersection2Action(Request $request, $id)
    {
        $requestData = $request->all();
        //dd($requestData);
        $teams = array();
        for ($i = 1; $i <= 2; $i++) {
            if (!empty($requestData['teams_front_' . $i])) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['team_type'] = 'Front';
                $tmp['team_id'] = $requestData['teams_front_' . $i];

                $teams[] = $tmp;
            }
            if (!empty($requestData['teams_back_' . $i])) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['team_type'] = 'Back';
                $tmp['team_id'] = $requestData['teams_back_' . $i];

                $teams[] = $tmp;
            }
            if (!empty($requestData['teams_fl_' . $i])) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['team_type'] = 'FL';
                $tmp['team_id'] = $requestData['teams_fl_' . $i];

                $teams[] = $tmp;
            }
            if (!empty($requestData['teams_ctrl_' . $i])) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['team_type'] = 'Ctrl';
                $tmp['team_id'] = $requestData['teams_ctrl_' . $i];

                $teams[] = $tmp;
            }
            if (!empty($requestData['teams_bfload_' . $i])) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['team_type'] = 'Bfload';
                $tmp['team_id'] = $requestData['teams_bfload_' . $i];

                $teams[] = $tmp;
            }
            if (!empty($requestData['teams_bkbox_' . $i])) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['team_type'] = 'Bkbox';
                $tmp['team_id'] = $requestData['teams_bkbox_' . $i];

                $teams[] = $tmp;
            }
        }

        LoadTeam::where('load_m_id', $id)->delete();
        //dd($teams);
        $loadm = LoadM::findOrFail($id);
        LoadTeam::insert($teams);

        return redirect('loadings?status=' . $loadm->status);
    }

    public function entersection3($id)
    {
        $loadm = array();
        $teams = array();
        $pics = array();
        $test = $this->to_show($id);
        // dd($test);
        if (!empty($test[0]))    $loadm = $test[0];
        if (!empty($test[1]))    $teams = $test[1];
        if (!empty($test[2]))    $pics = $test[2];
        if (!empty($test[3]))    $conveyor = $test[3];

        // $loadm = LoadM::findOrFail($id);
        $brokentypelist = BrokenType::orderBy('seq', 'ASC')->pluck('name', 'id');

        $brokens = array();
        foreach ($loadm->loadbrokens as $loadbrokenObj) {
            $brokens[$loadbrokenObj->broken_type_id] = $loadbrokenObj;
        }

        $imageData = [];
        foreach ($loadm->loadingimages as $imageList) {
            $imageData[$imageList->seq][$imageList->typeseq] = $imageList;
        }

        if ($loadm->type_loading == "3Rows") {
            return view('loadings.entersection3', compact('loadm', 'pics', 'brokentypelist', 'brokens', 'conveyor', 'imageData'));
        } elseif (
            $loadm->type_loading == "Pallet"
        ) {
            return view('loadings.entersection3pallet', compact('loadm', 'pics', 'brokentypelist', 'brokens','conveyor', 'imageData'));
        } else {
            return view('loadings.entersection3', compact('loadm', 'pics', 'brokentypelist', 'brokens','conveyor', 'imageData'));
        }
    }

    public function entersection3Action(Request $request, $id)
    {
        $requestData = $request->all();
        // dd($requestData);

        $loadm = array();
        $teams = array();
        $pics = array();
        $test = $this->to_show($id);
        // dd($test);
        if (!empty($test[0]))    $loadm = $test[0];
        if (!empty($test[1]))    $teams = $test[1];
        if (!empty($test[2]))    $pics = $test[2];
        if (!empty($test[3]))    $conveyor = $test[3];
        // $loadm = LoadM::findOrFail($id);

        if (isset($requestData['open_box'])) {
            $loadm->open_box = \Carbon\Carbon::parse($requestData['open_box'])->format('Y-m-d H:i');
        }

        if (isset($requestData['close_box'])) {
            $loadm->close_box = \Carbon\Carbon::parse($requestData['close_box'])->format('Y-m-d H:i');
        }

        $loadm->reason_box = trim($requestData['reason_box']);
        $loadm->conveyor_id = $requestData['conveyor_id'];
        // // dd('don\'t save');

        $brokentypelist = BrokenType::orderBy('seq', 'ASC')->pluck('name', 'id');
        $brokens = array();
        foreach ($brokentypelist as $key => $name) {
            if ($requestData['broken_type' . $key] > 0) {
                $tmp = array();
                $tmp['load_m_id'] = $id;
                $tmp['broken_type_id'] = $key;
                $tmp['box'] = $requestData['broken_type' . $key];

                $brokens[] = $tmp;
            }
        }

        LoadBroken::where('load_m_id', $id)->delete();

        if (!empty($brokens)) {
            LoadBroken::insert($brokens);
        }

        if (isset($requestData['broken_box'])) {
            $loadm->broken_box = $requestData['broken_box'];
        }

        if (isset($requestData['broken_box_case'])) {
            $loadm->broken_box_case = $requestData['broken_box_case'];
        }

        // dd($pics);
        foreach ($pics as $key => $value) {
            $pic_remove = 'remove_path' . $key;
            $pic_load = 'loading_img' . $key;
            $pic_path = 'loading_img_path' . $key;
            if (isset($requestData[$pic_remove]) && !empty($requestData[$pic_remove])) {
                // dd($loadm->$pic_path);
                //ลบรูป
                // echo $pic_remove.'->remove</br>';
                if (file_exists(public_path($loadm->$pic_path))) {
                    unlink($loadm->$pic_path);
                }

                $loadm->$pic_load = null;
                $loadm->$pic_path = null;
            }

            if ($request->hasFile($pic_load)) {
                //ลบรูป
                if (!empty($loadm->$pic_path)) {
                    echo $pic_load . '->update</br>';
                    if (file_exists(public_path($loadm->$pic_path))) {
                        unlink($loadm->$pic_path);
                    }
                }

                $zipfile = $request->file($pic_load);
                // $uploadname = $zipfile->getClientOriginalName();
                $fileName = uniqid() . '.jpg';

                $completedirectory = 'images/loadings/' . $id . '/';
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                }

                $width = 300; //*** Fix Width & Heigh (Autu caculate) ***//
                $size = GetimageSize($zipfile);
                $height = round($width * $size[1] / $size[0]);
                if ($zipfile->getClientOriginalExtension() == 'jpg' || $zipfile->getClientOriginalExtension() == 'jpeg')
                    $images_orig = ImageCreateFromJPEG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'png')
                    $images_orig = ImageCreateFromPNG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'gif')
                    $images_orig = ImageCreateFromGIF($zipfile);

                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
                ImageJPEG($images_fin, $completedirectory . $fileName);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $loadm->$pic_load = $fileName;
                $loadm->$pic_path = 'images/loadings/' . $id  . "/" . $fileName;
            }
        }

        $loadm->update();
        return redirect('loadings?status=' . $loadm->status);
    }

    public function closeload($id)
    {
        $loadm = LoadM::findOrFail($id);
        $loadm->status = 'Loaded';
        $loadm->user_close = Auth::user()->id;
        $loadm->update();
        return redirect('loadings?status=Loaded');
    }

    public function usemasterplan($id)
    {
        $loadm = LoadM::findOrFail($id);
        $masterplanlist = MasterPlanM::pluck('name', 'id');
        return view('loadings.usemasterplan', compact('loadm', 'masterplanlist'));
    }

    public function usemasterplanAction(Request $request, $id)
    {
        $requestData = $request->all();

        $loadm = LoadM::findOrFail($id);
        $masterplanm = MasterPlanM::findOrFail($requestData['master_plan_id']);

        foreach ($masterplanm->masterplands as $masterpland) {
            $tmpd = array();
            $tmpd['load_m_id'] = $id;
            $tmpd['product_id'] = $masterpland->product_id;
            $tmpd['main_type'] = $masterpland->main_type;
            $tmpd['row'] = $masterpland->row;
            $tmpd['t_base'] = $masterpland->t_base;
            $tmpd['t_height'] = $masterpland->t_height;
            $tmpd['t_excess'] = $masterpland->t_excess;
            $tmpd['t_product_id'] = $masterpland->t_product_id;
            $tmpd['t_total'] = $masterpland->t_total;
            $tmpd['l_base'] = $masterpland->l_base;
            $tmpd['l_height'] = $masterpland->l_height;
            $tmpd['l_excess'] = $masterpland->l_excess;
            $tmpd['l_product_id'] = $masterpland->l_product_id;
            $tmpd['l_total'] = $masterpland->l_total;
            $tmpd['note'] = $masterpland->note;
            $tmpd['all_total'] = $masterpland->all_total;
            $tmpd['t_product_m_id'] = $masterpland->t_product_m_id;
            $tmpd['l_product_m_id'] = $masterpland->l_product_m_id;
            $tmpd['t_product1_id'] = $masterpland->t_product1_id;
            $tmpd['t_product2_id'] = $masterpland->t_product2_id;
            $tmpd['l_product1_id'] = $masterpland->l_product1_id;
            $tmpd['l_product2_id'] = $masterpland->l_product2_id;
            $tmpd['t_excess1'] = $masterpland->t_excess1;
            $tmpd['t_excess2'] = $masterpland->t_excess2;
            $tmpd['l_excess1'] = $masterpland->l_excess1;
            $tmpd['l_excess2'] = $masterpland->l_excess2;
            //    / dd($tmpd);
            LoadD::create($tmpd);
        }

        return redirect('loadings/show/' . $id);
    }

    public function realtime($id)
    {
        $loadm = LoadM::findOrFail($id);
        $teams = array();
        foreach ($loadm->loadteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }
        $pics = array(
            1 => "รูปวัดแถวแนวยาว",
            2 => "รูปวัดแถวแนวขวาง",
            3 => "รูปวัดแถวที่ 10",
            4 => "รูปวัดแถวที่ 20",
            5 => "รูปป้ายโหลด",
            6 => "รูปท้ายตู้",
            7 => "รูปสินค้าตัวอย่าง",
            8 => "รูปการใส่ถุงลม",
            9 => "รูปล็อค SEAL",
            10 => "รูปท้ายรถหลังล็อคSeal",
            11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
        );
        return view('loadings.realtime', compact('loadm', 'teams', 'pics'));
    }

    public function realtimeno($id)
    {
        $loadm = LoadM::findOrFail($id);
        $teams = array();
        foreach ($loadm->loadteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }
        $pics = array(
            1 => "รูปวัดแถวแนวยาว",
            2 => "รูปวัดแถวแนวขวาง",
            3 => "รูปวัดแถวที่ 10",
            4 => "รูปวัดแถวที่ 20",
            5 => "รูปป้ายโหลด",
            6 => "รูปท้ายตู้",
            7 => "รูปสินค้าตัวอย่าง",
            8 => "รูปการใส่ถุงลม",
            9 => "รูปล็อค SEAL",
            10 => "รูปท้ายรถหลังล็อคSeal",
            11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
        );
        return view('loadings.realtimeno', compact('loadm', 'teams', 'pics'));
    }

    public function getrealcounter($load_m_id)
    {
        $data = LoadCounting::where('load_m_id', $load_m_id)->first();
        return response()->json($data);
    }

    public function entercountermanual($load_m_id)
    {
        $loadm = LoadM::findOrFail($load_m_id);
        return view('loadings.entercountermanual', compact('loadm'));
    }

    public function entercountermanualAction(Request $request, $load_m_id)
    {

        $requestData = $request->all();

        // /dd( $requestData );

        if (isset($requestData['accept_counting']) && $requestData['accept_counting'] > 0) {
            $tmp = array();
            $tmp['load_m_id'] = $load_m_id;
            $tmp['counter_name'] = 'manual';
            $tmp['type'] = 'accept';
            $tmp['counter'] = $requestData['reject_counting'];

            $id = LoadCountingLog::create($tmp)->id;
        }

        if (isset($requestData['reject_counting']) && $requestData['reject_counting'] > 0) {
            $tmp = array();
            $tmp['load_m_id'] = $load_m_id;
            $tmp['counter_name'] = 'manual';
            $tmp['type'] = 'reject';
            $tmp['counter'] = $requestData['reject_counting'];

            $id = LoadCountingLog::create($tmp)->id;
        }



        $loadCounting = LoadCounting::where('load_m_id', $load_m_id)->first();

        if (!empty($loadCounting)) {

            if (isset($requestData['accept_counting']) && $requestData['accept_counting'] > 0) {
                $loadCounting->accept_counting = $requestData['accept_counting'];
            }
            if (isset($requestData['reject_counting']) && $requestData['reject_counting'] > 0) {
                $loadCounting->reject_counting = $requestData['reject_counting'];
            }
            $loadCounting->all_counting = $loadCounting->accept_counting + $loadCounting->reject_counting;
            $loadCounting->update();
        } else {
            $tmpmain = array();
            $tmpmain['load_m_id'] = $load_m_id;
            $tmpmain['accept_counting'] = 0;
            $tmpmain['reject_counting'] = 0;

            if (isset($requestData['accept_counting']) && $requestData['accept_counting'] > 0) {
                $tmpmain['accept_counting'] = $requestData['accept_counting'];
            }
            if (isset($requestData['reject_counting']) && $requestData['reject_counting'] > 0) {
                $tmpmain['reject_counting'] = $requestData['reject_counting'];
            }
            $tmpmain['all_counting'] = $tmpmain['accept_counting'] + $tmpmain['reject_counting'];
            LoadCounting::create($tmpmain);
        }

        return redirect('loadings/realtime/' . $load_m_id);
    }

    public function to_show($id)
    {
        $loadm = LoadM::findOrFail($id);
        $teams = array();
        foreach ($loadm->loadteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }
        $pics = array(
            1 => "รูปวัดแถวแนวยาว",
            2 => "รูปวัดแถวแนวขวาง",
            3 => "รูปวัดแถวที่ 10",
            4 => "รูปวัดแถวที่ 20",
            5 => "รูปป้ายโหลด",
            6 => "รูปท้ายตู้",
            7 => "รูปสินค้าตัวอย่าง",
            8 => "รูปการใส่ถุงลม",
            9 => "รูปล็อค SEAL",
            10 => "รูปท้ายรถหลังล็อค Seal",
            11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
            12 => "ใบ Lot โหลด",
            13 => "Plan โหลด(สำหรับเก็บ plan ที่มีการเซ็นชื่อ)",
            14 => "แถวสุดท้าย สินค้า Product 1",
            15 => "แถวสุดท้าย สินค้า Product 2",
            16 => "แถวสุดท้าย  สินค้า Product 3",
            17 => "รูปปัญหาที่พบในการโหลด รูปที่ 1",
            18 => "รูปปัญหาที่พบในการโหลด รูปที่ 2  ",
            19 => "รูปปัญหาที่พบในการโหลด รูปที่ 3",
            20 => "รูปการตรวจตู้ 1",
            21 => "รูปการตรวจตู้ 2",
            22 => "รูปการตรวจตู้ 3",
        );

        $conveyor = array(
            1 => "สายพานยืดหด",
            2 => "สายพาน manual",
            3 => "สายพานทั้งคู่",
        );

        return [$loadm, $teams, $pics, $conveyor];
    }

    public function to_show3rows($id)
    {
        $loadm = LoadM::findOrFail($id);
        $teams = array();
        foreach ($loadm->loadteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }
        $pics = array(
            1 => "รูปวัดแถวแนวยาว",
            2 => "รูปวัดแถวแนวขวาง",
            3 => "รูปวัดแถวที่ 10",
            4 => "รูปวัดแถวที่ 20",
            5 => "รูปป้ายโหลด",
            6 => "รูปท้ายตู้",
            7 => "รูปสินค้าตัวอย่าง",
            8 => "รูปการใส่ถุงลม",
            9 => "รูปล็อค SEAL",
            10 => "รูปท้ายรถหลังล็อค Seal",
            11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
            12 => "ใบ Lot โหลด",
            13 => "Plan โหลด(สำหรับเก็บ plan ที่มีการเซ็นชื่อ)",
            14 => "แถวสุดท้าย สินค้า Product 1",
            15 => "แถวสุดท้าย สินค้า Product 2",
            16 => "แถวสุดท้าย  สินค้า Product 3",
            17 => "รูปปัญหาที่พบในการโหลด รูปที่ 1",
            18 => "รูปปัญหาที่พบในการโหลด รูปที่ 2  ",
            19 => "รูปปัญหาที่พบในการโหลด รูปที่ 3",
            20 => "รูปการตรวจตู้ 1",
            21 => "รูปการตรวจตู้ 2",
            22 => "รูปการตรวจตู้ 3",
        );

        $conveyor = array(
            1 => "สายพานยืดหด",
            2 => "สายพาน manual",
            3 => "สายพานทั้งคู่",
        );

        return [$loadm, $teams, $pics, $conveyor];
    }

    public function sent_mail(Request $request)
    {
        $chk = explode(',', $request->load_m_id);

        $result = array_filter($chk);

        $loadm = LoadM::whereIn('id', $result)->orderBy('load_date')->orderBy('open_box')->get();
        $sort_id = array();
        foreach ($loadm as $key) {
            $sort_id[] = $key->id;
        }
        if (count($loadm) > 0) {
            $file_pdf = $this->downloadPDF($sort_id);
            // dd($file_pdf);

            $mail_to = Mail::send(new PlanLoad($loadm, $file_pdf));

            return redirect('loadings')->with('success', ' Send Mail is complete!');
        } else {
            return redirect('loadings')->with('error', ' Send Mail is Error!');
        }
    }

    public function downloadPDF($id)
    {

        $path_pdf = array();
        foreach ($id as $key => $value) {
            $loadm = array();
            $teams = array();
            $pics = array();
            $test = $this->to_show($value);

            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];


            $staff = array();
            foreach ($teams as $kt => $vt) {
                $l_kn = 0;
                foreach ($vt as $kn) {
                    $staff[$kt][$l_kn] = '';
                    $ep = explode(' ', $kn->team->name);
                    foreach ($ep as $kname => $vname) {
                        $staff[$kt][$l_kn] .= $vname . '&nbsp;';
                    }
                    $l_kn++;
                }
            }
            // Customer Type-0, Customer-1, Order No-2, หัวลาก-3,
            // ทะเบียนหน้า-4, ทะเบียนหลัง-5, หมายเลขตู้-6, หมายเลข Seal-7,
            // หมายเหตุ-8, สายพานที่ใช้ในการโหลด-9, ปิดการ Load โดย-10, รายละเอียด-11, ประเภทตู้-12
            $chk_v = array(
                $loadm->customer_type_id, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $loadm->conveyor_id, $loadm->user_close, $loadm->broken_box_case, $loadm->container_type_id
            );

            if (!empty($loadm->conveyor_id)) $conv_name = $conveyor[$loadm->conveyor_id];
            else $conv_name = "";
            if (!empty($loadm->user_close)) $close_name = $loadm->user->name;
            else $close_name = "";
            if (!empty($loadm->customertype)) $customer_type = $loadm->customertype->name;
            else $customer_type = "";
            if (!empty($loadm->containertype)) $container_type = $loadm->containertype->name;
            else $container_type = "";

            $full_v = array(
                $customer_type, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $conv_name, $close_name, $loadm->broken_box_case, $container_type
            );

            $i = 0;
            foreach ($chk_v as $kchk => $vchk) {
                $pdf_val[$i] = '';
                if (!empty($vchk)) {
                    $ep = explode(' ', $full_v[$kchk]);
                    foreach ($ep as $kname => $vname) {
                        $pdf_val[$i] .= $vname . '&nbsp;';
                    }
                } else {
                    $pdf_val[$i] .= "-";
                }
                $i++;
            }

            Pdf::setOption(['dpi' => 150]);
            $pdf = PDF::loadView('loadings.pdf', compact('loadm', 'teams', 'pics', 'conveyor', 'staff', 'pdf_val'));

            $date = date('ymdHis');
            $path_2 = '/storage/PDF/' . date('Y') . "/" . date('m');
            $file_name = '/show_' . $value . '_' . $date . '.pdf';
            $path = public_path() . $path_2;
            // dd($path);
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }
            $location = $path . $file_name;
            $pdf->save($location);

            $path_pdf[] = $file_name;
        }
        return $path_pdf;
    }

    public function load_reason(Request $request)
    {
        // dd($request);
        $set_save['load_m_id'] = $request->loadm_id;
        $set_save['reason'] = $request->reason;
        reason::create($set_save);

        $set_status['status'] = 'Loading';
        $set_status['user_close'] = null;
        $loadm = LoadM::findOrFail($request->loadm_id);
        $loadm->update($set_status);
        return redirect('loadings')->with('success', 'ทำการคืนสถานะแล้ว!');
    }

    public function clone($id)
    {
        // $loadm = LoadM::findOrFail($id);
        // $teams = array();
        // foreach ($loadm->loadteams as $loadteamObj) {
        //     $teams[$loadteamObj->team_type][] = $loadteamObj;
        // }
        // $pics = array(
        //     1 => "รูปวัดแถวแนวยาว",
        //     2 => "รูปวัดแถวแนวขวาง",
        //     3 => "รูปวัดแถวที่ 10",
        //     4 => "รูปวัดแถวที่ 20",
        //     5 => "รูปป้ายโหลด",
        //     6 => "รูปท้ายตู้",
        //     7 => "รูปสินค้าตัวอย่าง",
        //     8 => "รูปการใส่ถุงลม",
        //     9 => "รูปล็อค SEAL",
        //     10 => "รูปท้ายรถหลังล็อคSeal",
        //     11 => "อื่นๆ(กรณีมีปัญหาหรือการติดตั้ง Data Locker)",
        //     12 => "ใบ Lot โหลด",
        //     13 => "Plan โหลด(สำหรับเก็บ plan ที่มีการเซ็นชื่อ)",
        // );

        $loadm = array();
        $teams = array();
        $pics = array();

        $test = $this->to_show($id);

        if (!empty($test[0]))    $loadm = $test[0];
        if (!empty($test[1]))    $teams = $test[1];
        if (!empty($test[2]))    $pics = $test[2];
        if (!empty($test[3]))    $conveyor = $test[3];

        return view('loadings.clone', compact('loadm', 'teams', 'pics', 'conveyor'));
    }

    public function cloneAction(Request $request, $id)
    {
        $requestData = $request->all();

        $data = Http::get('http://10.44.65.224:3001/copytomaster/', $requestData);
        //$posts = json_decode($request->getBody()->getContents());
        //dd($data);
        if ($data->getStatusCode() == 200) {
            return redirect('master-plans')->with('success', 'Clone สำเร็จ!');
        } else {
            return redirect('loadings/clone/' . $id)->with('error', ' Send Mail is Error!');
        }
    }

    public function entersection4($id)
    {
        $loadm = LoadM::findOrFail($id);
        $productidarray = LoadD::where('load_m_id', $id)->pluck('product_id', 'product_id');
        $productlist = Product::whereIn('id', $productidarray)->pluck('name', 'id');
        $loadlotrm = $loadm->loadlots;
        $loadlots = [];
        foreach ($loadlotrm as $loadlot) {
            $loadlots[$loadlot->seq] = $loadlot;
        }

        return view('loadings.entersection4', compact('loadm', 'productlist', 'loadlots'));
    }

    public function entersection4Action(Request $request, $id)
    {
        $requestData = $request->all();
        $loadm = LoadM::findOrFail($id);
        for ($i = 1; $i < 10; $i++) {

            if (!empty($requestData[$i . "_product_id"])) {
                $loadLot = LoadLot::where('load_m_id', $id)->where('seq', $i)->first();

                if (isset($loadLot->id)) {
                    //have update
                    $loadLot->product_id = $requestData[$i . "_product_id"];
                    $loadLot->seq =  $i;
                    $loadLot->start_no = $requestData[$i . "_start_no"];
                    $loadLot->end_no = $requestData[$i . "_end_no"];
                    $loadLot->quantity = $requestData[$i . "_quantity"];
                    $loadLot->exp_date = $requestData[$i . "_exp_date"];
                    $loadLot->best_date = $requestData[$i . "_best_date"];
                    $loadLot->lot = $requestData[$i . "_lot"];
                    $loadLot->remark = $requestData[$i . "_remark"];
                    $loadLot->update();
                } else {
                    //not have
                    $tmpLoadLot = [];
                    $tmpLoadLot['load_m_id'] = $id;
                    $tmpLoadLot['product_id'] = $requestData[$i . "_product_id"];
                    $tmpLoadLot['seq'] =  $i;
                    $tmpLoadLot['start_no'] = $requestData[$i . "_start_no"];
                    $tmpLoadLot['end_no'] = $requestData[$i . "_end_no"];
                    $tmpLoadLot['quantity'] = $requestData[$i . "_quantity"];
                    $tmpLoadLot['exp_date'] = $requestData[$i . "_exp_date"];
                    $tmpLoadLot['best_date'] = $requestData[$i . "_best_date"];
                    $tmpLoadLot['remark'] = $requestData[$i . "_remark"];
                    $tmpLoadLot['lot'] = $requestData[$i . "_lot"];
                    $tmpLoadLot['status'] = 'Active';
                    LoadLot::create($tmpLoadLot);
                }
            } else {
                LoadLot::where('load_m_id', $id)->where('seq', $i)->delete();
            }
        }
        return redirect('loadings?status=' . $loadm->status);
    }

    public function searchLoading(Request $request)
    {
        $keyword = $request->get('search');
        $sdate = $request->get('sdate');
        $edate = $request->get('edate');
        $customer = $request->get('customer');
        $order = $request->get('order');
        $status = 'Loaded';
        $perPage = 10;
        $loadmObj = new LoadM();
        if (!empty($status)) {
            $loadmObj = $loadmObj->where('status', $status);
        }

        if (!empty($sdate) && !empty($edate)) {
            $loadmObj = $loadmObj->where('load_date', '>=', $sdate)->where('load_date', '<=', $edate);
        } else {
            if (!empty($sdate)) {
                $loadmObj = $loadmObj->where('load_date',  $sdate);
            }

            if (!empty($edate)) {
                $loadmObj = $loadmObj->where('load_date', $edate);
            }
        }

        if (!empty($customer)) {

            $loadmObj = $loadmObj->where('customer', 'like', '%' . $customer . '%');
        }
        if (!empty($order)) {

            $loadmObj = $loadmObj->where('order_no', 'like', '%' . $order . '%');
        }


        $loadms = $loadmObj->latest()->paginate($perPage);
        $to_js = $loadmObj->get();
        $to_alert = array();
        foreach ($to_js as $key) {
            $to_alert[$key->id]['load_date'] = $key->load_date;
            $cus = $key->customertype->name ?? '-';
            $to_alert[$key->id]['customer'] = $cus . '/' . $key->customer;
            $to_alert[$key->id]['order_no'] = $key->order_no;
        }

        return view('loadings.search', compact('loadms', 'status', 'to_alert'));
    }

    public function viewLoadingData($id)
    {
        $loadm = array();
        $teams = array();
        $pics = array();

        $loaddata = $this->to_show($id);
        // dd($test);
        if (!empty($loaddata[0]))    $loadm = $loaddata[0];
        if (!empty($loaddata[1]))    $teams = $loaddata[1];
        if (!empty($loaddata[2]))    $pics = $loaddata[2];
        if (!empty($loaddata[3]))    $conveyor = $loaddata[3];
        return view('loadings.searchloadview', compact('loadm', 'teams', 'pics', 'conveyor'));
    }

    public function viewLoadingDataExtImage($id)
    {
        $loadm = array();
        $teams = array();
        $pics = array();

        $loaddata = $this->to_show($id);
        // dd($test);
        if (!empty($loaddata[0]))    $loadm = $loaddata[0];
        if (!empty($loaddata[1]))    $teams = $loaddata[1];
        if (!empty($loaddata[2]))    $pics = $loaddata[2];
        if (!empty($loaddata[3]))    $conveyor = $loaddata[3];


        $imageData = [];
        foreach ($loadm->loadingimages as $imageList) {
            $imageData[$imageList->seq][$imageList->typeseq] = $imageList;
        }

        return view('loadings.searchloadviewextimages', compact('loadm', 'teams', 'pics', 'conveyor', 'imageData'));
    }

    public function exportLoadingData($id)
    {
        $loadm = LoadM::where('id', $id)->orderBy('load_date')->orderBy('open_box')->get();
        $sort_id = array();
        foreach ($loadm as $key) {
            $sort_id[] = $key->id;
        }
        $file_pdf = $this->_generateLoadingData($sort_id);
        $path_2 = 'storage/PDF/' . date('Y') . "/" . date('m');
        $link = url($path_2 . "/" . $file_pdf[0]);
        return response()->download($path_2  . $file_pdf[0]);
    }
    public function _generateLoadingData($id)
    {
        $path_pdf = array();
        foreach ($id as $key => $value) {
            $loadm = array();
            $teams = array();
            $pics = array();
            $test = $this->to_show($value);

            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];


            $staff = array();
            foreach ($teams as $kt => $vt) {
                $l_kn = 0;
                foreach ($vt as $kn) {
                    $staff[$kt][$l_kn] = '';
                    $ep = explode(' ', $kn->team->name);
                    foreach ($ep as $kname => $vname) {
                        $staff[$kt][$l_kn] .= $vname . '&nbsp;';
                    }
                    $l_kn++;
                }
            }
            // Customer Type-0, Customer-1, Order No-2, หัวลาก-3,
            // ทะเบียนหน้า-4, ทะเบียนหลัง-5, หมายเลขตู้-6, หมายเลข Seal-7,
            // หมายเหตุ-8, สายพานที่ใช้ในการโหลด-9, ปิดการ Load โดย-10, รายละเอียด-11, ประเภทตู้-12
            $chk_v = array(
                $loadm->customer_type_id, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $loadm->conveyor_id, $loadm->user_close, $loadm->broken_box_case, $loadm->container_type_id
            );

            if (!empty($loadm->conveyor_id)) $conv_name = $conveyor[$loadm->conveyor_id];
            else $conv_name = "";
            if (!empty($loadm->user_close)) $close_name = $loadm->user->name;
            else $close_name = "";
            if (!empty($loadm->customertype)) $customer_type = $loadm->customertype->name;
            else $customer_type = "";
            if (!empty($loadm->containertype)) $container_type = $loadm->containertype->name;
            else $container_type = "";

            $full_v = array(
                $customer_type, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $conv_name, $close_name, $loadm->broken_box_case, $container_type
            );

            $i = 0;
            foreach ($chk_v as $kchk => $vchk) {
                $pdf_val[$i] = '';
                if (!empty($vchk)) {
                    $ep = explode(' ', $full_v[$kchk]);
                    foreach ($ep as $kname => $vname) {
                        $pdf_val[$i] .= $vname . '&nbsp;';
                    }
                } else {
                    $pdf_val[$i] .= "-";
                }
                $i++;
            }

            Pdf::setOption(['dpi' => 150]);
            $pdf = PDF::loadView('loadings.pdf2', compact('loadm', 'teams', 'pics', 'conveyor', 'staff', 'pdf_val'));

            $date = date('ymdHis');
            $path_2 = '/storage/PDF/' . date('Y') . "/" . date('m');
            $file_name = '/show2_' . $value . '_' . $date . '.pdf';
            $path = public_path() . $path_2;
            // dd($path);
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }
            $location = $path . $file_name;
            $pdf->save($location);

            $path_pdf[] = $file_name;
        }
        return $path_pdf;
    }

    public function exportLoadingExtImageData($id)
    {
        $loadm = LoadM::where('id', $id)->orderBy('load_date')->orderBy('open_box')->get();
        $sort_id = array();
        foreach ($loadm as $key) {
            $sort_id[] = $key->id;
        }
        $file_pdf = $this->_generateLoadingExtImageData($sort_id);
        $path_2 = 'storage/PDF/' . date('Y') . "/" . date('m');
        $link = url($path_2 . "/" . $file_pdf[0]);
        return response()->download($path_2  . $file_pdf[0]);
    }
    public function _generateLoadingExtImageData($id)
    {
        $path_pdf = array();
        foreach ($id as $key => $value) {
            $loadm = array();
            $teams = array();
            $pics = array();
            $test = $this->to_show($value);

            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];


            $staff = array();
            foreach ($teams as $kt => $vt) {
                $l_kn = 0;
                foreach ($vt as $kn) {
                    $staff[$kt][$l_kn] = '';
                    $ep = explode(' ', $kn->team->name);
                    foreach ($ep as $kname => $vname) {
                        $staff[$kt][$l_kn] .= $vname . '&nbsp;';
                    }
                    $l_kn++;
                }
            }
            // Customer Type-0, Customer-1, Order No-2, หัวลาก-3,
            // ทะเบียนหน้า-4, ทะเบียนหลัง-5, หมายเลขตู้-6, หมายเลข Seal-7,
            // หมายเหตุ-8, สายพานที่ใช้ในการโหลด-9, ปิดการ Load โดย-10, รายละเอียด-11, ประเภทตู้-12
            $chk_v = array(
                $loadm->customer_type_id, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $loadm->conveyor_id, $loadm->user_close, $loadm->broken_box_case, $loadm->container_type_id
            );

            if (!empty($loadm->conveyor_id)) $conv_name = $conveyor[$loadm->conveyor_id];
            else $conv_name = "";
            if (!empty($loadm->user_close)) $close_name = $loadm->user->name;
            else $close_name = "";
            if (!empty($loadm->customertype)) $customer_type = $loadm->customertype->name;
            else $customer_type = "";
            if (!empty($loadm->containertype)) $container_type = $loadm->containertype->name;
            else $container_type = "";

            $full_v = array(
                $customer_type, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $conv_name, $close_name, $loadm->broken_box_case, $container_type
            );

            $i = 0;
            foreach ($chk_v as $kchk => $vchk) {
                $pdf_val[$i] = '';
                if (!empty($vchk)) {
                    $ep = explode(' ', $full_v[$kchk]);
                    foreach ($ep as $kname => $vname) {
                        $pdf_val[$i] .= $vname . '&nbsp;';
                    }
                } else {
                    $pdf_val[$i] .= "-";
                }
                $i++;
            }

            $imageData = [];
            foreach ($loadm->loadingimages as $imageList) {
                $imageData[$imageList->seq][$imageList->typeseq] = $imageList;
            }

            Pdf::setOption(['dpi' => 150]);
            $pdf = PDF::loadView('loadings.pdfExtImage', compact('loadm', 'teams', 'pics', 'conveyor', 'staff', 'pdf_val', 'imageData'));

            $date = date('ymdHis');
            $path_2 = '/storage/PDF/' . date('Y') . "/" . date('m');
            $file_name = '/show2_' . $value . '_' . $date . '.pdf';
            $path = public_path() . $path_2;
            // dd($path);
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }
            $location = $path . $file_name;
            $pdf->save($location);

            $path_pdf[] = $file_name;
        }
        return $path_pdf;
    }

    public function uploadExtImage($load_m_id, $seqpic, $seqtype)
    {
        $piclist = $this->PUB_PICS;
        $loadm = LoadM::findOrFail($load_m_id);
        $currentImage = LoadingImage::where('load_m_id', $load_m_id)->where('seq', $seqpic)->where('typeseq', $seqtype)->first();
        return view('loadings.uploadextimg', compact('loadm', 'seqpic', 'seqtype', 'currentImage', 'piclist'));
    }

    public function uploadExtImageAction(Request $request, $load_m_id, $seqpic, $seqtype)
    {
        $piclist = $this->PUB_PICS;
        $loadm = LoadM::findOrFail($load_m_id);
        $currentImage = LoadingImage::where('load_m_id', $load_m_id)->where('seq', $seqpic)->where('typeseq', $seqtype)->first();
        if ($request->hasFile('uploadimage')) {
            //ลบรูป


            $zipfile = $request->file('uploadimage');
            // $uploadname = $zipfile->getClientOriginalName();
            $fileName = uniqid() . '.jpg';

            $completedirectory = 'images/loadings/' . $load_m_id . '/';
            if (!is_dir($completedirectory)) {
                mkdir($completedirectory, 0777, true);
            }

            $width = 300; //*** Fix Width & Heigh (Autu caculate) ***//
            $size = GetimageSize($zipfile);
            $height = round($width * $size[1] / $size[0]);
            if ($zipfile->getClientOriginalExtension() == 'jpg' || $zipfile->getClientOriginalExtension() == 'jpeg')
                $images_orig = ImageCreateFromJPEG($zipfile);
            elseif ($zipfile->getClientOriginalExtension() == 'png')
                $images_orig = ImageCreateFromPNG($zipfile);
            elseif ($zipfile->getClientOriginalExtension() == 'gif')
                $images_orig = ImageCreateFromGIF($zipfile);

            $photoX = ImagesX($images_orig);
            $photoY = ImagesY($images_orig);
            $images_fin = ImageCreateTrueColor($width, $height);
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageJPEG($images_fin, $completedirectory . $fileName);
            ImageDestroy($images_orig);
            ImageDestroy($images_fin);

            if ($currentImage) {
                if (file_exists(public_path($currentImage->loading_img_path))) {
                    unlink($currentImage->loading_img_path);
                }
                $currentImage->loading_img = $fileName;
                $currentImage->loading_img_path = 'images/loadings/' . $load_m_id  . "/" . $fileName;
                $currentImage->update();
            }else{
                $imageTmp = [];
                $imageTmp['load_m_id'] = $load_m_id;
                $imageTmp['loading_img'] = $fileName;
                $imageTmp['loading_img_path'] = 'images/loadings/' . $load_m_id  . "/" . $fileName;
                $imageTmp['seq'] = $seqpic;
                $imageTmp['typename'] = $piclist[$seqpic];
                $imageTmp['typeseq'] = $seqtype;
                LoadingImage::create($imageTmp);
            }
        }

        return redirect(url('/loadings/entersection3/' . $loadm->id));
    }

    public function downloadPDFExtImg($id)
    {

        $path_pdf = array();
        foreach ($id as $key => $value) {
            $loadm = array();
            $teams = array();
            $pics = array();
            $test = $this->to_show($value);

            if (!empty($test[0]))    $loadm = $test[0];
            if (!empty($test[1]))    $teams = $test[1];
            if (!empty($test[2]))    $pics = $test[2];
            if (!empty($test[3]))    $conveyor = $test[3];


            $staff = array();
            foreach ($teams as $kt => $vt) {
                $l_kn = 0;
                foreach ($vt as $kn) {
                    $staff[$kt][$l_kn] = '';
                    $ep = explode(' ', $kn->team->name);
                    foreach ($ep as $kname => $vname) {
                        $staff[$kt][$l_kn] .= $vname . '&nbsp;';
                    }
                    $l_kn++;
                }
            }
            // Customer Type-0, Customer-1, Order No-2, หัวลาก-3,
            // ทะเบียนหน้า-4, ทะเบียนหลัง-5, หมายเลขตู้-6, หมายเลข Seal-7,
            // หมายเหตุ-8, สายพานที่ใช้ในการโหลด-9, ปิดการ Load โดย-10, รายละเอียด-11, ประเภทตู้-12
            $chk_v = array(
                $loadm->customer_type_id, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $loadm->conveyor_id, $loadm->user_close, $loadm->broken_box_case, $loadm->container_type_id
            );

            if (!empty($loadm->conveyor_id)) $conv_name = $conveyor[$loadm->conveyor_id];
            else $conv_name = "";
            if (!empty($loadm->user_close)) $close_name = $loadm->user->name;
            else $close_name = "";
            if (!empty($loadm->customertype)) $customer_type = $loadm->customertype->name;
            else $customer_type = "";
            if (!empty($loadm->containertype)) $container_type = $loadm->containertype->name;
            else $container_type = "";

            $full_v = array(
                $customer_type, $loadm->customer, $loadm->order_no, $loadm->owner_type,
                $loadm->truck_license_plate, $loadm->convoy_license_plate, $loadm->convoy_no, $loadm->seal_no,
                $loadm->reason_box, $conv_name, $close_name, $loadm->broken_box_case, $container_type
            );

            $i = 0;
            foreach ($chk_v as $kchk => $vchk) {
                $pdf_val[$i] = '';
                if (!empty($vchk)) {
                    $ep = explode(' ', $full_v[$kchk]);
                    foreach ($ep as $kname => $vname) {
                        $pdf_val[$i] .= $vname . '&nbsp;';
                    }
                } else {
                    $pdf_val[$i] .= "-";
                }
                $i++;
            }

            $imageData = [];
            foreach ($loadm->loadingimages as $imageList) {
                $imageData[$imageList->seq][$imageList->typeseq] = $imageList;
            }

            Pdf::setOption(['dpi' => 150]);
            $pdf = PDF::loadView('loadings.pdfExtImageEmail', compact('loadm', 'teams', 'pics', 'conveyor', 'staff', 'pdf_val', 'imageData'));

            $date = date('ymdHis');
            $path_2 = '/storage/PDF/' . date('Y') . "/" . date('m');
            $file_name = '/show_' . $value . '_' . $date . '.pdf';
            $path = public_path() . $path_2;
            // dd($path);
            if (!File::exists($path)) {
                File::makeDirectory($path,  0777, true, true);
            }
            $location = $path . $file_name;
            $pdf->save($location);

            $path_pdf[] = $file_name;
        }
        return $path_pdf;
    }

    public function sent_mail_extImage(Request $request)
    {
        $chk = explode(',', $request->load_m_id);

        $result = array_filter($chk);

        $loadm = LoadM::whereIn('id', $result)->orderBy('load_date')->orderBy('open_box')->get();
        $sort_id = array();
        foreach ($loadm as $key) {
            $sort_id[] = $key->id;
        }
        if (count($loadm) > 0) {
            $file_pdf = $this->downloadPDFExtImg($sort_id);
            // dd($file_pdf);

            $mail_to = Mail::send(new PlanLoad($loadm, $file_pdf));

            return redirect('loadings')->with('success', ' Send Mail is complete!');
        } else {
            return redirect('loadings')->with('error', ' Send Mail is Error!');
        }
    }
}
