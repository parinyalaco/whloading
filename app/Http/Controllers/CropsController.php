<?php

namespace App\Http\Controllers;

use App\Models\Crop;
use App\Models\LoadCarryM;
use Illuminate\Http\Request;

class CropsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'crops';
    public $statusList = [
        'Active' => 'Active',
        'Inactive' => 'Inactive'
    ];

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $crops = Crop::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $crops = Crop::latest()->paginate($perPage);
        }

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('crops.index', compact('crops', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $statusList = $this->statusList;
        return view('crops.create', compact('mainnav', 'namenav', 'statusList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:crops',
        ]);

        Crop::create($requestData);

        return redirect('crops')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $crop = Crop::findOrFail($id);
        return view('crops.view', compact('crop', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $crop = Crop::findOrFail($id);

        $statusList = $this->statusList;
        return view('crops.edit', compact('crop', 'mainnav', 'namenav', 'statusList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $crop = Crop::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:crops,name,' . $crop->id,
        ]);


        $crop->update($requestData);

        return redirect('crops')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countcandelete = LoadCarryM::where('product_id', $id)
        ->count();

        if ($countcandelete == 0) {
            Crop::destroy($id);
            return redirect('crops')->with('flash_message', ' deleted!');
        } else {
            return back()->with('error', 'Delete not successfully');
        }
    }
}
