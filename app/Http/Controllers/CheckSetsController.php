<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CheckSet;

class CheckSetsController extends Controller
{
    public $mainnav = 'BaseData';
    public $namenav = 'check_sets';
    public $statusList = [
        'Active' => 'Active',
        'Inactive' => 'Inactive'
    ];


    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $checksets = CheckSet::where('name', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        } else {
            $checksets = CheckSet::latest()->paginate($perPage);
        }

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('admin.checksets.index', compact('checksets', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $statusList = $this->statusList;
        return view('admin.checksets.create', compact('mainnav', 'namenav', 'statusList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:check_sets',
        ]);

        CheckSet::create($requestData);

        return redirect('admin/check_sets')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $checkset = CheckSet::findOrFail($id);
        return view('admin.checksets.view', compact('checkset', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $checkset = CheckSet::findOrFail($id);

        $statusList = $this->statusList;
        return view('admin.checksets.edit', compact('checkset', 'mainnav', 'namenav', 'statusList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $checkset = CheckSet::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:check_sets,name,' . $checkset->id,
        ]);


        $checkset->update($requestData);

        return redirect('admin/check_sets')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countcandelete = CheckSet::findOrFail($id)->checkanswers()->count();

        if ($countcandelete == 0) {
            CheckSet::destroy($id);
            return redirect('admin/check_sets')->with('flash_message', ' deleted!');
        } else {
            return back()->with('error', 'Delete not successfully');
        }
    }
}
