<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\LoadD;
use App\Models\CustomerType;


// use App\Imports\QuizTypeImport;
use Carbon\Carbon;
use DB;
use SimpleXLSX;
// use App\Imports\PeriodImport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $mainnav = 'BaseData';
    public $namenav = 'products';

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if(!empty($keyword)){
            $products = Product::where('name','like','%'. $keyword .'%')
                ->orWhere('desc', 'like', '%' . $keyword . '%')
                ->orWhere('boxcode', 'like', '%' . $keyword . '%')
                ->orWhere('bagcode', 'like', '%' . $keyword . '%')
                ->paginate($perPage);
        }else{
            $products = Product::latest()->paginate($perPage);
        }

        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('products.index', compact('products', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        return view('products.create', compact('mainnav', 'namenav'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validated = $request->validate([
            'name' => 'required|unique:products',
        ]);

        Product::create($requestData);

        return redirect('products')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $product = Product::findOrFail($id);
        return view('products.view', compact('product', 'mainnav', 'namenav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainnav = $this->mainnav;
        $namenav = $this->namenav;
        $product = Product::findOrFail($id);
        return view('products.edit', compact('product', 'mainnav', 'namenav'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();


        $product = Product::findOrFail($id);

        $validated = $request->validate([
            'name' => 'required|unique:products,name,'. $product->id,
        ]);


        $product->update($requestData);

        return redirect('products')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countcandelete = LoadD::where('product_id', $id)
            ->orWhere('t_product_id', $id)
            ->orWhere('l_product_id', $id)
            ->orWhere('t_product_m_id', $id)
            ->orWhere('l_product_m_id', $id)
            ->orWhere('t_product1_id', $id)
            ->orWhere('t_product2_id', $id)
            ->orWhere('l_product1_id', $id)
            ->orWhere('l_product2_id', $id)
            ->count();

        if($countcandelete == 0){
            Product::destroy($id);
            return redirect('products')->with('flash_message', ' deleted!');
        }else{
            return back()->with('error','Delete not successfully');
        }

        // return redirect('products')->with('flash_message', ' deleted!');
    }

    public function importExportView()
    {
       return view('products.import');
    }

    public function import(Request $request)
    {
        // dd($id);
        set_time_limit(0);
        if ($request->hasFile('file_upload')) {
            $tmpfolder = md5(time());
            $zipfile = $request->file('file_upload');
            $uploadname = $zipfile->getClientOriginalName();
            $name = md5($zipfile->getClientOriginalName() . time()) . '.' . $zipfile->getClientOriginalExtension();
            $destinationPath = public_path('storage/upload/' . $tmpfolder);
            $zipfile->move($destinationPath, $name);

            $uploadpath = 'upload/' . $tmpfolder  . "/" . $name;
            $uploadfile = $destinationPath  . "/" . $name;
            // dd($uploadfile);
            $tmpUploadD = array();
            if ($xlsx = SimpleXLSX::parse($uploadfile)) {
                $product_name = array();
                $list_product =array();
                $check_product =array();
                $err_product =array();
                $sort_product =array();
                $unique_product =array();

                $product = Product::get();
                foreach($product as $key){
                    $product_name[] = $key->name;
                }
                // dd($product_name);
                $custom = CustomerType::get();
                foreach($custom as $key){
                    $custom_name[$key->name] = $key->id;
                }
                // dd($custom_name);

                foreach ($xlsx->rows() as $r => $row) {
                    if ($r > 0) {
                        $list_product[] = $row[1];
                        $check_product[$row[1]][] = $r+1;
                    }
                }
                // dd($check_product);
                //เช็คสินค้าซ้ำในไฟล์
                foreach ($check_product as $count => $row) {
                    if(count($check_product[$count]) > 1) {
                        $err_product[$check_product[$count][0]]['row'] = $check_product[$count][0];
                        $err_product[$check_product[$count][0]]['product'] = $count;
                        $err_product[$check_product[$count][0]]['note'] = 'พบสินค้านี้ในบรรทัดที่';
                        foreach ($row as $kid => $vrow) {
                            $err_product[$check_product[$count][0]]['note'] .= ' '.$vrow;
                        }
                    }
                }
                // dd($err_product);
                //เช็คสินค้าซ้ำใน db
                $unique_product = array_unique($list_product);
                foreach ($unique_product as $klist=>$vlist) {
                    $check_array = array_search($vlist, $product_name);
                    if($check_array!=FALSE){
                        // echo $err_product[$check_product[$vlist][0]]['note'].'ค้นพบคำว่า Toshiba</br>';
                        $err_product[$check_product[$vlist][0]]['row'] = $check_product[$vlist][0];
                        $err_product[$check_product[$vlist][0]]['product'] = $vlist;
                        if(!empty($err_product[$check_product[$vlist][0]]['note'])){
                            $err_product[$check_product[$vlist][0]]['note'] .= ', สินค้านี้มีอยู่แล้ว';
                        }else{
                            $err_product[$check_product[$vlist][0]]['note'] = 'สินค้านี้มีอยู่แล้ว';
                        }

                    }
                }
                // dd($err_product);

                //เรียงลำดับ array
                for($i=1;$i<=count($list_product)+1;$i++){
                    if(!empty($err_product[$i])){
                        // print_r($err_product[$i])."</br>";
                        $sort_product[$i]['sort'] = $i;
                        $sort_product[$i]['product'] = $err_product[$i]['product'];
                        $sort_product[$i]['note'] = $err_product[$i]['note'];
                    }
                }
                // dd($sort_product);

                if(!empty($sort_product)){
                    return view('products.err_import',compact('sort_product'));
                    // return redirect('product_err')->with('error',"Can't upload");
                }else{
                    $product_load = array();
                    foreach ($xlsx->rows() as $r => $row) {
                        if ($r > 0) {
                            // $requestData = $request->all();
                            $product_load['name'] = $row[1];
                            $product_load['desc'] = $row[2];
                            $product_load['boxcode'] = $row[3];
                            $product_load['bagcode'] = $row[4];
                            if(is_numeric($row[5])) $product_load['wide'] = $row[5];
                            if(is_numeric($row[6])) $product_load['long'] = $row[6];
                            if(is_numeric($row[7])) $product_load['height'] = $row[7];
                            if(is_numeric($row[8])) $product_load['weight'] = $row[8];
                            $product_load['note'] = $row[9];
                            $product_load['customer'] = $row[10];
                            if(is_numeric($row[11])) $product_load['numperbox'] = $row[11];
                            if(is_numeric($row[12])) $product_load['bagweight'] = $row[12];
                            $product_load['underboxcode'] = $row[13];
                            if(is_numeric($row[5])) $product_load['bundelnumber'] = $row[14];
                            // dd($product_load);
                            $query_product = Product::create($product_load);
                        }
                    }
                    return redirect('products')->with('success','Updated successfully');
                }
            } else {
                echo SimpleXLSX::parseError();
            }
        }
    }

    public function getById($id)
    {
        $product = Product::findOrFail($id);
        return response()
            ->json($product);
    }
}
