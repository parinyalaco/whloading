<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ListLoadM;
use App\Models\ListLoadD;
use App\Models\ListLoadSort;
use App\Models\ImgPd;
use App\Models\ImgContainer;

use Session;
use Storage;

use Auth;
use PDF;
use Illuminate\Support\Facades\File;

class ListLoadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $search_date = $request->get('search_date');
        $perPage = 10;
        $load_list = new ListLoadM;
        if (!empty($search_date)) {
            $load_list = $load_list->where('list_date', $search_date);
        }
        if (!empty($keyword)) {
            $load_list = $load_list->where('shipment_order', 'like', '%' . $keyword . '%')
                ->orWhere('supplier_tran', 'like', '%' . $keyword . '%');
        }
        $load_list = $load_list->latest()->paginate($perPage);

        $pd_data = $this->pd_name(); 

        $img_con = array();
        $img = ImgContainer::orderBy('created_at')->get();
        foreach ($img as $key) {
            $img_con[$key->list_load_m_id]['con_img'][$key->id] = $key->container_img;  
            $img_con[$key->list_load_m_id]['con_img_path'][$key->id] = $key->container_img_path;  
        }            

        $load_listD = array();
        $count_box = 0;
        $ll_d = ListLoadSort::orderBy('list_load_m_id')->orderBy('sort')->get(); 
        foreach ($ll_d as $key) {
            $load_listD[$key->list_load_m_id]['id'][$key->product_id] = $key->id;
            $load_listD[$key->list_load_m_id]['sort'][$key->product_id] = $key->sort;
            $load_listD[$key->list_load_m_id]['pd'][$key->product_id] = $pd_data[$key->product_id]['name'];   
            $arr_data = $this->to_weight($pd_data[$key->product_id]['name']);         
            $load_listD[$key->list_load_m_id]['qty'][$key->product_id] = $arr_data;
            $load_listD[$key->list_load_m_id]['num'][$key->product_id] = $key->quantity;
            if($key->sort==1){
                $load_listD[$key->list_load_m_id]['no'][$key->product_id] = '1-'.$key->quantity;
                $count_box = $key->quantity;
            }else{
                $load_listD[$key->list_load_m_id]['no'][$key->product_id] = ($count_box + 1).'-'.($count_box + $key->quantity);
                $count_box += $key->quantity;
            }
        } 

        $lmg_d = ImgPd::orderBy('created_at')->get();
        foreach ($lmg_d as $key) {
            $load_listD[$key->list_load_m_id]['img'][$key->product_id][$key->id] = $key->pd_img;  
            $load_listD[$key->list_load_m_id]['img_path'][$key->product_id][$key->id] = $key->pd_img_path;  
        } 

        return view('list_load.index', compact('load_list', 'img_con', 'load_listD'));
    }

    public function to_weight($pd)
    {
        $arr_int = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
        $word = str_replace($arr_int, "*", $pd);
        $a = str_split( $word);
        for($i = count($a); $i>0; $i--){
            if($a[$i-1] != '*'){
               $b = $i; 
               break;
            } 
        }
        $c = intval(substr($pd,$b,(count($a)-$b)))/1000;
        return $c;
    }

    public function pd_name(){
        $pd = array();
        $pd_data = array();
        $pd = ListLoadD::join('products', 'list_load_d_s.product_id', '=', 'products.id')
            ->select('list_load_d_s.product_id', 'products.name', 'products.desc_list', 'products.lot_load_no', 'products.lot_load_name')->orderBy('products.name')->get();
        foreach ($pd as $key) {
            $pd_data[$key->product_id]['name'] = $key->name;
            $pd_data[$key->product_id]['desc_list'] = $key->desc_list;
            $pd_data[$key->product_id]['lot_load_no'] = $key->lot_load_no;
            $pd_data[$key->product_id]['lot_load_name'] = $key->lot_load_name;
        }
        return $pd_data;
    }
    
    public function create()
    {        
        return view('list_load.create');
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        //check ค่าซ้ำจากวันที่+ตู้+ซีล
        $chk = ListLoadM::where('list_date', $requestData['list_date'])
            ->where('customer_po', $requestData['customer_po'])->count();
        if($chk == 0){   
            $requestData['user_id'] = Auth::user()->id;      
            $get_id = ListLoadM::create($requestData)->id;
            // $get_id = 1;
            
            if ($request->hasFile('list_img')) {
                $list_load = ListLoadM::findOrFail($get_id); 

                $image = $request->file('list_img');
                $fileName = uniqid() . '.jpg';
                
                // $completedirectory = 'images/list_load/'.$get_id.'/';                 
                $completedirectory = config('my.directory.lot_load.main').$get_id.'/'; 
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                } 

                $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
                $size=GetimageSize($image);
                $height=round($width*$size[1]/$size[0]);
                $images_orig = ImageCreateFromJPEG($image);
                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                ImageJPEG($images_fin,$completedirectory.$fileName);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $save_img = array();
                $save_img['list_img'] = $fileName;
                $save_img['list_img_path'] = $completedirectory . $fileName;

                $list_load->update($save_img);
            }
            
            return redirect('list_load')->with('success','Create successfully');
        }else{
            return back()->with('error','ข้อมูลซ้ำกับที่มีอยู่');
        }
    }

    public function show($id)
    {
        Session::put('sess_lld', $id);
        return redirect('list_load_det');
    }

    public function edit($id)
    {
        $list_load = ListLoadM::findOrFail($id);
        return view('list_load.edit', compact('list_load'));
    }

    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        //check ค่าซ้ำจากวันที่+ตู้+ซีล
        $chk = ListLoadM::where('list_date', $requestData['list_date'])
            ->where('customer_po', $requestData['customer_po'])
            ->whereNotIn('id', [$id])->count();
        
        if($chk == 0){  
            $list_load = ListLoadM::findOrFail($id);             
            $requestData['user_id'] = Auth::user()->id;   

            if ($requestData['chk_img'] != $list_load->list_img) {
                if(!empty($list_load->list_img)){
                    if (file_exists($list_load->list_img_path)) {
                        unlink($list_load->list_img_path);
                    }
                }
                if(!empty($requestData['chk_img'])){
                    $image = $request->file('list_img');
                    $fileName = uniqid() . '.jpg';
                    
                    // $completedirectory = 'images/list_load/'.$id.'/';                       
                    $completedirectory = config('my.directory.lot_load.main').$id.'/'; 
                    if (!is_dir($completedirectory)) {
                        mkdir($completedirectory, 0777, true);
                    } 

                    $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
                    $size=GetimageSize($image);
                    $height=round($width*$size[1]/$size[0]);
                    $images_orig = ImageCreateFromJPEG($image);
                    $photoX = ImagesX($images_orig);
                    $photoY = ImagesY($images_orig);
                    $images_fin = ImageCreateTrueColor($width, $height);
                    ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
                    ImageJPEG($images_fin,$completedirectory.$fileName);
                    ImageDestroy($images_orig);
                    ImageDestroy($images_fin);

                    $requestData['list_img'] = $fileName;
                    $requestData['list_img_path'] = $completedirectory . $fileName;
                }else{
                    $requestData['list_img'] = '';
                    $requestData['list_img_path'] = '';
                }                
            }
            $list_load->update($requestData);  
            return redirect('list_load')->with('success','Update successfully');
        }else{
            return back()->with('error','ข้อมูลซ้ำกับที่มีอยู่');
        }
    }

    public function destroy($id)
    {
        $ll_det = ListLoadD::where('list_load_m_id', $id)->count();
        if($ll_det>0){
            // $dir = array('list_load', 'webcam', 'webcam_pd', 'webcam_det');
            $dir = array('main', 'close_container', 'pdf', 'detail');
            foreach($dir as $key=>$val){
                $dirname = 'c:/project/whloading/data/'.$val.'/'.$id;
                $this->deleteDirectory($dirname);
            }
            ImgPd::where('list_load_m_id', $id)->delete();

            ListLoadD::where('list_load_m_id', $id)->delete();
            ListLoadSort::where('list_load_m_id', $id)->delete();
        }

        ImgContainer::where('list_load_m_id', $id)->delete();
        ListLoadM::destroy($id);

        return redirect('list_load')->with('success','Delete successfully');
    }

    function deleteDirectory($path){
        if(is_dir($path)){
            $files = array_diff(scandir($path), array('.','..')); 
            foreach($files as $file){
                if(is_dir($path.'/'.$file)){
                    $this->deleteDirectory("$path/$file");
                }
                if (file_exists("$path/$file")) {unlink("$path/$file");}
            }
            return rmdir($path);
        }
    } 

    public function capture_pd(Request $request, $id, $pd)
    {
        if ($request->hasFile('file_webcam_pd_'.$id.'_'.$pd)) {
            $zipfile = $request->file('file_webcam_pd_'.$id.'_'.$pd);
            $fileName = uniqid() . '.jpg';

            // $completedirectory = 'images/webcam_pd/'.$id.'/'.$pd.'/';
            $completedirectory = config('my.directory.lot_load.pdf').$id.'/'.$pd.'/';  
            if (!is_dir($completedirectory)) {
                mkdir($completedirectory, 0777, true);
            } 

            $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
            $size=GetimageSize($zipfile);
            $height=round($width*$size[1]/$size[0]);
            $images_orig = ImageCreateFromJPEG($zipfile);
            $photoX = ImagesX($images_orig);
            $photoY = ImagesY($images_orig);
            $images_fin = ImageCreateTrueColor($width, $height);
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
            ImageJPEG($images_fin,$completedirectory.$fileName);
            ImageDestroy($images_orig);
            ImageDestroy($images_fin);

            $requestData['list_load_m_id'] = $id;
            $requestData['product_id'] = $pd;
            $requestData['pd_img'] = $fileName;
            $requestData['pd_img_path'] = $completedirectory.$fileName;
            ImgPd::create($requestData);
            return back()->with('success','บันทึกรูปภาพแล้ว');
        }
    }

    public function del_capture_pd(Request $request)
    {
        $del_img = ImgPd::findOrFail($request->id);
        if (file_exists($del_img->pd_img_path)) {
            unlink($del_img->pd_img_path);
            $del_img->delete();
            return 'ลบรูปแล้ว';
        }else{
            $del_img->delete();
            return 'ไม่พบรูปภาพ';

        }
    }

    public function capture(Request $request, $id)
    {
        $requestData = $request->all();
        if ($request->hasFile('file_webcam'.$id)) {

            $zipfile = $request->file('file_webcam'.$id);
            $fileName = uniqid() . '.jpg';

            // $completedirectory = 'images/webcam/'.$id.'/';             
            $completedirectory = config('my.directory.lot_load.close_container').$id.'/'; 
            if (!is_dir($completedirectory)) {
                mkdir($completedirectory, 0777, true);
            }

            $width=500; //*** Fix Width & Heigh (Autu caculate) ***//
            $size=GetimageSize($zipfile);
            $height=round($width*$size[1]/$size[0]);
            $images_orig = ImageCreateFromJPEG($zipfile);
            $photoX = ImagesX($images_orig);
            $photoY = ImagesY($images_orig);
            $images_fin = ImageCreateTrueColor($width, $height);
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
            ImageJPEG($images_fin,$completedirectory.$fileName);
            ImageDestroy($images_orig);
            ImageDestroy($images_fin); 

            $requestData['list_load_m_id'] = $id;
            $requestData['container_img'] = $fileName;
            $requestData['container_img_path'] = $completedirectory.$fileName;
            ImgContainer::create($requestData);
            return back()->with('success','บันทึกรูปภาพแล้ว');
        }

    }

    public function del_capture(Request $request)
    {
        $del_img = ImgContainer::findOrFail($request->id);
        if (file_exists($del_img->container_img_path)) {
            unlink($del_img->container_img_path);
            $del_img->delete();
            return 'ลบรูปภาพแล้ว';
        }else{
            $del_img->delete();
            return 'ไม่พบรูปภาพ';
        }
    }

    public function to_sort(Request $request){
        $requestData = $request->all();
        $data = ListLoadSort::findOrFail($requestData['id']);
        if($requestData['type']=='down'){
            $to_save['sort'] = $requestData['sort']+1;            
            $to_save_type['sort'] = $to_save['sort']-1;            
        }else{
            $to_save['sort'] = $requestData['sort']-1;
            $to_save_type['sort'] = $to_save['sort']+1;
        }
        ListLoadSort::where('list_load_m_id',$data->list_load_m_id)->where('sort',$to_save['sort'])->update($to_save_type);
        ListLoadSort::where('id',$requestData['id'])->update($to_save);
        return redirect()->back();
    }

    public function to_pdf($id){
        $pd_data = $this->pd_name(); 

        $load_list = ListLoadM::findOrFail($id);
        $exp = explode('#',$load_list->shipment_order);   
        $load_list['name'] = $exp[0];          
        $load_list['cus_no'] = $load_list->customer_po; 
        
        $ll_d = ListLoadSort::where('list_load_m_id',$id)->orderBy('sort')->get();
        foreach ($ll_d as $key) {
            $load_listD['sort'][$key->product_id] = $key->sort;
            if(!empty($pd_data[$key->product_id]['lot_load_no']))  
                $load_listD['lot_load_no'][$key->product_id] = $pd_data[$key->product_id]['lot_load_no'];
            else
                $load_listD['lot_load_no'][$key->product_id] = $pd_data[$key->product_id]['name'];
            if(!empty($pd_data[$key->product_id]['lot_load_name']))
                $load_listD['lot_load_name'][$key->product_id] = $pd_data[$key->product_id]['lot_load_name'];
            else
                $load_listD['lot_load_name'][$key->product_id] = $pd_data[$key->product_id]['desc_list'];

            $load_listD['pd_code'][$key->product_id] = $pd_data[$key->product_id]['name']; 
            $load_listD['pd_desc'][$key->product_id] = $pd_data[$key->product_id]['desc_list'];   
                  
            $arr_data = $this->to_weight($pd_data[$key->product_id]['name']);         
            $load_listD['qty'][$key->product_id] = $arr_data*$key->quantity;
            $load_listD['num'][$key->product_id] = $key->quantity;
            if($key->sort==1){
                $load_listD['no'][$key->product_id] = '1-'.$key->quantity;
                $count_box = $key->quantity;
            }else{
                $load_listD['no'][$key->product_id] = ($count_box + 1).'-'.($count_box + $key->quantity);
                $count_box += $key->quantity;
            }
        }   

        $lmg_d = ImgPd::where('list_load_m_id',$id)->get();
        foreach ($lmg_d as $key) {
            $load_listD[$key->product_id]['img'][$key->id] = $key->pd_img;  
            $load_listD[$key->product_id]['img_path'][$key->id] = $key->pd_img_path;  
        } 

        // $path_pdf = '';

        // foreach ($load_listD['pd_code'] as $kpd => $vpd) {           
        //     $arr = array('pd_code', 'pd_desc', 'lot_load_no', 'lot_load_name');    
        //     foreach ($arr as $karr => $varr) {
        //         $pdf_val = '';
        //         if(!empty($load_listD[$varr][$kpd])){
        //             $ep = explode(' ',$load_listD[$varr][$kpd]);
        //             foreach($ep as $kname => $vname){
        //                 $pdf_val .= $vname.'&nbsp;';
        //             }
        //         }else{
        //             $pdf_val .= "-";
        //         }
        //         $load_listD[$varr][$kpd] = $pdf_val;
        //     }
        // }
        // dd($load_listD);
        Pdf::setOption(['dpi' => 150]);
        $pdf = PDF::loadView('list_load.pdf', compact('load_list', 'load_listD'));

        $date = date('ymdHis');
        $path_2 = '/storage/PDF/'.date('Y'). "/" . date('m');
        $file_name = '/LotLoad_'.$id.'_'.$date.'.pdf';
        $path = public_path() . $path_2 ;
        if (!File::exists($path)) {
            File::makeDirectory($path,  0777, true, true);
        }
        $location = $path . $file_name;
        $pdf->save($location);
        return response()->download($location);
    }
}
