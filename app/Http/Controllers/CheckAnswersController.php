<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CheckAnswer;
use App\Models\CheckAnsM;
use App\Models\CheckPoint;
use App\Models\LoadM;
use App\Models\CheckSet;
use App\Models\UploadDeliveryPlanD;

class CheckAnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($loading_id, Request $request)
    {
        $loadm = LoadM::findOrFail($loading_id);

        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $checkanswers = CheckAnsM::where('name', 'like', '%' . $keyword . '%')
                ->where('load_m_id', $loading_id)
                ->latest()
                ->paginate($perPage);
        } else {
            $checkanswers = CheckAnsM::where('load_m_id', $loading_id)
                ->latest()
            ->paginate($perPage);
        }

        return view('checkanswers.index', compact('checkanswers', 'loadm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $loading_id, Request $request)
    {
        $location = $request->get('location');
        $loadm = LoadM::findOrFail($loading_id);

        $uploaddeliveryplan = UploadDeliveryPlanD::where('order', $loadm->order_no)->first();

        $checkset = CheckSet::where('status','Active')->first();
        return view('checkanswers.create', compact('loadm', 'location', 'checkset', 'uploaddeliveryplan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($loading_id, Request $request)
    {
        $requestData = $request->all();
    //   /  dd($requestData);
        $alltmp = [];
        $checkset = CheckSet::where('status', 'Active')->first();

        $masterAnswer = [];
        $masterAnswer['load_m_id'] = $loading_id;
        $masterAnswer['check_point_id'] = $requestData['location'];
        $masterAnswer['status'] = 'Active';

        $ansData = CheckAnsM::create($masterAnswer);

        foreach($checkset->checkquestions()->orderBy('seq', 'ASC')->get() as $item) {
            if(isset($requestData['question-'. $item->id])){
                $tmpanswer = [];
                $tmpanswer['check_ans_m_id'] = $ansData->id;
                $tmpanswer['load_m_id'] = $loading_id;
                $tmpanswer['check_point_id'] = $requestData['location'];
                $tmpanswer['check_questions_id'] = $item->id;
                $tmpanswer['seq'] = $item->seq;
                $tmpanswer['type_question'] = $item->type_question;
                $tmpanswer['ans1_column'] = $item->ans1_column;
                $tmpanswer['ans2_column'] = $item->ans2_column;
                $tmpanswer['ans3_column'] = $item->ans3_column;
                $tmpanswer['status'] = 'Active';

                if($item->type_question == 'IMAGE'){


                    $file = $request->file('question-' . $item->id);

                    // dd($file);
                    $destinationPath = 'uploads/' . date('Ymdhis');
                    $file->move($destinationPath, $file->getClientOriginalName());

                    $tmpanswer[$item->ans1_column] = $file->getClientOriginalName();
                    $tmpanswer[$item->ans2_column] = $destinationPath . '/' . $file->getClientOriginalName();
                    $tmpanswer[$item->ans3_column] = $destinationPath . '/resize-' . $file->getClientOriginalName();

                    $namefile = 'resize-' . $file->getClientOriginalName();

                    $this->_resizeTextAdd($tmpanswer[$item->ans2_column], $namefile, $destinationPath . '/', 1000, date('Y-m-d H:i:s'));

                }else{
                    if ($item->type_question == 'TEXT') {
                        $tmpanswer[$item->ans1_column] = $requestData['question-' . $item->id];
                    } else {
                        $tmpanswer[$item->ans1_column] = $requestData['question-' . $item->id];
                    }
                }
                CheckAnswer::create($tmpanswer);
            }

        }
        return redirect('/chkpoint/'. $loading_id.'/answers/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($loading_id, $id)
    {
        $loadm = LoadM::findOrFail($loading_id);
        $checkansm = CheckAnsM::findOrFail($id);
        return view('checkanswers.view', compact('checkansm', 'loadm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($loading_id,$id)
    {
        $loadm = LoadM::findOrFail($loading_id);
        $checkanswer = CheckAnswer::findOrFail($id);
        return view('checkanswers.edit', compact('checkanswer', 'loadm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($loading_id, Request $request, $id)
    {
        $requestData = $request->all();

        $checkanswer = CheckAnswer::findOrFail($id);
        $column1 = $checkanswer->ans1_column;
        if ($checkanswer->type_question == 'IMAGE') {
            $file = $request->file('question-' . $checkanswer->id);

            // dd($file);
            $destinationPath = 'uploads/' . date('Ymdhis');
            $file->move($destinationPath, $file->getClientOriginalName());


            $column2 = $checkanswer->ans2_column;
            $column3 = $checkanswer->ans3_column;

            $checkanswer->$column1 = $file->getClientOriginalName();
            $checkanswer->$column2 = $destinationPath . '/' . $file->getClientOriginalName();
            $checkanswer->$column3 = $destinationPath . '/resize-' . $file->getClientOriginalName();

            $namefile = 'resize-' . $file->getClientOriginalName();

            $this->_resizeTextAdd($checkanswer->$column2, $namefile, $destinationPath . '/', 1000, date('Y-m-d H:i:s'));
        }else{
            if ($checkanswer->type_question == 'TEXT') {
                $checkanswer->$column1 = $requestData['question-' . $checkanswer->id];
            } else {
                $checkanswer->$column1 = $requestData['question-' . $checkanswer->id];
            }
        }

        $checkanswer->update();

        return redirect('/chkpoint/' . $loading_id . '/answers/'. $checkanswer->check_ans_m_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($loading_id,$id)
    {
        $maindata = CheckAnsM::findOrFail($id);
        $maindata->checkanswers()->delete();
        $maindata->delete();
        return redirect('/chkpoint/' . $loading_id . '/answers/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function scan($loading_id)
    {
        $loadm = LoadM::findOrFail($loading_id);
        return view('checkanswers.scan', compact('loadm'));
    }

    private function _resizeTextAdd($src, $name, $desc, $newWidth, $txt)
    {

        $img = imagecreatefromjpeg($src);

        $info = getimagesize($src);
        $mime = $info['mime'];

        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw new Exception('Unknown image type.');
        }

        $img = $image_create_func($src);
        list($width, $height) = getimagesize($src);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        $tmp = imagerotate($tmp, 270, 0);

        $dir = dirname(realpath(__FILE__));
        $sep = DIRECTORY_SEPARATOR;
        $fontFile = config('myconfig.font_link');
        // echo $fontFile;
        //$fontFile = realpath("arial.ttf"); //replace with your font
        $fontSize = 24;
        $fontColor = imagecolorallocate($tmp, 255, 255, 255);
        $black = imagecolorallocate($tmp, 0, 0, 0);
        $white = imagecolorallocate($tmp, 255, 255, 255);
        $angle = 0;

        $iWidth = imagesx($tmp);
        $iHeight = imagesy($tmp);

        $tSize = imagettfbbox($fontSize, $angle, $fontFile, $txt);
        $tWidth = max([$tSize[2], $tSize[4]]) - min([$tSize[0], $tSize[6]]);
        $tHeight = max([$tSize[5], $tSize[7]]) - min([$tSize[1], $tSize[3]]);

        // text is placed in center you can change it by changing $centerX, $centerY values
        $centerX = CEIL(($iWidth - $tWidth)) - 50;
        $centerX = $centerX < 0 ? 0 : $centerX;
        $centerY = CEIL(($iHeight - $tHeight)) - 50;
        $centerY = $centerY < 0 ? 0 : $centerY;

        imagettftext($tmp, $fontSize, $angle, $centerX, $centerY, $white, $fontFile, $txt);
        $image_save_func($tmp, "$desc$name");

        imagedestroy($img);
        imagedestroy($tmp);

        if (file_exists($src)) {
            unlink($src);
        }
    }

    public function createAll($loading_id, Request $request)
    {
        $checkpoints = CheckPoint::all();
        $location = $request->get('location');
        $loadm = LoadM::findOrFail($loading_id);

        $uploaddeliveryplan = UploadDeliveryPlanD::where('order', $loadm->order_no)->first();

        $checkset = CheckSet::where('status', 'Active')->first();
        return view('checkanswers.createall', compact('loadm', 'location', 'checkset', 'uploaddeliveryplan', 'checkpoints'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAll($loading_id, Request $request)
    {
        $requestData = $request->all();
        //   /  dd($requestData);
        $alltmp = [];
        $checkset = CheckSet::where('status', 'Active')->first();

        $checkpoints = CheckPoint::all();
        foreach ($checkpoints as $checkpoint) {
            $masterAnswer = [];
            $masterAnswer['load_m_id'] = $loading_id;
            $masterAnswer['check_point_id'] = $requestData['location-'. $checkpoint->id];
            $masterAnswer['status'] = 'Active';

            $ansData = CheckAnsM::create($masterAnswer);

            foreach ($checkset->checkquestions()->orderBy('seq', 'ASC')->get() as $item) {
                if (isset($requestData['question-' . $checkpoint->id.'-'. $item->id])) {
                    $tmpanswer = [];
                    $tmpanswer['check_ans_m_id'] = $ansData->id;
                    $tmpanswer['load_m_id'] = $loading_id;
                    $tmpanswer['check_point_id'] = $requestData['location-' . $checkpoint->id];
                    $tmpanswer['check_questions_id'] = $item->id;
                    $tmpanswer['seq'] = $item->seq;
                    $tmpanswer['type_question'] = $item->type_question;
                    $tmpanswer['ans1_column'] = $item->ans1_column;
                    $tmpanswer['ans2_column'] = $item->ans2_column;
                    $tmpanswer['ans3_column'] = $item->ans3_column;
                    $tmpanswer['status'] = 'Active';

                    if ($item->type_question == 'IMAGE') {


                        $file = $request->file('question-' . $checkpoint->id . '-' . $item->id);

                        // dd($file);
                        $destinationPath = 'uploads/' . date('Ymdhis');
                        $file->move($destinationPath, $file->getClientOriginalName());

                        $tmpanswer[$item->ans1_column] = $file->getClientOriginalName();
                        $tmpanswer[$item->ans2_column] = $destinationPath . '/' . $file->getClientOriginalName();
                        $tmpanswer[$item->ans3_column] = $destinationPath . '/resize-' . $file->getClientOriginalName();

                        $namefile = 'resize-' . $file->getClientOriginalName();

                        $this->_resizeTextAdd($tmpanswer[$item->ans2_column], $namefile, $destinationPath . '/', 1000, date('Y-m-d H:i:s'));
                    } else {
                        if ($item->type_question == 'TEXT') {
                            $tmpanswer[$item->ans1_column] = $requestData['question-' . $checkpoint->id . '-' . $item->id];
                        } else {
                            $tmpanswer[$item->ans1_column] = $requestData['question-' . $checkpoint->id . '-' . $item->id];
                        }
                    }
                    CheckAnswer::create($tmpanswer);
                }
            }
        }

        return redirect('/chkpoint/' . $loading_id . '/answers/');
    }
}
