<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoadM;

class DriversController extends Controller
{
    public function index(Request $request){
        $status = 'Loaded';
        $keyword = $request->get('search');
        $perPage = 10;
        $loadmObj = new LoadM();
        if (!empty($status)) {
            $loadmObj = $loadmObj->where('status', $status);
        }
        if (!empty($keyword)) {

            $loadmObj = $loadmObj->where('order_no', 'like', '%' . $keyword . '%')
                ->orWhere('customer', 'like', '%' . $keyword . '%');
        }
        $loadms = $loadmObj->latest()->paginate($perPage);

        return view('drivers.index', compact('loadms', 'status'));
    }
}
