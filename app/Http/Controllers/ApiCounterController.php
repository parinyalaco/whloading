<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LoadM;
use App\Models\LoadCounting;
use App\Models\LoadCountingLog;
use Illuminate\Support\Facades\DB;

class ApiCounterController extends Controller
{
    public function getlist(){

    //    / $datalist = LoadM::where('status','Loading')->orderBy('load_date','ASC')->pluck('id',DB::Raw("(order_no + '-' + customer) as newname"));
        $datamains = LoadM::where('status', 'Loading')->orderBy('load_date', 'DESC')->get();

        $datalist = array();
        foreach($datamains as $datamain){
            $datalist[$datamain->id]['main'] = $datamain;
            $datalist[$datamain->id]['counting'] = $datamain->loadcounting;
            $datalist[$datamain->id]['all_counting'] = $datamain->countingbox();
            
        }

        return response()->json($datalist);
    }

    public function getcurrentcounter($load_id)
    {

        $datalist = LoadCounting::where('load_m_id', $load_id)->first();

        if(empty($datalist)){
            $tmpmain = array();
            $tmpmain['load_m_id'] = $load_id;
            $tmpmain['all_counting'] = 0;
            $tmpmain['reject_counting'] = 0;
            $tmpmain['accept_counting'] = 0;
            
            LoadCounting::create($tmpmain);

            $datalist = LoadCounting::where('load_m_id', $load_id)->first();
        }

        $data['counter'] = $datalist;
        $data['product'] = $datalist->loadm->productdata();
        $data['all'] = $datalist-> loadm->countingbox();

        

        return response()->json($data);
    }

    public function postcounter(Request $request, $load_id)
    {
        $countername = $request->get('counter_name');
        $typename = $request->get('type');
        $counter = $request->get('counter');

        $tmp = array();
        $tmp['load_m_id'] = $load_id;
        $tmp['counter_name'] = $countername;
        $tmp['type'] = $typename;
        $tmp['counter'] = $counter;

        $id = LoadCountingLog::create($tmp)->id;

        $loadCounting = LoadCounting::where('load_m_id',$load_id)->first();

        if(!empty($loadCounting)){
            
            if($typename == 'accept'){
                $loadCounting->accept_counting = $counter;
            }else{
                $loadCounting->reject_counting = $counter;
            }
            $loadCounting->all_counting = $loadCounting->accept_counting + $loadCounting->reject_counting;
            $loadCounting->update();
        }else{
            $tmpmain = array();
            $tmpmain['load_m_id'] = $load_id;
            $tmpmain['all_counting'] = $counter;
            if ($typename == 'accept') {
                $tmpmain['reject_counting'] = 0;
                $tmpmain['accept_counting'] = $counter;
            }else{
                $tmpmain['reject_counting'] = $counter;
                $tmpmain['accept_counting'] = 0;
            }
            LoadCounting::create($tmpmain);
        }

        return response()->json($id);
    }
}
