<?php

namespace App\Http\Controllers;

use App\Models\Crop;
use App\Models\Product;
use App\Models\ExtWh;
use App\Models\LoadCarryM;
use App\Models\Team;
use App\Models\LoadCarryTeam;

use Auth;
use Illuminate\Http\Request;

class LoadingCarriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $trackTypelist = [
        "CONVOY" => "รถหัวลาก",
        "TRUCK10" => "รถสิบล้อ",
        "TRUCK12" => "รถสิบสองล้อ",
    ];

    public $usagelist = [
        "CONVOY" => "รถหัวลาก",
        "TRUCK10" => "รถสิบล้อ",
        "TRUCK12" => "รถสิบสองล้อ",
    ];

    private $picset1 = [
        1 => "รูปอุณหภูมิเปิดตู้",
        2 => "รูปอุณหภูมิปล่อยตู้",
        3 => "รูปอุณหภูมิปิดตู้ เสียบปลั๊ก",
        4 => "รูปอุณหภูมิตู้ขณะปล่อยตู้",
        5 => "รูปอุณหภูมิสินค้า พาเลทแรก",
        6 => "รูปอุณหภูมิสินค้า พาเลทกลางตู้",
        7 => "รูปอุณหภูมิสินค้า พาเลทสุดท้าย",
    ];

    private $picset2 = [
        8 => "รูปหน้ารถ(สภาพภายนอก)",
        9 => "รูปทะเบียนหาง(สภาพภายนอก)",
        10 => "รูปด้านหน้าตู้(สภาพภายนอก)",
        11 => "รูปด้านซ้าย(สภาพภายนอก)",
        12 => "รูปด้านขวา(สภาพภายนอก)",
        13 => "รูปด้านหลัง(สภาพภายนอก)",
    ];

    private $picset3 = [
        14 => "รูปหน้าตู้(สภาพภายใน)",
        15 => "รูปด้านซ้ายตู้(สภาพภายใน)",
        16 => "รูปด้านขวาตู้(สภาพภายใน)",
        17 => "รูปเพดานตู้(สภาพภายใน)",
        18 => "รูปพื้นตู้(สภาพภายใน)",
        19 => "รูปก่อนปิดตู้",
    ];

    private $picset4 = [
        20 => "รูปใบ Lot โหลดฝาก",
        21 => "รูปเอกสารบันทึกการตรวจสอบ การส่งสินค้าแช่แข็ง",
        22 => "รูปด้านขวาตู้(สภาพภายใน)",
        23 => "รูปเพดานตู้(สภาพภายใน)",
    ];

    private $fileset1 = [
        24 => "ใบ Lot โหลดฝาก",
        25 => "เอกสารบันทึกการตรวจสอบ การส่งสินค้าแช่แข็ง"
    ];


    public function index(Request $request)
    {
        if (Auth::user()->group->name == 'View')
            $status = 'Loaded';
        else
            $status = $request->get('status');

        $keyword = $request->get('search');
        $perPage = 10;
        $loadcarrymObj = new LoadCarryM();

        if (!empty($status)) {
            $loadcarrymObj = $loadcarrymObj->where('status', $status);
        }
        if (!empty($keyword)) {

            $productid = Product::where('name', 'like', '%' . $keyword . '%')
                ->orWhere('desc', 'like', '%' . $keyword . '%')
                ->orWhere('boxcode', 'like', '%' . $keyword . '%')
                ->orWhere('bagcode', 'like', '%' . $keyword . '%')
                ->pluck('id', 'id');

            $loadcarrymObj = $loadcarrymObj->whereIn('product_id', $productid);
        }
        $loadcarryms = $loadcarrymObj->latest()->paginate($perPage);

        return view('loading_carries.index', compact('loadcarryms', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $croplist = Crop::where('status', 'Active')->pluck('name', 'id');
        $productlist = Product::pluck('name', 'id');
        $exWhlist = ExtWh::where('status', 'Active')->pluck('name', 'id');
        $trackTypelist = $this->trackTypelist;
        $usagelist = $this->usagelist;
        return view('loading_carries.createsec1', compact('croplist', 'productlist', 'exWhlist', 'trackTypelist', 'usagelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $requestData['status'] = 'New';
        $loadcarrym = LoadCarryM::create($requestData);

        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $croplist = Crop::where('status', 'Active')->pluck('name', 'id');
        $productlist = Product::pluck('name', 'id');
        $exWhlist = ExtWh::where('status', 'Active')->pluck('name', 'id');
        $trackTypelist = $this->trackTypelist;
        $usagelist = $this->usagelist;
        $loadcarrym = LoadCarryM::findOrFail($id);
        $teamlistRw = Team::where('status', 'Active')->get();
        $teamlistType = array();
        foreach ($teamlistRw as $teamlistObj) {
            $teamlistType[$teamlistObj->type][$teamlistObj->id] = $teamlistObj->name;
        }
        $teams = array();
        foreach ($loadcarrym->loadcarryteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }
        $picset1 = $this->picset1;
        $picset2 = $this->picset2;
        $picset3 = $this->picset3;
        $picset4 = $this->picset4;
        $fileset1 = $this->fileset1;

        return view('loading_carries.show', compact(
            'croplist', 'productlist', 'exWhlist', 'trackTypelist', 'usagelist', 'loadcarrym',
            'teams', 'teamlistType',
            'picset1',
            'picset2',
            'picset3',
            'picset4',
            'fileset1'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $croplist = Crop::where('status', 'Active')->pluck('name', 'id');
        $productlist = Product::pluck('name', 'id');
        $exWhlist = ExtWh::where('status', 'Active')->pluck('name', 'id');
        $trackTypelist = $this->trackTypelist;
        $usagelist = $this->usagelist;
        $loadcarrym = LoadCarryM::findOrFail($id);
        return view('loading_carries.editsec1', compact('croplist', 'productlist', 'exWhlist', 'trackTypelist', 'usagelist', 'loadcarrym'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        $loadcarrym->update($requestData);

        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editsec2($id)
    {
        $loadcarrym = LoadCarryM::findOrFail($id);
        $teamlist = Team::where('status', 'Active')->pluck('name', 'id');
        $teamlistRw = Team::where('status', 'Active')->get();
        $teamlistType = array();
        foreach ($teamlistRw as $teamlistObj) {
            $teamlistType[$teamlistObj->type][$teamlistObj->id] = $teamlistObj->name;
        }
        // dd($teamlistType);

        $teams = array();
        foreach ($loadcarrym->loadcarryteams as $loadteamObj) {
            $teams[$loadteamObj->team_type][] = $loadteamObj;
        }

        // dd($teams);
        return view('loading_carries.editsec2', compact('loadcarrym', 'teams', 'teamlist', 'teamlistType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatesec2(Request $request, $id)
    {
        $requestData = $request->all();
        //dd($requestData);
        $teams = array();
        for ($i = 1; $i <= 2; $i++) {
            if (!empty($requestData['teams_front_' . $i])) {
                $tmp = array();
                $tmp['load_carry_m_id'] = $id;
                $tmp['team_type'] = 'Front';
                $tmp['team_id'] = $requestData['teams_front_' . $i];

                $teams[] = $tmp;
            }
            if (!empty($requestData['teams_back_' . $i])) {
                $tmp = array();
                $tmp['load_carry_m_id'] = $id;
                $tmp['team_type'] = 'Back';
                $tmp['team_id'] = $requestData['teams_back_' . $i];

                $teams[] = $tmp;
            }
        }
        if (!empty($requestData['teams_fl_1'])) {
            $tmp = array();
            $tmp['load_carry_m_id'] = $id;
            $tmp['team_type'] = 'FL';
            $tmp['team_id'] = $requestData['teams_fl_1'];

            $teams[] = $tmp;
        }
        if (!empty($requestData['teams_ctrl_1'])) {
            $tmp = array();
            $tmp['load_carry_m_id'] = $id;
            $tmp['team_type'] = 'Ctrl';
            $tmp['team_id'] = $requestData['teams_ctrl_1'];

            $teams[] = $tmp;
        }
        if (!empty($requestData['teams_write_1'])) {
            $tmp = array();
            $tmp['load_carry_m_id'] = $id;
            $tmp['team_type'] = 'WRITE';
            $tmp['team_id'] = $requestData['teams_write_1'];

            $teams[] = $tmp;
        }

        LoadCarryTeam::where('load_carry_m_id', $id)->delete();
        //dd($teams);
        $loadcarrym = LoadCarryM::findOrFail($id);
        LoadCarryTeam::insert($teams);

        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    public function editsec3($id)
    {
        $loadcarrym = LoadCarryM::findOrFail($id);

        // dd($teams);
        return view('loading_carries.editsec3', compact('loadcarrym'));
    }

    public function updatesec3(Request $request, $id)
    {
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        if (isset($requestData['open_box'])) {
            $requestData['open_box'] = \Carbon\Carbon::parse($requestData['open_box'])->format('Y-m-d H:i');
        }

        if (isset($requestData['close_box'])) {
            $requestData['close_box'] = \Carbon\Carbon::parse($requestData['close_box'])->format('Y-m-d H:i');
        }


        $loadcarrym->update($requestData);

        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    public function editpic1($id)
    {
        $pics = $this->picset1;
        $loadcarrym = LoadCarryM::findOrFail($id);

        //  dd($loadcarrym);
        return view('loading_carries.editpic1', compact('loadcarrym', 'pics'));
    }

    public function updatepic1(Request $request, $id)
    {
        $pics = $this->picset1;
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        foreach ($pics as $key => $value) {
            $pic_remove = 'remove_path' . $key;
            $pic_load = 'loading_img' . $key;
            $pic_path = 'loading_img_path' . $key;
            if (isset($requestData[$pic_remove]) && !empty($requestData[$pic_remove])) {
                // dd($loadm->$pic_path);
                //ลบรูป
                // echo $pic_remove.'->remove</br>';
                if (file_exists(public_path($loadcarrym->$pic_path))) {
                    unlink($loadcarrym->$pic_path);
                }

                $loadcarrym->$pic_load = null;
                $loadcarrym->$pic_path = null;
            }

            if ($request->hasFile($pic_load)) {
                //ลบรูป
                if (!empty($loadcarrym->$pic_path)) {
                    echo $pic_load . '->update</br>';
                    if (file_exists(public_path($loadcarrym->$pic_path))) {
                        unlink($loadcarrym->$pic_path);
                    }
                }

                $zipfile = $request->file($pic_load);
                // $uploadname = $zipfile->getClientOriginalName();
                $fileName = uniqid() . '.jpg';

                $completedirectory = 'images/loadings/' . $id . '/';
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                }

                $width = 300; //*** Fix Width & Heigh (Autu caculate) ***//
                $size = GetimageSize($zipfile);
                $height = round($width * $size[1] / $size[0]);
                if ($zipfile->getClientOriginalExtension() == 'jpg' || $zipfile->getClientOriginalExtension() == 'jpeg')
                    $images_orig = ImageCreateFromJPEG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'png')
                    $images_orig = ImageCreateFromPNG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'gif')
                    $images_orig = ImageCreateFromGIF($zipfile);

                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
                ImageJPEG($images_fin, $completedirectory . $fileName);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $loadcarrym->$pic_load = $fileName;
                $loadcarrym->$pic_path = 'images/loadings/' . $id  . "/" . $fileName;
            }
        }

        $loadcarrym->update();
        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    public function editpic2($id)
    {
        $pics = $this->picset2;
        $loadcarrym = LoadCarryM::findOrFail($id);

        //  dd($loadcarrym);
        return view('loading_carries.editpic2', compact('loadcarrym', 'pics'));
    }

    public function updatepic2(Request $request, $id)
    {
        $pics = $this->picset2;
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        foreach ($pics as $key => $value) {
            $pic_remove = 'remove_path' . $key;
            $pic_load = 'loading_img' . $key;
            $pic_path = 'loading_img_path' . $key;
            if (isset($requestData[$pic_remove]) && !empty($requestData[$pic_remove])) {
                // dd($loadm->$pic_path);
                //ลบรูป
                // echo $pic_remove.'->remove</br>';
                if (file_exists(public_path($loadcarrym->$pic_path))) {
                    unlink($loadcarrym->$pic_path);
                }

                $loadcarrym->$pic_load = null;
                $loadcarrym->$pic_path = null;
            }

            if ($request->hasFile($pic_load)) {
                //ลบรูป
                if (!empty($loadcarrym->$pic_path)) {
                    echo $pic_load . '->update</br>';
                    if (file_exists(public_path($loadcarrym->$pic_path))) {
                        unlink($loadcarrym->$pic_path);
                    }
                }

                $zipfile = $request->file($pic_load);
                // $uploadname = $zipfile->getClientOriginalName();
                $fileName = uniqid() . '.jpg';

                $completedirectory = 'images/loadings/' . $id . '/';
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                }

                $width = 300; //*** Fix Width & Heigh (Autu caculate) ***//
                $size = GetimageSize($zipfile);
                $height = round($width * $size[1] / $size[0]);
                if ($zipfile->getClientOriginalExtension() == 'jpg' || $zipfile->getClientOriginalExtension() == 'jpeg')
                $images_orig = ImageCreateFromJPEG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'png')
                $images_orig = ImageCreateFromPNG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'gif')
                $images_orig = ImageCreateFromGIF($zipfile);

                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
                ImageJPEG($images_fin, $completedirectory . $fileName);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $loadcarrym->$pic_load = $fileName;
                $loadcarrym->$pic_path = 'images/loadings/' . $id  . "/" . $fileName;
            }
        }

        $loadcarrym->update();
        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    public function editpic3($id)
    {
        $pics = $this->picset3;
        $loadcarrym = LoadCarryM::findOrFail($id);

        //  dd($loadcarrym);
        return view('loading_carries.editpic3', compact('loadcarrym', 'pics'));
    }

    public function updatepic3(Request $request, $id)
    {
        $pics = $this->picset3;
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        foreach ($pics as $key => $value) {
            $pic_remove = 'remove_path' . $key;
            $pic_load = 'loading_img' . $key;
            $pic_path = 'loading_img_path' . $key;
            if (isset($requestData[$pic_remove]) && !empty($requestData[$pic_remove])) {
                // dd($loadm->$pic_path);
                //ลบรูป
                // echo $pic_remove.'->remove</br>';
                if (file_exists(public_path($loadcarrym->$pic_path))) {
                    unlink($loadcarrym->$pic_path);
                }

                $loadcarrym->$pic_load = null;
                $loadcarrym->$pic_path = null;
            }

            if ($request->hasFile($pic_load)) {
                //ลบรูป
                if (!empty($loadcarrym->$pic_path)) {
                    echo $pic_load . '->update</br>';
                    if (file_exists(public_path($loadcarrym->$pic_path))) {
                        unlink($loadcarrym->$pic_path);
                    }
                }

                $zipfile = $request->file($pic_load);
                // $uploadname = $zipfile->getClientOriginalName();
                $fileName = uniqid() . '.jpg';

                $completedirectory = 'images/loadings/' . $id . '/';
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                }

                $width = 300; //*** Fix Width & Heigh (Autu caculate) ***//
                $size = GetimageSize($zipfile);
                $height = round($width * $size[1] / $size[0]);
                if ($zipfile->getClientOriginalExtension() == 'jpg' || $zipfile->getClientOriginalExtension() == 'jpeg')
                $images_orig = ImageCreateFromJPEG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'png')
                $images_orig = ImageCreateFromPNG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'gif')
                $images_orig = ImageCreateFromGIF($zipfile);

                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
                ImageJPEG($images_fin, $completedirectory . $fileName);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $loadcarrym->$pic_load = $fileName;
                $loadcarrym->$pic_path = 'images/loadings/' . $id  . "/" . $fileName;
            }
        }

        $loadcarrym->update();
        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    public function editpic4($id)
    {
        $pics = $this->picset4;
        $loadcarrym = LoadCarryM::findOrFail($id);

        //  dd($loadcarrym);
        return view('loading_carries.editpic4', compact('loadcarrym', 'pics'));
    }

    public function updatepic4(Request $request, $id)
    {
        $pics = $this->picset4;
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        foreach ($pics as $key => $value) {
            $pic_remove = 'remove_path' . $key;
            $pic_load = 'loading_img' . $key;
            $pic_path = 'loading_img_path' . $key;
            if (isset($requestData[$pic_remove]) && !empty($requestData[$pic_remove])) {
                // dd($loadm->$pic_path);
                //ลบรูป
                // echo $pic_remove.'->remove</br>';
                if (file_exists(public_path($loadcarrym->$pic_path))) {
                    unlink($loadcarrym->$pic_path);
                }

                $loadcarrym->$pic_load = null;
                $loadcarrym->$pic_path = null;
            }

            if ($request->hasFile($pic_load)) {
                //ลบรูป
                if (!empty($loadcarrym->$pic_path)) {
                    echo $pic_load . '->update</br>';
                    if (file_exists(public_path($loadcarrym->$pic_path))) {
                        unlink($loadcarrym->$pic_path);
                    }
                }

                $zipfile = $request->file($pic_load);
                // $uploadname = $zipfile->getClientOriginalName();
                $fileName = uniqid() . '.jpg';

                $completedirectory = 'images/loadings/' . $id . '/';
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                }

                $width = 300; //*** Fix Width & Heigh (Autu caculate) ***//
                $size = GetimageSize($zipfile);
                $height = round($width * $size[1] / $size[0]);
                if ($zipfile->getClientOriginalExtension() == 'jpg' || $zipfile->getClientOriginalExtension() == 'jpeg')
                $images_orig = ImageCreateFromJPEG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'png')
                $images_orig = ImageCreateFromPNG($zipfile);
                elseif ($zipfile->getClientOriginalExtension() == 'gif')
                $images_orig = ImageCreateFromGIF($zipfile);

                $photoX = ImagesX($images_orig);
                $photoY = ImagesY($images_orig);
                $images_fin = ImageCreateTrueColor($width, $height);
                ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
                ImageJPEG($images_fin, $completedirectory . $fileName);
                ImageDestroy($images_orig);
                ImageDestroy($images_fin);

                $loadcarrym->$pic_load = $fileName;
                $loadcarrym->$pic_path = 'images/loadings/' . $id  . "/" . $fileName;
            }
        }

        $loadcarrym->update();
        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

    public function editfile1($id)
    {
        $pics = $this->fileset1;
        $loadcarrym = LoadCarryM::findOrFail($id);

        //  dd($loadcarrym);
        return view('loading_carries.editfile1', compact('loadcarrym', 'pics'));
    }

    public function updatefile1(Request $request, $id)
    {
        $pics = $this->fileset1;
        $requestData = $request->all();
        $loadcarrym = LoadCarryM::findOrFail($id);
        foreach ($pics as $key => $value) {
            $pic_remove = 'remove_path' . $key;
            $pic_load = 'loading_img' . $key;
            $pic_path = 'loading_img_path' . $key;
            if (isset($requestData[$pic_remove]) && !empty($requestData[$pic_remove])) {
                // dd($loadm->$pic_path);
                //ลบรูป
                // echo $pic_remove.'->remove</br>';
                if (file_exists(public_path($loadcarrym->$pic_path))) {
                    unlink($loadcarrym->$pic_path);
                }

                $loadcarrym->$pic_load = null;
                $loadcarrym->$pic_path = null;
            }

            if ($request->hasFile($pic_load)) {
                //ลบรูป
                if (!empty($loadcarrym->$pic_path)) {
                    echo $pic_load . '->update</br>';
                    if (file_exists(public_path($loadcarrym->$pic_path))) {
                        unlink($loadcarrym->$pic_path);
                    }
                }

                $zipfile = $request->file($pic_load);
                // $uploadname = $zipfile->getClientOriginalName();
                $fileName = uniqid() . '.' . $zipfile->getClientOriginalExtension();

                $completedirectory = 'images/loadings/' . $id . '/';
                if (!is_dir($completedirectory)) {
                    mkdir($completedirectory, 0777, true);
                }

                $destinationPath = public_path('images/loadings/' . $id  . "/");
                $zipfile->move($destinationPath, $fileName);

                $loadcarrym->$pic_load = $fileName;
                $loadcarrym->$pic_path = 'images/loadings/' . $id  . "/" . $fileName;
            }
        }

        $loadcarrym->update();
        return redirect('loading_carries?status=' . $loadcarrym->status);
    }

}
