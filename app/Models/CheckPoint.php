<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckPoint extends Model
{
    use HasFactory;

    protected $fillable = [
            'name',
            'desc',
            'lat',
            'lng',
            'status'];

    public function checkanswers()
    {
        return $this->hasMany('App\Models\CheckAnswer', 'check_point_id');
    }
}
