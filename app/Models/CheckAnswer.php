<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
            'check_ans_m_id',
            'load_m_id',
            'check_point_id',
            'check_questions_id',
            'seq',
            'type_question',
            'ans1_column',
            'ans2_column',
            'ans3_column',
            'custom_f_1',
            'custom_f_2',
            'custom_f_3',
            'custom_s_1',
            'custom_s_2',
            'custom_s_3',
            'custom_t_1',
            'custom_t_2',
            'custom_t_3',
            'status'];

    public function checkans()
    {
        return $this->hasOne('App\Models\CheckAnsM', 'id', 'check_ans_m_id');
    }

    public function loadingm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }

    public function checkpoint()
    {
        return $this->hasOne('App\Models\CheckPoint', 'id', 'check_point_id');
    }

    public function checkquestion()
    {
        return $this->hasOne('App\Models\CheckQuestion', 'id', 'check_questions_id');
    }

}
