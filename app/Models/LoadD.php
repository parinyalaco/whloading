<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadD extends Model
{
    use HasFactory;

    protected $fillable = [
        'load_m_id',
        'product_id',
        'main_type',
        'row',
        't_base',
        't_height',
        't_excess',
        't_product_id',
        't_total',
        'l_base',
        'l_height',
        'l_excess',
        'l_product_id',
        'l_total',
        'note',
        'all_total',
        't_product_m_id',
        'l_product_m_id',
        't_product1_id',
        't_product2_id',
        'l_product1_id',
        'l_product2_id',
        't_excess1',
        't_excess2',
        'l_excess1',
        'l_excess2',
        'c_product_id', 'c_base', 'c_height', 'c_total', 'c_product_m_id',
        'c_product1_id', 'c_product2_id', 'c_excess', 'c_excess1', 'c_excess2', 't_box_style',
        'tx_box_style', 'l_box_style', 'lx_box_style', 'c_box_style', 'cx_box_style',
        'pallet_no', 'p_lot', 'p_exp_date'
    ];

    public function loadm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'product_id');
    }

    public function productt()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product_m_id');
    }

    public function productl()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product_m_id');
    }

    public function productc()
    {
        return $this->hasOne('App\Models\Product', 'id',  'c_product_m_id');
    }

    public function lproduct()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product_id');
    }
    public function l1product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product1_id');
    }
    public function l2product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product2_id');
    }

    public function tproduct()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product_id');
    }
    public function t1product()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product1_id');
    }
    public function t2product()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product2_id');
    }

    public function cproduct()
    {
        return $this->hasOne('App\Models\Product', 'id',  'c_product_id');
    }
    public function c1product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'c_product1_id');
    }
    public function c2product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'c_product2_id');
    }

    public function recalculate($id)
    {
        $data = self::where('id', $id)
            ->first();

        if (!empty($data)) {
            $total = 0;
            if (!empty($data->t_total)) {
                $total += $data->t_total;
            }
            if (!empty($data->l_total)) {
                $total += $data->l_total;
            }
            $data->all_total = $total;
            $data->update();
        }
    }

    public function recalculate3rows($id)
    {
        $data = self::where('id', $id)
            ->first();

        if (!empty($data)) {
            $total = 0;
            if (!empty($data->t_total)) {
                $total += $data->t_total;
            }
            if (!empty($data->l_total)) {
                $total += $data->l_total;
            }
            if (!empty($data->c_total)) {
                $total += $data->c_total;
            }
            $data->all_total = $total;
            $data->update();
        }
    }
}
