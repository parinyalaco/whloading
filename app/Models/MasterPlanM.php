<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPlanM extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'customer',
        'customer_type_id',
        'note',
        'status'
    ];

    public function masterplands()
    {
        return $this->hasMany('App\Models\MasterPlanD', 'master_plan_m_id');
    }

    public function customertype()
    {
        return $this->hasOne('App\Models\CustomerType', 'id',  'customer_type_id');
    }
}
