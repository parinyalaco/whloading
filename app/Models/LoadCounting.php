<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadCounting extends Model
{
    use HasFactory;
    protected $fillable = ['load_m_id','all_counting','reject_counting','accept_counting'];

    public function loadm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }
}
