<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListLoadM extends Model
{
    use HasFactory;

    protected $fillable = [
        'list_date',
        'shipment_order',
        'customer_po',
        'user_id',
        'list_img',
        'list_img_path',
    ];
      
    public function ListLoadDs()
    {
        return $this->hasMany('App\Models\ListLoadD', 'list_load_d_id');
    }

    public function Img_pds()
    {
        return $this->hasMany('App\Models\Img_pd', 'img_pd_id');
    }
     
}
