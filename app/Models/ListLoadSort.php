<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListLoadSort extends Model
{
    use HasFactory;

    protected $fillable = [
        'list_load_m_id',
        'product_id',
        'sort',
        'quantity',
        'user_id',
    ];
        
    public function ListLoadM()
    {
        return $this->hasOne('App\Models\ListLoadM', 'id', 'list_load_m_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'product_id');
    }
}
