<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListLoadD extends Model
{
    use HasFactory;

    protected $fillable = [
        'list_load_m_id',
        'sort',
        'st_location_id',
        'st_type_id',
        'product_id',
        'storage_bin',
        'pallet_no',
        'batch',
        'gr_date',
        'mfg_date',
        'exp_date',
        'laco_batch_id',
        'box_no',
        'shipment_id',
        'ur_qty',
        'block_qty',
        'ql_qty',
        'available_stock',
        'status_id',
        'usage_id',
        'sales_doc',
        'dales_doc_item',
        'inspection_los',
        'note',
        'list_img',
        'list_img_path',
        'user_id',
    ];
        
    public function ListLoadM()
    {
        return $this->hasOne('App\Models\ListLoadM', 'id', 'list_load_m_id');
    }

    public function StLocation()
    {
        return $this->hasOne('App\Models\StLocation', 'id', 'st_location_id');
    }

    public function StType()
    {
        return $this->hasOne('App\Models\StType', 'id', 'st_type_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'product_id');
    }

    public function LacoBatch()
    {
        return $this->hasOne('App\Models\LacoBatch', 'id', 'laco_batch_id');
    }

    public function Shipment()
    {
        return $this->hasOne('App\Models\Shipment', 'id', 'shipment_id');
    }

    public function ListStatus()
    {
        return $this->hasOne('App\Models\ListStatus', 'id', 'status_id');
    }

    public function usage()
    {
        return $this->hasOne('App\Models\ListStatus', 'id',  'usage_id');
    }

}
