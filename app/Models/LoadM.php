<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadM extends Model
{
    use HasFactory;

    protected $fillable = [
        'load_date',
        'act_date', 'customer',
        'customer_type_id',
        'order_no', 'owner_type', 'truck_license_plate', 'box_weight',
        'convoy_license_plate', 'convoy_no', 'seal_no', 'open_box', 'close_box', 'note',
        'status',
        'broken_box', 'broken_box_case',
        'loading_img1', 'loading_img_path1',
        'loading_img2', 'loading_img_path2',
        'loading_img3', 'loading_img_path3',
        'loading_img4', 'loading_img_path4',
        'loading_img5', 'loading_img_path5',
        'loading_img6', 'loading_img_path6',
        'loading_img7', 'loading_img_path7',
        'loading_img8', 'loading_img_path8',
        'loading_img9', 'loading_img_path9',
        'loading_img10', 'loading_img_path10',
        'loading_img11', 'loading_img_path11',
        'loading_img12', 'loading_img_path12',
        'loading_img13', 'loading_img_path13',
        'loading_img14', 'loading_img_path14',
        'loading_img15', 'loading_img_path15',
        'loading_img16', 'loading_img_path16',
        'loading_img17', 'loading_img_path17',
        'loading_img18', 'loading_img_path18',
        'loading_img19', 'loading_img_path19',
        'conveyor_id', 'user_close', 'container_type_id',
        'loading_img20', 'loading_img_path20',
        'type_loading',
    ];

    public function loadds()
    {
        return $this->hasMany('App\Models\LoadD', 'load_m_id');
    }

    public function loadsorts()
    {
        return $this->hasMany('App\Models\ListLoadSort', 'load_m_id');
    }

    public function loadteams()
    {
        return $this->hasMany('App\Models\LoadTeam', 'load_m_id');
    }

    public function customertype()
    {
        return $this->hasOne('App\Models\CustomerType', 'id',  'customer_type_id');
    }

    public function loadbrokens()
    {
        return $this->hasMany('App\Models\LoadBroken', 'load_m_id');
    }

    public function loadcounting()
    {
        return $this->hasOne('App\Models\LoadCounting', 'load_m_id',  'id');
    }

    public function containertype()
    {
        return $this->hasOne('App\Models\ContainerType', 'id',  'container_type_id');
    }

    public function loadlots()
    {
        return $this->hasMany('App\Models\LoadLot', 'load_m_id');
    }

    public function loadingimages()
    {
        return $this->hasMany('App\Models\LoadingImage', 'load_m_id');
    }

    public function countingbox()
    {
        $datarws = $this->hasMany('App\Models\LoadD', 'load_m_id')->get();
        $data = array();

        foreach ($datarws as $datarw) {
            if (isset($data['all'])) {
                $data['all'] += $datarw->all_total;
            } else {
                $data['all'] = $datarw->all_total;
            }
        }
        return $data;
    }

    public function productdata()
    {
        $datarws = $this->hasMany('App\Models\LoadD', 'load_m_id')->get();
        $data = array();

        foreach ($datarws as $datarw) {
            if (isset($data['all'])) {
                $data['all'] += $datarw->all_total;
            } else {
                $data['all'] = $datarw->all_total;
            }

            if (isset($datarw->productt->name)) {
                if (isset($data['product'][$datarw->productt->name])) {
                    $data['product'][$datarw->productt->name] += $datarw->t_base * $datarw->t_height;
                } else {
                    $data['product'][$datarw->productt->name] = $datarw->t_base * $datarw->t_height;
                    $data['productinfo'][$datarw->productt->name]['info'] = $datarw->productt;
                }
            }

            if (isset($datarw->productl->name)) {
                if (isset($data['product'][$datarw->productl->name])) {
                    $data['product'][$datarw->productl->name] += $datarw->l_base * $datarw->l_height;
                } else {
                    $data['product'][$datarw->productl->name] = $datarw->l_base * $datarw->l_height;
                    //dd($datarw->productl);
                    $data['productinfo'][$datarw->productl->name]['info'] = $datarw->productl;

                }
            }

            if (isset($datarw->lproduct->name)) {
                if (isset($data['product'][$datarw->lproduct->name])) {
                    $data['product'][$datarw->lproduct->name] += $datarw->l_excess;
                } else {
                    $data['product'][$datarw->lproduct->name] = $datarw->l_excess;

                    $data['productinfo'][$datarw->lproduct->name]['info'] = $datarw->lproduct;
                }
            }

            if (isset($datarw->l1product->name)) {
                if (isset($data['product'][$datarw->l1product->name])) {
                    $data['product'][$datarw->l1product->name] += $datarw->l_excess1;
                } else {
                    $data['product'][$datarw->l1product->name] = $datarw->l_excess1;
                    $data['productinfo'][$datarw->l1product->name]['info'] = $datarw->l1product;
                }
            }

            if (isset($datarw->l2product->name)) {
                if (isset($data['product'][$datarw->l2product->name])) {
                    $data['product'][$datarw->l2product->name] += $datarw->l_excess2;
                } else {
                    $data['product'][$datarw->l2product->name] = $datarw->l_excess2;

                    $data['productinfo'][$datarw->l2product->name]['info'] = $datarw->l2product;
                }
            }

            if (isset($datarw->tproduct->name)) {
                if (isset($data['product'][$datarw->tproduct->name])) {
                    $data['product'][$datarw->tproduct->name] += $datarw->t_excess;
                } else {
                    $data['product'][$datarw->tproduct->name] = $datarw->t_excess;

                    $data['productinfo'][$datarw->tproduct->name]['info'] = $datarw->tproduct;

                }
            }

            if (isset($datarw->t1product->name)) {
                if (isset($data['product'][$datarw->t1product->name])) {
                    $data['product'][$datarw->t1product->name] += $datarw->t_excess1;
                } else {
                    $data['product'][$datarw->t1product->name] = $datarw->t_excess1;

                    $data['productinfo'][$datarw->t1product->name]['info'] = $datarw->t1product;

                }
            }

            if (isset($datarw->t2product->name)) {
                if (isset($data['product'][$datarw->t2product->name])) {
                    $data['product'][$datarw->t2product->name] += $datarw->t_excess2;
                } else {
                    $data['product'][$datarw->t2product->name] = $datarw->t_excess2;

                    $data['productinfo'][$datarw->t2product->name]['info'] = $datarw->t2product;

                }
            }
        }
        return $data;
    }

    public function reasons()
    {
        return $this->hasMany('App\Models\reason', 'load_m_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_close');
    }
}
