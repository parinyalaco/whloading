<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
            'check_set_id',
            'name',
            'seq',
            'type_question',
            'ans1_column',
            'ans2_column',
            'ans3_column',
            'desc',
            'status',
            'required_flag'
        ];

    public function checkset()
    {
        return $this->hasOne('App\Models\CheckSet', 'id', 'check_set_id');
    }

    public function checkanswers()
    {
        return $this->hasMany('App\Models\CheckAnswer', 'check_questions_id');
    }
}
