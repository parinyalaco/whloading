<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadingImage extends Model
{
    use HasFactory;
     protected $fillable = [
        'load_m_id',
        'loading_img', 'loading_img_path',
        'seq',
        'typename',
        'typeseq'
    ];

    public function loadm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }
}
