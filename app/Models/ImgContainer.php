<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImgContainer extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'list_load_m_id', 'container_img', 'container_img_path', 
    ];

    public function ListLoadM()
    {
        return $this->hasOne('App\Models\ListLoadM', 'id', 'list_load_m_id');
    }

}
