<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadDeliveryPlanM extends Model
{
    use HasFactory;

    protected $fillable = [
            'file_name',
            'file_path',
            'filename_path',
            'status'];

    public function uploaddeliveryplands()
    {
        return $this->hasMany('App\Models\UploadDeliveryPlanD', 'upload_delivery_plan_m_id');
    }

}
