<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $connection = 'sqlsrv';

    protected $table = 'users';

    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'group_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        // $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
        $this->attributes['password'] = bcrypt($password);
    }

    public function group()
    {
        return $this->hasOne('App\Models\Group', 'id',  'group_id');
    }

    public function isAdmin()
    {
        if($this->group->name == trim('Admin') || $this->group->name == trim('Superadmin')){
            return true;
        }else{
            return false;
        }
    }

    public function isSuperAdmin()
    {
        if ($this->group->name == trim('Superadmin')) {
            return true;
        } else {
            return false;
        }
    }
}
