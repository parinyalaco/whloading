<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reason extends Model
{
    use HasFactory;
    protected $fillable = [
        'load_m_id',
        'reason',
    ];

    public function loadm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }
}
