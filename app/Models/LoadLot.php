<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadLot extends Model
{
    use HasFactory;
    protected $fillable = [
        'load_m_id', 'product_id', 'seq', 'start_no', 'end_no', 'quantity', 'exp_date', 'best_date', 'lot', 'remark', 'status'
    ];

    public function loadm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'product_id');
    }
}
