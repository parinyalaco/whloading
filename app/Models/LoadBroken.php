<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadBroken extends Model
{
    use HasFactory;
    protected $fillable = ['load_m_id','broken_type_id','box'];

    public function brokentype()
    {
        return $this->hasOne('App\Models\BrokenType', 'id', 'broken_type_id');
    }
}
