<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckAnsM extends Model
{
    use HasFactory;
    protected $fillable = ['load_m_id','check_point_id','status'];

    public function checkanswers()
    {
        return $this->hasMany('App\Models\CheckAnswer', 'check_ans_m_id');
    }



    public function loadingm()
    {
        return $this->hasOne('App\Models\LoadM', 'id', 'load_m_id');
    }

    public function checkpoint()
    {
        return $this->hasOne('App\Models\CheckPoint', 'id', 'check_point_id');
    }

}

