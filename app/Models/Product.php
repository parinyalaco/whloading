<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','desc', 'boxcode', 'bagcode',
        'wide','long','height', 'wide_er', 'long_er', 'height_er',
        'weight','note',
        'customer', 'numperbox', 'bagweight', 'underboxcode',
        'bundelnumber',
        'desc_list',
        'lot_load_no',
        'lot_load_name'
    ];

    public function loadsorts()
    {
        return $this->hasMany('App\Models\ListLoadSort', 'product_id');
    }
}
