<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPlanD extends Model
{
    use HasFactory;

    protected $fillable = [
        'master_plan_m_id',
        'product_id',
        'main_type',
        'row',
        't_base',
        't_height',
        't_excess',
        't_product_id',
        't_total',
        'l_base',
        'l_height',
        'l_excess',
        'l_product_id',
        'l_total',
        'note',
        'all_total',
        't_product_m_id',
        'l_product_m_id',
        't_product1_id',
        't_product2_id',
        'l_product1_id',
        'l_product2_id',
        't_excess1',
        't_excess2',
        'l_excess1',
        'l_excess2',
    ];

    public function masterplanm()
    {
        return $this->hasOne('App\Models\MasterPlanM', 'id', 'master_plan_m_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'product_id');
    }

    public function productt()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product_m_id');
    }

    public function productl()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product_m_id');
    }

    public function lproduct()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product_id');
    }
    public function l1product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product1_id');
    }
    public function l2product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'l_product2_id');
    }

    public function tproduct()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product_id');
    }
    public function t1product()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product1_id');
    }
    public function t2product()
    {
        return $this->hasOne('App\Models\Product', 'id',  't_product2_id');
    }

    public function recalculate($id)
    {
        $data = self::where('id', $id)
            ->first();

        if (!empty($data)) {
            $total = 0;
            if (!empty($data->t_total)) {
                $total += $data->t_total;
            }
            if (!empty($data->l_total)) {
                $total += $data->l_total;
            }
            $data->all_total = $total;
            $data->update();
        }
    }
}
