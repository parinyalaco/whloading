<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
    ];

    public function ListLoadDs()
    {
        return $this->hasMany('App\Models\ListLoadD', 'list_load_d_id');
    }
}
