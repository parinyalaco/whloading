<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadCarryTeam extends Model
{
    use HasFactory;

    protected $fillable = ['load_carry_m_id', 'team_type', 'team_id'];

    public function team()
    {
        return $this->hasOne('App\Models\Team', 'id', 'team_id');
    }
}
