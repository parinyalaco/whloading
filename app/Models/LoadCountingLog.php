<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadCountingLog extends Model
{
    use HasFactory;
    protected $fillable = ['load_m_id','counter_name','type','counter'];
}
