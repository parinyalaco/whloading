<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoadCarryM extends Model
{
    use HasFactory;

    protected $fillable = [
        'load_date',
        'act_date',
        'product_id',
        'ext_wh_id',
        'crop_id',
        'track_type',
        'num_of_product',
        'weight_per_product',
        'total_weight',
        'truck_license_plate',
        'convoy_license_plate',
        'usage',
        'open_box',
        'close_box',
        'open_temperature',
        'delivery_temperature',
        'boc_close_temperature',
        'boc_delivery_temperature',
        'first_pallet_temperature',
        'middle_pallet_temperature',
        'last_pallet_temperature',
        'note',
        'status',
        'broken_box', 'broken_box_case',
        'loading_img1', 'loading_img_path1',
        'loading_img2', 'loading_img_path2',
        'loading_img3', 'loading_img_path3',
        'loading_img4', 'loading_img_path4',
        'loading_img5', 'loading_img_path5',
        'loading_img6', 'loading_img_path6',
        'loading_img7', 'loading_img_path7',
        'loading_img8', 'loading_img_path8',
        'loading_img9', 'loading_img_path9',
        'loading_img10', 'loading_img_path10',
        'loading_img11', 'loading_img_path11',
        'loading_img12', 'loading_img_path12',
        'loading_img13', 'loading_img_path13',
        'loading_img14', 'loading_img_path14',
        'loading_img15', 'loading_img_path15',
        'loading_img16', 'loading_img_path16',
        'loading_img17', 'loading_img_path17',
        'loading_img18', 'loading_img_path18',
        'loading_img19', 'loading_img_path19',
        'loading_img20', 'loading_img_path20',
        'loading_img21', 'loading_img_path21',
        'loading_img22', 'loading_img_path22',
        'loading_img23', 'loading_img_path23',
        'loading_img24', 'loading_img_path24',
        'loading_img25', 'loading_img_path25',
        'loading_img26', 'loading_img_path26',
        'loading_img27', 'loading_img_path27',
        'loading_img28', 'loading_img_path28',
        'loading_img29', 'loading_img_path29',
        'loading_img30', 'loading_img_path30',
    ];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id',  'product_id');
    }

    public function extwh()
    {
        return $this->hasOne('App\Models\ExtWh', 'id',  'ext_wh_id');
    }

    public function crop()
    {
        return $this->hasOne('App\Models\Crop', 'id',  'crop_id');
    }

    public function loadcarryteams()
    {
        return $this->hasMany('App\Models\LoadCarryTeam', 'load_carry_m_id');
    }
}
