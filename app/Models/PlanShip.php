<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanShip extends Model
{
    use HasFactory;

    protected $fillable = ['plan_date','plan_num_ship','act_num_ship','note'];
}
