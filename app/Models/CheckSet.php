<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckSet extends Model
{
    use HasFactory;

    protected $fillable = [
            'name',
            'desc',
            'status'];

    public function checkquestions()
    {
        return $this->hasMany('App\Models\CheckQuestion', 'check_set_id');
    }
}
