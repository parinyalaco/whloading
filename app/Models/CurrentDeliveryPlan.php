<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrentDeliveryPlan extends Model
{
    use HasFactory;

    protected $fillable = [
            'upload_delivery_plan_m_id',
            'loading',
            'order',
            'license_plate_tail',
            'license_plate_head',
            'truck',
            'container_no',
            'seal_no',
            'driver1',
            'driver2',
            'status'];

    public function uploaddeliveryplanm()
    {
        return $this->hasOne('App\Models\UploadDeliveryPlanM', 'id', 'upload_delivery_plan_m_id');
    }
}
