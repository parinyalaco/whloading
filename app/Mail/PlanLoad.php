<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlanLoad extends Mailable
{
    use Queueable, SerializesModels;
    public $loadm;
    public $f_pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($var1,$var2)
    {
        $this->loadm = $var1;
        $this->f_pdf = $var2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tb1 = array();
        $fl = array();
        $load_m = $this->loadm; 
        // dd($load_m);     
        $file_all = $this->f_pdf;
        // dd($file_all);

        $mail_data = array();
        $mail_data['from'] = 'noreply@lannaagro.com';
        $mail_data['to'] = config('my.mail.real.to');     
        $mail_data['cc'] = config('my.mail.real.cc');

        // $mail_data['to'] = 'Paison@Lannaagro.com';     
        // $mail_data['to'] = 'yupa@lannaagro.com';  
        // $mail_data['cc'] = 'yupa@lannaagro.com'; 
        // dd($mail_data);
        
        $email = $this->from($mail_data['from'], 'LACO WH Loading')->to($mail_data['to'])->cc($mail_data['cc'])
            ->view('loadings.mail', compact('load_m','file_all'))
            ->subject('รายงานการ Load สินค้า');
        foreach ($file_all as $filePath) {
            $email->attach(public_path('/storage/PDF/'.date('Y'). "/" . date('m'). "/" . $filePath));
        }
        return $email;
    }
}
