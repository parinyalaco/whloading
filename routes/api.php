<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiCounterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('counters/list', [ApiCounterController::class, 'getlist']);
Route::get('counters/current/{load_id}', [ApiCounterController::class, 'getcurrentcounter']);
Route::get('counters/post/{load_id}/', [ApiCounterController::class, 'postcounter']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
