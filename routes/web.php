<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\LoadingsController;
use App\Http\Controllers\TeamsController;
use App\Http\Controllers\BrokenTypesController;
use App\Http\Controllers\ContainerTypeController;
use App\Http\Controllers\CustomerTypesController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\MasterPlansController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ShipPlansController;
use App\Http\Controllers\ListLoadController;
use App\Http\Controllers\ListLoadDetController;
use App\Http\Controllers\CropsController;
use App\Http\Controllers\ExtWhsController;
use App\Http\Controllers\LoadingCarriesController;
use App\Http\Controllers\CheckPointsController;
use App\Http\Controllers\CheckSetsController;
use App\Http\Controllers\CheckQuestionsController;
use App\Http\Controllers\CheckAnswersController;
use App\Http\Controllers\DriversController;
use App\Http\Controllers\UploadDeliveryPlansController;
use App\Http\Controllers\CheckPointPublicController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/checkscan', [CheckPointPublicController::class, 'scan']);


Route::get('/', [LoadingsController::class, 'list']);

Route::get('/userdatas/changepass', [UsersController::class, 'changepass'])->name('userdatas.changepass');
Route::post('/userdatas/changepassAction', [UsersController::class, 'changepassAction'])->name('userdatas.changepassaction');
Route::get('/user_importView', [UsersController::class, 'importExportView'])->name('user_importView');
Route::post('/userdatas/import', [UsersController::class, 'import'])->name('user_import');
Route::resource('userdatas', UsersController::class);
Route::get('/product_importView', [ProductsController::class, 'importExportView'])->name('product_importView');
Route::post('/products/import', [ProductsController::class, 'import'])->name('product_import');
Route::get('/products/getbyid/{id}', [ProductsController::class, 'getById'])->name('products.getById');
Route::get('/drivers', [DriversController::class, 'index'])->name('drivers.index');
Route::resource('uploaddeliveryplans', UploadDeliveryPlansController::class);


Route::prefix('admin')->group(function () {
    Route::get('check_points/qrcode', [CheckPointsController::class, 'qrcode'])->name('check_points.qrcode');
    Route::resource('check_points', CheckPointsController::class);
    Route::resource('check_sets', CheckSetsController::class);
    Route::prefix('{check_set_id}')->group(function () {
        Route::resource('check_questions', CheckQuestionsController::class);
    });
});

Route::prefix('chkpoint/{loading_id}')->group(
    function () {
        Route::get('answers/scan', [CheckAnswersController::class,'scan'])->name('answers.scan');
        Route::get('answers/createall', [CheckAnswersController::class, 'createAll'])->name('answers.createAll');
        Route::post('answers/storeall', [CheckAnswersController::class, 'storeAll'])->name('answers.storeAll');
        Route::resource('answers', CheckAnswersController::class);
});

Route::resource('products', ProductsController:: class);
Route::resource('crops', CropsController::class);
Route::resource('ext_whs', ExtWhsController::class);
Route::resource('groups', GroupsController::class);
Route::resource('planships', ShipPlansController::class);
Route::get('/team_importView', [TeamsController::class, 'importExportView'])->name('team_importView');
Route::post('/teams/import', [TeamsController::class, 'import'])->name('team_import');
Route::resource('teams', TeamsController::class);
Route::resource('broken-types', BrokenTypesController::class);
Route::resource('customer-types', CustomerTypesController::class);
Route::resource('container-types', ContainerTypeController::class);

Route::get('/master-plans/createdetail/{master_plan_m_id}', [MasterPlansController::class, 'createdetail'])->name('master-plans.createdetail');
Route::post('/master-plans/createdetailAction/{master_plan_m_id}', [MasterPlansController::class, 'createdetailAction'])->name('master-plans.createdetailAction');
Route::get('/master-plans/editdetail/{id}', [MasterPlansController::class, 'editdetail'])->name('master-plans.editdetail');
Route::post('/master-plans/editdetailAction/{id}', [MasterPlansController::class, 'editdetailAction'])->name('master-plans.editdetailAction');
Route::get('/master-plans/deletedetail/{id}', [MasterPlansController::class, 'deletedetail'])->name('master-plans.deletedetail');

Route::resource('master-plans', MasterPlansController::class);

Route::get('/loadings', [LoadingsController::class, 'list'])->name('loadings.index');
Route::get('/loadings/create', [LoadingsController::class, 'create'])->name('loadings.create');
Route::post('/loadings/createAction', [LoadingsController::class, 'createAction'])->name('loadings.createaction');
Route::get('/loadings/edit/{id}', [LoadingsController::class, 'edit'])->name('loadings.edit');
Route::post('/loadings/editAction/{id}', [LoadingsController::class, 'editAction'])->name('loadings.editaction');
Route::get('/loadings/show/{id}', [LoadingsController::class, 'show'])->name('loadings.show');
Route::get('/loadings/realtime/{id}', [LoadingsController::class, 'realtime'])->name('loadings.realtime');
Route::get('/loadings/realtimeno/{id}', [LoadingsController::class, 'realtimeno'])->name('loadings.realtimeno');
Route::get('/loadings/sent_mail', [LoadingsController::class, 'sent_mail'])->name('loadings.sent_mail');
Route::get('/loadings/sent_mail_extImage', [LoadingsController::class, 'sent_mail_extImage'])->name('loadings.sent_mail_extImage');

// Route::get('/loadings/downloadPDF/{id}', [LoadingsController::class, 'downloadPDF'])->name('loadings.downloadPDF');
Route::post('/loadings/load_reason', [LoadingsController::class, 'load_reason'])->name('loadings.load_reason');
Route::get('/loadings/clone/{id}', [LoadingsController::class, 'clone'])->name('loadings.clone');
Route::post('/loadings/cloneAction/{id}', [LoadingsController::class, 'cloneAction'])->name('loadings.cloneAction');

Route::get('/loadings/getrealcounter/{id}', [LoadingsController::class, 'getrealcounter'])->name('loadings.getrealcounter');
Route::get('/loadings/edit/{id}', [LoadingsController::class, 'edit'])->name('loadings.edit');
Route::get('/loadings/delete/{id}', [LoadingsController::class, 'delete'])->name('loadings.destroy');
Route::get('/loadings/planload/{id}', [LoadingsController::class, 'planload'])->name('loadings.planload');
Route::post('/loadings/planloadAction/{id}', [LoadingsController::class, 'planloadAction'])->name('loadings.planloadaction');
Route::get('/loadings/createdetail/{id}', [LoadingsController::class, 'createdetail'])->name('loadings.createdetail');
Route::post('/loadings/createdetailAction/{id}', [LoadingsController::class, 'createdetailAction'])->name('loadings.createdetailaction');
Route::get('/loadings/editdetail/{id}', [LoadingsController::class, 'editdetail'])->name('loadings.editdetail');
Route::post('/loadings/editdetailAction/{id}', [LoadingsController::class, 'editdetailAction'])->name('loadings.editdetailaction');
Route::get('/loadings/deletedetail/{id}', [LoadingsController::class, 'deletedetail'])->name('loadings.deletedetail');
Route::get('/loadings/planloadview/{id}', [LoadingsController::class, 'planloadview'])->name('loadings.planloadview');
Route::get('/loadings/changestage/{id}/{stage}', [LoadingsController::class, 'changestage'])->name('loadings.changestage');
Route::get('/loadings/entersection1/{id}', [LoadingsController::class, 'entersection1'])->name('loadings.entersection1');
Route::post('/loadings/entersection1Action/{id}', [LoadingsController::class, 'entersection1Action'])->name('loadings.entersection1action');
Route::get('/loadings/entersection2/{id}', [LoadingsController::class, 'entersection2'])->name('loadings.entersection2');
Route::post('/loadings/entersection2Action/{id}', [LoadingsController::class, 'entersection2Action'])->name('loadings.entersection2action');
Route::get('/loadings/entersection3/{id}', [LoadingsController::class, 'entersection3'])->name('loadings.entersection3');
Route::post('/loadings/entersection3Action/{id}', [LoadingsController::class, 'entersection3Action'])->name('loadings.entersection3action');
Route::get('/loadings/entersection4/{id}', [LoadingsController::class, 'entersection4'])->name('loadings.entersection4');
Route::post('/loadings/entersection4Action/{id}', [LoadingsController::class, 'entersection4Action'])->name('loadings.entersection4action');
Route::get('/loadings/closeload/{id}', [LoadingsController::class, 'closeload'])->name('loadings.closeload');
Route::get('/loadings/usemasterplan/{id}', [LoadingsController::class, 'usemasterplan'])->name('loadings.usemasterplan');
Route::post('/loadings/usemasterplanAction/{id}', [LoadingsController::class, 'usemasterplanAction'])->name('loadings.usemasterplanaction');
Route::get('/loadings/entercountermanual/{id}', [LoadingsController::class, 'entercountermanual'])->name('loadings.entercountermanual');
Route::post('/loadings/entercountermanualAction/{id}', [LoadingsController::class, 'entercountermanualAction'])->name('loadings.entercountermanualAction');
Route::get('/loadings/search', [LoadingsController::class, 'searchLoading'])->name('loadings.search');
Route::get( '/loadings/searchview/{id}', [LoadingsController::class, 'viewLoadingData'])->name('loadings.searchview');
Route::get('/loadings/searchviewextimage/{id}', [LoadingsController::class, 'viewLoadingDataExtImage'])->name('loadings.searchviewextimages');
Route::get('/loadings/exportLoadingData/{id}', [LoadingsController::class, 'exportLoadingData'])->name('loadings.exportLoadingData');
Route::get('/loadings/exportLoadingextimageData/{id}', [LoadingsController::class, 'exportLoadingExtImageData'])->name('loadings.exportLoadingExtImageData');
Route::get('/loadings/uploadextimg/{load_m_id}/{seqpic}/{seqtype}', [LoadingsController::class, 'uploadExtImage'])->name('loadings.uploadExtImage');
Route::post('/loadings/uploadextimgAct/{load_m_id}/{seqpic}/{seqtype}', [LoadingsController::class, 'uploadextimgAct'])->name('loadings.uploadextimgAct');

Route::get('/reports/loadingdoc/{id}', [ReportsController::class, 'loadingdoc'])->name('reports.loadingdoc');
Route::get('/reports/masterplan/{id}', [ReportsController::class, 'masterplan'])->name('reports.masterplan');
Route::get('/reports/exportdata', [ReportsController::class, 'exportdata'])->name('reports.exportdata');
Route::post('/reports/exportdataAction', [ReportsController::class, 'exportdataAction'])->name('reports.exportdataaction');
Route::get('/reports/loadplanvsact', [ReportsController::class, 'loadPlanVsAct'])->name('reports.loadplanvsact');
Route::post('/reports/loadplanvsactaction', [ReportsController::class, 'loadPlanVsActAction'])->name('reports.loadplanvsactaction');
Route::get('/reports/exportcheckpoint', [ReportsController::class, 'exportCheckPoint'])->name('reports.exportcheckpoint');
Route::post('/reports/exportcheckpointaction', [ReportsController::class, 'exportCheckPointAction'])->name('reports.exportcheckpointaction');

Route::get('/list_load/webcam_pd/{id}/{pd}', [ListLoadController::class, 'webcam_pd'])->name('list_load.webcam_pd');   //หน้า ถ่ายรูป
Route::post('/list_load/capture_pd/{id}/{pd}', [ListLoadController::class, 'capture_pd'])->name('list_load.capture_pd');   //หน้า save รูป
Route::get('/list_load/del_capture_pd', [ListLoadController::class, 'del_capture_pd'])->name('list_load.del_capture_pd');   //หน้า del รูป
//ปิดตู้
Route::get('/list_load/webcam/{id}', [ListLoadController::class, 'webcam'])->name('list_load.webcam');   //หน้า ถ่ายรูป
Route::post('/list_load/capture/{id}', [ListLoadController::class, 'capture'])->name('list_load.capture');   //หน้า save รูป
Route::get('/list_load/del_capture', [ListLoadController::class, 'del_capture'])->name('list_load.del_capture');   //หน้า del รูป
Route::get('/list_load/to_sort', [ListLoadController::class, 'to_sort'])->name('list_load.to_sort');
Route::get('/list_load/to_pdf/{id}', [ListLoadController::class, 'to_pdf'])->name('list_load.to_pdf');
Route::resource('list_load', ListLoadController::class);

Route::get('/list_load_importView', [ListLoadDetController::class, 'importExportView'])->name('list_load_det_importView');
Route::post('/list_load_det/import', [ListLoadDetController::class, 'import'])->name('list_load_det_import');
Route::get('/list_load_det/webcam/{id}', [ListLoadDetController::class, 'webcam'])->name('list_load_det.webcam');   //หน้า ถ่ายรูป
Route::post('/list_load_det/capture/{id}', [ListLoadDetController::class, 'capture'])->name('list_load_det.capture');   //หน้า save รูป
Route::get('/list_load_det/del_capture/{id}', [ListLoadDetController::class, 'del_capture'])->name('list_load_det.del_capture');   //หน้า del รูป
Route::resource('list_load_det', ListLoadDetController::class);


Route::get('/loading_carries/editsec2/{id}', [LoadingCarriesController::class, 'editsec2'])->name('loading_carries.editsec2');
Route::post('/loading_carries/updatesec2/{id}', [LoadingCarriesController::class, 'updatesec2'])->name('loading_carries.updatesec2');
Route::get('/loading_carries/editsec3/{id}', [LoadingCarriesController::class, 'editsec3'])->name('loading_carries.editsec3');
Route::post('/loading_carries/updatesec3/{id}', [LoadingCarriesController::class, 'updatesec3'])->name('loading_carries.updatesec3');
Route::get('/loading_carries/editpic1/{id}', [LoadingCarriesController::class, 'editpic1'])->name('loading_carries.editpic1');
Route::post('/loading_carries/updatepic1/{id}', [LoadingCarriesController::class, 'updatepic1'])->name('loading_carries.updatepic1');
Route::get('/loading_carries/editpic2/{id}', [LoadingCarriesController::class, 'editpic2'])->name('loading_carries.editpic2');
Route::post('/loading_carries/updatepic2/{id}', [LoadingCarriesController::class, 'updatepic2'])->name('loading_carries.updatepic2');
Route::get('/loading_carries/editpic3/{id}', [LoadingCarriesController::class, 'editpic3'])->name('loading_carries.editpic3');
Route::post('/loading_carries/updatepic3/{id}', [LoadingCarriesController::class, 'updatepic3'])->name('loading_carries.updatepic3');
Route::get('/loading_carries/editpic4/{id}', [LoadingCarriesController::class, 'editpic4'])->name('loading_carries.editpic4');
Route::post('/loading_carries/updatepic4/{id}', [LoadingCarriesController::class, 'updatepic4'])->name('loading_carries.updatepic4');
Route::get('/loading_carries/editfile1/{id}', [LoadingCarriesController::class, 'editfile1'])->name('loading_carries.editfile1');
Route::post('/loading_carries/updatefile1/{id}', [LoadingCarriesController::class, 'updatefile1'])->name('loading_carries.updatefile1');
Route::resource('loading_carries', LoadingCarriesController::class);
